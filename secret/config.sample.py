from celery.schedules import crontab

# Timezone of the application
# Value: string (Default: "Europe/Zurich")
CURRENT_TIMEZONE = "Europe/Zurich"
# Whether or not to use the werzeug proxy for requests
# Value: boolean (Default: True)
USE_PROXY = True
# Secret key for Flask
# Value: string (Default: Value generated with os.urandom(24))
SECRET_KEY = "<TODO>"
# Secret CSRF for the forms
# Value: string (Default: Value generated with os.urandom(24))
SECRET_CSRF_KEY = "<TODO>"
# Hostname of the server
# Value: string (Default: "localhost")
INSTANCE_HOSTNAME = "ces-prod.web.cern.ch"
# Whether or not this is a development instance
# Value: boolean (Default: False)
IS_DEV = False
#
# This line is only required during development if not using SSL
# !! It must be removed in production
# os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
# Whether or not this we are running on testing mode (only for the tests)
# Value: boolean (Default: False)
IS_TESTING = False
# Cernmedia server to store the recordings
# Value: string (Default: "cernmedia33.cern.ch")
SERVER = "cernmedia33.cern.ch"
# Cernmedia user to connect to the server
# Value: string
SERVER_USER = "<TODO>"
# Cernmedia password to connect to the server
# Value: string
SERVER_PASS = "<TODO>"
# Path to the recordings with no room associated
# Value: string (Default: "\\cernmedia33.cern.ch\RECORDINGS\External_URI")
DEFAULT_RECORDING_PATH = r"\\cernmedia33.cern.ch\RECORDINGS\External_URI"
# Whether or not to enable the caching
# Value: boolean (Default: True)
CACHE_ENABLE = True
# Hostname of the caching server
# Value: string (Default: "redis")
CACHE_REDIS_HOST = "redis"
# Port of the caching server
# Value: string (Default: "6379")
CACHE_REDIS_PORT = "6379"
# Password of the caching server
# Value: string (Default: "")
CACHE_REDIS_PASSWORD = "<TODO>"
# Redis database to use
# Value: string (Default: "0")
CACHE_REDIS_DB = "0"
# Logging level
# Value: string. Values: DEV|PROD (Default: "DEV")
LOG_LEVEL = "DEV"
# Whether or not to enable remote logging
# Value: boolean (Default: False)
LOG_REMOTE_ENABLED = False
# Whether or not to enable file logging
# Value: boolean (Default: False)
LOG_FILE_ENABLED = False
# The type of the remote logging. Used to distinguish between different logs.
# Value: string
LOG_REMOTE_TYPE = "<TODO>"
# The producer of the remote logging. Provided by the monit team.
# Value: string
LOG_REMOTE_PRODUCER = "<TODO>"
# The path to the log files
# Value: string (Default: "/logs")
LOG_FILE_PATH = "/logs"
# Whether or not to send ERROR log emails
# Value: boolean (Default: False)
LOG_MAIL_ENABLED = False
# Hostname of the mail server for logging
# Value: string (Default: "cermx.cern.ch")
LOG_MAIL_HOSTNAME = "cernmx.cern.ch"
# Email from address for the emails sent by the application
# Value: string (Default: "ces2@cern.ch")
LOG_MAIL_FROM = "ces2@cern.ch"
# A list of email addresses to send the emails to
# Value: list[string]
LOG_MAIL_TO = ["<TODO>@cern.ch"]
# OpenID client ID generated on the application's portal
# Value: string
CERN_OPENID_CLIENT_ID = "<TODO>"
# OpenID client secret generated on the application's portal
# Value: string
CERN_OPENID_CLIENT_SECRET = "<TODO>"
# URL for the OpenID certs
# Value: string (Default: "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/certs")
OIDC_JWKS_URL = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/certs"
# URL for the OpenID realms endpoint
# Value: string (Default: "https://auth.cern.ch/auth/realms/cern")
OIDC_ISSUER = "https://auth.cern.ch/auth/realms/cern"
# URL for the OpenID logout endpoint
# Value: string (Default: "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout")
OIDC_LOGOUT_URL = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout"
# Groups of the users that are allowed to access the application
# Default: "admins"
ADMIN_GROUP = "admins"
# Hostname of the mail server for notification emails
# Value: string (Default: "cermx.cern.ch")
MAIL_HOSTNAME = "cernmx.cern.ch"
# Email from address for the notification emails sent by the application
# Value: string (Default: "webcast-team@cern.ch")
MAIL_FROM = "webcast-team@cern.ch"
# A list of email addresses to send the notification emails to
# Value: list[string]
MAIL_TO = ["webcast-team@cern.ch"]
# Number of minutes before an event when the notification emails are sent
# Value: integer (Default: 15)
REMINDER_BEFORE_EVENT = 15
# Monarch encoders API user
# Value: string
MONARCH_USER = "<TODO>"
# Monarch encoders API password
# Value: string
MONARCH_PASS = "<TODO>"
# Epiphan encoders API user
# Value: string
EPIPHAN_NANO_USER = "<TODO>"
# Epiphan encoders API password
# Value: string
EPIPHAN_NANO_PASS = "<TODO>"
# Epiphan encoders recording path location
# Value: string (Default: r"\\cernmedia33.cern.ch\RECORDINGS\Epiphan")
EPIPHAN_NANO_RECORDING_PATH = r"\\cernmedia33.cern.ch\RECORDINGS\Epiphan"
# Indico URL endpoint
# Value: string (Default: "https://indico.cern.ch/")
INDICO_HOSTNAME = "https://indico.cern.ch"
# Indico API events path
# Value: string (Default: "/export/event/ID.json")
INDICO_APIPATH_EVENT = "/export/event/ID.json"
# Indico API webcasts and recordings path
# Value: string (Default: "/export/webcast-recording.json")
INDICO_APIPATH_WEBCAST_RECORDING = "/export/webcast-recording.json"
# Indico API key
# Value: string
INDICO_API_KEY = "<TODO>"
# Indico API secret
# Value: string
INDICO_API_SECRET = "<TODO>"
# Indico events URL
# Value: string (Default: "https://indico.cern.ch/event/%s")
INDICO_EVENT_URL = "https://indico.cern.ch/event/%s"
# NEW indico Bearer token: From the webcast service account. Using bearer token. NOT USED YET
# Value: string
INDICO_API_TOKEN = "<TODO>"
# Hostname of the wowza edge server
# Value: string (Default: "wowza.cern.ch")
WOWZA_WATCH_FROM = "wowza.cern.ch"
# Equivalent URLS for the Wowza origins using streamlock
#
STREAMLOCK_ORIGINS = {
    "wowzaqaorigin.cern.ch": "<STREAMLOCK_1>",
    "wowza20.cern.ch": "<STREAMLOCK_2>",
    "wowza21.cern.ch": "<STREAMLOCK_3>",
}
# Wowza test application
# Value: string (Default: "test")
TEST_APP = "test"
# RAVEM URL endpoint
# Value: string (Default: "https://ravem2-qa.cern.ch/")
RAVEM_URL = "https://ravem2-qa.web.cern.ch"
# RAVEM API key
# Value: string
RAVEM_API_KEY = "<TODO>"
# Webcast Website URL endpoint
# Value: string (Default: "https://webcast-qa.cern.ch/")
WEBCAST_WEBSITE_URL = "https://webcast-qa.web.cern.ch"
# Webcast Website API key
# Value: string
WEBCAST_WEBSITE_API_KEY = "<TODO>"
# Webcast Website API secret
# Value: string
WEBCAST_WEBSITE_SECRET_KEY = "<TODO>"
# Database name
# Value: string
DB_NAME = "<TODO>"
# Database user
# Value: string
DB_USER = "<TODO>"
# Database password
# Value: string
DB_PASS = "<TODO>"
# Database port
# Value: integer (Default: 3306)
DB_PORT = 3306
# Database hostname
# Value: string (Default: "127.0.0.1")
DB_SERVICE_NAME = "<TODO>.cern.ch"
# Database engine
# Value: string (mysql|postgresql) (Default: "mysql")
DB_ENGINE = "mysql"
# Celery beat schedule for tasks
# Value: dict
CELERY_BEAT_SCHEDULE = {
    "check_encoders": {
        "task": "app.tasks.encoders.check_encoders",
        "schedule": crontab(hour=7, minute=5),
    },
    "check_disk_space": {
        "task": "app.tasks.media_packages.check_disk_space",
        "schedule": crontab(hour=6, minute=5),
    },
    "dummy_task": {
        "task": "app.tasks.example.dummy_task",
        "schedule": crontab(minute=0, hour="*"),
    },
    "fetch_next_events": {
        "task": "app.tasks.indico_events.fetch_next_events",
        "schedule": crontab(hour="*/15"),
    },
    "move_epiphan_recordings": {
        "task": "app.tasks.move_epiphan_files.move_epiphan_recordings",
        "schedule": crontab(minute="*/5"),
    },
    "remove_older_media_packages": {
        "task": "app.tasks.media_packages.remove_older_content",
        "schedule": crontab(hour=6, minute=0),
    },
    "request_opencast_acls": {
        "task": "app.tasks.request_opencast_acls.request_opencast_acls",
        "schedule": crontab(minute="*/5"),
    },
    "request_cds_subformats": {
        "task": "app.tasks.request_cds_subformats.request_cds_subformats",
        "schedule": crontab(minute="*/10"),
    },
    "send_reminders": {
        "task": "app.tasks.reminders.send_reminders",
        "schedule": crontab(),
    },
    "stop_event": {
        "task": "app.tasks.auto_stop.stop_event",
        "schedule": crontab(),
    },
    "verify_speaker_releases": {
        "task": "app.tasks.verify_speaker_releases.verify_speaker_releases",
        "schedule": crontab(minute="*/15"),
    },
}
# Celery broker URL with caching variables.
#
CELERY_BROKER_URL = f"redis://:{CACHE_REDIS_PASSWORD}@{CACHE_REDIS_HOST}:{CACHE_REDIS_PORT}/{CACHE_REDIS_DB}"
# Celery results backend with database variables.
#
CELERY_RESULTS_BACKEND = (
    f"db+mysql+pymysql://{DB_USER}:{DB_PASS}@{DB_SERVICE_NAME}:{DB_PORT}/{DB_NAME}"
)
# Whether or not to use Service Now to register events on a ticket.
# Value: boolean (Default: False)
SNOW_NOTIFY = True
# URL of the Service Now Ticket
# Value: string (Default: "https://cern.service-now.com/u_request_fulfillment.do?sysparm_query=number=")
SNOW_TICKET_URL = (
    "https://cern.service-now.com/u_request_fulfillment.do?sysparm_query=number="
)
# Service NOW imap hostname (CERN's)
# Value: string (Default: "imap.cern.ch")
SN_IMAP_SERVER = "imap.cern.ch"
# Service NOW email address
# Value: string (Default: "webcast.service@cern.ch")
SN_MAIL = "webcast.service@cern.ch"
# Service NOW API password
# Value: string
SN_PASSWORD = "<TODO>"
# Service NOW API username
# Value: string
SN_USER = "<TODO>"
# Service NOW API URL
# Value: string (Default: "https://cern.service-now.com/api/now/v1/table/")
SN_API_URL = "https://cern.service-now.com/api/now/v1/table/"
# Service NOW email FROM address
# Value: string (Default: "micala.software@cern.ch")
MAIL_SNOW_FROM = "micala.software@cern.ch"
# Service NOW email TO address
# Value: list[string] (Default: ["recording-support@cern.ch"])
MAIL_SNOW_TO = ["recording-support@cern.ch"]
# Webcast service email address
# Value: string (Default: "service-avc-operation@cern.ch")
WEBCAST_SERVICE_MAIL = "service-avc-operation@cern.ch"
# Base callback URL for the CDS requests
# Value: string (Default: "https://ces-qa.web.cern.ch")
CDS_CALLBACK_BASE_URL = "https://ces-qa.web.cern.ch"
CDS_URL = "https://cds.cern.ch"  # do not add the / at the end
CDS_URL_RECORD = CDS_URL + "/record/%s"
CDS_API_KEY = "<TODO>"
CDS_API_SECRET = "<TODO>"
# Path to the Marc21 XML template file
# Value: string (Default: "/opt/app-root/src/app/templates/cds/cds_record_origin.xml")
CDS_MARC21_ORIGIN = "/opt/app-root/src/app/templates/cds/cds_record_origin.xml"
# Mediarchive server URL to weblectures
# Value: string (Default: "http://mediaarchive.cern.ch/MediaArchive/Video/Public/WebLectures/")
CDS_MEDIAARCHIVE_WEBLECTURES_BASE_URL = (
    "http://mediaarchive.cern.ch/MediaArchive/Video/Public/WebLectures/"
)
# Mediarchive server URL to conferences
# Value: string (Default: "http://mediaarchive.cern.ch/MediaArchive/Video/Public/Conferences/")
CDS_MEDIAARCHIVE_CONFERENCES_BASE_URL = (
    "http://mediaarchive.cern.ch/MediaArchive/Video/Public/Conferences/"
)
# DFS path to the weblectures
# Value: string (Default: r"\\cern.ch\dfs\Services\MediaArchive\Video\Public\WebLectures")
DIR_WEBLECTURES_DFS_FOLDER = (
    r"\\cern.ch\dfs\Services\MediaArchive\Video\Public\WebLectures"
)
# DFS path to the conferences
# Value: string (Default: r"\\cern.ch\dfs\Services\MediaArchive\Video\Public\Conferences")
DIR_PLAIN_VIDEOS_DFS_FOLDER = (
    r"\\cern.ch\dfs\Services\MediaArchive\Video\Public\Conferences"
)
# Opencast Server URL
# Value: string (Default: "https://ocweb-prod.cern.ch")
OPENCAST_ENDPOINT = "https://ocweb-prod.cern.ch"
# Downloads folder path
# Value: string (Default: "/downloads")
DOWNLOADS_FOLDER_PATH = "/downloads"
# Opencast user for the ingest and API requests
# Value: string
OPENCAST_USER = "<TODO>"
# Opencast password for the ingest and API requests
# Value: string
OPENCAST_PASSWORD = "<TODO>"
# Set URL prefix for access to media data, this is required by CDS while requesting subformats.
# Value: string
OPENCAST_DATAJSON = "https://toto.cern.ch/"
# Zoom API Key
# Value: string (https://zoom.us/developer/api/guides/authentication)
ZOOM_API_KEY = "<TODO>"
# Zoom API Secret
# Value: string (https://zoom.us/developer/api/guides/authentication)
ZOOM_API_SECRET = "<TODO>"
# The endpoint of the ttaas service
# Value: string (Default: "https://ttaas-test.web.cern.ch")
TTAAS_SERVICE_ENDPOINT = "https://ttaas-test.web.cern.ch"
# The token generated on the ttaas service UI
# Value: string (Default: "<TODO>")
TTAAS_SERVICE_TOKEN = "<TODO>"
# Sentry
ENABLE_SENTRY = True
SENTRY_DSN = ""
SENTRY_ENVIRONMENT = "test"
# Rundeck base URL to request data.json updates
#
RUNDECK_SERVER_BASE_URL = "https://avrundeck02.cern.ch"
# Rundeck API webhook token
#
RUNDECK_API_TOKEN = "<TODO>"  # noqa
MEDIA_SERVER_BASE_URL = "https://lecturemedia.cern.ch"

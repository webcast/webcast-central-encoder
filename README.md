# Webcast Central Encoding System (CES)

This application is used to control the encoders in the rooms (Matrox devices) and handle the publication of a video to
Opencast.

## Technologies

- Python 3.6
- Flask
- Celery
- Redis

- Filebeat

## Dependencies

- Cernmedia servers (cernmedia33)
- Opencast
- Indico
- CDS
- Gitlab
- Harbor

## Docs

- [Openshift Configuration](/docs/deployment.md): How to configure OKD4 for the project and restore the project in case it is necessary.

## Documents

- About workflows: https://codimd.web.cern.ch/rUhKWHZRTA6VB-uvnI6MGA?view
- Opencast workflows: https://codimd.web.cern.ch/eTIDVAZKQuSV0nHlMHLOow
- CES Input for Opencast Integration: https://codimd.web.cern.ch/K8yQ4XO9RBGJMGq_loDBZg?both

## Development

### Using Docker

```bash
docker compose up
```

```bash
docker compose run web bash
```

```bash
docker stop $(docker ps -a -q)
```

### Using local development

> Required Python version: `3.8.x` (Other version might require changes on the requirements)
```
python3 -m venv ./env
```

## Initialize the database

Run the following command to create the tables in the database
```
flask db upgrade
```

### Updating the database

If for some reason there are changes in the models, you can you the following commands in order to update the database"

```bash
cd app
flask db migrate -m "message"
flask db upgrade
```

This actions needs to be don on all the databases affected.

If for some reason, the command doesn't finish, it is possible to run the SQL manually:

```bash
flask db upgrade --sql
```

The command above will output the SQL, that can be used on the SQL editor.


## Configuration

Configuration parameters can be reviewed on the `secret/config.sample.py` file.
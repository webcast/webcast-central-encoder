FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

EXPOSE 8080

ENV USER_ID 1001
ENV WORKING_DIR /opt/app-root/src

RUN yum -y install epel-release
RUN yum -y install https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %rhel).noarch.rpm

RUN yum -y update
RUN yum -y install \
  iperf3 \
  gcc \
  libffi-devel \
  python \
  python-devel \
  python-pip \
  openssl-devel \
  mysql-devel \
  postgresql-devel \
  pcre \
  pcre-devel \
  libxml2-devel \
  libxslt-devel \
  CERN-CA-certs \
  ffmpeg \
  ffmpeg-libs

RUN yum clean -y all

ENV REQUESTS_CA_BUNDLE /etc/ssl/certs/ca-bundle.crt
ENV PYTHONPATH ${WORKING_DIR}
ENV FLASK_APP ${WORKING_DIR}/wsgi.py
ENV PYTHONUNBUFFERED 1
# The following environment variables are used by Poetry to install dependencies
ENV POETRY_VERSION 1.5.0
ENV POETRY_HOME /opt/poetry
ENV POETRY_VIRTUALENVS_IN_PROJECT true
ENV POETRY_CACHE_DIR ${WORKING_DIR}/.cache
ENV VIRTUAL_ENVIRONMENT_PATH ${WORKING_DIR}/.venv

# Adding the virtual environment to PATH in order to "activate" it.
# https://docs.python.org/3/library/venv.html#how-venvs-work
ENV PATH="$VIRTUAL_ENVIRONMENT_PATH/bin:$PATH"
# Pip
RUN python --version
RUN python -m ensurepip --upgrade
RUN python -m pip --version

RUN mkdir -p ${WORKING_DIR}
# Set folder permissions
RUN chgrp -R 0 ${WORKING_DIR} && \
  chmod -R g=u ${WORKING_DIR}

WORKDIR ${WORKING_DIR}

# Install Poetry and dependencies
COPY pyproject.toml ./
COPY poetry.lock ./

# Using Poetry to install dependencies without requiring the project main files to be present
RUN pip install poetry==${POETRY_VERSION} && poetry install --only main --no-root --no-directory


# Copy the application files
COPY app /opt/app-root/src/app
COPY migrations /opt/app-root/src/migrations
COPY etc /opt/app-root/src/etc
COPY secret /opt/app-root/src/secret
COPY server /opt/app-root/src/server
COPY wsgi.py /opt/app-root/src/wsgi.py
COPY celery_app.py /opt/app-root/src/celery_app.py

CMD ["/opt/app-root/src/etc/entrypoint.sh"]

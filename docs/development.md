# Development

## Requirements

- Python 3.9.10

## Technologies

- Flask

## Local install

### Database

You can install a database of your choice in your computer.
  - MySQL: https://dev.mysql.com/downloads/mysql/

### Dependencies

Install the project dependencies for Python:
```
python3 -m venv ./venv
source venv/bin/activate
pip install -r requirements.txt
pip install -r requirements.dev.txt
```

### Install the pre-commit Git hook

```
pre-commit install
```

### Configuration file

Rename `secret/config.py.sample` to `secret/config.py` and populate it with the correct values (Database, paths, etc).

### Migrate the database

Run the migrations on the database

```bash
FLASK_APP=wsgi.py flask db upgrade
```

## Run

### Webapp

```bash
FLASK_APP=wsgi.py FLASK_ENV=development flask run
```

Another option is to use Visual Studio Code for running and debugging. In this case just go to the Visual Studio Code menu and select "Run" and "Run without debugging" or "Start debugging".

### Celery and Redis

#### Redis

Redis is required to run celery. It can be installed with the following commands depending on your platform:

- Ubuntu: `sudo apt-get install redis-server`
- Mac: `brew install redis`

Then run the server:

```bash
redis-server
```

#### Celery

> Don't forget to set up the correct setting in the `config.py` file.

```bash
celery -A celery_app:celery worker -Q default --loglevel=info -P solo
```

Another option is to use Visual Studio Code for running and debugging. In this case just go to the Visual Studio Code menu and select "Run" and "Run without debugging" or "Start debugging".


## Other Available commands

Get a list of the available commands running the following:

```bash
FLASK_APP=wsgi.py flask --help
```

## Links to libs and other docs

- Paas Docs: https://paas.docs.cern.ch/
# Openshift

> ⚠️ Secrets and service accounts are not restored. You need to create them manually. These include:
> - Secret `gitlab-registry-auth`: To access the Gitlab Registry
> - Service Account `gitlabci-deployer`: To give Gitlab access to deploy in Openshift

## Install

### 1. Add the Openshift deploy key to Gitlab

> To grant access to the private Gitlab repository

```bash
oc login https://api.paas.okd.cern.ch
oc project (ces-prod|ces-qa|ces-test) # 3 options
oc get secrets/sshdeploykey -o go-template='{{index .metadata.annotations "cern.ch/ssh-public-key"}}{{println}}'
```

### 2. Deploy a Docker image

1. Run the following commands in Openshift

```bash
oc create serviceaccount gitlabci-deployer
```

```bash
oc policy add-role-to-user registry-editor -z gitlabci-deployer
```

```bash
oc policy add-role-to-user view -z gitlabci-deployer
```

```bash
oc serviceaccounts get-token gitlabci-deployer
```

2. Paste the generated token in Gitlab `Settings/CI/CD/Variables` as `IMAGE_IMPORT_TOKEN_<QA|MASTER|TEST>`.
3. Generate a deploy token in Gitlab. This will generate a username and a password that will be added to Openshift later.
   1. Needed rights for: read_registry
   2. The script is located on [/openshift/grant-registry-access.sh](/openshift/grant-registry-access.sh). Change the corresponging username/password.

- Imagestream being updated from Gitlab when `redeploy` stage finishes

### 3. Request a cephfs volume on Openshift

1. Go to https://openshift.cern.ch/console/project/<NAMESPACE>/create-pvc
2. Storage class: `cephfs-no-backup`
3. Name: `downloads`
4. Type: `RWX` (Shared access)
5. Size: Whathever (limited by the project quota on webservices website management)

### 4. Create the Redis pod for caching:

In OKD select the template `Redis Persistent` and deploy it (`Add to project` -> `Select from project`)


### 5. Restore the webapp
```bash
oc create -f okd4/webapp.yaml
```

### 6. Update the config maps values:

Update the values in the configmap to match the correct ones.

The secrets can be generated using python:

```python
import os
os.urandom(20)
```

### 8. Restore the celery taks

```bash
oc create -f okd4/celery-tasks.yaml
```

### 9. Restore filebeat

```bash
oc create -f okd4/filebeat.yaml
```


## Help

### Remove an already created template

```bash
oc delete template <TEMPLATE_NAME>
```

Example:
```bash
oc delete template ces-webapp
```






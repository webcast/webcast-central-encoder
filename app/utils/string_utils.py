from html.parser import HTMLParser
from io import StringIO


# https://stackoverflow.com/questions/753052/strip-html-from-strings-in-python?answertab=votes#tab-top
class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.text = StringIO()

    def handle_data(self, d):
        self.text.write(d)

    def get_data(self):
        return self.text.getvalue()


def strip_html_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

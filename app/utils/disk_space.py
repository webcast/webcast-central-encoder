import shutil

from flask import current_app


class DiskSpaceUtils:
    @classmethod
    def get_disk_space_in_downloads(cls):
        total, used, free = DiskSpaceUtils.get_disk_space(
            current_app.config["DOWNLOADS_FOLDER_PATH"]
        )

        return total, used, free

    @classmethod
    def get_disk_space_in_downloads_in_GB(cls):
        total, used, free = DiskSpaceUtils.get_disk_space_in_downloads()

        total_space = total // (2**30)
        used_space = used // (2**30)
        free_space = free // (2**30)

        return total_space, used_space, free_space

    @classmethod
    def get_disk_space(cls, folder):
        total, used, free = shutil.disk_usage(folder)

        return total, used, free

        # total_space = (total // (2 ** 30))
        # used_space = (used // (2 ** 30))
        # free_space = (free // (2 ** 30))

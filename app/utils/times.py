from datetime import datetime
from typing import Tuple

from app.models.events import IndicoEventContribution


def get_start_end_ms(start_time: str, end_time: str) -> Tuple[int, int]:
    """Get the start and end times in milliseconds.

      Args:
          start_time (str): Start time in HH:MM:SS format.
          end_time (str): End time in HH:MM:SS format.

    Returns:
        Tuple[int, int]: Tuple with start and end times in milliseconds.
    """
    initial_time = datetime.strptime("00:00:00", "%H:%M:%S")
    start_time_value = datetime.strptime(start_time, "%H:%M:%S")
    end_time_value = datetime.strptime(end_time, "%H:%M:%S")

    start_difference = start_time_value - initial_time

    start_diff = int(start_difference.total_seconds() * 1000)
    end_difference = end_time_value - initial_time
    end_diff = int(end_difference.total_seconds() * 1000)
    return start_diff, end_diff


def are_split_times_valid(contribution: IndicoEventContribution) -> bool:
    """Verify if the split times are valid. (Camera and slides have the same length)

    Args:
        contribution (IndicoEventContribution): The contribution to verify.

    Returns:
        bool: Valid or not (True|False)
    """
    if (
        contribution.has_camera_split_values()
        and contribution.has_slides_split_values()
    ):
        camera_start_diff, camera_end_diff = get_start_end_ms(
            contribution.camera_start_time.replace(" ", ""),
            contribution.camera_end_time.replace(" ", ""),
        )
        slides_start_diff, slides_end_diff = get_start_end_ms(
            contribution.slides_start_time.replace(" ", ""),
            contribution.slides_end_time.replace(" ", ""),
        )
        camera_duration = camera_end_diff - camera_start_diff
        slides_duration = slides_end_diff - slides_start_diff

        if camera_duration != slides_duration:
            return False

        return True
    return False

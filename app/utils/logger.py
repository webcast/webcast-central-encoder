import logging
import sys
from datetime import datetime
from logging import handlers

import sentry_sdk
from flask import Flask
from pytz import timezone, utc

from app.utils.uma_logger import UMAHandler


class SentryTagFilter(logging.Filter):
    def filter(self, record):
        # Extract extra fields (tags) from the record object
        tags = getattr(record, "tags", None)
        if tags and isinstance(tags, dict):
            with sentry_sdk.configure_scope() as scope:
                for key, value in tags.items():
                    scope.set_tag(key, value)
        return True


class CustomFormatter(logging.Formatter):
    def format(self, record):
        # Get the "tags" attribute from the record's extra field
        tags = record.__dict__.get("tags", None)

        # If tags are present, join them with a comma
        if tags:
            tags_str = ", ".join(f"{key}={value}" for key, value in tags.items())
        else:
            tags_str = ""

        # Add the tags string to the log message
        record.tags = tags_str

        # Call the parent formatter to complete the formatting
        return super(CustomFormatter, self).format(record)


def zurich_time(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Europe/Zurich")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()


def setup_logs(
    app: Flask,
    logger_name: str,
    to_stdout: bool = True,
    to_remote: bool = False,
    to_file: bool = False,
    to_mail: bool = False,
):
    """
    Set up the logs for the application

    :param app: Flask application where the configuration will be obtained
    :type app: flask.Flask
    :param logger_name: The name of the logger
    :type logger_name: str
    :param to_stdout: Whether or not to log to stdout
    :type to_stdout: bool
    :param to_remote: Whether or not to use remote logging
    :type to_remote: bool
    :return:
    :rtype:
    """
    logger = logging.getLogger(logger_name)

    print(f"Setting up logger: {logger_name}")

    if app.config["LOG_LEVEL"] == "DEV":
        logger.setLevel(logging.DEBUG)

    if app.config["LOG_LEVEL"] == "PROD":
        logger.setLevel(logging.INFO)

    format = (
        "%(asctime)s | %(levelname)s | %(name)s | %(message)s "
        "| %(pathname)s:%(lineno)d | %(funcName)s() | %(tags)s"
    )

    formatter = CustomFormatter(format)

    old_factory = logging.getLogRecordFactory()

    def record_factory(*args, **kwargs):
        record = old_factory(*args, **kwargs)
        record.custom_attribute = "tags"
        return record

    logging.setLogRecordFactory(record_factory)

    logging.Formatter.converter = zurich_time

    if to_stdout:
        configure_stdout_logging(
            logger, app=app, formatter=formatter, log_level=app.config["LOG_LEVEL"]
        )

    if to_remote:
        configure_remote_logging(logger, app)

    if to_file:
        configure_file_logging(logger, app, formatter=formatter, file_name=logger_name)

    if to_mail:
        configure_email_logging(logger, app)


def configure_stdout_logging(logger, app: Flask, formatter=None, log_level="DEV"):
    """
    Set up the stdout logging

    :param logger: The logger to be configured
    :type logger: logging.Logger
    :param formatter: Formatter to be used
    :type formatter: logging.Formatter
    :param log_level: The level of the logging: DEV|PROD
    :type log_level: str
    :return:
    :rtype:
    """
    stream_handler = logging.StreamHandler(stream=sys.stdout)

    stream_handler.setFormatter(formatter)
    if log_level == "DEV":
        stream_handler.setLevel(logging.DEBUG)
    if log_level == "PROD":
        stream_handler.setLevel(logging.INFO)

    logger.addHandler(stream_handler)

    if app.config["ENABLE_SENTRY"]:
        sentry_filter = SentryTagFilter()
        stream_handler.addFilter(sentry_filter)

    print(f"Logging {str(logger)} to stdout -> True")


def configure_remote_logging(logger: logging.Logger, app: Flask):
    """
    Set up the remote logging

    :param app: Flask application used to get the configuration
    :type app: flask.Flask
    :param logger: The logger to be configured
    :type logger: logging.Logger
    :param formatter: Formatter to be used
    :return:
    :rtype:
    """
    if not app.config.get("LOG_REMOTE_PRODUCER", None) or not app.config.get(
        "LOG_REMOTE_TYPE", None
    ):
        print("ERROR -> Unable to configure the remote logging. Attributes not set.")

    handler = UMAHandler(
        app.config.get("LOG_REMOTE_PRODUCER", None),
        app.config.get("LOG_REMOTE_TYPE", None),
    )

    def handle_exception(exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            sys.__excepthook__(exc_type, exc_value, exc_traceback)
            return

        logger.error(
            "Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback)
        )

    sys.excepthook = handle_exception
    if app.config.get("LOG_LEVEL", "DEV") == "DEV":
        handler.setLevel(logging.DEBUG)
    if app.config.get("LOG_LEVEL", "DEV") == "PROD":
        handler.setLevel(logging.INFO)

    logger.addHandler(handler)
    print(f"Logging {str(logger)} to remote -> True")


def configure_file_logging(
    logger: logging.Logger,
    app: Flask,
    formatter: logging.Formatter,
    file_name: str = "application",
):
    log_file_path = app.config.get("LOG_FILE_PATH", "/opt/app-root/src/logs")
    log_file_name = f"{log_file_path}/{file_name}.log"
    try:
        file_handler = handlers.TimedRotatingFileHandler(
            log_file_name, when="midnight", interval=7, backupCount=5
        )
        file_handler.setFormatter(formatter)
        if app.config.get("LOG_LEVEL", "DEV") == "DEV":
            file_handler.setLevel(logging.DEBUG)
        if app.config.get("LOG_LEVEL", "DEV") == "PROD":
            file_handler.setLevel(logging.INFO)

        logger.addHandler(file_handler)
        print(f"Logging {str(logger)} to file ({log_file_name}) -> True")

    except OSError as error:
        print(
            f"""It seems there is a problem with the volume.
            Logs will be only on stdout. Error: {str(error)}"""
        )


def configure_email_logging(logger: logging.Logger, app: Flask):
    fmt_email = logging.Formatter(
        """
            Message type:  %(levelname)s
            Name:          %(name)s
            Location:      %(pathname)s:%(lineno)d
            Module:        %(module)s/%(filename)s
            Function:      %(funcName)s
            Time:          %(asctime)s
            Message:

            %(message)s
        """
    )

    prefix = ""
    if app.config.get("IS_DEV", False):
        prefix = "-TEST"
    subject = "[CES2" + prefix + "] Application error"

    # email in case of errors
    mail_handler = logging.handlers.SMTPHandler(
        app.config.get("LOG_MAIL_HOSTNAME"),
        app.config.get("LOG_MAIL_FROM"),
        app.config.get("LOG_MAIL_TO"),
        subject,
    )
    mail_handler.setLevel(logging.ERROR)
    mail_handler.setFormatter(fmt_email)
    logger.addHandler(mail_handler)
    print(f"Logging {str(logger)} to email -> True")

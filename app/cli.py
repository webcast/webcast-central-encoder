import logging
import time

import click

from app.models.events import IndicoEventContribution
from app.services.cds.cds_record_service import CdsRecordService
from app.services.delete_old_events.delete_old_events_service import (
    DeleteOldEventsService,
)
from app.services.indico.indico_api_client_v2 import CreateLinkResult
from app.services.opencast.opencast_api import OpencastAPI
from app.services.opencast.opencast_media_package_service import (
    OpencastMediaPackageService,
)
from app.services.opencast.opencast_service import OpencastService
from app.services.video_player_link import VideoPlayerLinkService
from app.tasks.example import dummy_task
from app.tasks.media_packages import remove_older_content

logger = logging.getLogger("webapp.cli")


def initialize_cli(app):
    """
    Add additional commands to the CLI. These are loaded automatically on the main.py

    :param app: App to attach the cli commands to
    :return: None
    """

    @app.cli.command()
    def run_dummy_task():
        """
        Run a test task
        """
        click.echo("Running a dummy task")
        result = dummy_task.delay()
        result.wait()

    @app.cli.command()
    def trigger_error():
        """
        Run a test task
        """
        click.echo("Running trigger error")
        logger.error(
            "Error: Test error",
            extra={
                "contribution_id": "CCDDAA",
                "note": "Sample note",
                "exception": "Sample exception",
                "tags": {"contribution_id": "CCDDAA"},
            },
        )

    @app.cli.command()
    def remove_old_files():
        """
        Remove the old media packages from the /downloads folder
        """
        click.echo("Removing old media packages (>1 month)")
        result = remove_older_content.delay()
        result.wait()

    @app.cli.command("create-media-package-in-opencast")
    @click.argument("contribution_id")
    @click.option("--cut", default="false", type=str, help="Cut on Opencast")
    @click.option("--trans", default="false", type=str, help="Add transcripion")
    def create_media_package_in_opencast(contribution_id: str, cut: str, trans: str):
        """This method takes for granted that the files required have  been already created
        and are accessible at containers /downloads/opencast. It just get those files and send
        them to Opencast

        Args:
            contribution_id (str): _description_
            cut (bool): _description_
            trans (bool): _description_

        Returns:
            _type_: _description_
        """
        logger = logging.getLogger("webapp.cli")
        logger.info("Send local contents at /downloads to opencast.")
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()

        opencast_service = OpencastService(contribution, logger=logger)
        result = opencast_service.ingest_opencast_media_package(cut, trans)
        click.echo(
            f"""Finish sending media package to opencast for
            contribution {contribution_id}. Cut: {cut}, Trans: {trans}. Result: {result}"""
        )
        return result

    @app.cli.command("create-media-package")
    @click.argument("contribution_id")
    @click.option("--cut", default=False, help="Cut on Opencast")
    @click.option("--trans", default=False, help="Add transcripion")
    def create_media_package(contribution_id: str, cut: bool, trans: bool):
        """
        Creates the Opencast media package for a contribution

        :param contribution_id: The contribution ID of the contribution. E.g: 12345c1
        :type contribution_id: str
        :param cut: Whether or not is should be cut using Opencast
        :type cut: bool
        :param trans: Whether or not to add transcriptions
        :type trans: bool
        :return:
        :rtype:
        """
        click.echo(
            f"{contribution_id}: Creating media package for contribution Cut: {cut}, Trans: {trans}"
        )
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        service = OpencastMediaPackageService(contribution)
        result = service.create_media_package_files()
        click.echo(
            f"""Finish creating media package for
            contribution {contribution_id}. Cut: {cut}, Trans: {trans}. Result: {result}"""
        )

    @app.cli.command("check-cds")
    @click.argument("contribution_id")
    def check_cds_record(contribution_id: str):
        """
        Checks if a CDS record has been created and it's available

        :param contribution_id: The contribution_id of the contribution
        to check (must be present on CES)
        :type contribution_id: str
        :return: True|False
        :rtype: bool
        """
        click.echo(f"{contribution_id}: Checking CDS record creation for contribution.")
        logger = logging.getLogger("webapp.cli")
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        service = CdsRecordService(contribution, logger=logger)
        result = service.is_cds_record_ready()
        click.echo(f"Finished checking. CDS Record ready: {result}")
        return True

    @app.cli.command("delete-old-events")
    @click.option(
        "--from_time",
        default="2y",
        help="How old should be the deleted events. Values: 2y, 1y, 6m, 1m",
    )
    def delete_old_events(from_time: str):
        """
        Deletes the old Indico events from CES

        :param from_time: 2y, 1y, 6m, 1m (years and months)
        :type from_time: str
        :return:
        :rtype:
        """

        click.echo(f"Deleting old events from {from_time}")
        service = DeleteOldEventsService()
        result = service.delete_events_older_than(from_time)
        click.echo(f"Finish deleting old events. {result} events deleted")
        return True

    @app.cli.command("get-download-url")
    @click.argument("opencast_uid")
    def get_download_url(opencast_uid: str):
        click.echo(f"Getting download link for UID {opencast_uid}")
        logger = logging.getLogger("webapp.cli")
        contribution = IndicoEventContribution.query.get(448)
        service = OpencastAPI(contribution, logger=logger)
        response = service.get_event_publications(opencast_uid)

        wanted_tags = ["1080p-quality", "720p-quality"]
        url = None
        for media in response.json()[0]["media"]:
            if media.get("tags", []):
                for tag in media["tags"]:
                    if tag in wanted_tags:
                        url = media["url"]
                        break
        click.echo(url)

        click.echo(
            f"""{opencast_uid}: Finish get download url to Opencast for contribution."""
        )
        return True

    @app.cli.command("set-lecturemedia")
    @click.argument("year")
    def set_lecturemedia(year: int):
        """
        Deletes the old Indico events from CES

        :return:
        :rtype:
        """

        click.echo("Set lecturemedia on all events with Video Preview")

        # Get all contributions with is_cds_link_published=True and start_date in the year
        contributions = IndicoEventContribution.query.filter_by(
            is_cds_link_published=True
        ).all()
        processed_count = 0
        for contribution in contributions:
            if contribution.last_updated and contribution.last_updated.year == int(
                year
            ):
                click.echo(
                    f"Processing contribution {contribution.contribution_id} ({contribution.last_updated.year})"
                )
                time.sleep(1)
                service = VideoPlayerLinkService(
                    contribution.contribution_id,
                    contribution.db_id_full,
                    contribution.start_date.year,
                    logger=logger,
                )
                result = service.publish_video_player_link_on_indico()
                processed_count += 1
                if result == CreateLinkResult.CREATED:
                    click.echo(
                        f"Created link to lecturemedia on Indico ({contribution.contribution_id})"
                    )
                elif result == CreateLinkResult.UPDATED:
                    click.echo(
                        f"Updated link to lecturemedia on Indico ({contribution.contribution_id})"
                    )
                else:
                    click.echo(
                        f"Unable to create or update the lecturemedia link on Indico ({contribution.contribution_id})"
                    )
            else:
                click.echo(
                    (
                        f"Skipping contribution {contribution.contribution_id} ({contribution.last_updated.year})."
                        f"Not in the year {year}"
                    )
                )
        click.echo(f"Processed {processed_count} contributions of {len(contributions)}")
        return True

from flask import current_app
from zoomus import ZoomClient


class ZoomApiClient:
    @classmethod
    def get_instance(cls):
        return ZoomClient(
            current_app.config.get("ZOOM_API_KEY", None),
            current_app.config.get("ZOOM_API_SECRET", None),
        )

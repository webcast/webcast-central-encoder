import os
import subprocess


class FfmpegToolsException(Exception):
    pass


def trim_video_subprocess(input_file, start_time, end_time):
    input_file_temp = (
        os.path.splitext(input_file)[0] + "_original" + os.path.splitext(input_file)[1]
    )
    os.rename(input_file, input_file_temp)
    command = f"ffmpeg -i {input_file_temp} -ss {start_time} -to {end_time} -c:v copy -c:a copy {input_file}"
    result = subprocess.call(command, shell=True)  # nosec

    if result != 0:
        raise FfmpegToolsException("Error during trimming")

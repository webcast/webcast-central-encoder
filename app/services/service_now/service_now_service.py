import datetime
import logging

import pytz
from flask import current_app

from app.daos.metric import MetricsDAO
from app.extensions import db
from app.models.metrics import AvailabeMetricsNames
from app.services.service_now.service_now_api import ServiceNowAPI


class SnowSysName:
    MICALA = "Micala Software"
    WEBCAST_SERVICE = "Webcast and Recording Service"
    LECTURE_RECORDING_GROUP = "Lecture Recording 2nd Line Support"
    RECORDING = "Lecture Recording"
    POSTPROCESSING = "postprocessing"
    WEBCAST = "webcast"


class ServiceNowService:
    """
    Use ServiceNow API to manage ServiceNow Tickets.

    This class will only interact with Service Now if the:
    - SNOW_NOTIFY variable in config.py is set to True
    """

    def __init__(self, contribution, logger=None):
        self.contribution = contribution
        self.notify_snow = current_app.config["SNOW_NOTIFY"]
        self.indico_event_url = current_app.config["INDICO_EVENT_URL"]
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.snow")

    def get_ticket(self, name):
        """
        Get a ticket from ServiceNOW API
        :param name: Ticket identifier
        :type name: str
        :return:
        :rtype:
        """
        ticket = ServiceNowAPI(self.contribution).get_ticket(name)
        return ticket

    def update_ticket(self, name, data):
        if self.notify_snow:
            ticket = ServiceNowAPI(self.contribution).update_ticket(name, data)
            return ticket
        return ""

    def set_in_progress(self):
        """
        Set ticket state to in progress
        """
        self.logger.info(
            "{}: Set ticket {} state to in progress".format(
                self.contribution.contribution_id, self.contribution.snow_ticket
            )
        )
        status = 4
        data = {"u_current_task_state": status}
        ticket = self.update_ticket(self.contribution.snow_ticket, data)
        return ticket

    def add_comment(self, comment):
        try:
            data = {"comments": comment}
            ticket = self.update_ticket(self.contribution.snow_ticket, data)
            return ticket
        except Exception as e:
            self.logger.error(
                "Error: Add comment to Service Now ticket",
                extra={
                    "contribution_id": self.contribution.contribution_id,
                    "comment": comment,
                    "exception": str(e),
                    "tags": {"contribution_id": self.contribution.contribution_id},
                },
            )
            return None

    def add_work_note(self, note: str):
        try:
            data = {"work_notes": note}
            ticket = self.update_ticket(self.contribution.snow_ticket, data)
            return ticket
        except Exception as e:
            self.logger.error(
                "Error: Add work note to Service Now ticket",
                extra={
                    "contribution_id": self.contribution.contribution_id,
                    "note": note,
                    "exception": str(e),
                    "tags": {"contribution_id": self.contribution.contribution_id},
                },
            )
            return None

    def create_ticket(self):
        self.logger.info("Create ticket...")
        if not self.contribution.snow_ticket or self.contribution.snow_ticket == "":
            if self.notify_snow:
                result = ServiceNowAPI(self.contribution).create_ticket()

                if result and result["number"]:
                    # update lecture with ServiceNow ticket number
                    self.contribution.snow_ticket = result["number"]
                    self.contribution.is_snow_ticket_generated = True
                    self.contribution.is_snow_ticket_generated_date = (
                        datetime.datetime.now(pytz.timezone("Europe/Zurich"))
                    )
                    db.session.commit()
                    # Match with an indico ID
                    # self.matched_with_indico()
                    self.logger.info(
                        "SNOW ticket generated for contribution {}".format(
                            self.contribution.contribution_id
                        )
                    )
                    MetricsDAO.create(
                        AvailabeMetricsNames.CREATED_SNOW_TICKET,
                        AvailabeMetricsNames.CREATED_SNOW_TICKET,
                    )
                    return result
                else:
                    self.logger.error(
                        "Error: Unable to create SNOW ticket for contribution.",
                        extra={
                            "contribution_id": self.contribution.contribution_id,
                            "tags": {
                                "contribution_id": self.contribution.contribution_id
                            },
                        },
                    )
                return None
            return ""
        else:
            self.logger.info(
                "{}: Contribution already has a snow ticket. Skipping.".format(
                    self.contribution.contribution_id
                )
            )
            return None

    def matched_with_indico(self):
        """
        Update the ticket with information about indico match
        """

        self.logger.debug(
            "Add indico match to ticket: %s" % self.contribution.snow_ticket
        )
        # id = re.match('(\d+)(\w*)',str(indico_id)).group(1)
        comment = "Contribution matched with indico event: {}".format(
            self.indico_event_url.format(event_id=self.contribution.contribution_id)
        )
        self.add_work_note(comment)
        # Set ticket status as in progress
        self.set_in_progress()

    def add_eagreement_info(self, eagreement_status):
        """
        Update ticket with information about eagreement
        """
        self.logger.debug(
            "Add speaker release info to ticket: {}".format(
                self.contribution.snow_ticket
            )
        )

        if eagreement_status == "ACCEPTED":
            comment = "Speaker release has been accepted by all speakers"
        elif eagreement_status == "DECLINED":
            comment = "Speaker release has been declined, lecture can't be published"
        else:
            comment = "Speaker release info not known"

        self.add_work_note(comment)

    def add_requestor(self):
        """
        Add the requestor (creator) to the SNOW ticket watch list

        :param contribution_id: The contribution_id of the IndicoEventContribution object
        :type contribution_id: str
        :return: True
        :rtype: bool
        """
        creator_email = self.contribution.indico_event.creator_email
        snow_ticket = self.contribution.snow_ticket

        self.logger.debug(
            "Add requestor: %s to ticket: %s" % (creator_email, snow_ticket)
        )
        email = str(creator_email)
        data = {"watch_list": email}
        self.update_ticket(snow_ticket, data)
        return True

    def inserted_indico_link(self):
        """
        Update the ticket with information about inserted indico link and resolve ticket

        :param contribution_id: The contribution_id of the IndicoEventContribution object
        :type contribution_id: str
        :return: True
        :rtype: bool
        """
        self.logger.debug(
            "Indico link inserted to ticket: %s" % self.contribution.snow_ticket
        )
        comment = "Link to the lecture is available in Indico.  \nPostprocessing is finished\n" + current_app.config[
            "CDS_URL_RECORD"
        ] % str(
            self.contribution.cds_record
        )
        data = {"comments": comment}
        self.update_ticket(self.contribution.snow_ticket, data)
        return True

    #
    # def resolve_ticket(self, name, comment, code):
    #     """
    #     Resolve the ticket, it will be closed in 2 weeks if nobody will reopen it
    #     """
    #     logger.debug("Resolve ticket: %s, with code: %s" %(name,code))
    #     status  = 9
    #     data = { "comments" : comment,
    #              "u_close_code" : code,
    #              "u_current_task_state" : status }
    #     self.update_ticket(name, data)

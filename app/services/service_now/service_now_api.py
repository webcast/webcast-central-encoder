import logging

import requests
from flask import current_app
from requests.auth import HTTPBasicAuth

from secret.config import SN_API_URL, SN_PASSWORD, SN_USER


class SnowSysName:
    MICALA = "Micala Software"
    WEBCAST_SERVICE = "Webcast and Recording Service"
    LECTURE_RECORDING_GROUP = "Lecture Recording 2nd Line Support"
    RECORDING = "Lecture Recording"
    POSTPROCESSING = "postprocessing"
    WEBCAST = "webcast"


class ServiceNowAPI:
    """
    Use ServiceNow API to manage ServiceNow Tickets.
    """

    def __init__(self, contribution, logger=None):
        self.contribution = contribution
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.service_service_now")

    def _get(self, url):
        """
        call ServiceNow API with get request
        """
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        response = requests.get(
            url, headers=headers, verify=False, auth=HTTPBasicAuth(SN_USER, SN_PASSWORD)
        )
        if response.status_code != requests.codes.ok:
            response.raise_for_status()
        return response.json()["result"]

    def _post(self, url, data):
        """
        call ServiceNow API with post request
        """
        try:
            self.logger.debug("data: %s" % data)
            headers = {"Accept": "application/json", "Content-Type": "application/json"}
            response = requests.post(
                url,
                headers=headers,
                auth=HTTPBasicAuth(SN_USER, SN_PASSWORD),
                data=data,
            )
            if response.status_code != 201:
                self.logger.debug(response.json())
                response.raise_for_status()
            return response.json()["result"]

        except Exception as e:
            self.logger.warning(
                "Error on Service NOW API _post: {}. (Data: {})".format(str(e), data)
            )
            raise e

    def _put(self, url, data):
        """
        call ServiceNow API with put request
        """
        try:
            self.logger.debug("data: %s" % data)
            headers = {"Content-Type": "application/json", "Accept": "application/json"}
            response = requests.put(
                url,
                headers=headers,
                auth=HTTPBasicAuth(SN_USER, SN_PASSWORD),
                data=data,
            )
            if response.status_code != 201:
                response.raise_for_status()
            return response.json()

        except Exception as e:
            self.logger.warning(
                "Error on Service NOW API _put: {}. (Data: {})".format(str(e), data)
            )
            raise e

    def create_ticket(self):
        self.logger.info("Creating ticket on Snow API")
        table = "u_request_fulfillment"
        url = SN_API_URL + table

        # set Webcast and Recording Service as Service Element
        service = SnowSysName.WEBCAST_SERVICE

        # set Lecture recording as Functional Element
        functional_element = SnowSysName.RECORDING

        # set assignment group as Lecture Recording 2nd Line Support
        assignment_group = SnowSysName.LECTURE_RECORDING_GROUP

        prefix = ""
        if current_app.config.get("IS_DEV", False):
            prefix = "[TEST] "

        short_description = prefix + "Postprocessing of contribution {}".format(
            self.contribution.contribution_id
        )

        description = f"The contribution {self.contribution.contribution_id} is ready for postprocessing."

        # set functional category to postprocessing
        functional_category_id = SnowSysName.POSTPROCESSING

        # task state: ASSIGNED
        task_state = 2

        # Set category to Support & Consultancy
        category = 2

        contact_type = "monitoring"

        # set data vales, caller value will be set automatically to Micala
        data = {
            "u_business_service": service,
            "u_functional_element": functional_element,
            "u_current_task_state": task_state,
            "u_functional_category": functional_category_id,
            "assignment_group": assignment_group,
            "u_category": category,
            "short_description": short_description,
            "comments": description,
            "contact_type": contact_type,
        }
        self.logger.debug("data: %s" % data)

        response = self._post(url, str(data))

        return response

    def get_ticket(self, name):
        table = "u_request_fulfillment"
        url = SN_API_URL + table + "?sysparm_query=number=" + name
        return self._get(url)

    def update_ticket(self, name, data):
        sys_id = self.get_ticket(name)[0]["sys_id"]
        table = "u_request_fulfillment"
        url = SN_API_URL + table + "/" + sys_id
        self.logger.debug("Data: %s" % str(data))
        return self._put(url, str(data))

import datetime
import logging

import pytz

from app.extensions import db
from app.models.events import IndicoEvent


class DeleteOldEventsService:
    def __init__(self, logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.delete_old_event_service")

    def delete_events_older_than(self, from_time: str):
        """
        Deletes all the Indico events older than the given parameter

        :param from_time: String with the following values: 2y, 1y, 6m 1m
        :type from_time: str
        :return: The number of deleted events
        :rtype: int
        """
        older_than = datetime.timedelta(days=730)

        def older_than_2_years():
            older_than = datetime.timedelta(days=730)
            return older_than

        def older_than_1_year():
            older_than = datetime.timedelta(days=365)
            return older_than

        def older_than_6_months():
            older_than = datetime.timedelta(days=183)
            return older_than

        def older_than_1_month():
            older_than = datetime.timedelta(days=30)
            return older_than

        def error():
            self.logger.error(
                "Unknown command passed to delete_events_older_than: {}".format(
                    from_time
                )
            )

        from_period_switch = {
            "2y": older_than_2_years,
            "1y": older_than_1_year,
            "6m": older_than_6_months,
            "1m": older_than_1_month,
        }
        from_period_switch.get(from_time, error)()
        since = datetime.datetime.now(pytz.timezone("Europe/Zurich")) - older_than
        indico_events = IndicoEvent.query.filter(IndicoEvent.date >= since).all()

        self.logger.info(
            "Deleting events older than {}. Found {} events.".format(
                from_time, len(indico_events)
            )
        )

        count = 0
        for event in indico_events:
            db.session.delete(event)
            count += 1
        db.session.commit()

        self.logger.info(
            "Deleted {}/{} events older than {}.".format(
                count, len(indico_events), from_time
            )
        )

        return count

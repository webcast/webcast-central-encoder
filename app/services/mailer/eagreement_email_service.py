import logging

from flask import current_app, render_template

from app.services.indico.indico_eagreement_service import IndicoEagreemenentService
from app.services.mailer.mail_service import MailService


class EagreemenentEmailService:
    def __init__(self, contribution, logger=None):
        """

        :param contribution: The contribution that will be handled
        :type contribution: app.models.events.IndicoEventContribution
        :param logger:
        :type logger:
        """
        self.contribution = contribution
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.indico_eagreement")

    def _mail_to(self):
        """
        If it's a production instance, send mail to the creator of the event and inform webcast service as BCC.
        Otherwise, send an email to the MAIL_TO configuration parameter
        :return: A list with the MAIL_TO addresses
        :rtype: list[str]
        """
        if current_app.config["IS_DEV"]:
            mail_to = current_app.config["MAIL_TO"]
        else:
            mail_to = [
                self.contribution.indico_event.creator_email,
                current_app.config["WEBCAST_SERVICE_MAIL"],
            ]

        return mail_to

    def _populate_reminder_mail(self, missing_speakers=None):
        """
        Populate the email with the contribution's information

        :param missing_speakers: Array of speakers
        :type missing_speakers: list[str]
        :return: A tuple with the subject and the email content
        :rtype: tuple[str,str]
        """
        if missing_speakers is None:
            missing_speakers = []

        subject = "Speaker release missing"

        email_template = render_template(
            "emails/speaker_release_missing.html",
            creator_name=self.contribution.indico_event.creator,
            indico_event_url=current_app.config["INDICO_EVENT_URL"].format(
                event_id=self.contribution.indico_event.indico_id
            ),
            title=self.contribution.title,
            missing_speakers_list=missing_speakers,
            indico_id=self.contribution.indico_event.indico_id,
        )

        return subject, email_template

    def send_reminder(self):
        """
        Generate first reminding email for the contribution

        :return: SUCCESS or ERROR
        :rtype: str
        """
        try:
            # define indico id of set of contributions
            # lecture_indico_id = re.match('(\d+)(\w*)', lecture.indico_id).group(1)
            # lectures, _ = lm.fetch_all(indico_id=lecture_indico_id, first_reminder_sent=0)
            # lectures_ids = [l.indico_id for l in lectures if
            #                 l.eagreement_status != LectureEagreementStatus.ACCEPTED
            # and l.eagreement_status != LectureEagreementStatus.NO_INFORMATION]
            # Get speakers that will be listed in the email
            # speakers = self._get_speakers(lectures_ids)
            response, missing_agreements_list = IndicoEagreemenentService(
                self.contribution, logger=self.logger
            ).fetch_eagreement_status()

            if self.contribution.speakers != "":
                self.logger.info(
                    f"{self.contribution.contribution_id}: Send first warning mail to "
                    f"address {self.contribution.indico_event.creator_email}"
                )
                subject, message = self._populate_reminder_mail(
                    missing_speakers=missing_agreements_list
                )
                MailService(self.contribution, logger=self.logger).send_html_mail(
                    current_app.config["MAIL_FROM"], self._mail_to(), subject, message
                )

            else:
                self.logger.warning(
                    f"{self.contribution.contribution_id}: Not sending reminder since there are no speakers"
                )

            return "SUCCESS"

        except Exception as ex:
            self.logger.error(
                "{}: Exception occurred. Error: {}".format(
                    self.contribution.contribution_id, str(ex)
                )
            )
            return "ERROR"

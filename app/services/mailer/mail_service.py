import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from flask import current_app


class MailService:
    def __init__(self, contribution, logger=None):
        """

        :param contribution:
        :type contribution: app.models.events.IndicoEventContribution|None
        :param logger:
        :type logger: logging.Logger
        """
        self.contribution = contribution
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.mail_service")

    def send_html_mail(self, email_from, email_to, subject, html):
        """
        Send html formatted mail.

        :param email_from: Email address that will appear on the FROM
        :type email_from: str
        :param email_to: Email address to send the emait to
        :type email_to: str[]
        :param subject: Subject of the email
        :type subject: str
        :param html: Content of the email
        :type html: str
        :return: Nothing
        :rtype: void
        """
        if self.contribution:
            self.logger.info(
                "{}: Sending EMAIL...".format(self.contribution.contribution_id)
            )
        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart("alternative")
        msg["Subject"] = subject
        msg["From"] = email_from
        # set only the real receipent of the message (without BCC etc)
        msg["To"] = email_to[0]

        # encode html message

        body = MIMEText(html, "html", "utf-8")
        msg.attach(body)
        s = smtplib.SMTP(current_app.config["MAIL_HOSTNAME"])

        s.sendmail(email_from, email_to, msg.as_string())
        s.quit()
        if self.contribution:
            self.logger.info(
                "{}: EMAIL sent.".format(self.contribution.contribution_id)
            )

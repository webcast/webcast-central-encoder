import logging
from urllib.parse import parse_qs, urlparse

import requests
from requests import Response


class RundeckApiClient:
    def __init__(
        self, rundeck_base_url, api_token, logger: logging.Logger = None
    ) -> None:
        self.rundeck_url = rundeck_base_url
        self.api_token = api_token
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.rundeck_api_client")

    def make_post_request(self, url: str, data: dict) -> Response:
        """Make a POST request to the given URL and data.

        Args:
            url (str): The URL to send the request to
            data (dict): The data to send with the request

        Returns:
            Response: The response from the request
        """
        seconds_timeout = 4
        headers = {
            "X-Rundeck-Auth-Token": self.api_token,
        }
        response = requests.post(
            url, json=data, headers=headers, timeout=seconds_timeout, verify=False
        )
        return response

    def make_api_request(self, url: str, extra_data=None) -> Response:
        self.logger.debug(f"Sending API request to Rundeck to {url}")

        data = {}
        extra_items = extra_data if extra_data else {}

        for key, value in extra_items.items():
            data[key] = value

        self.logger.debug(f"Callback data: {data}")

        response = self.make_post_request(url, data)
        return response

    def extract_query_params_from_string(self, url: str) -> dict:
        """Extract the query parameters from a string.

        Args:
            url (str): _description_

        Returns:
            dict: Dict with all the query parameters
        """
        parsed_url = urlparse(url)
        captured_value = parse_qs(parsed_url.query)
        return captured_value

    def request_data_json_update(
        self, year: str, contribution_id: str, media: str
    ) -> Response:
        url = f"{self.rundeck_url}/api/50/webhook/4mVsmywaD7Yp9c4F09s1Kq6Bv9yy1Iqd/?year={year}&id={contribution_id}"  # noqa
        self.logger.debug(f"Sending API request to Rundeck to {url}")

        data = self.extract_query_params_from_string(url)

        extra_items = {"media_id": media}

        for key, value in extra_items.items():
            data[key] = value

        self.logger.debug(f"Callback data: {data}")

        response = self.make_post_request(url, data)

        self.logger.debug(f"Response from Rundeck: {response}")
        return response

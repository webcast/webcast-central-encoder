import logging

from flask import current_app

from app.models.events import IndicoEventContribution
from app.services.rundeck.rundeck_api_client import RundeckApiClient


class RundeckService:
    def __init__(
        self, contribution: IndicoEventContribution, logger: logging.Logger = None
    ) -> None:
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.rundeck_service")
        self.contribution = contribution

    def make_data_json_update_request(self):

        rundeck_api_client = RundeckApiClient(
            current_app.config["RUNDECK_SERVER_BASE_URL"],
            current_app.config["RUNDECK_API_TOKEN"],
            logger=self.logger,
        )
        response = rundeck_api_client.request_data_json_update(
            str(self.contribution.start_date.year),
            self.contribution.contribution_id,
            self.contribution.transcription_media_id,
        )

        return response

import logging
import re

import smbclient
from flask import current_app
from smbclient.path import exists, isdir
from smbprotocol.exceptions import SMBOSError


class MoveFilesFromFolderService:
    def __init__(self, logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("webapp.move_files_service")

        self.username = current_app.config["SERVER_USER"]
        self.password = current_app.config["SERVER_PASS"]
        self.server = current_app.config["SERVER"]

        smbclient.register_session(
            self.server, username=self.username, password=self.password
        )

    def move_files_to_subfolders(self, folder_path: str):
        """Moves all the camera and slides files to their respective folders

        Args:
            folder_path (str): The path of the folder to move the files from

        """
        self.logger.debug(f"Listing files in folder: {folder_path}")
        files = smbclient.listdir(folder_path)

        only_files = []
        for file_name in files:
            d_filename = rf"{folder_path}\{file_name}"
            if not isdir(d_filename):
                only_files.append(file_name)

        camera_files = [file for file in only_files if re.match(r".*_camera", file)]
        slides_files = [file for file in only_files if re.match(r".*_slides", file)]

        # For each one of the files, extract the prefix before the _camera or _slides string
        self.handle_files(folder_path, camera_files, "_camera", "camera.mp4")
        self.handle_files(folder_path, slides_files, "_slides", "slides.mp4")

        self.logger.debug(f"Camera files: {camera_files}")
        self.logger.debug(f"Slides files: {slides_files}")

    def handle_files(
        self,
        folder_path: str,
        file_names: list,
        file_identifier: str,
        default_filename: str,
    ):
        """Manages all the files in the list, generates the corresponding folders and moves the files

        Args:
            folder_path (str): Original path of the files to move
            file_names (list): List of names of the files
            file_identifier (str): Identifier of the file to split it (_camera or _slides)
            default_filename (str): Filename to use if the folder is empty to create the first file
        """
        for original_file_name in file_names:

            try:
                # recording_group is the prefix of the file name
                # It matches the event id, but it can be anything before the file_identifier
                recording_group = original_file_name.split(file_identifier)[0]

                # Check if the folder with the name of the prefix exists
                destination_foldername = rf"{folder_path}\{recording_group}"

                self.create_folder(destination_foldername)

                # If there are no files in the folder, the file name will be the prefix without _
                # Else, the file name will be the original file name
                files_count = self.get_dir_files_count(destination_foldername)

                destination_file_path = rf"{destination_foldername}\{default_filename}"

                if files_count in [0, 1] and not exists(destination_file_path):
                    destination_filename = default_filename
                else:
                    destination_filename = original_file_name

                original_file_path = rf"{folder_path}\{original_file_name}"
                destination_file_path = (
                    rf"{destination_foldername}\{destination_filename}"
                )

                self.move_file_to_folder(
                    original_file_path,
                    destination_file_path,
                )
            except SMBOSError as error:
                self.logger.warning(
                    f"Error: {error}. File: {original_file_path}. Destination: {destination_file_path}"
                )

    def move_file_to_folder(
        self,
        original_file_path: str,
        destination_file_path: str,
    ):
        """Creates a file (renaming it) in the destination folder if it doesn't exist

        Args:
            original_file_path (str): Original file path
            destination_file_path (str): Destination file path
        """
        # If the file doesn't exist in the destination folder, move the file to the folder
        if not exists(destination_file_path):
            self.logger.debug(
                f"Moving file: {original_file_path} to {destination_file_path}"
            )
            smbclient.rename(original_file_path, destination_file_path)
        else:
            self.logger.info(f"File {destination_file_path} already exists. Skipping.")

    def create_folder(self, folder_path: str):
        """Creates a folder if it doesn't exist

        Args:
            folder_path (str): Path to the folder to be created
        """
        if not exists(folder_path):
            smbclient.mkdir(folder_path)

    def get_dir_files_count(self, folder_path: str) -> int:
        """Counts the files in a folder

        Args:
            folder_path (str): Path to the folder

        Returns:
            int: The files count
        """
        files = smbclient.listdir(folder_path)
        return len(files)

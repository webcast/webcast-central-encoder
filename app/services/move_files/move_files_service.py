import logging
import os

from app.daos.indico_event import IndicoEventDAO
from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.services.smb_service import SMBService

logger = logging.getLogger("webapp.move_files_service")


class MoveFilesService:
    def __init__(self, event):
        """

        :param event: The Indico Event to be handled
        :type event: app.models.events.IndicoEvent
        """
        self.event = event

    def _create_folder_for_contribution(self, contribution):
        smb_service = SMBService()
        try:
            logger.debug(
                "Creating folder for contribution {}: {}".format(
                    contribution.contribution_id, contribution.recording_path
                )
            )
            smb_service.get_files_from_server(
                contribution.recording_path, is_full_path=True
            )
        except Exception as e:
            logger.debug(
                "Folder {} does not exist ({}). Creating".format(
                    contribution.recording_path, e
                )
            )
            smb_service.create_directory(contribution.recording_path, is_full_path=True)

    def create_folders_for_contributions(self, event_destination_path):
        logger.debug("Creating folders for contributions...")
        indico_contribution_dao = IndicoEventContributionDAO()
        for contribution in self.event.contributions:
            indico_contribution_dao.set_recording_path(
                contribution.id,
                event_destination_path + "\\" + contribution.contribution_id,
            )
            self._create_folder_for_contribution(contribution)
        logger.debug("Finished creating folders for contributions")

    def _create_folder_for_event(self, event_recording_path):
        # create the folder if it does not exist
        logger.debug(
            "Creating the folder for the event: {}".format(event_recording_path)
        )
        smb_service = SMBService()
        try:
            smb_service.get_files_from_server(event_recording_path, is_full_path=True)
        except Exception as e:
            logger.debug(
                "Folder for event ({}) does not exist ({}). Folder will be created.".format(
                    event_recording_path, e
                )
            )
            smb_service.create_directory(event_recording_path, is_full_path=True)

        indico_event_dao = IndicoEventDAO()
        indico_event_dao.set_recording_path(self.event.id, event_recording_path)

    def get_recording_final_destination(
        self, source_file_with_path, event_destination_path, files_to_move
    ):
        # If there is only 1 contribution, we move the file to the contribution folder
        # Otherwise we move it to the event folder
        # If there is only one file, we rename it to camera.mp4 or slides.mp4
        original_file_name = source_file_with_path.split("\\")[-1]

        smb_service = SMBService()
        count_files = self.get_number_of_files(original_file_name, files_to_move)
        file_name = original_file_name
        if count_files == 1:
            if "camera" in original_file_name:
                file_name = "camera.mp4"
            elif "slides" in original_file_name:
                file_name = "slides.mp4"

        def get_event_path(event_file_name):
            final_path_with_file_name = event_destination_path + "\\" + event_file_name
            if smb_service.file_exists(final_path_with_file_name):
                event_file_name = original_file_name
                final_path_with_file_name = (
                    event_destination_path + "\\" + event_file_name
                )
            return final_path_with_file_name

        def get_contribution_path(event_file_name):
            final_path_with_file_name = (
                event_destination_path
                + "\\"
                + self.event.contributions[0].contribution_id
                + "\\"
                + event_file_name
            )
            if smb_service.file_exists(final_path_with_file_name):
                event_file_name = original_file_name
                final_path_with_file_name = (
                    event_destination_path
                    + "\\"
                    + self.event.contributions[0].contribution_id
                    + "\\"
                    + event_file_name
                )
            return final_path_with_file_name

        if len(self.event.contributions) > 1:
            dest_path_with_file_name = get_event_path(file_name)
        else:
            dest_path_with_file_name = get_contribution_path(file_name)
        return dest_path_with_file_name

    def get_number_of_files(self, file_name, files_to_move):
        files_found = []
        if "camera" in file_name:
            files_found = [file for file in files_to_move if "camera" in file]
        elif "slides" in file_name:
            files_found = [file for file in files_to_move if "slides" in file]
        count_files = len(files_found)
        return count_files

    def get_files_to_move(self, recording_path):
        logger.debug("Recording path: {}".format(recording_path))
        srcdir = os.path.join(recording_path)
        logger.debug("Find files to move on : %s" % srcdir)

        camera_files = []
        slides_files = []
        file_list = SMBService().get_files_from_server(srcdir, is_full_path=True)

        # \\cernmedia33.cern.ch\Recordings\Main_Auditorium
        if file_list:
            for file in file_list:
                try:
                    # 12345_camera-[2021-03-09_14-38-24]-000
                    if file.startswith("{}_camera".format(self.event.indico_id)):
                        camera_files.append(srcdir + "\\" + file)
                    elif file.startswith("{}_slides".format(self.event.indico_id)):
                        slides_files.append(srcdir + "\\" + file)
                except Exception as ex:
                    logger.error(
                        "Exception occurred on get_files_to_move: {}".format(ex)
                    )
                    raise ex
        return camera_files, slides_files

    def move_recorded_files(self, recording_path: str) -> bool:
        try:
            logger.debug(
                f"Locating and moving the recorded files matching the current event: {recording_path} "
            )
            camera_files, slides_files = self.get_files_to_move(recording_path)
            logger.debug(f"Moving camera files: {camera_files}")
            self._move_recorded_files_list(recording_path, camera_files)
            logger.debug(f"Moving slides files: {slides_files}")
            self._move_recorded_files_list(recording_path, slides_files)
            return True
        except Exception as ex:
            logger.warning(
                f"Failed to move recorded files. Path: {recording_path} ({ex})"
            )
            raise Exception("Failed to move recorded files.") from ex

    def _move_recorded_files_list(self, recording_path: str, file_list: list):
        """
        Move and rename files after the recording stopped in a specific folder.
        """
        logger.debug("Moving Recordings to final location")
        # get all the files to move
        if len(file_list) > 0:
            smb_service = SMBService()
            event_destination_path = recording_path + "\\" + self.event.indico_id
            self._create_folder_for_event(event_destination_path)
            self.create_folders_for_contributions(event_destination_path)

            for source_file_with_path in file_list:
                try:
                    logger.debug(f"Moving file {source_file_with_path}")
                    dest_path_with_file_name = self.get_recording_final_destination(
                        source_file_with_path, event_destination_path, file_list
                    )
                    if len(self.event.contributions) > 1:
                        logger.debug(
                            "Event has more than contribution. Moving the recordings to the event's folder"
                        )
                    else:
                        logger.debug(
                            "Event has one contribution. Moving the recordings to the contribution's folder"
                        )

                    smb_service.move_file(
                        source_file_with_path,
                        dest_path_with_file_name,
                        is_full_path=True,
                    )

                    logger.debug(f"Finished moving the file {dest_path_with_file_name}")

                except Exception as ex:
                    logger.error(
                        f"Failed to move files {source_file_with_path}. (Error: {ex})"
                    )
                    raise ex

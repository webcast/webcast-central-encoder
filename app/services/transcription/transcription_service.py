import logging

from requests import ReadTimeout

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.models.contribution_workflow.transcription_workflow import (
    OpencastTranscriptionStates,
)
from app.models.events import IndicoEventContribution
from app.services.rundeck.rundeck_service import RundeckService
from app.services.transcription.ttaas_service import TtaasService


class TranscriptionService:
    def __init__(self, contribution: IndicoEventContribution, logger=None):
        """

        :param contribution: The contribution involved in the operation
        :type contribution: app.models.events.IndicoEventContribution
        :param logger:
        :type logger:
        """
        self.contribution = contribution
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.service_transcription")

    def request_transcription_for_contribution(self) -> bool:
        if not self.contribution.opencast_uid or self.contribution.opencast_uid == "":
            return None

        api = TtaasService(self.contribution.contribution_id, logger=self.logger)
        try:
            response = api.upload_to_ttaas()
            self.logger.debug(f"TTAAS response: {response}")
            if response and response.status_code == 201:
                indico_contribution_dao = IndicoEventContributionDAO(self.logger)
                indico_contribution_dao.set_transcription_error_message(
                    self.contribution.contribution_id, error_message=""
                )
                return True

            return False

        except ReadTimeout as error:
            self.logger.warning(
                (
                    f"{self.contribution.contribution_id}: Error on transcription request. "
                    f"No response from TTAAS. ({error})"
                ),
                exc_info=True,
            )
            return False

    def request_transcription_update(self) -> bool:
        if (
            not self.contribution.opencast_uid
            or self.contribution.transcription_media_id == ""
        ):
            return None

        api = RundeckService(self.contribution, logger=self.logger)
        try:
            indico_contribution_dao = IndicoEventContributionDAO()
            response = api.make_data_json_update_request()

            if response.status_code == 200:
                indico_contribution_dao.set_transcription_update_status(
                    self.contribution.contribution_id,
                    OpencastTranscriptionStates.COMPLETED,
                )
                return True

            if not response:
                self.logger.warning(
                    "Error on transcription update request.No response from Rundeck",
                    extra={
                        "tags": {"contribution_id": self.contribution.contribution_id}
                    },
                )
                indico_contribution_dao.set_transcription_update_status(
                    self.contribution.contribution_id,
                    OpencastTranscriptionStates.ERROR,
                )
                return False

            message = (
                f"{self.contribution.contribution_id}: Error on transcription update request workflow."
                f"{response.status_code} - {response.text}"
            )
            self.logger.warning(message)
            return False

        except ReadTimeout as error:
            self.logger.error(error)
        return False

import json
import logging
import os
from os import path

from flask import current_app

from app.helpers.helpers import convert_to_utc
from app.models.events import IndicoEventContribution
from app.services.opencast.opencast_service import OpencastService
from app.services.transcription.ttaas_api_client import TtaasApiClient
from app.services.video_player_link import VideoPlayerLinkService


class TtaasService:
    def __init__(self, contribution_id: str, logger=None) -> None:
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.ttaas_service")

        self.downloads_path = path.join(current_app.config["TTAAS_FOLDER_PATH"])
        self.ttaas_service_username = current_app.config["TTAAS_SERVICE_USERNAME"]
        self.ttaas_service_callback_url = current_app.config[
            "TTAAS_SERVICE_CALLBACK_URL"
        ]
        self.api_client = TtaasApiClient(
            current_app.config["TTAAS_SERVICE_ENDPOINT"],
            current_app.config["TTAAS_SERVICE_TOKEN"],
            logger=self.logger,
        )

        self.contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()

    def build_read_groups_string(self):
        contribution_access = self.contribution.allowed
        # Convert contribution_access string to json
        access_dict = json.loads(contribution_access.replace("'", '"'))
        groups = access_dict["groups"]
        new_access = ""
        if len(groups) > 0:
            new_access = (
                "weblecture-service," if "weblecture-service" not in groups else ""
            )
        for group in groups:
            new_access += f"{group},"
            # Add comma if not last group
            if group != groups[-1]:
                new_access += ","
        return new_access

    def build_read_users_string(self):
        contribution_access = self.contribution.allowed
        # Convert contribution_access string to json
        access_dict = json.loads(contribution_access.replace("'", '"'))
        users = access_dict["users"]
        new_access = ""
        for user in users:
            new_access += f"{user}"
            # Add comma if not last user
            if user != users[-1]:
                new_access += ","
        return new_access

    def upload_to_ttaas(self):
        oc_service = OpencastService(self.contribution, logger=self.logger)
        file_path = oc_service.download_video_from_opencast()

        language = "fr" if self.contribution.language == "fra" else "en"

        self.logger.debug(f"{self.contribution.contribution_id}: Language: {language}")

        users = self.build_read_users_string()
        groups = self.build_read_groups_string()

        self.logger.debug(
            f"{self.contribution.contribution_id}: Users: {users} Groups: {groups}"
        )

        is_public = "false"
        if users == "" and groups == "":
            is_public = "true"

        player_link_service = VideoPlayerLinkService(
            self.contribution.contribution_id,
            "",
            str(self.contribution.start_date.year),
            logger=self.logger,
        )
        player_link = player_link_service.generate_video_player_link()

        utc_datetime = convert_to_utc(self.contribution.start_date_time)
        formatted_datetime = utc_datetime.strftime("%Y-%m-%d, %H:%M:%S")

        data = {
            "title": self.contribution.title,
            "language": language,
            "username": self.ttaas_service_username,
            "notificationUrl": f"{self.ttaas_service_callback_url}?contribution_id={self.contribution.contribution_id}",
            "referenceUrl": player_link,
            "searchReadGroups": self.build_read_groups_string(),
            "searchReadUsers": self.build_read_users_string(),
            "searchTranscriptionIsPublic": is_public,
            "searchDateTime": formatted_datetime,
            "comments": "Sent using the CES postprocessing workflow",
        }

        self.logger.debug(
            f"{self.contribution.contribution_id}: Uploading video to TTAAS: {data}"
        )

        response = self.api_client.upload_to_ttaas(file_path, data)

        self.logger.debug(
            f"{self.contribution.contribution_id}: TTAAS response: {response.status_code} {response.text}"
        )

        os.remove(file_path)

        return response

import logging

import requests
from requests_toolbelt import MultipartEncoder


class TtaasApiClient:
    def __init__(self, service_endpoint: str, service_token: str, logger=None) -> None:
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.ttaas_api_client")

        self.service_endpoint = service_endpoint
        self.service_token = service_token

    @property
    def ttaas_api_session(self) -> requests.Session:
        session = requests.Session()
        session.headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {self.service_token}",
        }
        session.stream = True

        return session

    def upload_to_ttaas(self, video_path: str, data: dict) -> requests.Response:
        file_name = video_path.split("/")[-1]

        api_path = "/api/public/v1/uploads/ingest/"
        url = f"{self.service_endpoint}{api_path}"

        self.logger.debug(f"Creating MultipartEncoder for: {data['title']}")
        self.logger.debug(
            f"{data['title']} Using file and path: {file_name}, {video_path}. "
        )
        self.logger.debug(f"{data['title']} Using data: {data}. ")

        try:
            encoder = MultipartEncoder(
                fields={
                    "title": data["title"],
                    "language": data["language"],
                    "notificationMethod": "CALLBACK",
                    "username": data["username"],
                    "notificationUrl": data["notificationUrl"],
                    "searchReadGroups": data["searchReadGroups"],
                    "searchReadUsers": data["searchReadUsers"],
                    "referenceUrl": data["referenceUrl"],
                    "searchTranscriptionIsPublic": data["searchTranscriptionIsPublic"],
                    "searchDateTime": data["searchDateTime"],
                    "comments": data["comments"],
                    "mediaFile": (
                        file_name,
                        open(video_path, "rb"),
                        "application/octet-stream",
                    ),
                }
            )
        except Exception as exception:
            self.logger.error(
                f"Error creating MultipartEncoder: {exception}", exc_info=True
            )
            raise

        self.logger.debug(f"Calling TTAAS API URL: {url} for {data['title']}")

        response = self.ttaas_api_session.post(
            url,
            headers={"Content-Type": encoder.content_type},
            data=encoder,
            verify=False,
        )
        self.logger.debug(
            f"TTAAS API response for {data['title']}: {response.status_code}, {response.text}"
        )

        return response

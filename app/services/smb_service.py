import logging

import smbclient
from flask import current_app
from smbprotocol.exceptions import SMBOSError

logging.getLogger("smbclient").setLevel(logging.WARNING)
logging.getLogger("smbprotocol").setLevel(logging.WARNING)


class SMBService:
    smb_share = "RECORDINGS"

    def __init__(self, logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.smb_service")
        self.username = current_app.config["SERVER_USER"]
        self.password = current_app.config["SERVER_PASS"]
        self.server = current_app.config["SERVER"]

        smbclient.register_session(
            self.server, username=self.username, password=self.password
        )

    def build_recording_path_on_server(self, path):
        """
        Generates the full recording path from the last segment of the path

        :param path: Last part of the path
        :type path: str
        :return: The full path
        :rtype: str
        """
        if path.startswith("\\"):
            path = path[1:]
        return r"\\" + self.server + "\\" + self.smb_share + "\\" + path

    def move_file(self, source_path, dest_path, is_full_path=False):
        """
        Copy a file from one location to another
        :param source_path: Path from the SHARE (e.g. RECORDINGS) without starting "\"
        :type source_path: str
        :param dest_path: Path from the SHARE (e.g. RECORDINGS) without starting "\"
        :type dest_path: str
        :return:
        :rtype:
        """
        if not is_full_path:
            source_path = r"\\{config.SERVER}\{self.smb_share}\{source_path}"
            dest_path = r"\\{config.SERVER}\{self.smb_share}\{dest_path}"
        self.logger.debug(f"Moving file: {source_path} to {dest_path}")
        if not self.file_exists(dest_path):
            smbclient.rename(source_path, dest_path)

    def create_directory(self, path, is_full_path=False):
        if not is_full_path:
            path = rf"\\{self.server}\{self.smb_share}\{path}"
        smbclient.mkdir(path)

    def create_file(self, path, content, is_full_path=False):
        if not is_full_path:
            path = rf"\\{self.server}\{self.smb_share}\{path}"
        # Create a file and write to it
        with smbclient.open_file(path, mode="w") as file:
            file.write(content)

    def get_files_from_server(self, path, is_full_path=False):
        """
        Get a list of all the files in a give folder
        :param path: The folder absolute path from the RECORDINGS share
        :return: A list of files
        """
        if not is_full_path:
            path = rf"\\{self.server}\{self.smb_share}\{path}"

        self.logger.debug(f"Get files from server path: {path}")

        file_list = smbclient.listdir(path)
        return file_list

    def download_file_from_server(self, file_remote_path, file_local_path):
        """
        Download a file from a server using SMB

        :param file_remote_path: The absolute path of the file to download from the smb_share
        :type file_remote_path: str
        :param file_local_path: The absolute path of the file that will be downloaded
        :type file_local_path: str
        :return: True
        :rtype: bool
        """
        if file_remote_path.startswith(r"\\"):
            file_remote_path = file_remote_path[2:]
        remote_path = self.build_recording_path_on_server(file_remote_path)
        self.logger.info(f"Starting download of: {remote_path}")
        logging.getLogger("smbclient").setLevel(logging.WARNING)
        logging.getLogger("smbprotocol").setLevel(logging.WARNING)
        with smbclient.open_file(remote_path, mode="rb", share_access="r") as read_file:
            with open(file_local_path, "wb") as write_file:
                while True:
                    chunk = read_file.read(4096)
                    if not chunk:
                        break
                    write_file.write(chunk)
        self.logger.info(f"Download completed: {remote_path}")
        return True

    def file_exists(self, file_remote_path):
        try:
            smbclient.stat(file_remote_path)
            return True
        except SMBOSError:
            return False

import logging
from datetime import datetime

from pytz import timezone, utc

from app.services.indico.indico_api import IndicoAPI


class IndicoToCdsAPI:
    def __init__(self, logger=None):
        self.CURRENT_TIMEZONE = "Europe/Zurich"  # must be a standard timezone
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.indico_cds_api")

    def UTC_to_timezone(self, dt, timezone_to):
        """
        Convert a UTC datetime to a specific timezone.
        """
        # load the datetime in UTC timezone
        utc_dt = utc.localize(dt)
        # prepare the final timezone
        new_tz = timezone(timezone_to)
        # convert UTC time to new timezone time
        return new_tz.normalize(utc_dt.astimezone(new_tz))

    def _parse_date(self, datetime_dict, to_timezone=None, for_json=False):
        """
        Parse date and convert from to UTC to given timezone
        """
        if datetime_dict and "date" in datetime_dict and "time" in datetime_dict:
            # time can be in the format "17:40:32.432415", with milliseconds
            datetime_obj = datetime.strptime(
                "%s %s" % (datetime_dict["date"], datetime_dict["time"].split(".")[0]),
                "%Y-%m-%d %H:%M:%S",
            )
            if to_timezone:
                # convert timezone
                datetime_obj = self.UTC_to_timezone(datetime_obj, to_timezone)
            if for_json:
                return {
                    "date": datetime_obj.strftime("%Y-%m-%d"),
                    "time": datetime_obj.strftime("%H:%M:%S"),
                    "datetime": datetime_obj.strftime("%Y-%m-%d %H:%M:%S"),
                    "iso": datetime_obj.isoformat(),
                }
            else:
                return datetime_obj
        else:
            return None

    def _parse_requestor(self, person):
        """
        Return a list of dict adding a normalized person name
        """
        if person:
            return {
                "id": person["id"],
                "first_name": person["first_name"],
                "last_name": person["last_name"],
                "email": person["email"],
                "affiliation": person["affiliation"],
            }
        else:
            return None

    def _parse_person(self, person):
        """
        Return a list of dict adding a normalized person name
        """
        if person:
            return {
                "id": person["person_id"],
                "first_name": person["first_name"],
                "last_name": person["last_name"],
                "email": person["email"],
                "affiliation": person["affiliation"],
            }
        else:
            return None

    def _parse_session_slot(
        self, session_slot, indico_id, nesting_level, event_timezone, for_json
    ):
        """
        Parse the elements of a session slot
        """
        # filter out not scheduled ones
        if not session_slot["startDate"]:
            return

        # filter out posters
        if (
            "isPoster" in session_slot["session"]
            and session_slot["session"]["isPoster"]
        ):
            return

        # slot id is id_session-id_slot
        try:
            id = session_slot["session"]["id"]
            session_id = session_slot["session"]["db_id"]
            long_id = "%ss%s" % (indico_id, id)
            deprecated_long_id = "%ss%s" % (indico_id, id)
        except Exception:
            raise Exception(
                "Session Slot Id %s not recognized" % session_slot["session"]["id"]
            )

        contributions = []
        for c in session_slot["contributions"]:
            contrib = self._parse_contribution(
                c, indico_id, nesting_level + 1, event_timezone, for_json
            )
            if contrib:
                contributions.append(contrib)
        # sort
        contributions = sorted(contributions, key=lambda k: k["start_datetime"])

        return {
            "type": "session_slot",
            "nesting_level": nesting_level,
            "id_session": id,
            "session_id": session_id,
            "long_id": long_id,
            "deprecated_long_id": deprecated_long_id,
            "title": session_slot["title"],
            "title_slot": session_slot["slotTitle"],
            "description": session_slot["description"],
            "speakers": [self._parse_person(s) for s in session_slot["conveners"]],
            "location": session_slot["location"],
            "room": session_slot["room"],
            "room_fullname": session_slot["roomFullname"]
            or session_slot["room"],  # roomFullname can be NULL
            "start_datetime": self._parse_date(
                session_slot["startDate"], event_timezone, for_json
            ),
            "end_datetime": self._parse_date(
                session_slot["endDate"], event_timezone, for_json
            ),
            "allowed": session_slot["allowed"],  # users and groups
            "url": session_slot["url"],
            "contributions": contributions,
        }

    def _parse_contribution(
        self, contribution, indico_id, nesting_level, event_timezone, for_json
    ):
        """
        Parse the elements of a contribution
        """
        # filter out not scheduled ones
        if not contribution["startDate"]:
            return

        long_id = "%sc%s" % (indico_id, contribution["id"])
        start_datetime = self._parse_date(
            contribution["startDate"], event_timezone, for_json
        )
        end_datetime = self._parse_date(
            contribution["endDate"], event_timezone, for_json
        )
        allowed = contribution["allowed"]

        subcontributions = []
        for sc in contribution["subContributions"]:
            s = self._parse_subcontribution(sc, long_id, nesting_level + 1)
            # add missing fields
            s["description"] = contribution["description"]
            s["start_datetime"] = start_datetime
            s["end_datetime"] = end_datetime
            s["url"] = contribution["url"]
            s["allowed"] = allowed
            s["contribution_id"] = contribution["db_id"]

            subcontributions.append(s)
        # sort
        subcontributions = sorted(subcontributions, key=lambda k: int(k["id"]))

        return {
            "type": "contribution",
            "nesting_level": nesting_level,
            "contribution_id": contribution["db_id"],
            "long_id": long_id,
            "title": contribution["title"],
            "description": contribution["description"],
            "speakers": [self._parse_person(s) for s in contribution["speakers"]],
            "location": contribution["location"],
            "room": contribution["room"],
            "room_fullname": contribution["roomFullname"]
            or contribution["room"],  # roomFullname can be NULL
            "start_datetime": start_datetime,
            "end_datetime": end_datetime,
            "duration": contribution["duration"],
            "allowed": allowed,  # users and groups
            "url": contribution["url"],
            "subcontributions": subcontributions,
        }

    def _parse_subcontribution(self, sub, indico_id, nesting_level):
        """
        Parse the elements of a subcontribution
        """
        long_id = "%ssc%s" % (indico_id, sub["id"])

        return {
            "type": "subcontribution",
            "nesting_level": nesting_level,
            "id": sub["id"],
            "subcontribution_id": sub["db_id"],
            "long_id": long_id,
            "title": sub["title"],
            "speakers": [self._parse_person(s) for s in sub["speakers"]],
            "duration": sub["duration"],
        }

    def fetch_event_for_cds(self, indico_id, for_json=False):
        """
        Fetch one specific event from Indico.
        """
        try:
            response = IndicoAPI().fetch_event(indico_id)

            event = {}
            if (
                "count" in response
                and int(response["count"]) > 0
                and "results" in response
                and response["results"]
            ):
                r = response["results"][0]
                event_type = r["type"]
                if event_type not in ["conference", "meeting", "simple_event"]:
                    raise Exception(
                        "Event type not recognized. It should be one of %s but %s found instead."
                        % (["conference", "meeting", "simple_event"], event_type)
                    )

                event_timezone = (
                    r["timezone"] if "timezone" in r else self.CURRENT_TIMEZONE
                )
                # dates and times
                event_start = self._parse_date(
                    datetime_dict=r["startDate"],
                    to_timezone=event_timezone,
                    for_json=for_json,
                )
                event_end = self._parse_date(
                    datetime_dict=r["endDate"],
                    to_timezone=event_timezone,
                    for_json=for_json,
                )
                event_created = self._parse_date(
                    datetime_dict=r["creationDate"], to_timezone=None, for_json=for_json
                )
                # event_modified = self._parse_date(datetime_dict=r['modificationDate'],
                # to_timezone=None, for_json=for_json)

                event = {
                    "type": event_type,
                    "nesting_level": 0,  # added to know at which level we are
                    "indico_id": indico_id,
                    "long_id": indico_id,
                    "title": r["title"],
                    "category_id": r["categoryId"],
                    "category_name": r["category"],
                    "description": r["description"],
                    "location": r["location"],
                    "timezone": event_timezone,
                    "room": r["room"],
                    "room_fullname": r["roomFullname"]
                    or r["room"],  # roomFullname can be NULL
                    "requestor": self._parse_requestor(r["creator"]),
                    "speakers": [self._parse_person(s) for s in r["chairs"]],
                    "start_datetime": event_start,  # datetime object
                    "end_datetime": event_end,  # datetime object
                    "is_protected": r["hasAnyProtection"],
                    "allowed": r["allowed"],  # users and groups
                    "creation_datetime": event_created,
                    # 'modification_datetime': event_modified,
                    "url": r["url"],
                    "contributions": [],
                    "session_slots": [],
                    "organizer": r["organizer"],
                }

                # [sessions] -> contributions -> [subcontributions]
                for s in r["sessions"]:
                    ses = self._parse_session_slot(
                        s, indico_id, 1, event_timezone, for_json
                    )
                    if ses:
                        event["session_slots"].append(ses)
                # sort
                event["session_slots"] = sorted(
                    event["session_slots"], key=lambda k: k["start_datetime"]
                )

                for c in r["contributions"]:
                    contrib = self._parse_contribution(
                        c, indico_id, 1, event_timezone, for_json
                    )
                    if contrib:
                        event["contributions"].append(contrib)
                # sort
                event["contributions"] = sorted(
                    event["contributions"], key=lambda k: k["start_datetime"]
                )
                # logger.debug("Event fetched : %s" %event)
                return event

        except Exception as ex:
            self.logger.exception("CDS record Exception: %s" % ex)

        return {}

import logging

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.services.indico.indico_api import IndicoAPI
from app.services.service_now.service_now_service import ServiceNowService


class IndicoEagreementStatus:
    ACCEPTED = "ACCEPTED"
    DECLINED = "DECLINED"
    EMAIL_NOT_SENT = "EMAIL_NOT_SENT"
    SENT = "SENT"
    NO_INFORMATION = "NO_INFORMATION"
    UNKNOWN = "UNKNOWN"
    PENDING = "PENDING"
    FORCED = "FORCED"


class IndicoEagreemenentService:
    def __init__(self, contribution, logger=None):
        """

        :param contribution:
        :type contribution: app.models.events.IndicoEventContribution
        :param logger:
        :type logger:
        """
        self.contribution = contribution
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.indico_eagreement")

    # fetch e-agreement status from Indico, define speaker to fetch eagreement status for him/her
    def fetch_eagreement_status(self, api_response=None):
        """
        Fetch the status of the e-agreement for the given contribution

        :return: A tuple that contains the str indicating the eagreement
         status and a list of speakers that hadn't signed yet
        :rtype: tuple(str, list)
        """
        try:
            if api_response:
                response_json = api_response
            else:
                response_json = IndicoAPI().fetch_agreement(
                    self.contribution.indico_event.indico_id
                )

            not_accepted_list = []

            if "count" not in response_json:
                self.logger.info(
                    "Cannot fetch eagreement. Indico event doesn't exist (ID: {})".format(
                        self.contribution.indico_event.indico_id
                    )
                )
                raise Exception(
                    "Cannot fetch eagreement. Indico event doesn't exist (ID: {})".format(
                        self.contribution.indico_event.indico_id
                    )
                )
            # return response_json
            # # If there is response but without data then the lecture has no speakers
            # registered in Indico, return no information status
            elif "count" in response_json and int(response_json["count"]) == 0:
                self.logger.info("No speakers e-agreement information for this event")
                return IndicoEagreementStatus.NO_INFORMATION, not_accepted_list
            if "count" in response_json:
                self.logger.debug("Count in response")
            if int(response_json["count"]) > 0:
                self.logger.debug("Count > 0")
            if "results" in response_json:
                self.logger.debug("Results in response_json")
            # # check if there are results

            if (
                "count" in response_json
                and int(response_json["count"]) > 0
                and "results" in response_json
            ):
                self.logger.debug("Iterating results")
                results = response_json["results"]

                accepted = False
                declined = False
                email_not_sent = False
                pending = False

                for result in results:
                    # if lecture is:
                    # - regular event, but without contributions (contribution id is the same as event id)
                    # - contribution
                    # - subcontribution
                    # then fetch eagreement status for the lecture from Indico

                    # if contribution.speakers is not None:
                    self.logger.debug(
                        "{}: Contribution type is {}".format(
                            self.contribution.contribution_id,
                            self.contribution.contribution_type,
                        )
                    )
                    prefix = ""
                    agreement_id = self.contribution.contribution_id
                    if self.contribution.contribution_type == "Contribution":
                        prefix = "c:"
                        agreement_id = self.contribution.db_id
                    elif self.contribution.contribution_type == "SubContribution":
                        prefix = "sc:"
                        agreement_id = self.contribution.db_id

                    self.logger.debug(agreement_id)
                    if result["contrib"] == prefix + agreement_id:
                        # Update the speakers list
                        # if the e-mail  wasn't sent to at least one person the
                        # status is EMAIL_NOT_SENT
                        if not result["sent"]:
                            not_accepted_list.append(result["speaker"]["name"])
                            email_not_sent = True

                        # if the e-agreement  wasn't signed by at least one person
                        # the status is PENDING
                        elif result["accepted"] is None:
                            not_accepted_list.append(result["speaker"]["name"])
                            pending = True

                        # if the e-agreement  was signed but wasn't accepted by at least
                        # one person the status is DECLINED
                        elif not result["accepted"]:
                            declined = True
                            self.logger.debug("STATUS IS DECLINED")
                        # if the e-agreement was signed and accepted by every speaker
                        # the status is ACCEPTED,
                        elif result["accepted"]:
                            accepted = True
                            # if only fetching the stauts of one speaker return it
                            self.logger.debug("STATUS IS ACCEPTED")

                if declined:
                    # update ServiceNow ticket with information about eagreement
                    if (
                        self.contribution.snow_ticket is not None
                        and self.contribution.snow_ticket != ""
                    ):
                        ServiceNowService(self.contribution).add_eagreement_info(
                            IndicoEagreementStatus.DECLINED
                        )
                    return IndicoEagreementStatus.DECLINED, not_accepted_list

                if email_not_sent:
                    return IndicoEagreementStatus.EMAIL_NOT_SENT, not_accepted_list

                if pending:
                    return IndicoEagreementStatus.PENDING, not_accepted_list

                if accepted:
                    # update ServiceNow ticket with information about eagreement
                    if (
                        self.contribution.snow_ticket is not None
                        and self.contribution.snow_ticket != ""
                    ):
                        ServiceNowService(self.contribution).add_eagreement_info(
                            IndicoEagreementStatus.ACCEPTED
                        )

                    return IndicoEagreementStatus.ACCEPTED, not_accepted_list

            # if the event/contribution/subcontribution doesn't appear in response, set the status to NO INFORMATION
            return IndicoEagreementStatus.NO_INFORMATION, not_accepted_list

        except Exception as ex:
            self.logger.exception("Exception: %s" % ex)
            raise ex

    def fetch_eagreement_and_update(self, api_response=None):
        """

        :param contribution: app.models.events.IndicoEventContribution
        :type contribution:
        :return:
        :rtype:
        """
        response, _ = self.fetch_eagreement_status(api_response=api_response)

        if response:
            indico_contribution_dao = IndicoEventContributionDAO(self.logger)
            result = indico_contribution_dao.set_eagreement_status(
                self.contribution.contribution_id, response
            )
        else:
            result = False

        return result

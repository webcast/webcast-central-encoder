import hashlib
import hmac
import logging
import time
from urllib.parse import parse_qs, urlencode

import requests
from flask import current_app

from app.extensions import cache

logger = logging.getLogger("webapp.indico_api")


class IndicoAPI:
    """
    Use Indico Export API to fetch next webcast/recordings.
    """

    def __init__(self):
        self.INDICO_APIPATH_CATEGORIES = "/export/categ/{}.json"
        self.INDICO_APIPATH_EVENT = "/export/event/{}.json"
        self.INDICO_APIPATH_WEBCAST_RECORDING = "/export/webcast-recording.json"
        self.INDICO_APIPATH_EAGREEMENT = (
            "/export/agreements/cern-speaker-release/{}.json"
        )
        self.INDICO_APIPATH_INSERT_LINK = "/api/create_cds_link.json"

        self.INDICO_HOSTNAME = current_app.config["INDICO_HOSTNAME"]
        self.INDICO_API_KEY = current_app.config["INDICO_API_KEY"]
        self.INDICO_API_SECRET = current_app.config["INDICO_API_SECRET"]

    def _build_indico_request(self, path, params, only_public=False, persistent=False):
        """
        Build an Indico API request generating the required signature and sorting the parameters.

        :param path: Path for the API call
        :type path:  str
        :param params: Dictionary with all the parameters that will be appended to the path
        :type params: dict
        :param only_public: Whether or not is a public request
        :type only_public: bool
        :param persistent: Whether or not is a persistent request
        :type persistent: bool
        :return: A tuple with the request path and the encoded parameters as string
        :rtype: tuple(str, str)
        """
        items = list(params.items()) if hasattr(params, "items") else list(params)
        items.append(("apikey", self.INDICO_API_KEY))
        if only_public:
            items.append(("onlypublic", "yes"))
        if not persistent:
            items.append(("timestamp", str(int(time.time()))))
        items = sorted(items, key=lambda x: x[0].lower())
        url = "%s?%s" % (path, urlencode(items))
        url = str.encode(url)
        secret_key = str.encode(self.INDICO_API_SECRET)
        signature = hmac.new(secret_key, url, hashlib.sha1).hexdigest()
        items.append(("signature", signature))
        if not items:
            return path, None
        return "%s?%s" % (path, urlencode(items)), urlencode(items)

    def make_request(self, url, params):
        logger.debug(f"Making Indico request to {url}")
        path, _ = self._build_indico_request(url, params)

        url = self.INDICO_HOSTNAME + path

        response = (requests.get(url, timeout=10)).json()

        return response

    @cache.memoize(10)
    def fetch_event(self, indico_id):
        """
        Fetch an Indico Event by its indico_id

        :param indico_id: The Indico ID of the eent
        :type indico_id: str
        :return: A dict with the event information as is
        :rtype: dict
        """
        logger.debug(f"Fetching Indico event {indico_id}")
        params = {"tz": "UTC", "o": "start", "detail": "sessions"}
        # create and send request to Indico to fetch all events.
        url = self.INDICO_APIPATH_EVENT.format(indico_id)
        logger.debug(f"Replaced request URL: {url}")
        response = self.make_request(url, params)

        return response

    @cache.memoize(10)
    def fetch_webcasts_recordings(self):
        """
        Fetch all the webcasts and recordings of the next 15 days
        :return: JSON encoded response
        :rtype: dict
        """
        params = {"from": "today", "to": "+15d", "tz": "UTC", "o": "start"}
        path, _ = self._build_indico_request(
            self.INDICO_APIPATH_WEBCAST_RECORDING, params
        )
        request = self.INDICO_HOSTNAME + path
        response = requests.get(request)
        response = response.json()
        return response

    @cache.memoize(10)
    def fetch_agreement(self, indico_id):
        """
        Make a request to the Indico API to fetch the agreement status for the selected indico_id
        :param indico_id: The Indico ID that will be obtained
        :type indico_id: str
        :return: JSON encoded response
        :rtype: dict
        """
        params = {}
        path, _ = self._build_indico_request(
            self.INDICO_APIPATH_EAGREEMENT.format(indico_id), params
        )
        url = self.INDICO_HOSTNAME + path
        response = requests.get(url, timeout=3)
        response_json = response.json()

        return response_json

    @cache.memoize(10)
    def get_categories_tree(self, category_id):
        """
        Get the list of parent categories for an event.

        :param category_id: Indico ID of the category that will be fetched
        :type category_id: str
        :return: A list with all the parent categories to the current one
        :rtype: list
        """
        params = {
            "limit": "1",  # super fast query
        }

        path, _ = self._build_indico_request(
            self.INDICO_APIPATH_CATEGORIES.format(category_id), params
        )

        url = self.INDICO_HOSTNAME + path

        response = requests.get(url)
        response_json = response.json()

        # collect all the indico_ids and title
        if (
            "count" in response_json
            and int(response_json["count"]) > 0
            and "results" in response_json
            and response_json["results"]
        ):
            return [
                p["id"]
                for p in response_json["additionalInfo"]["eventCategories"][0]["path"]
                if "id" in p
            ]

        return []

    def insert_cds_link(self, cds_record, contribution_id):
        """
        Make a request to the Indico API to insert the CDS record link on the Indico event related contribution
        :param cds_record: Identifier of the CDS record
        :type cds_record: str
        :param contribution_id: The Indico contribution ID
        :type contribution_id: str
        :return: JSON encoded response
        :rtype: dict
        """
        params = {"cid": cds_record, "iid": contribution_id}

        path, items = self._build_indico_request(
            self.INDICO_APIPATH_INSERT_LINK, params
        )

        path = self.INDICO_HOSTNAME + self.INDICO_APIPATH_INSERT_LINK
        params = parse_qs(items)
        response = requests.post(path, data=params)
        """
        Expected response is:
        {
        'count': 1,
        'additionalInfo': {},
        '_type': 'HTTPAPIResult',
        'url': 'https://indico.cern.ch/api/create_cds_link.json?apikey=43f1b7f0',
        'results': {'success': True}
        }
        """

        response_json = response.json()

        return response_json

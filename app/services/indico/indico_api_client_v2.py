import requests
from flask import current_app


class CreateLinkResult:
    CREATED = "created"
    UPDATED = "updated"
    ERROR = "error"


class IndicoAPIClientV2:
    INDICO_API_PATH_CREATE_LINK = "/api/audiovisual/create-link"

    def __init__(self):

        self.INDICO_HOSTNAME = current_app.config["INDICO_HOSTNAME"]
        self.INDICO_API_TOKEN = current_app.config["INDICO_API_TOKEN"]

    def create_link(self, contribution_db_id_full, title, link_href):
        """

        :param contribution_db_id_full:
        :type contribution_db_id_full:
        :param title:
        :type title:
        :param link_href:
        :type link_href:
        :return:
        :rtype:
        """

        json_body = {"title": title, "url": link_href, "obj": contribution_db_id_full}
        url = self.INDICO_HOSTNAME + self.INDICO_API_PATH_CREATE_LINK
        headers = {"Authorization": "Bearer {}".format(self.INDICO_API_TOKEN)}
        response = requests.post(url, json=json_body, headers=headers)

        result = CreateLinkResult.ERROR
        if response:
            if response.status_code == 201:
                result = CreateLinkResult.CREATED
            if response.status_code == 200:
                result = CreateLinkResult.UPDATED
        return result

import logging
from datetime import datetime

from flask import current_app
from requests import ReadTimeout

from app.daos.indico_event import IndicoEventDAO
from app.daos.metric import MetricsDAO
from app.extensions import db
from app.helpers.helpers import Helpers
from app.models.events import IndicoEvent
from app.models.metrics import AvailabeMetricsNames
from app.services.indico.indico_api import IndicoAPI
from app.services.service_now.service_now_service import ServiceNowService


class IndicoServiceV2:
    """
    Use Indico Export API to fetch next webcast/recordings.
    """

    # Used on events with no room associated
    DEFAULT_ROOM_NAME = "External URI"

    def __init__(self, logger=None):
        self.current_timezone = current_app.config["CURRENT_TIMEZONE"]
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.indico_service")

    def get_events(self, static=True):
        try:
            events_json = self._get_events_from_indico_api()
            if (
                "count" in events_json
                and int(events_json["count"]) >= 0
                and "results" in events_json
            ):
                if int(events_json["count"]) == 0:
                    self.logger.info("No Indico events for the next 15 days")
                    return "OK"

                accepted_events = [
                    event for event in events_json["results"] if event["status"] == "A"
                ]

                self.logger.debug(
                    "Found {} accepted events in the next 15 days".format(
                        len(accepted_events)
                    )
                )
                accepted_events_length = 0
                processed_ids = []
                for event_data in accepted_events:
                    indico_id = event_data["event_id"]
                    if indico_id in processed_ids:
                        continue
                    accepted_events_length += 1
                    webcast_request = "webcast" in event_data["services"]
                    # check audience restriction
                    audience = self._extract_audience(event_data)
                    self.logger.debug(
                        "Found webcast with status A: {}".format(indico_id)
                    )
                    self.get_event(
                        indico_id,
                        audience=audience,
                        webcast=webcast_request,
                        static=static,
                    )
                    processed_ids.append(indico_id)

                self.logger.debug(
                    f"Found {accepted_events_length} accepted events (after loop) in the next 15 days"
                )
                return (
                    "OK",
                    f"Found {accepted_events_length} accepted events in the next 15 days",
                )
        except Exception as ex:
            self.logger.exception("Exception: %s" % ex)
            return "Failed to fetch events from Indico"

    def get_event(self, indico_id, audience=None, webcast=False, static=True):
        event_dict = self.get_event_details(indico_id, static)
        if event_dict:
            if not audience:
                event_dict["audience"] = "PUBLIC"
            else:
                try:
                    event_dict["audience"] = audience
                except TypeError as error:
                    self.logger.debug(
                        f"{indico_id}: {error} - Event dict: {event_dict} Audience: {audience}"
                    )
                    raise error
            event_dict["webcast"] = webcast

            event = IndicoEvent.query.filter_by(indico_id=indico_id).one_or_none()
            # If event is not running, let's replace it with the latest data
            if event is None:
                self.create_event(indico_id, event_dict)
            else:
                if event.running is False:
                    self.update_event(indico_id, event_dict)

        else:
            message = f"{indico_id} Event not found while fetching the event"
            self.logger.error(message)
            return "ERROR", message

        return "OK", "Event {} got from Indico successfully".format(indico_id)

    def _get_event_details_from_indico_api(self, indico_id):
        self.logger.debug("{} Get event details from Indico API".format(indico_id))
        try:
            event_data = IndicoAPI().fetch_event(indico_id)
            return event_data
        except Exception as ex:
            self.logger.exception("Exception: %s" % ex)
            raise ex

    def _extract_audience(self, event_data):
        if event_data["audience"] == "ATLAS collaborators only":
            audience = "ATLAS"
        elif event_data["audience"] == "CMS collaborators only":
            audience = "CMS"
        else:
            audience = "PUBLIC"
        return audience

    def _get_events_from_indico_api(self):
        self.logger.debug("Fetching Indico events from Indico API...")
        try:
            events = IndicoAPI().fetch_webcasts_recordings()
            self.logger.debug("Found {} events".format(len(events["results"])))
            return events
        except Exception as ex:
            self.logger.exception("Exception: %s" % ex)
            raise ex

    def get_event_details(self, indico_id, static=True):
        self.logger.debug(f"{indico_id} Get event details")
        try:
            event_data = self._get_event_details_from_indico_api(indico_id)
            if (
                "count" in event_data
                and int(event_data["count"]) > 0
                and "results" in event_data
                and event_data["results"]
            ):
                event_json = event_data["results"][0]
                event_dict = self._extract_event_information(event_json, static)

                return event_dict
        except ReadTimeout as error:
            self.logger.exception(
                f"{indico_id} Failed to fetch event details from Indico: {error}",
                exc_info=True,
            )
            return None

    def update_event(self, indico_id, event_dict):
        self.logger.debug(f"{indico_id} Updating event")
        indico_event_dao = IndicoEventDAO(self.logger)
        result = indico_event_dao.update(indico_id, event_dict)
        if result:
            self.logger.debug(f"{indico_id} event updated successfully")
        else:
            self.logger.warning(f"{indico_id} event could not be updated")

    def create_event(self, indico_id, event_dict):
        self.logger.debug(f"{indico_id} Creating event")
        indico_event_dao = IndicoEventDAO(self.logger)
        indico_event_dao.create(event_dict)

    def _extract_event_information(self, event_json, static=True):
        # self.logger.debug("Extract information from Event JSON")
        # fix timezone according to the current one
        start_date_time = self._extract_start_date_time(event_json)
        end_date_time = self._extract_end_date_time(event_json)
        title = self._extract_event_title(event_json)
        first_contribution_title = self._extract_first_contribution_title(event_json)
        indico_id = event_json["id"]
        allowed = str(event_json["allowed"])
        creator = "{} {}".format(
            event_json["creator"]["first_name"], event_json["creator"]["last_name"]
        )
        creator_email = event_json["creator"]["email"]
        creator_id = event_json["creator"]["id"]
        category = event_json["category"]
        category_id = event_json["categoryId"]
        room = event_json["room"]
        url = event_json["url"]
        description = event_json["description"]
        timezone = event_json["timezone"]
        _type = event_json["_type"]
        event_type = event_json["type"]

        contributions_json_array = event_json["contributions"]
        sessions_json_array = event_json["sessions"]

        event_dict = {
            "indico_id": indico_id,
            "title": title,
            "first_contrib": first_contribution_title,
            "room": room,
            "start_date_time": start_date_time,
            "end_date_time": end_date_time,
            "date": start_date_time.strftime("%Y-%m-%d"),
            "time": start_date_time.strftime("%H:%M"),
            "timezone": timezone,
            "recording": True,
            "running": False,
            "static": static,
            "custom_app_name": "",
            "allowed": allowed,
            "creator": creator,
            "creator_email": creator_email,
            "creator_id": creator_id,
            "category": category,
            "category_id": category_id,
            "url": url,
            "description": description,
            "friendly_id": "",
            "db_id_full": indico_id,
            "type": event_type,
            "_type": _type,
        }

        contributions_dict_array = []
        for contribution_json in contributions_json_array:
            contribution_dict = self._extract_contribution_information(
                contribution_json, event_dict
            )
            contributions_dict_array.append(contribution_dict)

        for session in sessions_json_array:
            session_contributions = session["contributions"]
            for contribution_json in session_contributions:
                contribution_dict = self._extract_contribution_information(
                    contribution_json, event_dict
                )
                contributions_dict_array.append(contribution_dict)

        event_dict["contributions"] = contributions_dict_array

        # Create always the main contribution for the event
        contribution_dict = self._extract_contribution_information(
            event_json, event_dict, with_event_as_contribution=True
        )
        contributions_dict_array.append(contribution_dict)

        return event_dict

    def _extract_contribution_information(
        self, contribution_json, parent_dict, with_event_as_contribution=False
    ):
        try:
            allowed = str(contribution_json["allowed"])
        except KeyError:
            allowed = parent_dict["allowed"]
        try:
            description = str(contribution_json["description"])
        except KeyError:
            description = parent_dict["description"]
        try:
            url = str(contribution_json["url"])
        except KeyError:
            url = parent_dict["url"]

        try:
            room = contribution_json["room"]
        except KeyError:
            room = parent_dict["room"]

        if with_event_as_contribution:
            parent_id = contribution_json["id"]
            db_id = contribution_json["id"]
            db_id_full = contribution_json["id"]
            contribution_id = contribution_json["id"]
            session = ""
        else:
            parent_id = contribution_json["friendly_id"]
            db_id = contribution_json["db_id"]
            db_id_full = self._extract_db_id_full(contribution_json, parent_dict)
            contribution_id = self._extract_contribution_id(
                contribution_json, parent_dict
            )

            try:
                session = str(contribution_json["session"])
            except KeyError:
                session = parent_dict["session"]

        title = contribution_json["title"]

        _type = contribution_json["_type"]
        parent_type = parent_dict["type"]

        start_date_time = self._extract_contribution_start_date_time(
            contribution_json, parent_dict
        )
        end_date_time = self._extract_contribution_end_date_time(
            contribution_json, parent_dict
        )

        # self.logger.debug("Extracting contribution's {} information".format(contribution_id))

        try:
            speakers = self._get_speakers_formatted(contribution_json["speakers"])
        except KeyError:
            try:
                speakers = self._get_speakers_formatted(contribution_json["chairs"])
            except KeyError:
                speakers = ""

        try:
            sub_contributions_json_array = contribution_json["subContributions"]
        except KeyError:
            sub_contributions_json_array = []

        contribution_dict = {
            "title": title,
            "allowed": allowed,
            "room": room,
            "start_date_time": start_date_time,
            "end_date_time": end_date_time,
            "start_date": start_date_time.strftime("%Y-%m-%d"),
            "start_time": start_date_time.strftime("%H:%M"),
            "end_date": end_date_time.strftime("%Y-%m-%d"),
            "end_time": end_date_time.strftime("%H:%M"),
            "description": description,
            "url": url,
            "session": session,
            "friendly_id": parent_id,
            "db_id": db_id,
            "speakers": speakers,
            "type": parent_type,
            "_type": _type,
            "db_id_full": db_id_full,
            "contribution_id": contribution_id,
        }

        sub_contributions_dict_array = []
        for contribution_json in sub_contributions_json_array:
            sub_contribution_dict = self._extract_contribution_information(
                contribution_json, contribution_dict
            )
            sub_contributions_dict_array.append(sub_contribution_dict)

        contribution_dict["contributions"] = sub_contributions_dict_array

        return contribution_dict

    def _extract_contribution_id(self, contribution_json, parent_dict):
        if parent_dict["_type"] == "Contribution":
            contribution_id = "{}sc{}".format(
                parent_dict["contribution_id"], contribution_json["friendly_id"]
            )
        elif parent_dict["_type"] == "Event" or parent_dict["_type"] == "Conference":
            contribution_id = "{}c{}".format(
                parent_dict["indico_id"], contribution_json["friendly_id"]
            )
        else:
            raise Exception(
                "Unknow type of parent contribution: {}".format(parent_dict["_type"])
            )
        return contribution_id

    def _extract_db_id_full(self, contribution_json, parent_dict):
        if parent_dict["_type"] == "Contribution":
            db_id_full = "{}sc{}".format(
                parent_dict["db_id_full"], contribution_json["db_id"]
            )
        elif parent_dict["_type"] == "Event" or parent_dict["_type"] == "Conference":
            db_id_full = "{}c{}".format(
                parent_dict["indico_id"], contribution_json["db_id"]
            )
        else:
            raise Exception(
                "Unknow type of parent contribution: {}".format(parent_dict["_type"])
            )
        return db_id_full

    def _get_speakers_formatted(self, speakers_array):
        """
        Get all the speakers from the speakers element and format them in a single line split by ','
        :param speakers_array: The array of speakers from the contribution
        :type speakers_array: list
        :return: A string with the speakers
        :rtype: str
        """
        speakers = ""
        speaker_count = 0
        for speaker in speakers_array:
            speaker_count += 1
            speakers += speaker["first_name"] + " " + speaker["last_name"]
            if speaker_count < len(speakers_array):
                speakers += ", "
        return speakers

    def _extract_contribution_start_date_time(self, contribtution_json, event_dict):
        try:
            start_date_time = self._extract_start_date_time(contribtution_json)
            return start_date_time
        except Exception:
            return event_dict["start_date_time"]

    def _extract_contribution_end_date_time(self, contribtution_json, event_dict):
        try:
            start_date_time = self._extract_end_date_time(contribtution_json)
            return start_date_time
        except Exception:
            return event_dict["end_date_time"]

    def _extract_first_contribution_title(self, event_json):
        title = event_json["title"]
        return title

    def _extract_event_title(self, event_json):
        title = event_json["title"]
        title = title.strip()
        return title

    def _extract_end_date_time(self, event_json):
        end_date = event_json["endDate"]["date"]
        end_time = event_json["endDate"]["time"]
        event_end_date_time = datetime.strptime(
            "%s %s" % (end_date, end_time), "%Y-%m-%d %H:%M:%S"
        )
        event_end_date_time = Helpers.UTC_to_timezone(
            event_end_date_time, self.current_timezone
        )
        return event_end_date_time

    def _extract_start_date_time(self, event_json):
        start_date = event_json["startDate"]["date"]
        start_time = event_json["startDate"]["time"]
        event_start_date_time = datetime.strptime(
            "%s %s" % (start_date, start_time), "%Y-%m-%d %H:%M:%S"
        )
        event_start_date_time = Helpers.UTC_to_timezone(
            event_start_date_time, self.current_timezone
        )
        return event_start_date_time

    def insert_cds_link(self, contribution, force=False):
        """
        Submit the request to insert a new link to the published video

        :param contribution: The contribution where the link will be inserted
        :type contribution: IndicoEventContribution
        :return: A parsed JSON response from the IndicoAPI request
        :rtype: dict
        """
        self.logger.debug(
            "{}: Contribution type is {}".format(
                contribution.contribution_id, contribution.contribution_type
            )
        )

        self.logger.info(
            "{}: Creating link to CDS Record {} on Indico contribution with force: {}".format(
                contribution.cds_record, contribution.contribution_id, force
            )
        )

        if current_app.config["IS_DEV"] and not force:
            self.logger.warning(
                "{}: Not Creating link to CDS Record {}. CES is on DEV mode".format(
                    contribution.contribution_id, contribution.cds_record
                )
            )
            return False

        if contribution.is_cds_record_set:
            response = IndicoAPI().insert_cds_link(
                contribution.cds_record, contribution.db_id_full
            )
            self.logger.debug(response)

            """
            {
             'count': 1,
             'additionalInfo': {},
             '_type': 'HTTPAPIResult',
             'url': 'https://indico.cern.ch/api/create_cds_link.json?
             apikey=43f1b7f0-2c47-475e-8ded-e9974f8b5fd9&cid=2241604
             &iid=979731c4127311&timestamp=1606471813
             &signature=9905f5796554b81cbeee97b7f0eb927f64bbc6ca' ,
             'results': {'success': True},
             'ts': 1606471813
            }
            """

            if (
                "count" in response
                and int(response["count"]) >= 0
                and "results" in response
            ):
                if response["results"]["success"]:
                    contribution.is_cds_link_published = True
                    contribution.is_cds_link_published_date = datetime.now()
                    db.session.commit()
                    # Resolve ServiceNow ticket and put comment about adding indico link
                    if (
                        contribution.snow_ticket is not None
                        and contribution.snow_ticket != ""
                    ):
                        ServiceNowService(contribution).inserted_indico_link()
                        if not current_app.config["IS_DEV"]:
                            ServiceNowService(contribution).add_requestor()

                        MetricsDAO.create(
                            AvailabeMetricsNames.PUBLISHED_CDS_LINK_INDICO,
                            AvailabeMetricsNames.PUBLISHED_CDS_LINK_INDICO,
                        )
            self.logger.info(response)

            return response

        raise Exception("Unable to insert CDS link if cds record is not set")

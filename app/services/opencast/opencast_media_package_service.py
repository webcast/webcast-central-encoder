import errno
import fnmatch
import json
import logging
import os
import shutil
from datetime import datetime, timezone

import requests
from flask import current_app

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.daos.metric import MetricsDAO
from app.helpers.file_system_helper import read_file_content, write_content_to_file
from app.models.contribution_workflow.opencast_workflow import MediaPackageStatus
from app.models.metrics import AvailabeMetricsNames
from app.services.ffmpeg.ffmpeg_tools import trim_video_subprocess
from app.services.ldap.cern_ldap_service import CernLdapService
from app.services.smb_service import SMBService
from app.services.zoom.zoom_api import ZoomApiClient
from app.utils.disk_space import DiskSpaceUtils


def convert_allowed_list(allow_str):
    export_allowed_list = {"groups": [], "users": [], "externals": []}
    not_found_list = {"groups": [], "users": []}
    allow_str = allow_str.replace("'", '"')

    allow_dict = json.loads(allow_str)
    for group in allow_dict["groups"]:
        export_allowed_list["groups"].append(group)
    for email in allow_dict["users"]:
        ldap_user = CernLdapService.get_user_by_email(email)
        if not ldap_user:
            ldap_user = CernLdapService.get_user_by_email(email, external=True)
            if ldap_user:
                export_allowed_list["externals"].append(
                    {"uid": ldap_user["saMAccountName"], "email": email}
                )
                continue
        if ldap_user:
            export_allowed_list["users"].append(ldap_user["username"])
        else:
            not_found_list["users"].append(email)

    return export_allowed_list, not_found_list


class OpencastMediaPackageService:
    def __init__(self, contribution, logger=None):
        """

        :param contribution: The involved contribution
        :type contribution: app.models.events.IndicoEventContribution
        :param logger:
        :type logger:
        """
        self.contribution = contribution
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.opencast")

    def is_disk_space_free(self):
        _, _, free = DiskSpaceUtils.get_disk_space(
            current_app.config["DOWNLOADS_FOLDER_PATH"]
        )
        if free < 1:
            self.logger.error(
                f"""{self.contribution.contribution_id}: Disk space is less than 1 GiB ({free} Gib).
                Please free some space before downloading"""
            )
            return False
        return True

    def is_downloadable(self, url):
        """
        Does the url contain a downloadable resource
        """
        size_limit_in_bytes = 21500000000  # ~20Gb
        head_request = requests.head(url, allow_redirects=True, timeout=5)
        header = head_request.headers
        content_type = header.get("content-type")
        content_length = header.get("content-length")

        if not content_type or not content_length:
            self.logger.warning(
                f"""{self.contribution.contribution_id}: Bypassing headers check since
                content type and content length are not set"""
            )
            return True

        if "mp4" in content_type.lower():
            return True

        if int(content_length) > size_limit_in_bytes:
            size = int(content_length) / 1000000
            limit = 21500000000 / 1000000
            self.logger.error(
                f"{self.contribution.contribution_id}: File size is over the limit {size}/{limit}GB"
            )
            return False

        self.logger.error(
            f"""{self.contribution.contribution_id}: Content type
            is not supported: {content_type.lower()}"""
        )
        return False

    def download_file(self, url, local_file_path):
        # NOTE the stream=True parameter below
        if ".zoom.us" in url:
            url = self.generate_zoom_url_with_token(url)
            MetricsDAO.create(
                AvailabeMetricsNames.USED_ZOOM_MEDIA_PACKAGE,
                AvailabeMetricsNames.USED_ZOOM_MEDIA_PACKAGE,
            )
        else:
            MetricsDAO.create(
                AvailabeMetricsNames.USED_URL_MEDIA_PACKAGE,
                AvailabeMetricsNames.USED_URL_MEDIA_PACKAGE,
            )

        with requests.get(url, stream=True, timeout=5) as req:
            req.raise_for_status()
            with open(local_file_path, "wb") as file:
                shutil.copyfileobj(req.raw, file)
        return local_file_path

    def generate_zoom_url_with_token(self, url):
        client = ZoomApiClient.get_instance()
        self.logger.debug(client.config["token"])
        url += "?access_token=" + client.config["token"]
        return url

    def retrieve_video_from_url(self, url, local_file_path):
        camera_found = False

        if self.is_downloadable(url) and self.is_disk_space_free():
            self.download_file(url, local_file_path)
            camera_found = True
        else:
            self.logger.error(
                f"""{self.contribution.contribution_id}: Unable to download the url: {url}.
                The content-type is not correct or disk space is not enough."""
            )

        return camera_found

    def retrieve_camera_and_slides_smb(self, folder_path):
        """
        Get the camera.mp4 and slides.mp4 files from the given path
        :param folder_path: Path where the camera and slides are going to be searched
        :return: A tupple with True/False in case the files were found
        """
        camera_found = False
        slides_found = False

        file_list = SMBService(logger=self.logger).get_files_from_server(folder_path)
        if file_list:
            for file in file_list:
                if fnmatch.fnmatch(file, "camera.mp4"):
                    camera_found = True
                elif fnmatch.fnmatch(file, "slides.mp4"):
                    slides_found = True

        return camera_found, slides_found

    def create_event_acl_xml(self):
        """
        Create am Opencast's acl.xml file for the contribution.
        It will contain the access control list for the event
        :return:
        """
        self.logger.info(
            f"{self.contribution.contribution_id}: Creating contribution ACLs xml file"
        )

        base_file_path = (
            "/opt/app-root/src/app/templates/opencast_media_templates/acl_base.xml"
        )
        file_to_write = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/acl.xml"
        )
        row_file_path = (
            "/opt/app-root/src/app/templates/opencast_media_templates/acl_row.xml"
        )

        # Open the local file that will be used as template
        row_content = read_file_content(row_file_path)
        # For each  group or user in the 'allowed' field, we want to generate a rule.
        # We first generate a string
        rules_string = ""
        allow_str = self.contribution.allowed.replace("'", '"')
        allow_dict = json.loads(allow_str)
        for group in allow_dict["groups"]:
            rules_string += row_content.format(group.upper())
        for user in allow_dict["users"]:
            rules_string += row_content.format(user.upper())

        # Open the base template
        base_content = read_file_content(base_file_path)

        file_content = base_content.format(rules_string)
        # Write the content in a new file
        write_content_to_file(file_content, file_to_write)

        self.logger.info(
            f"{self.contribution.contribution_id}: Finished creating ACLs xml file"
        )
        return True

    def create_event_series(self):
        """
        Create am Opencast's series.xml file for the contribution.
        It will contain the information about the event
        :return:
        """
        self.logger.info(
            f"{self.contribution.contribution_id}: Creating event series file..."
        )

        file_path = (
            "/opt/app-root/src/app/templates/opencast_media_templates/series.xml"
        )
        file_to_write = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/series.xml"
        )
        # Open the local file that will be used as template
        file_content = read_file_content(file_path)
        today_date = datetime.now(timezone.utc).isoformat()
        string_date = str(today_date).replace("+00:00", "Z")
        file_content = file_content.format(
            self.contribution.indico_event.category_id,
            string_date,
            self.contribution.indico_event.category,
        )
        # Write the content in a new file
        write_content_to_file(file_content, file_to_write)

        self.logger.info(
            f"{self.contribution.contribution_id}: Finished creating contribution series file."
        )
        return True

    def create_event_indico(self):
        """
        Create am Opencast's indico.xml file for the contribution.
        It will contain the indico custom information about
        the contribution
        :return:
        """
        self.logger.info(
            f"{self.contribution.contribution_id}: Creating contribution indico file..."
        )

        file_path = (
            "/opt/app-root/src/app/templates/opencast_media_templates/indico.xml"
        )
        file_to_write = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/indico.xml"
        )
        # Get the template and write the custom content
        file_content = read_file_content(file_path)
        file_content = file_content.format(
            indico_id=self.contribution.indico_event.indico_id,
            contribution_id=self.contribution.contribution_id,
            event_type=self.contribution.postprocessing_type,
            snow_id=self.contribution.snow_ticket,
            camera_start_time=self.contribution.camera_start_time,
            camera_end_time=self.contribution.camera_end_time,
            slides_start_time=self.contribution.slides_start_time,
            slides_end_time=self.contribution.slides_end_time,
            url=self.contribution.url,
            title=self.contribution.contribution_id,
        )
        # Write the content in a new file
        write_content_to_file(file_content, file_to_write)

        self.logger.info(
            f"{self.contribution.contribution_id}: Finished creating contribution indico file."
        )
        return True

    def create_event_episode(self):
        """
        Create am Opencast's episode.xml file for the contribution.
        It will contain the information about
        the contribution

        :return:
        :rtype:
        """
        self.logger.info(
            f"{self.contribution.contribution_id}: Creating contribution episode file..."
        )

        file_path = (
            "/opt/app-root/src/app/templates/opencast_media_templates/episode.xml"
        )
        file_to_write = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/episode.xml"
        )
        # Get the template and write the custom content
        creation_date = datetime.now(timezone.utc).isoformat()
        creation_date = str(creation_date).replace("+00:00", "Z")

        start_date = datetime.combine(
            self.contribution.start_date, self.contribution.start_time
        ).isoformat()
        start_date = str(start_date).replace("+00:00", "Z")

        end_date = datetime.combine(
            self.contribution.end_date, self.contribution.end_time
        ).isoformat()
        end_date = str(end_date).replace("+00:00", "Z")

        file_content = read_file_content(file_path)

        creator = self.contribution.speakers.replace("&", "&amp;")

        file_content = file_content.format(
            contributor=self.contribution.indico_event.creator,
            created=creation_date,
            creator=creator,
            description="",
            language=self.contribution.language,
            spatial=self.contribution.indico_event.room.indico_name,
            subject=self.contribution.indico_event.category,
            start=start_date,
            end=end_date,
            title=self.contribution.contribution_id,
            isPartOf=self.contribution.indico_event.category_id,
        )
        # Write the content in a new file
        write_content_to_file(file_content, file_to_write)

        self.logger.info(
            f"{self.contribution.contribution_id}: Finished creating contribution ACL file."
        )
        return True

    def create_event_manifest_file(self):
        """
        Create am Opencast's manifes.xml file for the contribution.
        It will contain the information about
        the contribution
        :param edit_on_opencast: Whether or not to use the Opencast time editor
        :type edit_on_opencast: bool
        :return:
        """
        self.logger.info(
            f"{self.contribution.contribution_id}: Creating contribution manifest file..."
        )

        manifest_file_name = self.get_manifest_file_name()

        file_path = (
            "/opt/app-root/src/app/templates/opencast_media_templates/"
            + manifest_file_name
        )
        output_manifest_file_name = "manifest.xml"
        file_to_write = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/"
            + output_manifest_file_name
        )
        # Get the template and write the custom content
        file_content = read_file_content(file_path)
        # Write the content in a new file
        write_content_to_file(file_content, file_to_write)

        self.logger.info(
            f"{self.contribution.contribution_id}: Finished creating contribution manifest file."
        )
        return True

    def trim_video_files(self):

        self.logger.info(
            f"{self.contribution.contribution_id}: Trimming video files..."
        )
        has_camera = self.contribution.has_camera_file
        has_slides = self.contribution.has_slides_file

        if has_camera and self.contribution.has_camera_split_values():
            self.logger.info(f"{self.contribution.contribution_id}: Trimming camera...")
            file_to_trim = os.path.join(
                current_app.config["DOWNLOADS_FOLDER_PATH"],
                self.contribution.contribution_id,
                "camera.mp4",
            )
            trim_video_subprocess(
                file_to_trim,
                self.contribution.camera_start_time,
                self.contribution.camera_end_time,
            )

        if has_slides and self.contribution.has_slides_split_values():
            self.logger.info(f"{self.contribution.contribution_id}: Trimming slides...")
            file_to_trim = os.path.join(
                current_app.config["DOWNLOADS_FOLDER_PATH"],
                self.contribution.contribution_id,
                "slides.mp4",
            )
            trim_video_subprocess(
                file_to_trim,
                self.contribution.slides_start_time,
                self.contribution.slides_end_time,
            )
        return True

    def get_manifest_file_name(self):
        manifest_file_name = "manifest-no-smil.xml"
        has_camera = self.contribution.has_camera_file
        has_slides = self.contribution.has_slides_file

        if has_camera and has_slides:
            return manifest_file_name

        if has_camera:
            manifest_file_name = "manifest_camera-no-smil.xml"

        if has_slides:
            manifest_file_name = "manifest_slides-no-smil.xml"

        return manifest_file_name

    def create_event_acl_file(self):
        self.logger.info(
            f"{self.contribution.contribution_id}: Creating contribution ACL file..."
        )

        file_to_write = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/acl.json"
        )
        with open(
            file_to_write, "w", encoding="utf-8"
        ) as file:  # Use file to refer to the file object
            export_allowed_list = convert_allowed_list(self.contribution.allowed)
            file.write(json.dumps(export_allowed_list, indent=2))
            file.close()

        self.logger.info(
            f"{self.contribution.contribution_id}: Finished creating contribution ACL file."
        )
        return True

    def create_media_package_files(self):
        """

        :param send_to_opencast_after_finish:
        :type send_to_opencast_after_finish:
        :param edit_on_opencast:
        :type edit_on_opencast:
        :param add_transcription:
        :type add_transcription:
        :return:
        :rtype:
        """
        self.logger.info(
            f"{self.contribution.contribution_id}: Create media package files"
        )
        indico_contribution_dao = IndicoEventContributionDAO(self.logger)
        indico_contribution_dao.set_media_package_generated_status(
            self.contribution.id,
            MediaPackageStatus.RUNNING,
            last_report="Running...",
        )

        opencast_media_package_service = OpencastMediaPackageService(self.contribution)

        self.create_local_contribution_folder()

        try:
            camera_found, slides_found = self.download_videos()

            if not (camera_found or slides_found):
                # response_data = self.display_camera_slides_error()
                self.logger.info(
                    f"""
                    {self.contribution.contribution_id}:
                    Camera found: {camera_found} - Slides found: {slides_found}
                    """
                )
                indico_contribution_dao = IndicoEventContributionDAO()
                indico_contribution_dao.set_media_package_generated_status(
                    self.contribution.id,
                    MediaPackageStatus.ERROR,
                    last_report="Neither camera nor slides found",
                )
                return "ERROR"

            # We expect an array of True's
            files_steps = [
                opencast_media_package_service.create_event_acl_file(),
                opencast_media_package_service.create_event_acl_xml(),
                opencast_media_package_service.create_event_episode(),
                opencast_media_package_service.create_event_indico(),
                opencast_media_package_service.create_event_manifest_file(),
                opencast_media_package_service.create_event_series(),
                opencast_media_package_service.trim_video_files(),
            ]

            self.logger.info(
                f"{self.contribution.contribution_id}: Finished generating contribution media package"
            )
            indico_contribution_dao = IndicoEventContributionDAO(self.logger)
            indico_contribution_dao.set_media_package_generated_status(
                self.contribution.id, MediaPackageStatus.OK, last_report="Finished"
            )
            if False in files_steps:
                indico_contribution_dao.set_media_package_generated_status(
                    self.contribution.id,
                    MediaPackageStatus.ERROR,
                    last_report="Error generating media package",
                )
                self.logger.error(
                    f"{self.contribution.contribution_id}: Error Generating the media package"
                )
                return "ERROR"

            return "OK"

        except FileNotFoundError as error:
            self.logger.critical(error, exc_info=True)
            indico_contribution_dao.set_media_package_generated_status(
                self.contribution.id,
                MediaPackageStatus.ERROR,
                last_report="Recording path not found",
            )
            return "ERROR"
        except ValueError as error:
            self.logger.critical(error, exc_info=True)
            indico_contribution_dao.set_media_package_generated_status(
                self.contribution.id,
                MediaPackageStatus.ERROR,
                last_report=str(error),
            )
            return "ERROR"

    def download_videos(self):
        camera_found = False
        slides_found = False

        if not self.contribution.recording_path:
            raise Exception(
                f"{self.contribution.recording_path}: Recording path is None"
            )

        if self.contribution.recording_path.startswith(
            "https"
        ) or self.contribution.recording_path.startswith("http"):
            camera_found = self.handle_http_video()
        else:
            camera_found, slides_found = self.handle_smb_video()
        return camera_found, slides_found

    def handle_smb_video(self):
        recording_path = self.contribution.recording_path
        if recording_path.startswith(r"\\"):
            recording_path = recording_path[2:]

        opencast_media_package_service = OpencastMediaPackageService(self.contribution)

        path = recording_path.split("\\")
        folder_path = "\\" + ("\\".join(str(path_element) for path_element in path[2:]))
        (
            camera_found,
            slides_found,
        ) = opencast_media_package_service.retrieve_camera_and_slides_smb(folder_path)
        smb_service = SMBService(logger=self.logger)

        self.contribution.has_camera_file = False
        self.contribution.has_slides_file = False
        if camera_found:
            self.contribution.has_camera_file = True
            camera_remote_path = folder_path + r"\camera.mp4"
            camera_local_path = (
                current_app.config["DOWNLOADS_FOLDER_PATH"]
                + "/"
                + self.contribution.contribution_id
                + "/camera.mp4"
            )
            self.logger.info(
                f"""
                {self.contribution.contribution_id}:
                Downloading {camera_remote_path} to {camera_local_path}
                """
            )
            smb_service.download_file_from_server(camera_remote_path, camera_local_path)
        if slides_found:
            self.contribution.has_slides_file = True
            smb_service.download_file_from_server(
                folder_path + r"\slides.mp4",
                current_app.config["DOWNLOADS_FOLDER_PATH"]
                + "/"
                + self.contribution.contribution_id
                + "/slides.mp4",
            )
        MetricsDAO.create(
            AvailabeMetricsNames.USED_SMB_MEDIA_PACKAGE,
            AvailabeMetricsNames.USED_SMB_MEDIA_PACKAGE,
        )

        return camera_found, slides_found

    def handle_http_video(self):
        opencast_media_package_service = OpencastMediaPackageService(self.contribution)

        self.logger.debug(
            f"Recording path starts with https: {self.contribution.recording_path}"
        )
        camera_local_path = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/camera.mp4"
        )
        self.logger.info(
            f"""{self.contribution.contribution_id}: Downloading
            {self.contribution.recording_path} to {camera_local_path}"""
        )
        try:
            camera_found = opencast_media_package_service.retrieve_video_from_url(
                self.contribution.recording_path, camera_local_path
            )
        except Exception as error:
            self.logger.error(
                f"""{self.contribution.contribution_id}: Error while retrieving
                video file from URL: (Error: {error})"""
            )
            raise error
        return camera_found

    def create_local_contribution_folder(self):
        try:
            self.logger.info("Creating contribution's folder...")
            os.makedirs(
                current_app.config["DOWNLOADS_FOLDER_PATH"]
                + "/"
                + self.contribution.contribution_id
            )
        except OSError as error:
            if error.errno == errno.EEXIST:
                pass

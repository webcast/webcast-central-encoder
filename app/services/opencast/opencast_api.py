import json
import logging
import shutil
import uuid

import requests
from flask import current_app
from requests_toolbelt import MultipartEncoder


class OpencastAPI:
    def __init__(self, contribution, logger=None):
        """

        :param contribution: The contribution involved in the operation
        :type contribution: app.models.events.IndicoEventContribution
        :param logger:
        :type logger:
        """
        self.contribution = contribution
        self.opencast_endpoint = current_app.config["OPENCAST_ENDPOINT"]
        self.opencast_user = current_app.config["OPENCAST_USER"]
        self.opencast_pass = current_app.config["OPENCAST_PASSWORD"]

        self.downloads_path = current_app.config["DOWNLOADS_FOLDER_PATH"]

        self.session = requests.Session()
        self.session.auth = (self.opencast_user, self.opencast_pass)
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.opencast_api")

    def create_media_package(self):
        """
        First step of the media package creation

        :return:
        :rtype:
        """
        api_path = "/ingest/createMediaPackage"
        url = self.opencast_endpoint + api_path
        self.logger.info(
            f"{self.contribution.contribution_id}: Creating Opencast media package"
        )
        try:
            self.logger.info(
                f"{self.contribution.contribution_id}: Create media package URL: {url}"
            )
            response = self.session.get(url, timeout=10, verify=False)
            self.logger.info(f"Opencast - Opencast media package requested: {url}")
            return response
        except Exception as ex:
            self.logger.warning(
                f"{self.contribution.contribution_id}: Opencast - Error while sending a request. URL: {url} Error: {ex}"
            )
            return None

    def ingest_media_package(
        self,
        media_package_text,
        flag_for_cutting="false",
        publish_to_indico="true",
        straight_to_publishing="false",
        add_transcription="false",
    ):
        """
        First step of the media package creation

        :return:
        :rtype:
        """
        api_path = "/ingest/ingest/cern-indico-publish"
        url = self.opencast_endpoint + api_path
        self.logger.info(
            f"{self.contribution.contribution_id}: Ingest Opencast media package"
        )
        try:
            payload = {
                "mediaPackage": media_package_text,
                "publishToIndico": publish_to_indico,
                "flagForCutting": flag_for_cutting,
                "flagForTranscription": add_transcription,
                "straightToPublishing": straight_to_publishing,
                "eventType": self.contribution.event_type,
            }
            self.logger.info(
                f"{self.contribution.contribution_id}: Ingest media package URL: {url}"
            )
            response = self.session.post(url, data=payload, timeout=10, verify=False)
            self.logger.info(f"Opencast - Ingest media package requested: {url}")
            return response
        except Exception as ex:
            self.logger.warning(
                f"{self.contribution.contribution_id}: Opencast - Error while sending a request. URL: {url} Error: {ex}"
            )
            return None

    def add_dc_catalog(self, media_package_text, file_path, flavor):
        """
        First step of the media package creation

        :return:
        :rtype:
        """
        api_path = "/ingest/addDCCatalog"
        url = self.opencast_endpoint + api_path
        self.logger.info(f"{self.contribution.contribution_id}: Adding DC catalog")
        try:
            dublin_core_text = ""
            with open(file_path, "r", encoding="utf-8") as file:
                dublin_core_text = file.read().replace("\n", "")
            payload = {
                "mediaPackage": media_package_text,
                "flavor": flavor,
                # plain file object, no filename or mime type produces a
                # Content-Disposition header with just the part name
                "dublinCore": dublin_core_text,
            }
            self.logger.info(
                f"{self.contribution.contribution_id}: Adding DC catalog URL: {url}"
            )
            response = self.session.post(
                url,
                data=payload,  # The MultipartEncoder is posted as data, don't use files=...!
                # The MultipartEncoder provides the content-type header with the boundary:
                timeout=10,
                verify=False,
            )
            self.logger.info(f"Opencast - Opencast add DC catalog requested: {url}")
            return response
        except FileNotFoundError as error:
            self.logger.warning(
                f"{self.contribution.contribution_id}: Opencast - Error while sending a request. {url}. Error: {error}",
                exc_info=True,
            )
            return None

    def add_catalog(self, media_package_text, file_path, flavor):
        """
        First step of the media package creation

        :return:
        :rtype:
        """
        api_path = "/ingest/addCatalog"
        url = self.opencast_endpoint + api_path
        self.logger.info(f"{self.contribution.contribution_id}: Adding catalog")
        try:
            content_text = ""
            with open(file_path, "r", encoding="utf-8") as file:
                content_text = file.read().replace("\n", "")
            files = {"BODY": content_text}
            payload = {
                "mediaPackage": media_package_text,
                "flavor": flavor,
                # plain file object, no filename or mime type produces a
                # Content-Disposition header with just the part name
            }
            self.logger.info(
                f"{self.contribution.contribution_id}: Adding catalog. URL: {url}"
            )
            response = self.session.post(
                url,
                data=payload,  # The MultipartEncoder is posted as data, don't use files=...!
                files=files,
                # The MultipartEncoder provides the content-type header with the boundary:
                timeout=10,
                verify=False,
            )
            self.logger.debug(f"Opencast - Opencast add catalog requested: {url}")
            return response
        except Exception as ex:
            self.logger.error(
                (
                    f"{self.contribution.contribution_id}: Opencast - "
                    f"Error while sending a request. {self.contribution.contribution_id} Error: {ex}"
                )
            )
            return None

    def add_attachment(self, media_package_text, file_path, flavor):
        """
        First step of the media package creation

        :return:
        :rtype:
        """
        api_path = "/ingest/addAttachment"
        url = self.opencast_endpoint + api_path
        self.logger.info(f"{self.contribution.contribution_id}: Adding attachment")
        try:
            content_text = ""
            with open(file_path, "r", encoding="utf-8") as file:
                content_text = file.read().replace("\n", "")
            files = {"BODY": content_text}
            payload = {
                "mediaPackage": media_package_text,
                "flavor": flavor,
                # plain file object, no filename or mime type produces a
                # Content-Disposition header with just the part name
            }
            self.logger.info(
                f"{self.contribution.contribution_id}: Adding attachment. URL: {url}"
            )
            response = self.session.post(
                url,
                data=payload,  # The MultipartEncoder is posted as data, don't use files=...!
                files=files,
                # The MultipartEncoder provides the content-type header with the boundary:
                timeout=10,
                verify=False,
            )
            self.logger.debug(f"Opencast | Opencast add attachment requested: {url}")
            return response
        except Exception as ex:
            self.logger.warning(
                f"{self.contribution.contribution_id}: Opencast - Error while sending a request. {url} Error: {ex}"
            )
            return None

    def add_track_requests(self, media_package_text, file_path, flavor):
        api_path = "/ingest/addTrack"
        url = self.opencast_endpoint + api_path
        self.logger.info(
            f"{self.contribution.contribution_id}: Adding track using requests"
        )
        file_name = "camera.mp4" if flavor == "presenter/source" else "slides.mp4"
        try:
            self.logger.debug(url)
            self.logger.debug(file_name)
            self.logger.debug(file_path)
            self.logger.debug(media_package_text)

            encoder = MultipartEncoder(
                fields={
                    "mediaPackage": media_package_text,
                    "flavor": flavor,
                    "file": (
                        file_name,
                        open(file_path, "rb"),
                        "application/octet-stream",
                    ),
                }
            )
            self.logger.info(f"Content-Type: {encoder.content_type}. file: {file_name}")
            self.logger.info(
                f"{self.contribution.contribution_id}: Adding track using requests URL: {url}"
            )
            response = self.session.post(
                url,
                headers={"Content-Type": encoder.content_type},
                data=encoder,
                verify=False,
            )

            self.logger.info(
                f"{self.contribution.contribution_id}: Add track sent to Opencast: {url}"
            )
            return response
        except Exception as ex:
            self.logger.warning(
                f"{self.contribution.contribution_id}: Opencast - Error while sending a request. {url} Error: {ex}"
            )
            return None

    def make_acl_update_request(self):
        """
        Make a request to the Opencast API to update the acls of a contribution

        :return: The API response
        :rtype: requests.Response
        """
        api_path = "/admin-ng/tasks/new"
        url = self.opencast_endpoint + api_path
        self.logger.info(
            f"{self.contribution.contribution_id}: Opencast - Sending update ACLs request: {url}"
        )

        data = {
            "workflow": "cern-media-acl",
            "configuration": {
                self.contribution.opencast_uid: {"exportOtherEvent": "false"}
            },
        }
        self.logger.debug(
            f"{self.contribution.contribution_id}: ACLs request data: {data}"
        )
        try:
            self.logger.info(
                f"{self.contribution.contribution_id}: Opencast - Update ACLs request URL: {url}"
            )
            response = self.session.post(
                url,
                data=f"metadata={json.dumps(data)}",
                timeout=5,
                verify=False,
            )
            self.logger.info(
                f"{self.contribution.contribution_id}: Opencast - Update ACLs request completed: {url}"
            )
            return response
        except Exception as ex:
            self.logger.warning(
                f"{self.contribution.contribution_id}: Opencast - Error while sending a request:\n{url}\n{ex}"
            )
            raise ex

    def get_workflow_status(self):
        """
        Make a request to the Opencast API to retrieve the status of a given workflow

        :return: The API response
        :rtype: requests.Response
        """
        api_path = "/events/"

        if (
            self.contribution.opencast_uid is None
            or self.contribution.opencast_uid == ""
        ):
            return None

        url = self.opencast_endpoint + api_path + self.contribution.opencast_uid
        self.logger.info(
            f"{self.contribution.contribution_id}: Opencast - Sending get workflow status request: {url}"
        )

        try:
            self.logger.info(
                f"{self.contribution.contribution_id}: Opencast - Get workflow status URL: {url}"
            )
            response = self.session.post(url, timeout=5, verify=False)
            self.logger.info(
                f"{self.contribution.contribution_id}: Opencast - get workflow status completed: {url}"
            )
            return response
        except Exception as ex:
            self.logger.warning(
                f"{self.contribution.contribution_id}: Opencast - Error while sending a request:\n{url}\n{ex}"
            )
            raise ex

    def make_transcription_update_request(self):
        """
        Make a request to the Opencast API to request the transcription of a contribution

        :return: The API response
        :rtype: requests.Response
        """
        api_path = "/api/workflows/"
        url = self.opencast_endpoint + api_path
        self.logger.info(
            f"{self.contribution.contribution_id}: Opencast - Sending transcription request: {url}"
        )

        data = {
            "workflow_definition_identifier": "cern-transcription-download",
            "event_identifier": self.contribution.opencast_uid,
            "configuration": json.dumps(
                {
                    "upload_id": self.contribution.transcription_media_id,
                    "year": str(self.contribution.start_date.year),
                    "contribution_id": self.contribution.contribution_id,
                }
            ),
            "withoperations": False,
            "withconfiguration": False,
        }
        self.logger.debug(
            f"{self.contribution.contribution_id}: Transcription request update data: {data}"
        )
        response = self.session.post(
            url,
            data=data,
            timeout=5,
            verify=False,
        )
        self.logger.info(
            f"{self.contribution.contribution_id}: Opencast - Transcription request update completed: {url}"
        )
        return response

    def get_event_publications(self, opencast_uid: str = None):
        """

        :return: The API response
        :rtype: requests.Response
        """
        oc_uid = self.contribution.opencast_uid
        if opencast_uid:
            oc_uid = opencast_uid

        path = f"/api/events/{oc_uid}/publications"
        url = self.opencast_endpoint + path
        self.logger.info(
            f"{self.contribution.contribution_id}: Opencast - Sending event publications request: {url}"
        )

        response = self.session.get(
            url,
            timeout=4,
            verify=False,
        )

        self.logger.info(
            f"{self.contribution.contribution_id}: Opencast - Transcription request completed: {url}"
        )
        return response

    def download_video_from_url(self, video_url: str) -> str:
        local_filename = video_url.split("/")[-1]
        file_extension = local_filename.split(".")[-1]
        file_name = local_filename.split(".")[0]
        new_file_name = f"{self.contribution.contribution_id}_{file_name}_{uuid.uuid4()}.{file_extension}"

        local_path = f"{self.downloads_path}/{new_file_name}"
        with self.session.get(video_url, stream=True, verify=False) as req:
            req.raise_for_status()
            with open(local_path, "wb") as file:
                shutil.copyfileobj(req.raw, file)

        self.logger.debug(
            f"{self.contribution.contribution_id}: Video stored on {local_path}"
        )

        return local_path

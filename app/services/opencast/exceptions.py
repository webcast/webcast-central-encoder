class OpencastMediaPackageError(Exception):
    pass


class OpencastError(Exception):
    pass

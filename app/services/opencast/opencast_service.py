import logging
import xml.etree.ElementTree as ET

from flask import current_app
from requests import ReadTimeout

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.daos.metric import MetricsDAO
from app.extensions import db
from app.models.contribution_workflow.opencast_workflow import SentToOpencastStatus
from app.models.metrics import AvailabeMetricsNames
from app.services.indico.indico_service_2 import IndicoServiceV2
from app.services.opencast.opencast_api import OpencastAPI
from app.services.service_now.service_now_service import ServiceNowService


class OpencastService:
    def __init__(self, contribution, logger=None):
        """

        :param contribution: The contribution involved in the operation
        :type contribution: app.models.events.IndicoEventContribution
        :param logger:
        :type logger:
        """
        self.contribution = contribution
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.service_opencast")

    def create_media_package(self):
        self.logger.info(
            "{}: Creating media package".format(self.contribution.contribution_id)
        )
        api = OpencastAPI(self.contribution, logger=self.logger)
        response = api.create_media_package()
        if response and response.status_code == 200:
            self.logger.debug(response.text)
            root = ET.fromstring(response.text)
            opencast_uid = root.find(".").attrib["id"]
            self.contribution.opencast_uid = opencast_uid
            db.session.commit()

            ServiceNowService(self.contribution, logger=self.logger).add_work_note(
                "Opencast ID set to: {}".format(opencast_uid)
            )

            self.logger.info(
                "{}: Media package created OK".format(self.contribution.contribution_id)
            )
            return response.text
        else:
            status_code = response.status_code if response else "Response is None"
            response_text = response.text if response else "-"
            raise Exception(
                "{}: Status code not the expected on create media package ({} - {})".format(
                    self.contribution.contribution_id, status_code, response_text
                )
            )

    def add_episode_xml(self, media_package_text):
        self.logger.info(
            "{}: Adding episode xml".format(self.contribution.contribution_id)
        )

        episode_xml_path = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/episode.xml"
        )
        self.logger.debug("XML path: {}".format(episode_xml_path))
        api = OpencastAPI(self.contribution, logger=self.logger)
        response = api.add_dc_catalog(
            media_package_text, episode_xml_path, "dublincore/episode"
        )
        if response and response.status_code == 200:
            self.logger.debug(response.text)
            self.logger.info(
                "{}: Adding episode xml OK".format(self.contribution.contribution_id)
            )
            return response.text
        else:
            status_code = response.status_code if response else "Response is None"
            response_text = response.text if response else "-"
            raise Exception(
                "{}: Status code not the expected adding dublincore/episode ({} - {})".format(
                    self.contribution.contribution_id, status_code, response_text
                )
            )

    def add_series_xml(self, media_package_text):
        self.logger.info(
            "{}: Adding series xml".format(self.contribution.contribution_id)
        )

        xml_path = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/series.xml"
        )
        self.logger.debug("XML path: {}".format(xml_path))
        api = OpencastAPI(self.contribution, logger=self.logger)
        response = api.add_dc_catalog(media_package_text, xml_path, "dublincore/series")
        if response and response.status_code == 200:
            self.logger.debug(response.text)
            self.logger.info(
                "{}: Adding series xml OK".format(self.contribution.contribution_id)
            )
            return response.text
        else:
            status_code = response.status_code if response else "Response is None"
            response_text = response.text if response else "-"
            raise Exception(
                "{}: Status code not the expected adding the series.xml ({} - {})".format(
                    self.contribution.contribution_id, status_code, response_text
                )
            )

    def add_indico_xml(self, media_package_text):
        self.logger.info(
            "{}: Adding Indico xml".format(self.contribution.contribution_id)
        )

        xml_path = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/indico.xml"
        )
        self.logger.debug("XML path: {}".format(xml_path))
        api = OpencastAPI(self.contribution, logger=self.logger)
        response = api.add_catalog(media_package_text, xml_path, "indico/episode")
        if response and response.status_code == 200:
            self.logger.debug(response.text)
            self.logger.info(
                "{}: Adding Indico xml OK".format(self.contribution.contribution_id)
            )
            return response.text
        else:
            status_code = response.status_code if response else "Response is None"
            response_text = response.text if response else "-"
            raise Exception(
                "{}: Status code not the expected adding indico.xml ({} - {})".format(
                    self.contribution.contribution_id, status_code, response_text
                )
            )

    def add_acls_xml(self, media_package_text):
        self.logger.info("{}: Adding ACL xml".format(self.contribution.contribution_id))

        xml_path = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/acl.xml"
        )
        self.logger.debug("XML path: {}".format(xml_path))
        api = OpencastAPI(self.contribution, logger=self.logger)
        response = api.add_attachment(
            media_package_text, xml_path, "security/xacml+episode"
        )
        if response and response.status_code == 200:
            self.logger.debug(response.text)
            self.logger.info(
                "{}: Adding ACL xml OK".format(self.contribution.contribution_id)
            )
            return response.text
        else:
            status_code = response.status_code if response else "Response is None"
            response_text = response.text if response else "-"
            raise Exception(
                "{}: Status code not the expected adding acl.xml ({} - {})".format(
                    self.contribution.contribution_id, status_code, response_text
                )
            )

    def add_camera_track(self, media_package_text):
        self.logger.info(
            "{}: Adding camera track".format(self.contribution.contribution_id)
        )

        video_path = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/camera.mp4"
        )
        self.logger.debug("Video path: {}".format(video_path))
        api = OpencastAPI(self.contribution, logger=self.logger)
        response = api.add_track_requests(
            media_package_text, video_path, "presenter/source"
        )

        if response:
            response_text = response.text
            self.logger.debug(response_text)
            if "mediapackage" in response_text:
                self.logger.info(
                    "{}: Adding Camera track OK".format(
                        self.contribution.contribution_id
                    )
                )
                return response_text
            else:
                raise Exception(
                    "{}: mediapackage not in the received response".format(
                        self.contribution.contribution_id
                    )
                )
        else:
            message = "No response from Opencast (add_camera_track)"
            self.logger.error(
                message,
                extra={
                    "contribution_id": self.contribution.id,
                    "tags": {"contribution_id": self.contribution.contribution_id},
                },
            )
            raise Exception(message)

    def add_slides_track(self, media_package_text):
        self.logger.info(
            "{}: Adding slides track".format(self.contribution.contribution_id)
        )

        video_path = (
            current_app.config["DOWNLOADS_FOLDER_PATH"]
            + "/"
            + self.contribution.contribution_id
            + "/slides.mp4"
        )
        self.logger.debug("Video path: {}".format(video_path))
        api = OpencastAPI(self.contribution, logger=self.logger)
        response = api.add_track_requests(
            media_package_text, video_path, "presentation/source"
        )
        if response:
            response_text = response.text
            self.logger.debug(response_text)
            if "mediapackage" in response_text:
                self.logger.info(
                    "{}: Adding slides track OK".format(
                        self.contribution.contribution_id
                    )
                )
                return response_text
            else:
                raise Exception(
                    "{}: mediapackage not in the received response".format(
                        self.contribution.contribution_id
                    )
                )
        else:
            message = "No response from Opencast (add_slides_track)"
            self.logger.warning(
                message, extra={"contribution_id": self.contribution.id}
            )
            raise Exception(message)

    def final_ingest_media_package(
        self, media_package_text, edit_on_opencast, add_transcription
    ):
        self.logger.info(
            "{}: Ingesting media package".format(self.contribution.contribution_id)
        )
        api = OpencastAPI(self.contribution, logger=self.logger)

        publish_to_indico = "true"
        straight_to_publishing = "true"
        if edit_on_opencast == "true":
            publish_to_indico = "false"
            straight_to_publishing = "false"

        response = api.ingest_media_package(
            media_package_text,
            flag_for_cutting=edit_on_opencast,
            publish_to_indico=publish_to_indico,
            straight_to_publishing=straight_to_publishing,
            add_transcription=add_transcription,
        )
        if response and response.status_code == 200:
            self.logger.info(
                "{}: Media package ingest OK".format(self.contribution.contribution_id)
            )
            return response.text
        else:
            status_code = response.status_code if response else "Response is None"
            response_text = response.text if response else "-"
            raise Exception(
                "{}: Status code not the expected on final ingest ({} - {})".format(
                    self.contribution.contribution_id, status_code, response_text
                )
            )

    def ingest_media_package_step_by_step(self, edit_on_opencast, add_transcription):
        indico_contribution_dao = IndicoEventContributionDAO(self.logger)
        indico_contribution_dao.set_sent_to_opencast_status(
            self.contribution.id, SentToOpencastStatus.RUNNING, last_report="Running..."
        )
        try:
            # Create the workflow
            result_mp = self.create_media_package()
            # Add the XML files with metadata
            result_mp = self.add_episode_xml(result_mp)
            result_mp = self.add_series_xml(result_mp)
            result_mp = self.add_indico_xml(result_mp)

            result_mp = self.add_acls_xml(result_mp)
            # Add the video files
            if self.contribution.has_camera_file:
                result_mp = self.add_camera_track(result_mp)
            if self.contribution.has_slides_file:
                result_mp = self.add_slides_track(result_mp)
            # Finalize the Ingest
            result_ingest = self.final_ingest_media_package(
                result_mp, edit_on_opencast, add_transcription
            )
            indico_contribution_dao.set_sent_to_opencast_status(
                self.contribution.id, SentToOpencastStatus.OK, last_report="Finished"
            )
            self.logger.debug(
                f"{self.contribution.contribution_id}: Finished with result ({result_ingest}))"
            )
            return "OK"
        except Exception as e:
            indico_contribution_dao.set_sent_to_opencast_status(
                self.contribution.id,
                SentToOpencastStatus.ERROR,
                last_report=str(e)[:240],
            )
            self.logger.exception(
                "{}: Unable to create the media package. Exception {}".format(
                    self.contribution.contribution_id, e
                )
            )

    def request_acl_update_workflow(self):
        """
        Request the update of ACLs on Opencast by obtaining the up to date information from Indico.
        :param contribution: The involved contribution
        :type contribution: app.models.events.IndicoEventContribution
        :return:
        :rtype:
        """
        acls_requested = False
        if self.contribution.opencast_uid and self.contribution.opencast_uid != "":
            api = OpencastAPI(self.contribution, logger=self.logger)

            try:
                response = api.make_acl_update_request()
            except ReadTimeout:
                response = None

            is_requested = False
            status = "ERROR"

            if not response:
                self.logger.error(
                    "Error on ACLs request workflow. No response from Opencast",
                    extra={
                        "contribution_id": self.contribution.contribution_id,
                        "tags": {"contribution_id": self.contribution.contribution_id},
                    },
                )
                last_report = "No response from Opencast"
            else:
                if response.status_code == 201:
                    is_requested = True
                    status = "OK"
                    last_report = ""
                    acls_requested = True
                    ServiceNowService(
                        self.contribution, logger=self.logger
                    ).add_work_note("ACLs update request sent to Opencast")
                else:
                    message = "{}: Error on ACLs request workflow. {} - {}".format(
                        self.contribution.contribution_id,
                        response.status_code,
                        response.text,
                    )
                    self.logger.warning(message)
                    last_report = "Invalid response from Opencast: {}".format(
                        response.status_code
                    )
            indico_contribution_dao = IndicoEventContributionDAO()
            indico_contribution_dao.set_contribution_acls_status(
                self.contribution.contribution_id, is_requested, status, last_report
            )

        return acls_requested

    def ingest_opencast_media_package(
        self, edit_on_opencast: str, add_transcription: str
    ):
        # indico_contribution_dao.set_sent_to_opencast_status(
        #     self.contribution.id, "REQUESTED", last_report="Request sent"
        # )

        self.logger.debug(
            f"{self.contribution.contribution_id}: Fetching the updated Event data from Indico"
        )
        indico_service = IndicoServiceV2(logger=self.logger)
        result, message = indico_service.get_event(
            self.contribution.indico_event.indico_id,
            audience=self.contribution.indico_event.audience,
            webcast=self.contribution.indico_event.webcast,
        )
        self.logger.debug(
            f"{self.contribution.contribution_id}: "
            f"Finished fetching the updated Event data from Indico ({result}) - {message}"
        )

        service = OpencastService(self.contribution, logger=self.logger)
        ingest_finished = service.ingest_media_package_step_by_step(
            edit_on_opencast, add_transcription
        )
        self.logger.debug(
            f"{self.contribution.contribution_id}: Ingest finished. edit_on_opencast: {edit_on_opencast}"
        )

        MetricsDAO.create(
            AvailabeMetricsNames.SENT_TO_OPENCAST, AvailabeMetricsNames.SENT_TO_OPENCAST
        )
        indico_contribution_dao = IndicoEventContributionDAO(self.logger)
        if ingest_finished and edit_on_opencast == "false":
            indico_contribution_dao.set_opencast_processing_status(
                self.contribution.contribution_id,
                "WAITING",
                last_report="Waiting for Opencast",
            )
            return "OK"
        elif ingest_finished and edit_on_opencast == "true":
            indico_contribution_dao.set_opencast_ready_for_cutting_status(
                self.contribution.contribution_id, "WAITING"
            )
            return "OK"
        return "ERROR"

    def request_workflow_status(self):
        """
        Request the status of an Opencast workflow.

        It uses internally the opencast_uid stored in the current contribution

        :return: The status of the workflow
        :rtype: str|None
        """
        if not self.contribution.opencast_uid or self.contribution.opencast_uid == "":
            return None

        api = OpencastAPI(self.contribution, logger=self.logger)
        try:
            response = api.get_workflow_status()
        except ReadTimeout as error:
            self.logger.error(error)
            response = None

        if not response:
            self.logger.warning(
                "Error on Workflow status request. No response from Opencast",
                extra={
                    "contribution_id": self.contribution.contribution_id,
                    "tags": {"contribution_id": self.contribution.contribution_id},
                },
            )
        else:
            if response.status_code == 200:
                result = response.json()
                return result["status"]
            self.logger.error(
                "Error on ACLs request workflow",
                extra={
                    "contribution_id": self.contribution.contribution_id,
                    "status_code": response.status_code,
                    "response_text": response.text,
                    "tags": {"contribution_id": self.contribution.contribution_id},
                },
            )
            return None

    def get_event_download_url(self):
        if not self.contribution.opencast_uid or self.contribution.opencast_uid == "":
            return None

        api = OpencastAPI(self.contribution, logger=self.logger)
        try:
            response = api.get_event_publications()

            data = response.json()
            self.logger.debug(data)
            medias = data[0]["media"]
            wanted_tags = ["1080p-quality", "720p-quality"]
            wanted_flavor = "presentation/delivery"

            for media in medias:
                if media.get("tags", []):
                    for tag in media["tags"]:
                        if tag in wanted_tags and media["flavor"] == wanted_flavor:
                            return media["url"]

            for media in medias:
                if media.get("tags", []):
                    for tag in media["tags"]:
                        if tag in wanted_tags:
                            return media["url"]

        except ReadTimeout as error:
            self.logger.error(error)
            response = None

    def download_video_from_opencast(self) -> str:
        # Get opencast url

        # Download video from url
        video_url = self.get_event_download_url()

        self.logger.debug(f"Downloading video from {video_url}")

        opencast_api = OpencastAPI(self.contribution, logger=self.logger)
        local_path = opencast_api.download_video_from_url(video_url)

        return local_path

import logging

from lxml import etree

logger = logging.getLogger("webapp.cds_marc21")


class CdsMarc21:
    # MARC 21 info: http://www.loc.gov/marc/bibliographic/
    def __init__(self, filepath_input):
        self.CATEGORIES_INDICO_CDS = {
            "6803": "Restricted_CMS_Talks",
            "6733": "Restricted_ATLAS_Talks",
            "7442": "E-LEARNING",
            "108": "ACAD",
            "72": "ACAD",
            "75": "Colloquia",
            "167": "Colloquia",
            "6725": "CR",
            "9553": "CR",
            "12398": "CR",
            "1249": "OE",
            "92": "OE",
            "2384": "SSW",
            "6254": "SSW",
            "78": "SSW",
            "3221": "SSW",
            "11090": "SSW",
            "2109": "SSW",
            "2387": "SSW",
            "375": "SSW",
            "374": "SSW",
            "8607": "SSW",
            "376": "SSW",
            "104": "SSW",
            "100": "SSW",
            "11616": "SSW",
            "1546": "SSW",
            "7501": "SSW",
            "5397": "SSW",
            "2264": "SSW",
            "1279": "SSW",
            "80": "SSW",
            "81": "SSW",
            "71": "SSW",
            "97": "SL",
            "5885": "SL",
            "14212": "SL",
            "2857": "SL",
            "343": "SL",
            "5886": "TP",
            "425": "CMTE",
            "59": "CMTE",
            "6735": "CMTE",
            "6737": "CMTE",
            "6740": "CMTE",
            "6742": "CMTE",
            "3220": "CMTE",
            "2959": "CMTE",
            "88": "CMTE",
            "2949": "CMTE",
            "86": "CMTE",
            "1996": "CMTE",
            "7968": "CMTE",
            "7894": "CMTE",
            "10303": "CMTE",
            "8642": "CMTE",
            "7127": "CMTE",
            "4282": "CMTE",
            "6364": "CMTE",
            "11583": "CMTE",
            "8749": "CMTE",
            "10129": "CMTE",
            "11786": "CMTE",
            "5105": "CMTE",
            "1183": "CMTE",
            "4926": "CMTE",
            "4951": "CMTE",
            "5851": "CMTE",
            "6499": "CMTE",
            "2980": "CMTE",
            "4709": "CMTE",
            "4706": "CMTE",
            "4710": "CMTE",
            "4708": "CMTE",
            "8510": "CMTE",
            "8486": "CMTE",
            "4705": "CMTE",
            "5999": "CMTE",
            "5490": "CMTE",
            "6007": "CMTE",
            "6255": "CMTE",
            "6392": "CMTE",
            "6517": "CMTE",
            "6666": "CMTE",
            "8004": "CMTE",
            "8005": "CMTE",
            "8514": "CMTE",
            "8663": "CMTE",
            "8665": "CMTE",
            "10431": "CMTE",
            "12821": "CMTE",
            "8584": "CMTE",
            "8995": "CMTE",
            "9460": "CMTE",
            "9558": "CMTE",
            "10509": "CMTE",
            "10624": "CMTE",
            "10962": "CMTE",
            "11935": "CMTE",
            "8511": "CMTE",
            "12242": "CMTE",
            "11849": "CMTE",
            "12851": "CMTE",
            "13008": "CMTE",
            "13734": "CMTE",
            "13812": "CMTE",
            "13813": "CMTE",
            "633": "CMTE",
            "600": "CMTE",
            "601": "CMTE",
            "602": "CMTE",
            "599": "CMTE",
            "598": "CMTE",
            "1605": "CMTE",
            "9136": "CMTE",
            "9137": "CMTE",
            "9659": "CMTE",
            "6427": "CMTE",
            "6428": "CMTE",
            "13584": "CMTE",
            "9008": "CMTE",
            "12858": "CMTE",
            "12863": "CMTE",
            "12862": "CMTE",
            "12909": "CMTE",
            "12864": "CMTE",
            "4121": "CMTE",
            "5557": "CMTE",
            "9626": "CMTE",
            "10320": "CMTE",
            "10682": "CMTE",
            "13967": "CMTE",
            "15185": "CMTE",
            "16051": "CMTE",
            "16052": "CMTE",
            "1871": "CMTE",
            "5199": "CMTE",
            "2314": "CMTE",
            "2003": "CMTE",
            "2469": "CMTE",
            "7090": "CMTE",
            "4920": "CMTE",
            "11319": "CMTE",
            "1792": "CMTE",
            "3589": "CMTE",
            "3530": "CMTE",
            "1801": "CMTE",
            "9843": "CMTE",
            "9882": "CMTE",
            "5216": "CMTE",
            "1882": "CMTE",
            "8520": "CMTE",
            "8519": "CMTE",
            "8905": "CMTE",
            "4810": "CMTE",
            "1791": "CMTE",
            "1878": "CMTE",
            "3819": "CMTE",
            "5070": "CMTE",
            "3588": "CMTE",
            "3649": "CMTE",
            "2892": "CMTE",
            "1919": "CMTE",
            "1795": "CMTE",
            "2519": "CMTE",
            "2942": "CMTE",
            "2630": "CMTE",
            "1857": "CMTE",
            "4349": "CMTE",
            "4375": "CMTE",
            "2000": "CMTE",
            "10963": "CMTE",
            "13053": "CMTE",
            "14106": "CMTE",
            "14285": "CMTE",
            "14850": "CMTE",
            "15515": "CMTE",
            "4341": "CMTE",
            "11067": "CMTE",
            "7383": "CMTE",
            "14579": "CMTE",
            "4007": "CMTE",
            "1799": "CMTE",
            "13097": "CMTE",
            "5567": "CMTE",
            "7349": "CMTE",
            "3051": "CMTE",
            "5267": "CMTE",
            "4340": "CMTE",
            "5130": "CMTE",
            "1803": "CMTE",
            "4339": "CMTE",
            "2055": "CMTE",
            "4570": "CMTE",
            "10120": "CMTE",
            "5699": "CMTE",
            "3222": "CMTE",
            "7263": "CMTE",
            "5487": "CMTE",
            "10114": "CMTE",
            "8571": "CMTE",
            "5575": "CMTE",
            "5210": "CMTE",
            "5211": "CMTE",
            "4679": "CMTE",
            "4772": "CMTE",
            "2844": "CMTE",
            "4379": "CMTE",
            "2533": "CMTE",
            "8241": "CMTE",
            "8616": "CMTE",
            "2742": "CMTE",
            "2606": "CMTE",
            "4692": "CMTE",
            "4690": "CMTE",
            "8542": "CMTE",
            "9525": "CMTE",
            "9112": "CMTE",
            "8976": "CMTE",
            "9776": "CMTE",
            "9777": "CMTE",
            "9778": "CMTE",
            "9779": "CMTE",
            "9780": "CMTE",
            "9781": "CMTE",
            "9872": "CMTE",
            "9873": "CMTE",
            "9874": "CMTE",
            "12481": "CMTE",
            "14161": "CMTE",
            "4936": "CMTE",
            "8934": "CMTE",
            "3709": "CMTE",
            "2422": "CMTE",
            "10285": "CMTE",
            "12893": "CMTE",
            "8155": "CMTE",
            "6806": "CMTE",
            "2378": "CMTE",
            "7192": "CMTE",
            "10483": "CMTE",
            "3698": "CMTE",
            "12043": "CMTE",
            "861": "CMTE",
            "4458": "CMTE",
            "1657": "CMTE",
            "11128": "CMTE",
            "11874": "CMTE",
            "4907": "CMTE",
            "215": "CMTE",
            "199": "CMTE",
            "198": "CMTE",
            "891": "CMTE",
            "194": "CMTE",
            "195": "CMTE",
            "197": "CMTE",
            "196": "CMTE",
            "191": "CMTE",
            "190": "CMTE",
            "193": "CMTE",
            "192": "CMTE",
            "231": "CMTE",
            "42": "CMTE",
            "43": "CMTE",
            "182": "CMTE",
            "406": "CMTE",
            "187": "CMTE",
            "328": "CMTE",
            "549": "CMTE",
            "1393": "CMTE",
            "200": "CMTE",
            "894": "CMTE",
            "674": "CMTE",
            "676": "CMTE",
            "1239": "CMTE",
            "168": "CMTE",
            "169": "CMTE",
            "488": "CMTE",
            "1886": "CMTE",
            "353": "CMTE",
            "475": "CMTE",
            "290": "CMTE",
            "291": "CMTE",
            "1699": "CMTE",
            "11853": "CMTE",
            "5615": "CMTE",
            "6232": "CMTE",
            "5020": "CMTE",
            "3412": "CMTE",
            "6360": "CMTE",
            "14553": "CMTE",
            "3813": "CMTE",
            "3814": "CMTE",
            "3815": "CMTE",
            "3725": "CMTE",
            "6536": "CMTE",
            "7264": "CMTE",
            "8674": "CMTE",
            "12837": "CMTE",
            "14020": "CMTE",
            "585": "CMTE",
            "2561": "CMTE",
            "3013": "CMTE",
            "3032": "CMTE",
            "3932": "CMTE",
            "3000": "CMTE",
            "3003": "CMTE",
            "3077": "CMTE",
            "2562": "CMTE",
            "2563": "CMTE",
            "2910": "CMTE",
            "2371": "CMTE",
            "2392": "CMTE",
            "2368": "CMTE",
            "2369": "CMTE",
            "2370": "CMTE",
            "4853": "CMTE",
            "12240": "CMTE",
            "2423": "CMTE",
            "15640": "CMTE",
            "13323": "CMTE",
            "3924": "CMTE",
            "1001": "CMTE",
            "1000": "CMTE",
            "1123": "CMTE",
            "1168": "CMTE",
            "1915": "CMTE",
            "5808": "CMTE",
            "5809": "CMTE",
            "5810": "CMTE",
            "2556": "CMTE",
            "2558": "CMTE",
            "7822": "CMTE",
            "12673": "CMTE",
            "15698": "CMTE",
            "1224": "CMTE",
            "2815": "CMTE",
            "2816": "CMTE",
            "2817": "CMTE",
            "4820": "CMTE",
            "4821": "CMTE",
            "4822": "CMTE",
            "4823": "CMTE",
            "4824": "CMTE",
            "4825": "CMTE",
            "4826": "CMTE",
            "4827": "CMTE",
            "4828": "CMTE",
            "4829": "CMTE",
            "4830": "CMTE",
            "4831": "CMTE",
            "4832": "CMTE",
            "4834": "CMTE",
            "3424": "CMTE",
            "4197": "CMTE",
            "14115": "CMTE",
            "162": "CMTE",
            "8991": "CMTE",
            "2493": "CMTE",
            "6458": "CMTE",
            "6459": "CMTE",
            "6460": "CMTE",
            "6461": "CMTE",
            "6462": "CMTE",
            "6713": "CMTE",
            "7645": "CMTE",
            "12546": "CMTE",
            "12547": "CMTE",
            "12548": "CMTE",
            "12550": "CMTE",
            "15422": "CMTE",
            "7969": "CMTE",
            "10840": "CMTE",
            "8608": "CMTE",
            "15025": "CMTE",
            "15244": "CMTE",
            "5258": "CMTE",
            "5259": "CMTE",
            "5260": "CMTE",
            "5741": "CMTE",
            "5307": "CMTE",
            "12894": "CMTE",
            "14193": "CMTE",
            "15156": "CMTE",
            "14192": "CMTE",
            "5665": "CMTE",
            "8678": "CMTE",
            "14198": "CMTE",
            "15168": "CMTE",
            "9242": "CMTE",
            "15759": "CMTE",
            "15760": "CMTE",
            "5262": "CMTE",
            "5297": "CMTE",
            "6528": "CMTE",
            "6613": "CMTE",
            "14330": "CMTE",
            "14384": "CMTE",
            "15512": "CMTE",
            "12483": "CMTE",
            "5670": "CMTE",
            "7118": "CMTE",
            "13405": "CMTE",
            "13409": "CMTE",
            "13636": "CMTE",
            "13637": "CMTE",
            "5900": "CMTE",
            "5901": "CMTE",
            "6176": "CMTE",
            "5343": "CMTE",
            "8611": "CMTE",
            "5623": "CMTE",
            "9226": "CMTE",
            "9232": "CMTE",
            "8686": "CMTE",
            "9233": "CMTE",
            "9236": "CMTE",
            "9848": "CMTE",
            "9304": "CMTE",
            "5902": "CMTE",
            "6211": "CMTE",
            "12484": "CMTE",
            "12522": "CMTE",
            "12485": "CMTE",
            "12486": "CMTE",
            "12487": "CMTE",
            "12488": "CMTE",
            "12489": "CMTE",
            "12490": "CMTE",
            "7151": "CMTE",
            "7152": "CMTE",
            "7414": "CMTE",
            "13408": "CMTE",
            "13409": "CMTE",
            "13410": "CMTE",
            "13411": "CMTE",
            "13412": "CMTE",
            "13636": "CMTE",
            "13637": "CMTE",
            "14026": "CMTE",
            "14027": "CMTE",
            "14028": "CMTE",
            "14029": "CMTE",
            "14031": "CMTE",
            "14032": "CMTE",
            "14033": "CMTE",
            "13359": "CMTE",
            "13361": "CMTE",
            "13362": "CMTE",
            "13363": "CMTE",
            "13364": "CMTE",
            "13365": "CMTE",
            "15805": "CMTE",
            "14363": "CMTE",
            "14364": "CMTE",
            "14365": "CMTE",
            "14366": "CMTE",
            "14367": "CMTE",
            "5225": "CMTE",
            "9178": "CMTE",
            "10693": "CMTE",
            "12882": "CMTE",
            "15049": "CMTE",
            "11687": "CMTE",
            "4738": "CMTE",
            "5563": "CMTE",
            "4906": "CMTE",
            "11103": "CMTE",
            "2798": "CMTE",
            "2799": "CMTE",
            "2800": "CMTE",
            "2948": "CMTE",
            "2802": "CMTE",
            "2965": "CMTE",
            "4136": "CMTE",
            "4281": "CMTE",
            "4133": "CMTE",
            "5606": "CMTE",
            "8935": "CMTE",
            "9977": "CMTE",
            "11025": "CMTE",
            "11208": "CMTE",
            "11349": "CMTE",
            "13787": "CMTE",
            "2801": "CMTE",
            "5967": "CMTE",
            "5968": "CMTE",
            "7568": "CMTE",
            "8257": "CMTE",
            "9867": "CMTE",
            "322": "CMTE",
            "4808": "CMTE",
            "4592": "CMTE",
            "4866": "CMTE",
            "4454": "CMTE",
            "5625": "CMTE",
            "4562": "CMTE",
            "5136": "CMTE",
            "4620": "CMTE",
            "8537": "CMTE",
            "9269": "CMTE",
            "10175": "CMTE",
            "11276": "CMTE",
            "11790": "CMTE",
            "1838": "CMTE",
            "1686": "CMTE",
            "5687": "CMTE",
            "7970": "CMTE",
            "7971": "CMTE",
            "7972": "CMTE",
            "10392": "CMTE",
            "11386": "CMTE",
            "15893": "CMTE",
            "13907": "CMTE",
            "10572": "CMTE",
            "3206": "CMTE",
            "13": "CMTE",
            "2617": "CMTE",
            "13657": "CMTE",
            "13658": "CMTE",
            "13720": "CMTE",
            "13721": "CMTE",
            "13722": "CMTE",
            "13723": "CMTE",
            "13724": "CMTE",
            "13725": "CMTE",
            "13726": "CMTE",
            "13727": "CMTE",
            "13728": "CMTE",
            "13729": "CMTE",
            "13730": "CMTE",
            "13731": "CMTE",
            "13732": "CMTE",
            "13733": "CMTE",
            "7773": "CMTE",
            "4999": "CMTE",
            "5000": "CMTE",
            "9527": "CMTE",
            "9772": "CMTE",
            "3660": "CMTE",
            "8529": "CMTE",
            "9210": "CMTE",
            "11137": "CMTE",
            "11384": "CMTE",
            "15788": "CMTE",
            "4507": "CMTE",
            "4887": "CMTE",
            "7797": "CMTE",
            "13956": "CMTE",
            "5733": "CMTE",
            "3458": "CMTE",
            "11944": "CMTE",
            "11945": "CMTE",
            "7695": "CMTE",
            "8189": "CMTE",
            "9537": "CMTE",
            "4440": "CMTE",
            "4018": "CMTE",
            "4250": "CMTE",
            "4437": "CMTE",
            "5835": "CMTE",
            "8883": "CMTE",
            "4094": "CMTE",
            "4096": "CMTE",
            "4411": "CMTE",
            "7762": "CMTE",
            "7763": "CMTE",
            "7849": "CMTE",
            "8577": "CMTE",
            "8578": "CMTE",
            "9989": "CMTE",
            "10520": "CMTE",
            "10557": "CMTE",
            "11608": "CMTE",
            "12389": "CMTE",
            "12896": "CMTE",
            "13058": "CMTE",
            "4574": "CMTE",
            "8048": "CMTE",
            "5873": "CMTE",
            "9572": "CMTE",
            "8972": "CMTE",
            "5583": "CMTE",
            "7869": "CMTE",
            "4573": "CMTE",
            "13751": "CMTE",
            "4425": "CMTE",
            "5142": "CMTE",
            "8521": "CMTE",
            "15528": "CMTE",
            "14351": "CMTE",
            "14352": "CMTE",
            "14353": "CMTE",
            "13021": "CMTE",
            "10764": "CMTE",
            "10765": "CMTE",
            "10766": "CMTE",
            "10767": "CMTE",
            "11769": "CMTE",
            "11872": "CMTE",
            "13546": "CMTE",
            "3072": "CMTE",
            "4417": "CMTE",
            "5646": "CMTE",
            "3070": "CMTE",
            "9873": "CMTE",
            "9874": "CMTE",
            "10051": "CMTE",
            "10932": "CMTE",
            "12158": "CMTE",
            "12765": "CMTE",
            "13086": "CMTE",
            "14593": "CMTE",
            "15135": "CMTE",
            "3069": "CMTE",
            "4361": "CMTE",
            "4626": "CMTE",
            "9424": "CMTE",
            "9353": "CMTE",
            "10972": "CMTE",
            "3068": "CMTE",
            "5132": "CMTE",
            "5133": "CMTE",
            "6534": "CMTE",
            "12202": "CMTE",
            "12626": "CMTE",
            "11188": "CMTE",
            "11189": "CMTE",
            "11609": "CMTE",
            "12091": "CMTE",
            "13413": "CMTE",
            "13487": "CMTE",
            "14408": "CMTE",
            "7771": "CMTE",
            "7772": "CMTE",
            "6625": "CMTE",
            "6626": "CMTE",
            "6628": "CMTE",
            "6389": "CMTE",
            "6627": "CMTE",
            "6629": "CMTE",
            "6631": "CMTE",
            "6630": "CMTE",
            "6388": "CMTE",
            "9965": "CMTE",
            "9375": "CMTE",
            "9528": "CMTE",
            "10684": "CMTE",
            "6042": "CMTE",
            "7846": "CMTE",
            "7940": "CMTE",
            "9611": "CMTE",
            "10190": "CMTE",
            "15718": "CMTE",
            "10103": "CMTE",
            "11069": "CMTE",
            "11961": "CMTE",
            "15329": "CMTE",
            "9845": "CMTE",
            "15719": "CMTE",
            "15721": "CMTE",
            "15722": "CMTE",
            "15723": "CMTE",
            "15724": "CMTE",
            "15725": "CMTE",
            "15726": "CMTE",
            "15727": "CMTE",
            "15728": "CMTE",
            "5875": "CMTE",
            "6043": "CMTE",
            "6124": "CMTE",
            "6126": "CMTE",
            "5330": "CMTE",
            "9746": "CMTE",
            "10228": "CMTE",
            "13654": "CMTE",
            "6125": "CMTE",
            "11286": "CMTE",
            "8149": "CMTE",
            "11134": "CMTE",
            "12788": "CMTE",
            "12790": "CMTE",
            "13034": "CMTE",
            "13035": "CMTE",
            "13036": "CMTE",
            "13038": "CMTE",
            "13071": "CMTE",
            "823": "CMTE",
            "9434": "CMTE",
            "4937": "CMTE",
            "5045": "CMTE",
            "10491": "CMTE",
            "10317": "CMTE",
            "10723": "CMTE",
            "11754": "CMTE",
            "6046": "CMTE",
            "6240": "CMTE",
            "8547": "CMTE",
            "13207": "CMTE",
            "9657": "CMTE",
            "9658": "CMTE",
            "9663": "CMTE",
            "9674": "CMTE",
            "9660": "CMTE",
            "9661": "CMTE",
            "12378": "CMTE",
            "9662": "CMTE",
            "4637": "CMTE",
            "5584": "CMTE",
            "10519": "CMTE",
            "10570": "CMTE",
            "10571": "CMTE",
            "10793": "CMTE",
            "10869": "CMTE",
            "10889": "CMTE",
            "10989": "CMTE",
            "11204": "CMTE",
            "11294": "CMTE",
            "11329": "CMTE",
            "11518": "CMTE",
            "11519": "CMTE",
            "12245": "CMTE",
            "1714": "CMTE",
            "1025": "CMTE",
            "12702": "CMTE",
            "845": "CMTE",
            "2637": "CMTE",
            "4354": "CMTE",
            "650": "CMTE",
            "6866": "CMTE",
            "1449": "CMTE",
            "4400": "CMTE",
            "9769": "CMTE",
            "675": "CMTE",
            "147": "CMTE",
            "1677": "CMTE",
            "661": "CMTE",
            "9261": "CMTE",
            "6890": "CMTE",
            "4475": "CMTE",
            "1540": "CMTE",
            "666": "CMTE",
            "9452": "CMTE",
            "10147": "CMTE",
            "10360": "CMTE",
            "10490": "CMTE",
            "10627": "CMTE",
            "11525": "CMTE",
            "68": "CMTE",
            "7": "CMTE",
            "3772": "CMTE",
            "3754": "CMTE",
            "4401": "CMTE",
            "3770": "CMTE",
            "4318": "CMTE",
            "3812": "CMTE",
            "4393": "CMTE",
            "5793": "CMTE",
            "6241": "CMTE",
            "4392": "CMTE",
            "3768": "CMTE",
            "8128": "CMTE",
            "9733": "CMTE",
            "12951": "CMTE",
            "14661": "CMTE",
            "8156": "CMTE",
            "1327": "CMTE",
            "4372": "CMTE",
            "11155": "CMTE",
            "11683": "CMTE",
            "890": "CMTE",
            "1613": "CMTE",
            "6455": "CMTE",
            "1367": "CMTE",
            "284": "CMTE",
            "258": "CMTE",
            "407": "CMTE",
            "39": "CMTE",
            "229": "CMTE",
            "2439": "CMTE",
            "2691": "CMTE",
            "1228": "CMTE",
            "351": "CMTE",
            "637": "CMTE",
            "654": "CMTE",
            "1703": "CMTE",
            "594": "CMTE",
            "1217": "CMTE",
            "505": "CMTE",
            "10526": "CMTE",
            "6852": "CMTE",
            "6851": "CMTE",
            "146": "CMTE",
            "6849": "CMTE",
            "699": "CMTE",
            "479": "CMTE",
            "7776": "CMTE",
            "40": "CMTE",
            "2623": "CMTE",
            "4565": "CMTE",
            "7957": "CMTE",
            "3006": "CMTE",
            "2512": "CMTE",
            "2331": "CMTE",
            "1612": "CMTE",
            "2726": "CMTE",
            "3402": "CMTE",
            "3404": "CMTE",
            "3405": "CMTE",
            "3406": "CMTE",
            "4150": "CMTE",
            "5706": "CMTE",
            "1952": "CMTE",
            "4971": "CMTE",
            "2896": "CMTE",
            "2788": "CMTE",
            "2689": "CMTE",
            "2687": "CMTE",
            "9411": "CMTE",
            "13647": "CMTE",
            "5366": "CMTE",
            "4870": "CMTE",
            "2848": "CMTE",
            "2849": "CMTE",
            "3803": "CMTE",
            "3696": "CMTE",
            "3290": "CMTE",
            "4463": "CMTE",
            "7245": "CMTE",
            "7246": "CMTE",
            "8009": "CMTE",
            "12311": "CMTE",
            "12374": "CMTE",
            "13769": "CMTE",
            "7234": "CMTE",
            "4119": "CMTE",
            "9531": "CMTE",
            "1874": "CMTE",
            "2100": "CMTE",
            "12236": "CMTE",
            "11600": "CMTE",
            "5177": "CMTE",
            "7730": "CMTE",
            "6019": "CMTE",
            "6020": "CMTE",
            "6668": "CMTE",
            "6017": "CMTE",
            "11602": "CMTE",
            "11603": "CMTE",
            "11605": "CMTE",
            "11606": "CMTE",
            "11607": "CMTE",
            "11788": "CMTE",
            "14804": "CMTE",
            "14985": "CMTE",
            "15177": "CMTE",
            "2782": "CMTE",
            "2783": "CMTE",
            "3391": "CMTE",
            "3340": "CMTE",
            "6012": "CMTE",
            "8942": "CMTE",
            "1229": "CMTE",
            "10643": "CMTE",
            "10066": "CMTE",
            "10163": "CMTE",
            "10172": "CMTE",
            "10497": "CMTE",
            "11559": "CMTE",
            "11658": "CMTE",
            "11779": "CMTE",
            "12072": "CMTE",
            "12530": "CMTE",
            "14146": "CMTE",
            "12762": "CMTE",
            "12763": "CMTE",
            "12764": "CMTE",
            "12766": "CMTE",
            "12831": "CMTE",
            "13898": "CMTE",
            "13958": "CMTE",
            "13962": "CMTE",
            "14574": "CMTE",
            "14635": "CMTE",
            "14797": "CMTE",
            "15489": "CMTE",
            "15983": "CMTE",
            "12646": "CMTE",
            "12647": "CMTE",
            "12792": "CMTE",
            "13145": "CMTE",
            "11840": "CMTE",
            "11941": "CMTE",
            "12426": "CMTE",
            "12597": "CMTE",
            "12696": "CMTE",
            "13899": "CMTE",
            "14577": "CMTE",
            "14739": "CMTE",
            "14763": "CMTE",
            "14912": "CMTE",
            "15691": "CMTE",
            "8602": "CMTE",
            "3041": "CMTE",
            "2525": "CMTE",
            "2847": "CMTE",
            "15293": "CMTE",
            "1545": "CMTE",
            "1040": "CMTE",
            "11205": "CMTE",
            "4384": "CMTE",
            "10462": "CMTE",
            "11702": "CMTE",
            "1902": "CMTE",
            "2037": "CMTE",
            "4957": "CMTE",
            "11524": "CMTE",
            "11710": "CMTE",
            "13890": "CMTE",
            "13892": "CMTE",
            "8430": "CMTE",
            "7939": "CMTE",
            "8814": "CMTE",
            "14436": "CMTE",
            "16055": "CMTE",
            "11111": "CMTE",
            "10930": "CMTE",
            "15132": "CMTE",
            "15943": "CMTE",
            "15294": "CMTE",
            "9609": "CMTE",
            "10790": "CMTE",
            "3893": "CMTE",
            "1877": "CMTE",
            "15189": "CMTE",
            "4434": "CMTE",
            "11877": "CMTE",
            "8512": "CMTE",
            "2921": "CMTE",
            "8274": "CMTE",
            "10407": "CMTE",
            "10293": "CMTE",
            "10588": "CMTE",
            "8641": "CMTE",
            "11566": "CMTE",
            "287": "CMTE",
            "11931": "CMTE",
            "3087": "CMTE",
            "11728": "CMTE",
            "1754": "CMTE",
            "10801": "CMTE",
            "2390": "CMTE",
            "2721": "CMTE",
            "2391": "CMTE",
            "2392": "CMTE",
            "2740": "CMTE",
            "2589": "CMTE",
            "4790": "CMTE",
            "7316": "CMTE",
            "7354": "CMTE",
            "8910": "CMTE",
            "7317": "CMTE",
            "6583": "CMTE",
            "7356": "CMTE",
            "12526": "CMTE",
            "8419": "CMTE",
            "12855": "CMTE",
            "14318": "CMTE",
            "1173": "CMTE",
            "4912": "CMTE",
            "12574": "CMTE",
            "9504": "CMTE",
            "8429": "CMTE",
            "3884": "CMTE",
            "3282": "CMTE",
            "13169": "CMTE",
            "4796": "CMTE",
            "9750": "CMTE",
            "8433": "CMTE",
            "1099": "CMTE",
            "2705": "CMTE",
            "1193": "CMTE",
            "10176": "CMTE",
            "6640": "CMTE",
            "5709": "CMTE",
            "10825": "CMTE",
        }
        self.CATEGORIES_INDICO_CDS_EXPERIMENTS = {
            "6803": "CMS",
            "6734": "LHCb",
            "6733": "ATLAS",
            "6746": "ALICE",
        }

        parser = etree.XMLParser(
            encoding="utf-8",
            ns_clean=True,
            remove_comments=True,
            remove_blank_text=True,
        )
        self.document = etree.parse(filepath_input, parser=parser)

    def generate(
        self,
        is_conference,
        event_id,
        contribution_id,
        languages,
        event_title,
        contribution_title,
        location_name,
        location_room,
        start_date,
        end_date,
        creation_date,
        modification_date,
        category_id,
        category_name,
        categories_list,
        allowed,
        description,
        creator,
        event_speakers,
        contribution_speakers,
        event_url,
        contribution_url,
        organizer,
    ):
        """
        Generate the xml.
        start_date and end_date has to be a datetime object
        """
        self._langs(langs=languages)
        self._details(
            is_conference=is_conference,
            event_title=event_title,
            contribution_title=contribution_title,
            location_name=location_name,
            location_room=location_room,
            start_date=start_date,
            end_date=end_date,
            event_id=event_id,
            contribution_id=contribution_id,
        )
        self._dates(
            start_date=start_date,
            creation_date=creation_date,
            modification_date=modification_date,
        )
        # self._video_format(video_format=video_format)
        self._categories(
            is_conference=is_conference,
            category_id=category_id,
            category_name=category_name,
            categories_list=categories_list,
            start_date=start_date,
        )
        self._protection(allowed=allowed)
        self._copyright(start_date=start_date, categories_list=categories_list)
        self._description(description=description)
        self._speakers(
            creator=creator,
            event_speakers=event_speakers,
            contribution_speakers=contribution_speakers,
            organizer=organizer,
            category_id=category_id,
        )
        self._url(
            is_conference=is_conference,
            event_url=event_url,
            contribution_url=contribution_url,
        )
        self._finalcleanup(["859", "270"])

    def _finalcleanup(self, tags):
        """As per RQF1989555:

        Yes please, remove completely the tags 270 and 859 or have an empty value inside.
        It is ok to have an empty value, not ok to have a missing subfield.
        OK:
        <datafield tag="859" ind1=" " ind2=" ">
        <subfield tag="a"></subfield>
        </datafield>

        Not OK:
        <datafield tag="859" ind1=" " ind2=" ">
        </datafield>

        Let me know if this solves the issue

        Args:
            tags: it's an array of fields to be removed if empty
        """
        for tag in tags:
            if (
                len(self.document.xpath('/record/datafield[@tag="{}"]'.format(tag)))
                == 1
            ):
                if (
                    len(
                        self.document.xpath('/record/datafield[@tag="{}"]'.format(tag))[
                            0
                        ]
                    )
                    == 0
                ):
                    el = self.document.xpath(
                        '/record/datafield[@tag="{}"]'.format(tag)
                    )[0]
                    el.getparent().remove(el)
                    logger.info("datafield {} has been removed".format(tag))

    def _copyright(self, start_date, categories_list):
        """
        Create element with tag 542, subfield code d for copyright and  g for year:
        EXAMPLE
        <subfield code="d">CERN</subfield>
        <subfield code="g">2014</subfield>
        """
        # TAG 542
        el = self.document.xpath('/record/datafield[@tag="542"]')[0]
        new_el = etree.Element("subfield", code="d")
        new_el.text = "CERN"
        el.append(new_el)
        for ct in categories_list:
            if ct in self.CATEGORIES_INDICO_CDS:
                if self.CATEGORIES_INDICO_CDS[ct] == "Restricted_CMS_Talks":
                    new_el = etree.Element("subfield", code="f")
                    new_el.text = (
                        "© 2014 CERN, for the benefit of the CMS Collaboration"
                    )
                    el.append(new_el)
                elif self.CATEGORIES_INDICO_CDS[ct] == "Restricted_ATLAS_Talks":
                    new_el = etree.Element("subfield", code="f")
                    new_el.text = "ATLAS Experiment © 2014 CERN"
                    el.append(new_el)

        new_el = etree.Element("subfield", code="g")
        new_el.text = start_date.strftime("%Y")
        el.append(new_el)

    def _langs(self, langs):
        """
        Create element with tag 041, subfield code a for languages codes:
        EXAMPLE
        <subfield code="a">eng</subfield>
        """
        # TAG 041
        el = self.document.xpath('/record/datafield[@tag="041"]')[0]
        for lang in langs:
            new_el = etree.Element("subfield", code="a")
            new_el.text = lang
            el.append(new_el)

    def _details(
        self,
        is_conference,
        event_title,
        contribution_title,
        location_name,
        location_room,
        start_date,
        end_date,
        event_id,
        contribution_id,
    ):
        """
        Create element with tag 111, subfield codes:
         - a for title
         - c for location name and room
         - 9 for startDate
         - z for endDate
         - g for event ID
        Create element with tag 245, subfield code a
        Create element with tag 970, subfield code a
        EXAMPLE
        <subfield code="a">Is Large-Scale-Structure formation a new probe of
        the Dark Matter interactions with Standard Model particles?</subfield>
        <subfield code="c">CERN - 4-3-006 - TH Conference Room</subfield>
        <subfield code="9">2014-02-19T14:00:00</subfield>
        <subfield code="z">2014-02-19T15:00:00</subfield>
        <subfield code="g">301209</subfield>

        <subfield code="a">Is Large-Scale-Structure formation...</subfield>

        <subfield code="a">INDICO.301209</subfield>
        """
        # TAG 111
        el = self.document.xpath('/record/datafield[@tag="111"]')[0]
        new_el = etree.Element("subfield", code="a")
        new_el.text = event_title
        el.append(new_el)

        new_el = etree.Element("subfield", code="c")
        new_el.text = "%s - %s" % (location_name, location_room)
        el.append(new_el)

        new_el = etree.Element("subfield", code="9")
        # new_el.text = start_date.isoformat()  removed Timezone +HH:MM
        new_el.text = start_date.strftime("%Y-%m-%dT%H:%M:%S")
        el.append(new_el)

        new_el = etree.Element("subfield", code="z")
        # new_el.text = end_date.isoformat()  removed Timezone +HH:MM
        new_el.text = end_date.strftime("%Y-%m-%dT%H:%M:%S")
        el.append(new_el)

        new_el = etree.Element("subfield", code="g")
        new_el.text = event_id
        el.append(new_el)

        # TAG 245
        el = self.document.xpath('/record/datafield[@tag="245"]')[0]
        new_el = etree.Element("subfield", code="a")
        new_el.text = contribution_title
        el.append(new_el)

        # TAG 490
        if not is_conference:
            # if not a conference, titles are different, so insert it
            new_el = etree.Element("datafield", tag="490", ind1=" ", ind2=" ")
            sub_el = etree.Element("subfield", code="a")
            sub_el.text = event_title
            new_el.append(sub_el)

            # insert after the tag LAST 490
            el = self.document.xpath('/record/datafield[@tag="490"]')[-1]
            p = el.getparent()
            p.insert(p.index(el) + 1, new_el)

        # TAG 970
        el = self.document.xpath('/record/datafield[@tag="970"]')[0]
        new_el = etree.Element("subfield", code="a")
        new_el.text = "INDICO.%s" % contribution_id
        el.append(new_el)

    def _dates(self, start_date, creation_date, modification_date):
        """
        Create elements:
         - tag 260, subfield code c for year
         - tag 269, subfield code c for date only
         - tag 518, subfield code d for full date time
         - tag 961, subfield code x for creation date
         - tag 961, subfield code c for modification_date
        EXAMPLE
        <subfield code="c">2014</subfield>
        <subfield code="c">2014-02-19</subfield>
        <subfield code="d">2014-02-19T14:00:00</subfield>
        """
        # TAG 260
        el = self.document.xpath('/record/datafield[@tag="260"]')[0]
        new_el = etree.Element("subfield", code="c")
        new_el.text = start_date.strftime("%Y")
        el.append(new_el)

        # TAG 269
        el = self.document.xpath('/record/datafield[@tag="269"]')[0]
        new_el = etree.Element("subfield", code="c")
        new_el.text = start_date.strftime("%Y-%m-%d")
        el.append(new_el)

        # TAG 518
        el = self.document.xpath('/record/datafield[@tag="518"]')[0]
        new_el = etree.Element("subfield", code="d")
        new_el.text = start_date.strftime("%Y-%m-%dT%H:%M:%S")
        el.append(new_el)

        # TAG 961
        el = self.document.xpath('/record/datafield[@tag="961"]')[0]
        new_el = etree.Element("subfield", code="x")
        new_el.text = creation_date.strftime("%Y-%m-%dT%H:%M:%S")
        el.append(new_el)
        new_el = etree.Element("subfield", code="c")
        new_el.text = modification_date.strftime("%Y-%m-%dT%H:%M:%S")
        el.append(new_el)

    # def _video_format(self, video_format):
    #     """
    #         Create element with tag 300, subfield code b for video_format
    #         EXAMPLE
    #         <subfield code="b">WLAPLectureObject-v0.2</subfield> or
    #         <subfield code="b">720x576 4/3, 25</subfield> or
    #         <subfield code="b">720x576 16/9, 25</subfield>
    #     """
    #     el = self.document.xpath('/record/datafield[@tag="300"]')[0]
    #         new_el = etree.Element('subfield', code='b')
    #         new_el.text = ''
    #         el.append(new_el)

    def _categories(
        self, is_conference, category_id, category_name, categories_list, start_date
    ):
        """
        Create elements:
         - tag 490, subfield code a for category (and with series years)
         - tag 650, ind1 = 1, ind2 = 7, subfield code a for category
         - tag 693, subfield code e for cds categories experiments
         - tag 980, subfield code a for cds categories
         - tag 980, subfield code b for cds categories
        EXAMPLE
        <subfield code="a">Academic Training Lecture Regular Programme</subfield>
        <subfield code="v">2013-2014</subfield>

        <datafield tag="693" ind1=" " ind2=" ">
            <subfield code="e">ATLAS</subfield>

        <datafield tag="980" ind1=" " ind2=" ">
            <subfield code="a">Restricted_ATLAS_Talks</subfield>

        <datafield tag="980" ind1=" " ind2=" ">
            <subfield code="b">TALK</subfield>
        """
        # TAG 084
        el = self.document.xpath('/record/datafield[@tag="084"]')[0]
        new_el = etree.Element("subfield", code="a")
        new_el.text = str(category_id)
        el.append(new_el)

        # TAG 490
        el = self.document.xpath('/record/datafield[@tag="490"]')[0]
        new_el = etree.Element("subfield", code="a")
        new_el.text = category_name
        el.append(new_el)

        if is_conference:
            if category_name.startswith("Academic Training Lecture"):
                # add year field with prev_year-this_year if first 8 months of the year, else this_year-next_year
                new_el = etree.Element("subfield", code="v")
                if int(start_date.strftime("%m")) in range(1, 8):
                    new_el.text = "%s-%s" % (
                        int(start_date.strftime("%Y")) - 1,
                        start_date.strftime("%Y"),
                    )
                else:
                    new_el.text = "%s-%s" % (
                        start_date.strftime("%Y"),
                        int(start_date.strftime("%Y")) + 1,
                    )
                el.append(new_el)

            if category_name.startswith("Summer Student Lecture"):
                new_el = etree.Element("subfield", code="v")
                new_el.text = start_date.strftime("%Y")
                el.append(new_el)

        # TAG 650
        el = self.document.xpath('/record/datafield[@tag="650"]')[0]
        new_el = etree.Element("subfield", code="a")
        new_el.text = category_name
        el.append(new_el)

        # TAG 693 subfield e
        # check if the category id (or parents) of this contribution is inside conf.CATEGORIES_INDICO_CDS_EXPERIMENTS
        for ct in categories_list:
            if ct in self.CATEGORIES_INDICO_CDS_EXPERIMENTS:
                new_el = etree.Element("datafield", tag="693", ind1=" ", ind2=" ")
                sub_el = etree.Element("subfield", code="e")
                sub_el.text = self.CATEGORIES_INDICO_CDS_EXPERIMENTS[ct]
                new_el.append(sub_el)

                # insert after the tag LAST 690
                el = self.document.xpath('/record/datafield[@tag="690"]')[-1]
                p = el.getparent()
                p.insert(p.index(el) + 1, new_el)

                break

        # TAG 980 subfield a
        # check if the category id (or parents) of this contribution is inside conf.CATEGORIES_INDICO_CDS
        for ct in reversed(categories_list):
            if str(ct) in self.CATEGORIES_INDICO_CDS:
                el = self.document.xpath('/record/datafield[@tag="980"]')[0]
                new_el = etree.Element("subfield", code="b")
                new_el.text = self.CATEGORIES_INDICO_CDS[str(ct)]
                el.append(new_el)
                break

        # if it was not as CATEGORIES_INDICO_CDS let's add a default
        el = self.document.xpath('/record/datafield[@tag="980"]')[0]
        if el.find('./subfield[@code="b"]') is None:
            # print(el.find('./subfield[@code="b"]'))
            new_el = etree.Element("subfield", code="b")
            new_el.text = "TALK"
            el.append(new_el)

        # TAG 980 subfield b
        # new_el = etree.Element('datafield', tag='980', ind1=' ', ind2=' ')
        # sub_el = etree.Element('subfield', code='b')

        # if is_conference and category_name.startswith('Academic Training Lecture'):
        #    sub_el.text = "ACAD"
        # elif is_conference and category_name.startswith('Summer Student Lecture'):
        #    sub_el.text = "SSW"
        # else:
        #    sub_el.text = "TALK"

        # new_el.append(sub_el)
        # insert after the tag LAST 980
        # el = self.document.xpath('/record/datafield[@tag="980"]')[-1]
        # p = el.getparent()
        # p.insert(p.index(el) + 1, new_el)

    def _protection(self, allowed):
        """
        Create element with tag 506, subfield code (many) for protection
        EXAMPLE
        <datafield tag="506" ind1="1" ind2=" ">
            <subfield code="a">Restricted</subfield>
        """
        # TAG 506
        if allowed:
            if allowed["users"]:
                new_el = etree.Element("datafield", tag="506", ind1="1", ind2=" ")

                sub_el = etree.Element("subfield", code="a")
                sub_el.text = "Restricted"
                new_el.append(sub_el)

                for e in allowed["users"]:
                    sub_el = etree.Element("subfield", code="d")
                    sub_el.text = e
                    new_el.append(sub_el)

                sub_el = etree.Element("subfield", code="f")
                sub_el.text = "email"
                new_el.append(sub_el)

                sub_el = etree.Element("subfield", code="2")
                sub_el.text = "CDS Invenio"
                new_el.append(sub_el)

                sub_el = etree.Element("subfield", code="5")
                sub_el.text = "SzGeCERN"
                new_el.append(sub_el)

                # insert after the LAST tag 490
                el = self.document.xpath('/record/datafield[@tag="490"]')[-1]
                p = el.getparent()
                p.insert(p.index(el) + 1, new_el)

            if allowed["groups"]:
                new_el = etree.Element("datafield", tag="506", ind1="1", ind2=" ")
                sub_el = etree.Element("subfield", code="a")
                sub_el.text = "Restricted"
                new_el.append(sub_el)

                for g in allowed["groups"]:
                    sub_el = etree.Element("subfield", code="d")
                    # egroup_name [CERN] needed by CDS to know it is a CERN e-group
                    sub_el.text = "%s [CERN]" % g
                    new_el.append(sub_el)

                sub_el = etree.Element("subfield", code="f")
                sub_el.text = "group"
                new_el.append(sub_el)

                sub_el = etree.Element("subfield", code="2")
                sub_el.text = "CDS Invenio"
                new_el.append(sub_el)

                sub_el = etree.Element("subfield", code="5")
                sub_el.text = "SzGeCERN"
                new_el.append(sub_el)

                # insert after the tag LAST 490
                el = self.document.xpath('/record/datafield[@tag="490"]')[-1]
                p = el.getparent()
                p.insert(p.index(el) + 1, new_el)

    def _description(self, description):
        """
        Create element with tag 520, subfield code a for description
        EXAMPLE
        <datafield tag="520" ind1=" " ind2=" ">
            <subfield code="a">&lt;!--HTML--&gt;In the last few decades, there have ...</subfield>
        """
        if description:
            # TAG 520
            new_el = etree.Element("datafield", tag="520", ind1=" ", ind2=" ")

            sub_el = etree.Element("subfield", code="a")
            # <!--HTML--> needed by CDS to renders as HTML
            sub_el.text = (
                "<!--HTML-->%s"
                % description.rstrip().encode("utf-8", "xmlcharrefreplace").decode()
                if isinstance(description, str)
                else description.rstrip()
            )
            new_el.append(sub_el)

            # insert after the LAST tag 518
            el = self.document.xpath('/record/datafield[@tag="518"]')[-1]
            p = el.getparent()
            p.insert(p.index(el) + 1, new_el)

    def _speakers(
        self, creator, event_speakers, contribution_speakers, organizer, category_id
    ):
        """
        Create elements with tags:
         - 700, subfield code a, e, u for contribution_speakers
         - 859, subfield code f for creator email or organizer for category 72
         - 906, subfield code p, u for event_speakers
        EXAMPLE
        <datafield tag="700" ind1="1" ind2=" ">
            <subfield code="a">Boehm, Celine</subfield>
            <subfield code="e">speaker</subfield>
            <subfield code="u">Durham University</subfield>
        """
        # TAG 700
        if contribution_speakers:
            for s in contribution_speakers:
                new_el = etree.Element("datafield", tag="700", ind1=" ", ind2=" ")

                sub_el = etree.Element("subfield", code="a")
                sub_el.text = s["last_name"] + ", " + s["first_name"]
                new_el.append(sub_el)

                sub_el = etree.Element("subfield", code="e")
                sub_el.text = "speaker"
                new_el.append(sub_el)

                if s["affiliation"]:
                    sub_el = etree.Element("subfield", code="u")
                    sub_el.text = s["affiliation"]
                    new_el.append(sub_el)

                # insert after the LAST tag 693, else after LAST tag 690
                el = self.document.xpath('/record/datafield[@tag="693"]')
                if el:
                    el = el[-1]
                else:
                    el = self.document.xpath('/record/datafield[@tag="690"]')[-1]
                p = el.getparent()
                p.insert(p.index(el) + 1, new_el)

        if category_id == 72:
            el = self.document.xpath('/record/datafield[@tag="270"]')[0]
            new_el = etree.Element("subfield", code="p")
            if organizer.find("/"):
                new_el.text = organizer.split("/")[0].rstrip()
            else:
                new_el.text = organizer
            el.append(new_el)
        else:
            el = self.document.xpath('/record/datafield[@tag="859"]')[0]
            new_el = etree.Element("subfield", code="f")
            new_el.text = creator["email"]
            el.append(new_el)

        # TAG 906
        if event_speakers:
            for s in event_speakers:
                new_el = etree.Element("datafield", tag="906", ind1=" ", ind2=" ")

                sub_el = etree.Element("subfield", code="p")
                sub_el.text = s["last_name"] + ", " + s["first_name"]
                new_el.append(sub_el)

                if s["affiliation"]:
                    sub_el = etree.Element("subfield", code="u")
                    sub_el.text = s["affiliation"]
                    new_el.append(sub_el)

                # insert after the LAST tag 859
                el = self.document.xpath('/record/datafield[@tag="859"]')[-1]
                p = el.getparent()
                p.insert(p.index(el) + 1, new_el)

    def _url(self, is_conference, event_url, contribution_url):
        """
        Create element with tag 856 ind1 = 4, subfield code u e y for url
        EXAMPLE
        <subfield code="u">http://indico.cern.ch/conferenceDisplay.py?confId=301209</subfield>
        <subfield code="y">Event details</subfield>
        """
        # TAG 856
        if not is_conference:
            new_el = etree.Element("datafield", tag="856", ind1="4", ind2=" ")

            sub_el = etree.Element("subfield", code="u")
            sub_el.text = contribution_url
            new_el.append(sub_el)

            sub_el = etree.Element("subfield", code="y")
            sub_el.text = "Talk details"
            new_el.append(sub_el)

            # insert BEFORE the FIRST tag 859
            el = self.document.xpath('/record/datafield[@tag="859"]')[0]
            p = el.getparent()
            p.insert(p.index(el), new_el)

        new_el = etree.Element("datafield", tag="856", ind1="4", ind2=" ")

        sub_el = etree.Element("subfield", code="u")
        sub_el.text = event_url
        new_el.append(sub_el)

        sub_el = etree.Element("subfield", code="y")
        sub_el.text = "Event details"
        new_el.append(sub_el)

        # insert BEFORE the FIRST tag 859
        el = self.document.xpath('/record/datafield[@tag="859"]')[0]
        p = el.getparent()
        p.insert(p.index(el), new_el)

    def to_xml(self, contribution_id):
        # destination_file = os.path.join(self.OUTPUT_FOLDER, contribution_id + '_2.xml')
        # self.document.write(destination_file, xml_declaration=True,
        #                     encoding='utf-8', pretty_print=True)
        xml_result = etree.tostring(
            self.document, xml_declaration=True, encoding="utf-8", pretty_print=True
        )
        return xml_result

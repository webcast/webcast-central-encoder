import copy
import hashlib
import hmac
import logging
import re
import time
from datetime import datetime
from urllib.parse import urlencode

import requests
from flask import current_app

from app.services.cds.cds_marc21 import CdsMarc21
from app.services.indico.indico_api import IndicoAPI
from app.services.indico.indico_to_cds_api import IndicoToCdsAPI

# CDS guide: http://invenio-demo.cern.ch/help/admin/bibupload-admin-guide


class CdsAPIException(Exception):
    pass


class CdsAPI:
    def __init__(self, logger=None):
        self.cds_url = current_app.config["CDS_URL"]
        self.api_key = current_app.config["CDS_API_KEY"]
        self.api_secret = current_app.config["CDS_API_SECRET"]

        self.callback_base_url = current_app.config["CDS_CALLBACK_BASE_URL"]

        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("webapp.cds_api")

    def _build_request(self, path, params):
        """
        Build a request to CDS API, generating the signature from the parameters and the path
        :param path: The path of the request. Must start with "/"
        :type path: str
        :param params: A dictionary with the additional parameters of the request
        :type params: dict
        :return: A tuple with the request path with params and the params encoded
        :rtype: tuple[str, str]
        """
        items = list(params.items()) if hasattr(params, "items") else list(params)
        items.append(("apikey", self.api_key))
        items.append(("timestamp", str(int(time.time()))))
        items = sorted(items, key=lambda x: x[0].lower())
        url = "%s?%s" % (path, urlencode(items))
        url = str.encode(url)
        secret_key = str.encode(self.api_secret)
        signature = hmac.new(secret_key, url, hashlib.sha1).hexdigest()
        items.append(("signature", signature))
        if not items:
            return path
        return "%s?%s" % (path, urlencode(items)), urlencode(items)

    def request_create_cds_record(self, contribution_id, send_request=True):
        """
        Create a CDS Record request by making a request to the CDS API.

        :param contribution_id: Indico contribution ID
        :type contribution_id: str
        :return:
        :rtype:
        """

        is_event = re.match(r"(\w*\d+)$", contribution_id)
        is_session = re.match(r"(\w*\d+)s(\d+)$", contribution_id)
        is_session_slot = re.match(r"(\w*\d+)s(\d+)l(\d+)$", contribution_id)
        is_contribution = re.match(r"(\w*\d+)c(\d+|s\d+t\d+)$", contribution_id)
        is_subcontribution = re.match(
            r"(\w*\d+)c(\d+|s\d+t\d+)sc(\d+)$", contribution_id
        )

        #  fetch contribution
        conference_only = False
        selected = {}
        # SUBCONTRIB ####################################################################
        if is_subcontribution:
            event = IndicoToCdsAPI(logger=self.logger).fetch_event_for_cds(
                is_subcontribution.group(1)
            )

            for s in event["session_slots"]:
                for c in s["contributions"]:
                    for sc in c["subcontributions"]:
                        if sc["long_id"] == contribution_id:
                            selected = copy.deepcopy(sc)
                            break
            if not selected:
                for c in event["contributions"]:
                    for sc in c["subcontributions"]:
                        if sc["long_id"] == contribution_id:
                            selected = copy.deepcopy(sc)
                            break

            if not selected:
                raise Exception(
                    "No subcontribution found for %s" % is_subcontribution.group(1)
                )

        # CONTRIB ####################################################################
        elif is_contribution:
            event = IndicoToCdsAPI().fetch_event_for_cds(is_contribution.group(1))
            try:
                for s in event["session_slots"]:
                    for c in s["contributions"]:
                        if c["long_id"] == contribution_id:
                            # found, remove any sub contribution and copy it
                            del c["subcontributions"]
                            selected = copy.deepcopy(c)
                            break
            except KeyError as e:
                self.logger.debug("No session slots: {}".format(e))
            if not selected:
                try:
                    for c in event["contributions"]:
                        if c["long_id"] == contribution_id:
                            # found, remove any sub contribution and copy it
                            del c["subcontributions"]
                            selected = copy.deepcopy(c)
                            break
                except KeyError as e:
                    self.logger.debug("No contributions: {}".format(e))

            if not selected:
                raise Exception(
                    "No contribution found for %s. Is the contribution scheduled in the Indico event?"
                    % is_contribution.group(1)
                )

        # SESSION SLOTS ####################################################################
        elif is_session or is_session_slot:
            if is_session:
                # fetch the event
                event = IndicoToCdsAPI().fetch_event_for_cds(is_session.group(1))

                # loop inside sessions and find the one to select
                for s in event["session_slots"]:
                    if s["deprecated_long_id"] == contribution_id:
                        # found, remove any contribution and copy it
                        del s["contributions"]
                        selected = copy.deepcopy(s)
                        break
                else:
                    # no break statement
                    raise Exception("No session found for %s" % is_session.group(1))
            else:  # session slot
                # fetch the event
                event = IndicoToCdsAPI().fetch_event_for_cds(is_session_slot.group(1))

                for s in event["session_slots"]:
                    if s["long_id"] == contribution_id:
                        # found, remove any contribution and copy it
                        del s["contributions"]
                        selected = copy.deepcopy(s)
                        break
                else:
                    # no break statement
                    raise Exception(
                        "No session slot found for %s" % is_session_slot.group(1)
                    )

        # EVENT ####################################################################
        elif is_event:
            conference_only = True
            event = IndicoToCdsAPI().fetch_event_for_cds(contribution_id)
            # remove contributions and session_slots, not needed and less to copy
            del event["session_slots"]
            del event["contributions"]
            selected = copy.deepcopy(event)
        else:
            raise Exception("Indico event id %s not recognized" % contribution_id)

        # remove contributions and session_slots, not needed and less to copy
        if "session_slots" in event:
            del event["session_slots"]
        if "contributions" in event:
            del event["contributions"]

        # get the categories tree from Indico
        categories_list = IndicoAPI().get_categories_tree(event["category_id"])

        # get the origin xml
        marc21 = CdsMarc21(filepath_input=current_app.config["CDS_MARC21_ORIGIN"])
        # generate the xml
        marc21.generate(
            is_conference=conference_only,
            event_id=contribution_id,
            contribution_id=selected["long_id"],
            languages=["eng"],
            event_title=event["title"],
            contribution_title=selected["title"],
            location_name=event["location"],
            location_room=event["room_fullname"],
            start_date=selected["start_datetime"],
            end_date=selected["end_datetime"],
            creation_date=event["creation_datetime"],
            modification_date=datetime.now(),
            category_id=event["category_id"],
            category_name=event["category_name"],
            categories_list=categories_list,
            allowed=selected["allowed"],
            description=selected["description"],
            creator=event["requestor"],
            event_speakers=event["speakers"],
            contribution_speakers=selected["speakers"],
            event_url=event["url"],
            contribution_url=selected["url"],
            organizer=event["organizer"],
        )
        # write the file
        xml_file_content = marc21.to_xml(contribution_id)

        path = "/batchuploader/robotupload/insertorreplace"
        callback_path = "/api/v1/cds/callback?contribution_id={}".format(
            contribution_id
        )
        params = {
            "special_treatment": "oracle",
            "nonce": self.generate_nonce(contribution_id),
            "callback_url": self.callback_base_url + callback_path,
        }

        headers = {
            "Content-Type": "application/marcxml+xml",
            "User-Agent": "invenio_webupload",
        }

        generated_path, _ = self._build_request(path, params)
        url = self.cds_url + generated_path
        self.logger.debug("Making a request to CDS API: {}".format(url))
        self.logger.debug("CDS API request data: {}".format(xml_file_content))
        if send_request:
            response = requests.get(
                url, headers=headers, data=xml_file_content, verify=False, timeout=10
            )
        else:
            response = xml_file_content
        return response

    def generate_nonce(self, contribution_id):
        return hashlib.sha1(contribution_id.encode("utf-8")).hexdigest()

    def notify_cds_subformats_ready(self, info_xml_url):
        """
        Notify CDS that the subformats are generated.

        Please be aware that we are using the same API KEY and SECRET to make all the queries to CDS,
        but this may change in the future since these endpoints are independent.

        :param info_xml_url:
        :type info_xml_url: str
        :return:
        :rtype:
        """
        path = "/tools/mediaarchive.py/add_slave_url"

        params = {"url_xml": info_xml_url}

        generated_path, _ = self._build_request(path, params)
        url = self.cds_url + generated_path
        self.logger.debug(f"Making a request to CDS API: {url}")

        response = requests.get(url, verify=False, timeout=10)
        return response

    def notify_cds_subformats_ready_datajson(self, data_json_url, contribution_id):
        path = "/tools/mediaarchive.py/update_subformats"

        params = {"url_json": data_json_url, "indico_contribution_id": contribution_id}

        generated_path, _ = self._build_request(path, params)
        url = self.cds_url + generated_path
        self.logger.debug(f"Making a request to CDS API: {url}")

        response = requests.get(url, verify=False, timeout=10)
        return response

    def get_metadata(self, master_path: str = None, indico_contribution_id: str = None):
        """
        Checks if the CDS record for a given contribution is available or not
        :param master_path: The master path of the files for the contribution
        :type master_path: str
        :return: The xml metadata returned by CDS or False
        :rtype: str|bool
        """
        path = "/tools/mediaarchive.py/get_media_metadata"  # get info if the lecture can be published or not
        params = {}
        if master_path:
            params = {"master_path": master_path}
        elif indico_contribution_id:
            params = {"indico_contribution_id": indico_contribution_id}
        generated_path, _ = self._build_request(path, params)
        url = self.cds_url + generated_path
        try:
            self.logger.info(f"Getting metadata from CDS - url: {url}")
            response = requests.get(url, verify=False)
            xml_metadata = response.text
            if response.status_code != 200:
                message = (
                    f"Unable to fetch metadata from CDS. "
                    f"Response is not 200: {response.text if response else 'No response'}"
                )
                self.logger.error(message)
                raise CdsAPIException(message)

            if "mediaarchive_status_error" in xml_metadata:
                self.logger.warning(
                    f"Unable to fetch metadata from CDS. Response is: {xml_metadata}"
                )

            return (
                xml_metadata
                if "mediaarchive_status_error" not in xml_metadata
                else False
            )
        except CdsAPIException as ex:
            self.logger.exception(
                f"Error while using CDS webservice (get_media_metadata): {str(ex)}"
            )

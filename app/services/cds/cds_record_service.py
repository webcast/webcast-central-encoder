import datetime
import logging

import pytz
from flask import current_app

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.helpers.http import is_http_file_accesible
from app.models.events import PostprocessingType
from app.services.cds.cds_api import CdsAPI
from app.services.service_now.service_now_service import ServiceNowService


class CdsRecordServiceException(Exception):
    pass


class CdsRecordService:
    def __init__(self, contribution, logger=None):
        """

        :param contribution:
        :type contribution: app.models.events.IndicoEventContribution
        :param logger:
        :type logger: logging.Logger
        """
        self.contribution = contribution
        self.weblectures_base_url = current_app.config[
            "CDS_MEDIAARCHIVE_WEBLECTURES_BASE_URL"
        ]
        self.conferences_base_url = current_app.config[
            "CDS_MEDIAARCHIVE_CONFERENCES_BASE_URL"
        ]
        self.opencast_datajson = current_app.config["OPENCAST_DATAJSON"]

        self.weblectures_dfs_folder = current_app.config["DIR_WEBLECTURES_DFS_FOLDER"]
        self.conferences_dfs_folder = current_app.config["DIR_PLAIN_VIDEOS_DFS_FOLDER"]

        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("webapp.cds_service")

    def request_create_cds_record(self):
        """
        Request the creation of a CDS record for an Indico Event Contribution

        :return: (True|False) depending on the status code returned by the CDS API and a message (str)
        :rtype: (bool|str)
        """
        is_requested = False
        status = "ERROR"
        message = "No response from CDS"
        self.logger.debug(
            f"{self.contribution.contribution_id}: Calling create_cds_records"
        )
        response = CdsAPI(logger=self.logger).request_create_cds_record(
            self.contribution.contribution_id
        )

        if response:
            self.logger.debug(f"CDS response: {response.text}")
            if response.status_code == 200:
                status = "OK"
                is_requested = True
                message = "CDS record creation requested"
                ServiceNowService(self.contribution, logger=self.logger).add_work_note(
                    "CDS record creation requested"
                )
                self.logger.info(
                    f"{self.contribution.contribution_id}: Successful response from "
                    f"CDS requesting CDS record creation"
                )
            else:
                message = (
                    f"Unexpected response from CDS. Status code: {response.status_code}"
                )
                self.logger.warning(
                    f"{self.contribution.contribution_id}: Unexpected response from CDS "
                    f"requesting CDS record creation. Status code: {response.status_code}"
                )
                is_requested = False
        else:
            self.logger.warning(
                f"{self.contribution.contribution_id}: No response from CDS requesting "
                f"CDS record creation"
            )
        indico_contribution_dao = IndicoEventContributionDAO()
        indico_contribution_dao.set_cds_record_status(
            self.contribution.contribution_id, is_requested, status, message
        )

        return is_requested, message

    def request_cds_subformats_ready_datajson(self):
        """
        To allow CDS integration, this method should build a call to CDS with CDSRecord and
        data.json location as params.
        """
        is_requested = False
        status = "ERROR"
        is_cds_record_ready = False
        is_data_json_available = False
        self.logger.debug(
            f"{self.contribution.contribution_id}: Calling cds add_slave_url API (Subformats) datajson"
        )

        url_datajson = self.build_contribution_data_json_url()

        if self.is_cds_record_ready(ceph=True):
            is_cds_record_ready = True
        else:
            message = (
                f"{self.contribution.contribution_id}: CDS record is not ready yet "
                f"(CDS get_media_metadata not working as expected) for data.json: {url_datajson}"
            )

        if is_http_file_accesible(url_datajson):
            is_data_json_available = True
        else:
            message = f"{self.contribution.contribution_id}: CDS data.json not available on URL yet: {url_datajson}"

        message = "No message"
        if is_cds_record_ready and is_data_json_available:
            response = CdsAPI(logger=self.logger).notify_cds_subformats_ready_datajson(
                url_datajson, self.contribution.contribution_id
            )
            if response:
                self.logger.debug(
                    "CDS response: {}".format(response.text.encode("ascii", "ignore"))
                )
                cds_error_text = "mediaarchive_status_error"
                if response.status_code == 200 and cds_error_text not in response.text:
                    status = "OK"
                    is_requested = True
                    message = "CDS data.json submitted"
                    ServiceNowService(self.contribution).add_work_note(
                        message + " " + url_datajson
                    )
                    self.logger.info(
                        f"{self.contribution.contribution_id}: Successful response from CDS "
                        "submitting data.json location"
                    )
                else:  # CDS record not ready
                    message = (
                        f"{self.contribution.contribution_id}: Unexpected response "
                        "from CDS data.json submit. "
                        f"Status code: {response.status_code} mediaarchive_status_error: "
                        f"{cds_error_text in response.text}"
                    )
                    self.logger.warning(message)
            else:  # No response
                message = (
                    f"{self.contribution.contribution_id}: Unsuccessful response from "
                    f"CDS subformats API: {response}"
                )

                self.logger.error(message)

        else:
            self.logger.warning(message)
            message = (
                f"{self.contribution.contribution_id}: Unable to request subformats. "
                f"CDS record ready: {is_cds_record_ready}. data.json available: {is_data_json_available}"
            )
        indico_contribution_dao = IndicoEventContributionDAO(self.logger)
        indico_contribution_dao.set_cds_subformats_request_status(
            self.contribution.contribution_id, is_requested, status, message
        )

        return is_requested, message

    def request_cds_subformats_ready(self):
        """
        Send a request to CDS to notify the subformats are ready and the info.xml is in place.
        This is the publishing's last step to make all the files available for the users.
        :return:
        :rtype:
        """
        is_requested = False
        status = "ERROR"
        is_cds_record_ready = False
        is_info_xml_available = False
        message = "No message"

        self.logger.debug(
            f"{self.contribution.contribution_id}: Calling cds add_slave_url API (Subformats)"
        )

        url_xml = self.build_contribution_info_xml_url()

        if self.is_cds_record_ready():
            is_cds_record_ready = True
        else:
            message = (
                f"{self.contribution.contribution_id}: CDS record is not ready yet "
                f"(CDS get_media_metadata not working as expected) for info.xml: {url_xml}"
            )

        if is_http_file_accesible(url_xml):
            is_info_xml_available = True
        else:
            message = f"{self.contribution.contribution_id}: CDS info.xml not available on URL yet: {url_xml}"
        if is_cds_record_ready and is_info_xml_available:
            response = CdsAPI(logger=self.logger).notify_cds_subformats_ready(url_xml)
            if response:
                self.logger.debug(f"CDS response: {response.text}")
                cds_error_text = "mediaarchive_status_error"
                if response.status_code == 200 and cds_error_text not in response.text:
                    status = "OK"
                    is_requested = True
                    message = "CDS info.xml submitted"
                    ServiceNowService(
                        self.contribution, logger=self.logger
                    ).add_work_note(message + " " + url_xml)
                    self.logger.info(
                        f"{self.contribution.contribution_id}: Successful response from CDS "
                        "submitting info.xml location"
                    )
                else:  # CDS record not ready
                    message = (
                        f"{self.contribution.contribution_id}: Unexpected response "
                        "from CDS info.xml submit. "
                        f"Status code: {response.status_code} mediaarchive_status_error: "
                        f"{cds_error_text in response.text}"
                    )
                    self.logger.warning(message)
            else:  # No response
                message = (
                    f"{self.contribution.contribution_id}: Unsuccessful response from "
                    f"CDS subformats API: {response}"
                )

                self.logger.error(message)

        else:
            self.logger.warning(message)
        indico_contribution_dao = IndicoEventContributionDAO(self.logger)
        indico_contribution_dao.set_cds_subformats_request_status(
            self.contribution.contribution_id, is_requested, status, message
        )

        return is_requested, message

    def build_contribution_info_xml_url(self):
        """
        Generates the info.xml mediarchive URL for the contribution
        Example: https://mediaarchive.cern.ch/MediaArchive/Video/Public/Conferences/2021/1019078c132/info.xml

        :return: The URL of the info.xml file for the contribution
        :rtype: str
        """
        # ['web_lecture', 'plain_video']
        if self.contribution.postprocessing_type == PostprocessingType.WEB_LECTURE:
            url_xml = (
                self.weblectures_base_url
                + str(self.contribution.start_date.year)
                + "/"
                + self.contribution.contribution_id
                + "/info.xml"
            )
        elif self.contribution.postprocessing_type == PostprocessingType.PLAIN_VIDEO:
            url_xml = (
                self.conferences_base_url
                + str(self.contribution.start_date.year)
                + "/"
                + self.contribution.contribution_id
                + "/info.xml"
            )
        else:
            raise CdsRecordServiceException(
                f"{self.contribution.contribution_id}: We cannot send to CDS the subformats request. "
                f"Event type is not accepted: {self.contribution.event_type}"
            )
        return url_xml

    def build_contribution_data_json_url(self):
        """
        Generates the data.json URL for the contribution
        Example: https://ocmedia-bakony.cern.ch/2023/1275631/data.json

        :return: The URL of the data.json file for the contribution
        :rtype: str
        """
        # ['web_lecture', 'plain_video']
        if not self.contribution.start_date:
            raise CdsRecordServiceException(
                f"{self.contribution.contribution_id}: We cannot send to CDS the subformats request. "
                f"Event start date is missing."
            )
        year = self.contribution.start_date.year
        datajson_url = (
            self.opencast_datajson
            + str(year)
            + "/"
            + self.contribution.contribution_id
            + "/data.v2.json"
        )
        return datajson_url

    def build_contribution_dfs_path(self):
        """
        Generates DFS path for the contribution files

        :return: The DFS path for the contribution
        :rtype: str
        """
        # ['web_lecture', 'plain_video']
        if self.contribution.postprocessing_type == PostprocessingType.WEB_LECTURE:
            url = (
                self.weblectures_dfs_folder
                + "\\"
                + str(self.contribution.start_date.year)
                + "\\"
                + self.contribution.contribution_id
            )
        elif self.contribution.postprocessing_type == PostprocessingType.PLAIN_VIDEO:
            url = (
                self.conferences_dfs_folder
                + "\\"
                + str(self.contribution.start_date.year)
                + "\\"
                + self.contribution.contribution_id
            )
        else:
            raise CdsRecordServiceException(
                f"{self.contribution.contribution_id}: We cannot send to CDS the subformats request. "
                f"Event type is not accepted: {self.contribution.event_type}"
            )
        return url

    def build_contribution_ceph_path(self):
        """
        Generates local CEPH path used by the service.

        :return: The local mount of cephfs  for the contribution
        :rtype: str
        """
        return (
            "/mnt/master_share/master_data/"
            + str(self.contribution.start_date.year)
            + self.contribution.contribution_id
        )

    def is_waiting_time_valid_for_subformats(self):
        """
        We need to wait a few minutes before the cds subformats can be requested
        :return:
        :rtype:
        """
        minutes_after_cds_record_set = 5
        timezone = pytz.timezone("Europe/Zurich")
        if self.contribution.is_cds_record_set_date:
            date_with_timezone = timezone.localize(
                self.contribution.is_cds_record_set_date
            )
            cds_records_since = datetime.datetime.now(timezone) - datetime.timedelta(
                minutes=minutes_after_cds_record_set
            )
            if date_with_timezone < cds_records_since:
                return True
        else:
            self.logger.debug(
                f"{self.contribution.contribution_id}: is_cds_records_date_set is not set. "
                "Assuming the CDS record was set manually"
            )
            return True
        return False

    def is_cds_record_ready(self, ceph=False):
        self.logger.debug(
            f"{self.contribution.contribution_id}: Calling cds get_metadata API"
        )
        if ceph:
            path_to_master = self.build_contribution_ceph_path()
            response = CdsAPI(logger=self.logger).get_metadata(
                indico_contribution_id=self.contribution.contribution_id
            )
        else:
            path_to_master = self.build_contribution_dfs_path()
            response = CdsAPI(logger=self.logger).get_metadata(path_to_master)
        if not response:
            message = (
                f"{self.contribution.contribution_id}: CDS record is not ready yet"
            )
            self.logger.info(message)
            return False
        message = f"{self.contribution.contribution_id}: CDS record ready"
        self.logger.info(message)
        return True

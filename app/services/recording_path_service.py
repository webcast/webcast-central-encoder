import datetime
import logging

import pytz

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.extensions import db


class RecordingPathService:
    def __init__(self, logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.recording_path_service")

    def set_event_recording_path(self, event, path=None):
        if path is None:
            return False
        event.recording_path = path
        event.recording_path_date = datetime.datetime.now(
            pytz.timezone("Europe/Zurich")
        )
        db.session.commit()
        return True

    def set_contribution_recording_path(self, contribution, recording_path=None):
        """
        Set the recording path of a contribution if the associated Indico Event has it defined
        :param contribution: Contribution that will be modified
        :type contribution: app.models.events.IndicoEventContribution
        :return: True|False
        :rtype: bool
        """
        indico_contribution_dao = IndicoEventContributionDAO(self.logger)
        if recording_path:
            indico_contribution_dao.set_recording_path(contribution.id, recording_path)
            return True
        else:
            recording_path = contribution.indico_event.recording_path
            if recording_path:
                recording_path = recording_path + "\\" + contribution.contribution_id
                indico_contribution_dao.set_recording_path(
                    contribution.id, recording_path
                )
                path_set = True
            else:
                path_set = False
            return path_set

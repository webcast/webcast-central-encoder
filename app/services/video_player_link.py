import logging

from flask import current_app

from app.services.indico.indico_api_client_v2 import IndicoAPIClientV2


class VideoPlayerLinkService:
    def __init__(
        self, contribution_id, contribution_db_id_full, contribution_year, logger=None
    ):
        self.contribution_id = contribution_id
        self.contribution_db_id_full = contribution_db_id_full
        self.year = contribution_year
        self.media_server_base_url = current_app.config["MEDIA_SERVER_BASE_URL"]
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.recording_path_service")

    def generate_video_player_link(self):
        path = f"/{str(self.year)}/{self.contribution_id}/"
        return self.media_server_base_url + path

    def publish_video_player_link_on_indico(self):
        # Make request to Indico
        link_title = "Video preview"
        link_url = self.generate_video_player_link()
        result = IndicoAPIClientV2().create_link(
            self.contribution_db_id_full, link_title, link_url
        )
        return result

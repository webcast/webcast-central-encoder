def write_content_to_file(file_content, file_to_write):
    """
    Write content on a file
    :param file_content: The string to write
    :param file_to_write: The file absolute path
    :return: True once completed
    """
    with open(file_to_write, "w") as file:
        file.write(file_content)
        file.close()

    return True


def read_file_content(file_path):
    with open(file_path, "r") as content_file:
        file_content = content_file.read()
    return file_content

import smtplib
from email.mime.text import MIMEText

import pytz

from secret import config


class Helpers:
    @staticmethod
    def UTC_to_timezone(dt, timezone_to):
        """
        Convert a UTC datetime to a specific timezone.
        """
        # load the datetime in UTC timezone
        utc_dt = pytz.utc.localize(dt)
        # prepare the final timezone
        new_tz = pytz.timezone(timezone_to)
        # convert UTC time to new timezone time
        return new_tz.normalize(utc_dt.astimezone(new_tz))

    @staticmethod
    def sendmail(subject, body, room=None):
        """
        Send a mail.
        """
        s = smtplib.SMTP(config.MAIL_HOSTNAME)

        body = MIMEText(body.encode("utf-8"), "plain", "utf-8")
        body["Subject"] = subject
        body["From"] = config.MAIL_FROM
        body["To"] = ", ".join(config.MAIL_TO)

        s.sendmail(config.MAIL_FROM, config.MAIL_TO, body.as_string())

        s.quit()


def change_timezone(dt, timezone):
    """Convert a UTC datetime to a specific timezone"""
    utc_dt_naive = pytz.utc.localize(dt)
    utc_dt = utc_dt_naive.replace(tzinfo=pytz.utc)
    # prepare the final timezone
    new_time = utc_dt.astimezone(pytz.timezone(timezone))
    return new_time


def convert_to_utc(dt):
    dt_utc = dt.astimezone(pytz.utc)
    return dt_utc

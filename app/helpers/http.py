import re

import requests


def is_http_file_accesible(url: str):
    response = requests.get(url)
    if (
        response.status_code == 200
        and re.match(
            r"(application/json|text/xml)", response.headers["content-type"], re.I
        )
        and int(response.headers["content-length"]) > 0
    ):
        return True
    return False

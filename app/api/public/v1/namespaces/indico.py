import logging
import re

from flask_restx import Namespace, Resource, abort, reqparse

from app.api.api_authorizations import authorizations
from app.services.indico.indico_api import IndicoAPI
from app.services.indico.indico_service_2 import IndicoServiceV2
from app.services.opencast.opencast_media_package_service import convert_allowed_list

logger = logging.getLogger("webapp.indico_api")

namespace = Namespace(
    "indico",
    description="Indico related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/acl")
class IndicoAclsEndpoint(Resource):
    @namespace.doc("indico_acls")
    @namespace.doc(params={"contribution_id": "Contribution ID that will fetched"})
    def get(self):
        """
        Fetches from Indico the ACLs of a contribution given its id (12345c1s1)

        Request example:
        ```
        https://ces-qa.web.cern.ch/api/v1/indico/acl?contribution_id=949975c1sc2
        ```

        Request response:
        ```json
        {
            "result": {
                "indico_id": "86898",
                "is_event": true,
                "is_contribution": false,
                "is_subcontribution": false,
                "is_session": false,
                "allowed": {
                    "groups": [
                        "cern-cms",
                        "cern-staff",
                        "cms-cern"
                    ],
                    "users": [
                        "user1",
                        "user2",
                        "user3",
                        "user4",
                        "user5",
                        "user6",
                        "user7",
                        "user8"
                    ],
                    "unable": {
                        "groups": [],
                        "users": [
                            "another-email@cern.ch",
                            "other-emailn@cern.ch",
                            "email@cern.ch",
                            "email@imperial.ac.uk"
                        ]
                    }
                }
            },
            "error": false
        }
        ```

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("Indico API ACLs endpoint request")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id", type=str, help="The contribution ID to be fetched"
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id

        if not contribution_id:
            abort(400, "No contribution_id provided", error=True)

        try:
            is_event = re.match(r"(\d+)$", contribution_id)
            is_session = re.match(r"(\d+)s(\d+)$", contribution_id)
            is_session_slot = re.match(r"(\d+)s(\d+)l(\d+)$", contribution_id)
            is_contribution = re.match(r"(\d+)c(\d+|s\d+t\d+)$", contribution_id)
            is_subcontribution = re.match(r"(\d+)c(\d+)sc(\d+)$", contribution_id)

            iid = None

            if is_event:
                iid = contribution_id
            elif is_session:
                iid = is_session.group(1)
            elif is_session_slot:
                iid = is_session_slot.group(1)
            elif is_contribution:
                iid = is_contribution.group(1)
            elif is_subcontribution:
                iid = is_subcontribution.group(1)

            logger.debug(iid)
            logger.debug(f"is_event: {is_event}")
            logger.debug(f"is_session: {is_session}")
            logger.debug(f"is_contribution: {is_contribution}")
            logger.debug(f"is_subcontribution: {is_subcontribution}")

            result = IndicoServiceV2().get_event_details(iid)

            allowed = result["allowed"]
            if is_session or is_subcontribution or is_contribution:
                for contribution in result["contributions"]:
                    if contribution["contribution_id"] == contribution_id:
                        found_element = contribution
                        allowed = found_element["allowed"]
                        break

            allowed, not_found = convert_allowed_list(allowed)

            if not result:
                abort(
                    400,
                    "Unable to fetch the ACls. Does the contribution exist?",
                    error=True,
                )
            return {
                "result": {
                    "indico_id": contribution_id,
                    "is_event": True if is_event else False,
                    "is_contribution": True if is_contribution else False,
                    "is_subcontribution": True if is_subcontribution else False,
                    "is_session": True if is_session else False,
                    "allowed": allowed,
                    "not_found": not_found,
                },
                "error": False,
            }, 200
        except Exception as e:
            logger.warning(
                f"{contribution_id}: Error: Indico API endpoint. (Error: {str(e)})"
            )
            abort(400, str(e), error=True)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/event")
class IndicoEventsEndpoint(Resource):
    @namespace.doc("indico_events")
    @namespace.doc(params={"indico_id": "Indico ID that will fetched"})
    def get(self):
        logger.info("Indico API Event's endpoint request")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "indico_id",
            type=str,
            help="The Indico ID to be fetched (Not contribution ID)",
        )
        args = parser.parse_args()
        indico_id = args.indico_id

        if not indico_id:
            abort(400, "No indico_id provided", error=True)

        try:
            event_data = IndicoAPI().fetch_event(indico_id)
            return event_data
        except Exception as e:
            logger.error(
                f"{indico_id}: Error: Indico Event API endpoint. (Error: {str(e)})"
            )
            abort(400, str(e), error=True)

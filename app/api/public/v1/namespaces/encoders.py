import logging

from flask_restx import Namespace, Resource, fields

from app.api.decorators import jwt_resource_required
from app.daos.epiphan_files_to_move import EpiphanFilesToMoveDAO
from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.extensions import db
from app.models.epiphan_files_to_move import EpiphanFilesToMoveStatus
from app.models.events import IndicoEvent
from app.services.indico.indico_service_2 import IndicoServiceV2
from app.services.move_files.move_files_service import MoveFilesService
from app.services.recording_path_service import RecordingPathService

logger = logging.getLogger("webapp.encoders_api")

namespace = Namespace(
    "encoders", description="Encoders API callbacks related operations"
)

encoders_notification = namespace.model(
    "EncoderNotification",
    {
        "indico_id": fields.String(),
        "event_title": fields.String(),
        "encoder_type": fields.String(),
        "encoder_camera": fields.String(),
        "encoder_slides": fields.String(),
        "recording_path": fields.String(),
        "recording_camera_path": fields.String(),
        "recording_slides_path": fields.String(),
        "stream_camera_name": fields.String(),
        "stream_slides_name": fields.String(),
        "stream_camera_url": fields.String(),
        "event_type": fields.String(),
        "audience": fields.String(),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class EncodersEndpoint(Resource):
    decorators = [
        jwt_resource_required("encoders"),
    ]

    @namespace.doc("encoders_api")
    @namespace.expect(encoders_notification, validate=True)
    def post(self) -> dict:
        logger.info("Encoders API endpoint 1")
        args = namespace.payload
        indico_id = args["indico_id"]
        audience = args["audience"]
        event_type = args["event_type"]

        # Fetch the event from Indico
        has_webcast = False
        if event_type == "RECORDING" or event_type == "STREAM_RECORDING":
            has_webcast = True

        indico_service = IndicoServiceV2(logger=logger)
        _, message = indico_service.get_event(
            indico_id, audience=audience, webcast=has_webcast
        )
        logger.debug(args)
        logger.debug(message)

        indico_event = IndicoEvent.query.filter_by(indico_id=indico_id).first()

        if indico_event:
            logger.info(
                f"Updating event {indico_id}. Setting is_recorded to True and recording_path "
                f"to {args['recording_path']}"
            )
            indico_event.is_recorded = True
            indico_event.recording_path = args["recording_path"]

            db.session.commit()

            logger.info(
                f"Updating contributions for event {indico_id} ({len(indico_event.contributions)})"
            )

            encoder_camera_type = args["encoder_camera_type"].lower()
            encoder_slides_type = args["encoder_slides_type"].lower()

            is_epiphan = False
            if "epiphan" in encoder_camera_type or "epiphan" in encoder_slides_type:
                is_epiphan = True

            if is_epiphan:
                logger.debug(f"Epiphan detected for event {indico_id}")
                # Files are not created straight away. They are uploaded
                # by the device and it may take some
                # time to upload them. We create a new model here to be
                # used by a background celery task.
                created = EpiphanFilesToMoveDAO.create(indico_id)
                if not created:
                    EpiphanFilesToMoveDAO.set_status_by_indico_id(
                        indico_id, EpiphanFilesToMoveStatus.PENDING
                    )
            else:
                logger.debug(
                    f"Epiphan not detected for event {indico_id}. Moving recorded files..."
                )
                move_files_service = MoveFilesService(indico_event)
                move_files_service.move_recorded_files(indico_event.recording_path)

            # Update the recording path for the contributions
            for contribution in indico_event.contributions:

                if contribution.recording_path == "":
                    recording_path_service = RecordingPathService()

                    recording_path_service.set_contribution_recording_path(contribution)
                    indico_contribution_dao = IndicoEventContributionDAO()
                    indico_contribution_dao.update_last_updated(
                        contribution.id, "Set recording path", user="API"
                    )

        return {"message": "ok"}

import json
import logging

import sqlalchemy
from flask import request
from flask_restx import Namespace, Resource, abort, fields, reqparse

from app.api.api_authorizations import authorizations
from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.extensions import csrf
from app.models.events import IndicoEventContribution
from app.services.cds.cds_api import CdsAPI
from app.services.cds.cds_record_service import CdsRecordService
from app.services.indico.indico_eagreement_service import IndicoEagreementStatus
from app.services.service_now.service_now_service import ServiceNowService

logger = logging.getLogger("webapp.cds_api")

namespace = Namespace(
    "cds",
    description="CDS related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

results_model = namespace.model(
    "CdsResultsModel",
    {
        "recid": fields.String(
            readOnly=True,
            required=True,
            description="The CDS record ID",
            example="1234",
        ),
        "success": fields.Boolean(
            readOnly=True,
            required=True,
            description="Whether or not the request was successful",
            example=True,
        ),
        "error_message": fields.String(
            readOnly=True,
            description="Error Message",
            example="An error occurred handling the request",
        ),
    },
)


@namespace.route("/callback")
class CdsEndpoint(Resource):
    decorators = [csrf.exempt]

    @namespace.doc("cds_callback")
    @namespace.doc(
        params={
            "contribution_id": "Contribution internal ID that will be notified",
            "nonce": "Verification string that must match the one in the CDS record request",
        }
    )
    def post(self):
        """
        Notifies when a CDS record has been created for a given contribution

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("CDS API callback endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id",
            type=str,
            help="The internal contribution ID to be updated",
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id

        logger.info(f"{contribution_id}: CDS API callback endpoint request")

        form_data = request.form
        decoded = form_data["results"]
        results3 = json.loads(decoded, strict=False)  # avoid control chars
        nonce = results3["nonce"]
        result = results3["results"][0]

        generated_nonce = CdsAPI().generate_nonce(contribution_id)
        if nonce == generated_nonce:
            if len(results3) > 0 and result.get("recid"):
                cds_record = result.get("recid")
                indico_contribution_dao = IndicoEventContributionDAO()
                indico_contribution_dao.set_cds_record(contribution_id, cds_record)
                contribution = IndicoEventContribution.query.filter_by(
                    contribution_id=contribution_id
                ).one()
                ServiceNowService(contribution).add_work_note(
                    f"CDS Record created: {cds_record}"
                )
            result = {}
            return result, 200
        else:
            logger.error(
                f"{contribution_id}: Error CDS API callback endpoint request not valid. Nonce does not match"
            )

        abort(400, "Request is not valid", error=True)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/marcxml")
class CdsEndpointMarcXML(Resource):
    decorators = [csrf.exempt]

    @namespace.doc("cds_marcxml")
    @namespace.doc(
        params={
            "contribution_id": "Contribution internal ID that will be notified",
            "send_request": "Send to CDS. Leave empty for False",
        }
    )
    def get(self):
        logger.info("CDS API MarcXML endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id", type=str, help="The contribution ID to be fetched"
        )
        parser.add_argument(
            "send_request", type=bool, default=False, help="Send to CDS endpoint"
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id
        send_request = args.send_request

        response = CdsAPI(logger=logger).request_create_cds_record(
            contribution_id, send_request=send_request
        )
        if not response:
            abort(
                response.status_code,
                "Not proper response or MarcXML while CDS record creation, response error: {}".format(
                    response.text
                ),
                error=True,
            )
        if isinstance(response, bytes):
            return {"result": response.decode("utf-8"), "error": False}, 200
        else:
            return {"result": response.text, "error": False}, response.status_code


@namespace.doc(security=["Bearer Token"])
@namespace.route("/requestrecord")
class CdsEndpointRequestRecord(Resource):
    @namespace.doc("cds_requestrecord")
    @namespace.doc(
        params={
            "contribution_id": "Contribution internal ID that will be notified",
        }
    )
    def get(self):
        logger.info("CDS Request record endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id",
            type=str,
            help="The contribution ID to request a record about",
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id

        response = CdsAPI(logger=logger).request_create_cds_record(contribution_id)
        if not response:
            abort(
                response.status_code,
                "Not proper response or MarcXML while CDS record creation, response error: {}".format(
                    response.text
                ),
                error=True,
            )
        if isinstance(response, bytes):
            return {"result": response.decode("utf-8"), "error": False}, 200
        else:
            return {"result": response.text, "error": False}, response.status_code


@namespace.doc(security=["Bearer Token"])
@namespace.route("/subformats")
class CdsEndpointSubformats(Resource):
    @namespace.doc("cds_subformats")
    @namespace.doc(
        params={
            "contribution_id": "Contribution internal ID that will be notified",
        }
    )
    def get(self):
        logger.info("CDS Request subformats")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id",
            type=str,
            help="The contribution ID to request subformats",
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id
        try:
            contribution = self._get_contribution(contribution_id)
        except sqlalchemy.orm.exc.NoResultFound as e:
            logger.warning(
                "{}: Unable to find that contribution (with conditions applied), throw exception ({})".format(
                    contribution_id, e
                )
            )
            return {"result": f"Unable to find {contribution_id}", "error": True}, 404
        try:
            result = CdsRecordService(
                contribution, logger=logger
            ).request_cds_subformats_ready_datajson()
            if result[0]:
                logger.debug(
                    "{}: CDS subformats successfully requested for contribution".format(
                        contribution.contribution_id
                    )
                )
                return {"result": result[1], "error": False}, 200
            else:
                logger.warning(
                    "{}: Unable to request CDS subformats for contribution".format(
                        contribution.contribution_id
                    )
                )
                return {"result": result[1], "error": True}, 404
        except Exception as e:
            logger.warning(
                "{}: Unable to request CDS subformats for contribution ({})".format(
                    contribution.contribution_id, e
                )
            )
            return {"result": result[1], "error": True}, 404

    def _get_contribution(self, contribution_id):
        """Helper method to retrieve a single contribution with certain characteristics

        Args:
            contribution_id (_type_):

        Returns:
            _type_: _description_
        """
        contribution = (
            IndicoEventContribution.query.filter(
                IndicoEventContribution.e_agreement_status.in_(
                    [IndicoEagreementStatus.ACCEPTED, IndicoEagreementStatus.FORCED]
                )
            )
            .filter(IndicoEventContribution.is_cds_subformats_requested_status != "OK")
            .filter_by(
                opencast_processing_finished=True,
                opencast_acls_finished=True,
                is_cds_record_set=True,
                is_cds_record_requested_status="OK",
                run_workflows=True,
                contribution_id=contribution_id,
            )
            .one()
        )
        return contribution

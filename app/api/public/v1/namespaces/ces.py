import logging

from flask_restx import Namespace, Resource, fields
from sqlalchemy.orm import aliased

from app.api.decorators import jwt_resource_required
from app.models.events import IndicoEvent, IndicoEventContribution

logger = logging.getLogger("webapp.encoders_api")

namespace = Namespace("info", description="CES API callbacks related operations")


event_model = namespace.model(
    "CesIndicoContribution",
    {
        "indicoId": fields.String(attribute="indico_event.indico_id"),
        "cdsRecord": fields.String(attribute="cds_record"),
        "title": fields.String(),
        "startDate": fields.Date(attribute="start_date"),
        "startTime": fields.String(attribute="start_time"),
        "isRestricted": fields.Boolean(attribute="is_restricted"),
        "url": fields.String(attribute="contribution_url"),
        "speakers": fields.String(attribute="speakers"),
        "indicoUrl": fields.String(attribute="indico_event.indico_url"),
    },
)

newest_events_model = namespace.model(
    "NewestEvents",
    {
        "results": fields.List(fields.Nested(event_model)),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/last-processed/")
class CesInfoEndpoint(Resource):
    decorators = [
        jwt_resource_required("ces"),
    ]

    @namespace.marshal_with(newest_events_model, as_list=True)
    def get(self) -> dict:

        IndicoEventAlias = aliased(IndicoEvent)

        latest_events = (
            IndicoEventContribution.query.join(
                IndicoEventAlias, IndicoEventContribution.indico_event
            )
            .filter(
                IndicoEventAlias.allowed == "{'users': [], 'groups': []}",
                IndicoEventContribution.cds_record.isnot(None),
                IndicoEventContribution.cds_record != "",
                IndicoEventContribution.is_cds_record_set == True,  # noqa
            )
            .order_by(IndicoEventContribution.is_cds_record_set_date.desc())
            .limit(10)
            .all()
        )

        return {"results": latest_events}

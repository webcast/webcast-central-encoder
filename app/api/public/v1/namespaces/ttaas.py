import enum
import logging

from flask import Response, request
from flask_restx import Namespace, Resource

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.extensions import csrf
from app.models.contribution_workflow.transcription_workflow import TranscriptionStates
from app.models.events import IndicoEventContribution
from app.services.service_now.service_now_service import ServiceNowService
from app.services.transcription.transcription_service import TranscriptionService

logger = logging.getLogger("webapp.ttaas_api")

namespace = Namespace("ttaas", description="TTAAS callbacks related operations")


class TtaasCallbackType(enum.Enum):
    FINISHED = "FINISHED"
    ALREADY_EXISTS = "ALREADY_EXISTS"
    ERROR = "ERROR"
    CAPTION_UPDATED = "CAPTION_UPDATED"


@namespace.route("/")
class TtaasCallbackEndpoint(Resource):
    decorators = [csrf.exempt]

    @namespace.doc("ttaas_callback")
    def post(self) -> Response:
        logger.info("TTAAS API callback endpoint")
        logger.debug(request.json)
        callback_type = request.json["callbackType"]
        contribution_id = request.json["contribution_id"][0]
        media_id = request.json["mediaId"]

        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        indico_contribution_dao = IndicoEventContributionDAO()

        if callback_type == TtaasCallbackType.ALREADY_EXISTS.value:
            logger.info("Received already-exists from ttaas service")
            state = request.json["state"]
            media_id = request.json["existingMediaId"]
            if (
                state == TranscriptionStates.COMPLETED_CALLBACK_FAILED.value
                or state == TranscriptionStates.COMPLETED_EMAIL_FAILED.value
                or state == TranscriptionStates.TRANSLATION_FINISHED.value
                or state == TranscriptionStates.TRANSCRIPTION_FINISHED.value
            ):
                indico_contribution_dao.set_transcription_status(
                    contribution.contribution_id,
                    TranscriptionStates.COMPLETED,
                    media_id=media_id,
                )
                if not contribution.is_cds_link_published:
                    ServiceNowService(contribution, logger=logger).add_work_note(
                        (
                            f"Transcription 'TTAAS: {media_id}' completed. "
                            "However, it was sent more than once and this one is not the main one."
                        )
                    )
                service = TranscriptionService(contribution, logger=logger)
                service.request_transcription_update()
            else:
                indico_contribution_dao.set_transcription_status(
                    contribution.contribution_id,
                    TranscriptionStates.TRY_LATER,
                    media_id=media_id,
                )
                if not contribution.is_cds_link_published:
                    ServiceNowService(contribution, logger=logger).add_work_note(
                        (
                            f"Waiting for transcription 'TTAAS: {media_id}' to finish. "
                            "However, it was sent more than once, therefore this one is not the main one."
                        )
                    )

        if callback_type == TtaasCallbackType.FINISHED.value:
            logger.info("Received finished from ttaas service")
            indico_contribution_dao.set_transcription_status(
                contribution.contribution_id,
                TranscriptionStates.COMPLETED,
                media_id=media_id,
            )
            if not contribution.is_cds_link_published:
                ServiceNowService(contribution, logger=logger).add_work_note(
                    "Transcription completed"
                )
            service = TranscriptionService(contribution, logger=logger)
            service.request_transcription_update()

        if callback_type == TtaasCallbackType.ERROR.value:
            logger.info("Received error callback from ttaas service")
            indico_contribution_dao.set_transcription_status(
                contribution.contribution_id,
                TranscriptionStates.ERROR,
                media_id=media_id,
            )
            if not contribution.is_cds_link_published:
                ServiceNowService(contribution, logger=logger).add_work_note(
                    "Transcription completed"
                )

        if callback_type == TtaasCallbackType.CAPTION_UPDATED.value:
            logger.info("Received caption-updated from ttaas service")

        return "", 200

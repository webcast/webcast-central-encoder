import logging

from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.extensions import csrf

logger = logging.getLogger("webapp.api.services")

namespace = Namespace(
    "hello",
    description="Hello related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


test_model = namespace.model(
    "HelloModel",
    {
        "result": fields.String(required=True, description="The hello result"),
    },
)


@namespace.doc()
@namespace.route("/")
class HelloEndpoint(Resource):

    decorators = [csrf.exempt]

    @namespace.doc("hello_endpoint")
    @namespace.marshal_with(test_model)
    def get(self):
        """
        Test response with unsecured endpoint

        This endpoint can be used to check the API connectivity.

        """
        result = {"result": "ok"}

        return result

    @namespace.doc("hello_endpoint_post")
    @namespace.marshal_with(test_model)
    def post(self):
        """
        Test response with unsecured endpoint

        This endpoint can be used to check the API connectivity.

        """
        result = {"result": "ok"}

        return result

import logging

from flask_restx import Namespace, Resource, abort, reqparse

from app.api.api_authorizations import authorizations
from app.api.decorators import jwt_resource_required
from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.daos.metric import MetricsDAO
from app.models.contribution_workflow.opencast_workflow import OpencastProcessingStatus
from app.models.contribution_workflow.transcription_workflow import (
    OpencastTranscriptionStates,
)
from app.models.events import IndicoEventContribution
from app.models.metrics import AvailabeMetricsNames
from app.services.service_now.service_now_service import ServiceNowService

logger = logging.getLogger("webapp.opencast_api")

namespace = Namespace(
    "opencast",
    description="Opencast related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/processing-finished")
class OpencastEndpoint(Resource):
    method_decorators = [jwt_resource_required("opencast")]

    @namespace.doc("opencast_processing_finished")
    @namespace.doc(params={"contribution_id": "Contribution ID that will be notified"})
    def get(self):
        """
        Notifies when the Opencast processing has finished for a contribution given its id (12345c1s1)

        This endpoint will return error code 400 if the contribution is not on CES.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("Opencast API processing endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id", type=str, help="The contribution ID to be updated"
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id

        if not contribution_id:
            abort(400, "No contribution_id provided", error=True)

        try:
            logger.info(
                "{}: Opencast API processing endpoint request".format(contribution_id)
            )
            indico_contribution_dao = IndicoEventContributionDAO()

            indico_contribution_dao.set_opencast_processing_finished(
                contribution_id, True
            )
            result = indico_contribution_dao.set_opencast_processing_status(
                contribution_id, OpencastProcessingStatus.OK, last_report="OK"
            )

            contribution = IndicoEventContribution.query.filter_by(
                contribution_id=contribution_id
            ).one()
            if not contribution.is_cds_link_published:
                ServiceNowService(contribution, logger=logger).add_work_note(
                    "Opencast processing finished"
                )

            if not result:
                logger.error(
                    (
                        f"{contribution_id}: Error: Opencast API processing endpoint request error. "
                        "Unable to update the acl status. Does the contribution exist?"
                    )
                )
                abort(
                    400,
                    "Unable to change the processing status. Does the contribution exist?",
                    error=True,
                )
            return {"result": "OK", "error": False}, 200
        except Exception as e:
            logger.error(
                "{}: Error: Opencast API processing endpoint. Exception: {}".format(
                    contribution_id, str(e)
                )
            )
            abort(400, str(e), error=True)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/processing-failed")
class OpencastProcessingFailedEndpoint(Resource):
    method_decorators = [jwt_resource_required("opencast")]

    @namespace.doc("opencast_processing_failed")
    @namespace.doc(params={"contribution_id": "Contribution ID that will be notified"})
    def get(self):
        """
        Notifies when the Opencast processing has failed for a contribution given its id (12345c1s1)

        This endpoint will return error code 400 if the contribution is not on CES.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("Opencast API processing failed endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id", type=str, help="The contribution ID to be updated"
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id

        if not contribution_id:
            abort(400, "No contribution_id provided", error=True)

        try:
            logger.info(
                "{}: Opencast API processing failed endpoint request".format(
                    contribution_id
                )
            )
            indico_contribution_dao = IndicoEventContributionDAO()

            result = indico_contribution_dao.set_opencast_processing_status(
                contribution_id,
                OpencastProcessingStatus.ERROR,
                last_report="Processing failed. Check Opencast",
            )
            contribution = IndicoEventContribution.query.filter_by(
                contribution_id=contribution_id
            ).one()
            if not contribution.is_cds_link_published:
                ServiceNowService(contribution, logger=logger).add_work_note(
                    "Opencast processing failed"
                )

            if not result:
                logger.error(
                    (
                        f"{contribution_id}: Error: Opencast API processing failed endpoint request error. "
                        "Unable to set the processing status as failed. Does the contribution exist?"
                    )
                )
                abort(
                    400,
                    "Unable to change the processing status to failed. Does the contribution exist?",
                    error=True,
                )
            return {"result": "OK", "error": False}, 200
        except Exception as e:
            logger.error(
                "{}: Error: Opencast API processing failed endpoint. Exception: {}".format(
                    contribution_id, str(e)
                )
            )
            abort(400, str(e), error=True)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/cutting-finished")
class OpencastCuttingFinishedEndpoint(Resource):
    method_decorators = [jwt_resource_required("opencast")]

    @namespace.doc("opencast_cutting_finished")
    @namespace.doc(params={"contribution_id": "Contribution ID that will be notified"})
    def get(self):
        """
        Notifies when the Opencast cutting has finished for a contribution given its id (12345c1s1). Once the cutting
        has finished, it is expected that Opencast will start the processing of the videos.

        This endpoint will return error code 400 if the contribution is not on CES.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("Opencast API cutting endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id", type=str, help="The contribution ID to be updated"
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id

        if not contribution_id:
            abort(400, "No contribution_id provided", error=True)

        try:
            logger.info(
                "{}: Opencast API cutting endpoint request".format(contribution_id)
            )
            indico_contribution_dao = IndicoEventContributionDAO()
            result = indico_contribution_dao.set_opencast_processing_status(
                contribution_id,
                OpencastProcessingStatus.WAITING,
                last_report="Waiting for Opencast",
            )

            contribution = IndicoEventContribution.query.filter_by(
                contribution_id=contribution_id
            ).one()
            if not contribution.is_cds_link_published:
                ServiceNowService(contribution, logger=logger).add_work_note(
                    "Opencast cutting finished"
                )

            MetricsDAO.create(
                AvailabeMetricsNames.CUTTING_IN_OPENCAST,
                AvailabeMetricsNames.CUTTING_IN_OPENCAST,
            )

            if not result:
                logger.error(
                    (
                        f"{contribution_id}: Error: Opencast API cutting finished endpoint request error. "
                        "Unable to update the opencast processing status. Does the contribution exist?"
                    )
                )
                abort(
                    400,
                    "Unable to change the cutting status. Does the contribution exist?",
                    error=True,
                )
            return {"result": "OK", "error": False}, 200
        except Exception as e:
            logger.error(
                "{}: Error: Opencast API cutting finished endpoint. Exception: {}".format(
                    contribution_id, str(e)
                )
            )
            abort(400, str(e), error=True)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/update-acl-finished")
class OpencastExportEndpoint(Resource):
    method_decorators = [jwt_resource_required("opencast")]

    @namespace.doc("opencast_update_final_acl_finished")
    @namespace.doc(params={"contribution_id": "Contribution ID that will be notified"})
    def get(self):
        """
        Notifies when the Opencast acl regeneration has finished for a contribution given its id (12345c1s1)

        This endpoint will return error code 400 if the contribution is not on CES.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("Opencast API update acl finished endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id", type=str, help="The contribution ID to be updated"
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id

        if not contribution_id:
            abort(400, "No contribution_id provided", error=True)

        logger.info(
            "{}: Opencast API update acl finished endpoint request".format(
                contribution_id
            )
        )
        indico_contribution_dao = IndicoEventContributionDAO()
        result = indico_contribution_dao.set_opencast_acls_finished(
            contribution_id, True
        )
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        if not contribution.is_cds_link_published:
            ServiceNowService(contribution, logger=logger).add_work_note(
                "Opencast ACLs update finished"
            )

        if not result:
            logger.error(
                (
                    f"{contribution_id}: Error: Opencast API update acl finished endpoint request error. "
                    "Unable to update the acl status. Does the contribution exist?"
                )
            )
            abort(
                400,
                "Unable to update the acl status. Does the contribution exist?",
                error=True,
            )
        return {"result": "OK", "error": False}, 200


@namespace.doc(security=["Bearer Token"])
@namespace.route("/ready-for-cutting")
class OpencastReadyForCuttingEndpoint(Resource):
    method_decorators = [jwt_resource_required("opencast")]

    @namespace.doc("opencast_ready_for_cutting")
    @namespace.doc(params={"contribution_id": "Contribution ID that will be notified"})
    def get(self):
        """
        Notifies when Opencast is ready for cutting for a contribution given its id (12345c1s1)

        This endpoint will return error code 400 if the contribution is not on CES.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("Opencast API ready for cutting endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id", type=str, help="The contribution ID to be updated"
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id

        if not contribution_id:
            abort(400, "No contribution_id provided", error=True)

        logger.info(
            "{}: Opencast API ready for cutting endpoint request".format(
                contribution_id
            )
        )
        indico_contribution_dao = IndicoEventContributionDAO()
        result = indico_contribution_dao.set_opencast_ready_for_cutting(
            contribution_id, True
        )
        indico_contribution_dao.set_opencast_ready_for_cutting_status(
            contribution_id, "OK"
        )
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        if not contribution.is_cds_link_published:
            ServiceNowService(contribution, logger=logger).add_work_note(
                "Contribution is ready for cutting in Opencast."
            )

        if not result:
            logger.error(
                (
                    f"{contribution_id}: Error: Opencast API ready for cutting endpoint error. "
                    "Uanble to update the contribution's status (Does it exist?)"
                )
            )
            abort(
                400,
                "Unable to update ready for cutting status. Does the contribution exist?",
                error=True,
            )
        return {"result": "OK", "error": False}, 200


@namespace.doc(security=["Bearer Token"])
@namespace.route("/event-deleted")
class OpencastEventDeletedEndpoint(Resource):
    method_decorators = [jwt_resource_required("opencast")]

    @namespace.doc("opencast_event_deleted")
    @namespace.doc(params={"contribution_id": "Contribution ID that will be notified"})
    def get(self):
        """
        Opencast will notify this endpoint when it has deleted an event.

        This endpoint will return error code 400 if the contribution is not on CES.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("Opencast API event deleted endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id", type=str, help="The contribution ID to be updated"
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id

        if not contribution_id:
            abort(400, "No contribution_id provided", error=True)

        logger.info(
            f"Opencast informed CES that event {contribution_id} has been deleted."
        )
        indico_contribution_dao = IndicoEventContributionDAO()
        result = indico_contribution_dao.set_opencast_uid_by_contribution_id(
            contribution_id, ""
        )
        return_message = "not_found"
        if result:
            return_message = "deleted"
        return {"result": "OK", "message": return_message, "error": False}, 200


@namespace.doc(security=["Bearer Token"])
@namespace.route("/transcription-update-finished")
class OpencastTranscriptionEndpoint(Resource):
    method_decorators = [jwt_resource_required("opencast")]

    @namespace.doc("opencast_transcription_update_finished")
    @namespace.doc(params={"contribution_id": "Contribution ID that will be notified"})
    def get(self):
        """
        Notifies when the Opencast transcription update has finished for a contribution given its id (12345c1s1)

        This endpoint will return error code 400 if the contribution is not on CES.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("Opencast API transcription update finished endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument(
            "contribution_id", type=str, help="The contribution ID to be updated"
        )
        args = parser.parse_args()
        contribution_id = args.contribution_id

        if not contribution_id:
            abort(400, "No contribution_id provided", error=True)

        logger.info(
            f"{contribution_id}: Opencast API transcription update finished endpoint request"
        )
        indico_contribution_dao = IndicoEventContributionDAO()
        result = indico_contribution_dao.set_transcription_update_status(
            contribution_id, OpencastTranscriptionStates.COMPLETED
        )
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()

        if not contribution.is_cds_link_published:
            ServiceNowService(contribution, logger=logger).add_work_note(
                "Transcription update in Opencast finished"
            )

        if not result:
            logger.error(
                f"""{contribution_id}: Error: Opencast transcription update finished endpoint request error.
                Unable to update the transcription status. Does the contribution exist?"""
            )
            abort(
                400,
                "Unable to update the transcription ingest status. Does the contribution exist?",
                error=True,
            )
        return {"result": "OK", "error": False}, 200

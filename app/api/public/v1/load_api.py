from flask import Blueprint
from flask_restx import Api

from app.api.public.v1.namespaces.cds import namespace as cds_namespace
from app.api.public.v1.namespaces.ces import namespace as ces_namespace
from app.api.public.v1.namespaces.encoders import namespace as encoders_namespace
from app.api.public.v1.namespaces.hello import namespace as hello_namespace
from app.api.public.v1.namespaces.indico import namespace as indico_namespace
from app.api.public.v1.namespaces.opencast import namespace as opencast_namespace
from app.api.public.v1.namespaces.ttaas import namespace as ttaas_namespace
from app.extensions import csrf


def load_api_namespaces():
    blueprint = Blueprint("public_api", "public_api")
    api = Api(blueprint, decorators=[csrf.exempt])
    api.add_namespace(cds_namespace)
    api.add_namespace(opencast_namespace)
    api.add_namespace(indico_namespace)
    api.add_namespace(hello_namespace)
    api.add_namespace(ttaas_namespace)
    api.add_namespace(encoders_namespace)
    api.add_namespace(ces_namespace)

    return blueprint

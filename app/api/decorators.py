import logging

from flask import jsonify
from flask_jwt_extended import get_jwt_identity, verify_jwt_in_request
from sqlalchemy.orm.exc import NoResultFound

from app.daos.api_tokens import ApiTokenDAO

logger = logging.getLogger("webapp.require_token")


def jwt_resource_required(resource):
    """
    Custom decorator that verifies the JWT is present in
    the request, as well as insuring that this user has access to the given resource.
    This way, the access token will only work with the selected API endpoints

    :param resource: Name of the resource (videoconference, resources, etc)
    :return:
    """

    def decorator(fn):
        def wrapper(*args, **kwargs):
            logger.info("Loading jwt_resource_required")
            verify_jwt_in_request()
            name = get_jwt_identity()
            try:
                api_key = ApiTokenDAO.get_by_name(name)
                if api_key.resource != resource:
                    return jsonify(msg="Forbidden"), 401

            except NoResultFound:
                return jsonify(msg="Forbidden"), 401

            else:
                return fn(*args, **kwargs)

        return wrapper

    return decorator

from flask_wtf import FlaskForm
from wtforms import BooleanField, StringField


class SetDoNotTranscribeForm(FlaskForm):
    contribution_id = StringField()
    do_not_transcribe = BooleanField()

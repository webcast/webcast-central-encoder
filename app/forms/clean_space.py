from flask_wtf import FlaskForm
from wtforms import SelectField
from wtforms.validators import InputRequired


class FreeDiskForm(FlaskForm):
    since = SelectField(
        "Remove all the files older than",
        choices=[
            ("1m", "1 month"),
            ("2w", "2 weeks"),
            ("1w", "1 week"),
            ("2d", "2 days"),
            ("now", "Now"),
        ],
        validators=[InputRequired()],
    )

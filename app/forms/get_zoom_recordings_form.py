from wtforms import Form, IntegerField


class GetZoomRecordingsForm(Form):
    meeting_id = IntegerField()

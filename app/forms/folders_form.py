from flask_wtf import FlaskForm
from wtforms import StringField


class CreateFoldersForm(FlaskForm):
    recording_path = StringField()

from flask_wtf import FlaskForm
from wtforms import IntegerField, StringField


class FetchEventsForm(FlaskForm):
    pass


class FetchSingleEventForm(FlaskForm):
    indico_id = IntegerField()
    event_type = StringField()
    audience = StringField()

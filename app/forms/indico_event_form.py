from wtforms import BooleanField, Form, IntegerField, StringField, validators


class GetIndicoEventForm(Form):
    indico_id = IntegerField()
    webcast = BooleanField()
    audience = StringField([validators.Length(min=1, max=35)])

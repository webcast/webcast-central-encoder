from flask_wtf import FlaskForm
from wtforms import BooleanField, StringField


class ContributionIdForm(FlaskForm):
    contribution_id = StringField()


class SetPostprocessingTypeForm(FlaskForm):
    contribution_id = StringField()
    postprocessing_type = StringField()


class SendToOpencastForm(FlaskForm):
    contribution_id = StringField()
    edit_on_opencast = BooleanField()
    add_transcription = BooleanField()


class PublishCDSLinkToIndicoForm(FlaskForm):
    contribution_id = StringField()
    force = BooleanField()

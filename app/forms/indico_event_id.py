from wtforms import BooleanField, Form, IntegerField


class IndicoEventIDForm(Form):
    indico_id = IntegerField()
    paused_for_splitting = BooleanField()

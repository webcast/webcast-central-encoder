from flask_wtf import FlaskForm
from wtforms import SelectField
from wtforms.validators import InputRequired


class MetricsForm(FlaskForm):
    selected_year = SelectField(
        "Select the metrics for the following year",
        choices=[("current_year", "Current year"), ("previous_year", "Previous year")],
        validators=[InputRequired()],
    )

from wtforms import Form, IntegerField


class IndicoIDForm(Form):
    indico_id = IntegerField()

def show_date(date):
    # format date
    try:
        result = date.strftime("%e %B %Y")
    except Exception:
        result = date
    return result


def show_time(time):
    # format time
    try:
        result = time.strftime("%H:%M")
    except Exception:
        result = time
    return result


def show_space(space):
    # format disc space
    try:
        result = abs(space * 1024)
        # result = size(space * 1024, system = alternative)
    except Exception:
        result = " "

    return result

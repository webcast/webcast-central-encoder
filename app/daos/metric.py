import datetime
import logging

import pytz

from app.extensions import db
from app.models.metrics import Metric

logger = logging.getLogger("webapp.daos_metric")


class MetricsDAO:
    @staticmethod
    def create(name, description):
        logger.debug("Creating a new metric")
        metric = Metric(name=name, description=description)
        metric.last_updated = datetime.datetime.now(pytz.timezone("Europe/Zurich"))
        db.session.add(metric)
        db.session.commit()
        return True

    @staticmethod
    def get_all():
        """

        :return: All the metrics
        :rtype: list[app.models.metric.Metric]
        """
        metrics = Metric.query.all()
        return metrics

    @staticmethod
    def get_by_name(name: str):
        """

        :return: All the metrics
        :rtype: list[app.models.metric.Metric]
        """
        metrics = Metric.query.filter_by(name=name).all()
        return metrics

    @staticmethod
    def get_by_name_period(name: str, from_date: str, to_date: str):
        """

        :return: All the metrics
        :rtype: list[app.models.metric.Metric]
        """
        metrics = (
            Metric.query.filter_by(name=name)
            .filter(Metric.created_date.between(from_date, to_date))
            .all()
        )
        return metrics

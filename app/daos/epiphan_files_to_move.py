import logging

from sqlalchemy.orm.exc import NoResultFound

from app.extensions import db
from app.models.epiphan_files_to_move import (
    EpiphanFilesToMove,
    EpiphanFilesToMoveStatus,
)

logger = logging.getLogger("webapp.daos_epiphan_files_to_move")


class EpiphanFilesToMoveDAOException(Exception):
    pass


class EpiphanFilesToMoveDAO:
    @staticmethod
    def get_all():
        elements = EpiphanFilesToMove.query.all()
        return elements

    @staticmethod
    def get_all_pending():
        """

        :return:
        :rtype: list[app.models.epiphan_files_to_move.EpiphanFilesToMove]
        """
        elements = EpiphanFilesToMove.query.filter_by(
            status=EpiphanFilesToMoveStatus.PENDING
        ).all()
        return elements

    @staticmethod
    def create(indico_id):

        try:
            EpiphanFilesToMove.query.filter_by(indico_id=indico_id).one()
            logger.info(f"The file to move with Indico ID {indico_id} already exists")
            return False
        except NoResultFound:
            logger.info(f"Creating FilesToMove  {indico_id}")
            file_to_move = EpiphanFilesToMove(indico_id=indico_id)
            db.session.add(file_to_move)
            db.session.commit()
            return True

    @staticmethod
    def set_status_by_indico_id(indico_id: int, new_status: str):
        try:
            file_to_move = EpiphanFilesToMove.query.filter_by(indico_id=indico_id).one()
            file_to_move.status = new_status
            db.session.commit()
            return True
        except Exception as e:
            logger.error(
                "Error: Unable to set status for EpiphanFilesToMove {}. (error:{})".format(
                    indico_id, e
                )
            )
            raise e

    @staticmethod
    def set_status(internal_id: int, new_status: str):
        try:
            file_to_move = EpiphanFilesToMove.query.get(internal_id)
            file_to_move.status = new_status
            db.session.commit()
            return True
        except Exception as e:
            logger.error(
                "Error: Unable to set status for EpiphanFilesToMove {}. (error:{})".format(
                    internal_id, e
                )
            )
            raise e

    @staticmethod
    def set_run_times(internal_id: int, new_run_times: int):
        try:
            file_to_move = EpiphanFilesToMove.query.get(internal_id)
            file_to_move.run_times = new_run_times
            db.session.commit()
            return True
        except Exception as e:
            logger.error(
                "Error: Unable to set run_times for EpiphanFilesToMove {}. (error:{})".format(
                    internal_id, e
                )
            )
            raise e

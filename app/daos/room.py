import logging

from app.extensions import db
from app.models.encoders import Room

logger = logging.getLogger("webapp.daos_room")


class RoomDAOException(Exception):
    pass


class RoomDAO:
    @staticmethod
    def get_or_create_room(room_name):
        if not room_name:
            room_object = Room.query.filter_by(indico_name="External URI").one()
        else:
            room_object = Room.query.filter_by(indico_name=room_name).one_or_none()
            if not room_object:
                room_object = Room(indico_name=room_name)
                db.session.add(room_object)
                db.session.commit()
        return room_object

    @staticmethod
    def get_supported_rooms(only_names=True):
        rooms = Room.query.filter_by(supported=True).all()
        if only_names:
            rooms = [room.indico_name for room in rooms]
        return rooms

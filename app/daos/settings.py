import logging

from app.extensions import db
from app.models.settings import Settings

logger = logging.getLogger("webapp.daos_settings")


class SettingsDAOException(Exception):
    pass


class SettingsDAO:
    @staticmethod
    def create():
        settings = Settings.query.get(1)

        if not settings:
            logger.info("No settings object found in the DB")
            settings = Settings()
            db.session.add(settings)
            db.session.commit()

    @staticmethod
    def get_all():
        """

        :return: The application settings
        :rtype: app.models.settings.Settings
        """
        settings = Settings.query.get(1)
        return settings

    @staticmethod
    def update(details):
        logger.info("Updating settings...")

        settings = Settings.query.get(1)

        if not settings:
            logger.info("Settings not set. Creating a new Settings object")
            settings = Settings()

        enable_ravem_notifications = details["enable_ravem_notifications"]
        enable_webcast_website_notifications = details[
            "enable_webcast_website_notifications"
        ]

        settings.enable_ravem_notifications = enable_ravem_notifications
        settings.enable_webcast_website_notifications = (
            enable_webcast_website_notifications
        )

        db.session.add(settings)
        db.session.commit()

        return settings

import datetime
import logging
from typing import Optional

import pytz
from flask_login import current_user

from app.daos.room import RoomDAO
from app.extensions import db
from app.models.contribution_workflow.transcription_workflow import (
    OpencastTranscriptionStates,
    TranscriptionStates,
)
from app.models.events import IndicoEventContribution


class IndicoEventContributionDAOException(Exception):
    pass


class IndicoEventContributionDAO:
    def __init__(self, logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("webapp.daos_indico_event_contribution")

    def get_all(self):
        """
        Get all the IndicoEventContribution objects
        :return: A list of contributions
        :rtype: list[app.models.IndicoEventContribution]
        """
        contributions = IndicoEventContribution.query.all()
        return contributions

    def get_by_contribution_id(self, contribution_id):
        """
        Get the IndicoEventContribution object by its contribution_id
        :return: A list of contributions
        :rtype: app.models.IndicoEventContribution
        """
        contribution = IndicoEventContribution.query.filter_by(contribution_id).one()
        return contribution

    def set_media_package_generated_status(
        self, contribution_internal_id, value, last_report=None
    ):
        """
        Set the value of media_package_generated_status
        :param contribution_id: Internal ID of the contribution
        :type contribution_id: int
        :param value: The new value
        :type value: str
        :return: True if success
        :rtype: bool
        """
        contribution = IndicoEventContribution.query.get(contribution_internal_id)
        contribution.media_package_generated_status = value
        contribution.media_package_generated_date = datetime.datetime.now(
            pytz.timezone("Europe/Zurich")
        )

        if last_report is not None:
            contribution.media_package_generated_last_report = last_report

        db.session.commit()
        return True

    def set_postprocessing_type(self, contribution_internal_id, new_type):
        contribution = IndicoEventContribution.query.get(contribution_internal_id)
        contribution.postprocessing_type = new_type
        contribution.postprocessing_type_date = datetime.datetime.now(
            pytz.timezone("Europe/Zurich")
        )
        db.session.commit()
        return True

    def set_do_not_transcribe(self, contribution_internal_id: int, value: bool) -> bool:
        contribution = IndicoEventContribution.query.get(contribution_internal_id)
        contribution.do_not_transcribe = value
        contribution.do_not_transcribe_date = datetime.datetime.now(
            pytz.timezone("Europe/Zurich")
        )
        db.session.commit()
        return True

    def set_sent_to_opencast_status(
        self, contribution_internal_id, value, last_report=None
    ):
        """
        Set sent_to_opencast value

        :return: True
        :rtype: str
        """
        contribution = IndicoEventContribution.query.get(contribution_internal_id)
        contribution.sent_to_opencast_status = value
        contribution.sent_to_opencast_date = datetime.datetime.now(
            pytz.timezone("Europe/Zurich")
        )

        if last_report is not None:
            contribution.sent_to_opencast_last_report = last_report

        db.session.commit()
        return True

    def set_cds_record(self, contribution_id, value):
        """
        Set the value of cds_record for the given contribution

        :param contribution_id: Indico ID of the contribution
        :type contribution_id: str
        :param value: Identifier of the CDS Record
        :type value: str
        :return: True on success
        :rtype: bool
        """
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        contribution.cds_record = value
        contribution.is_cds_record_set = True
        if value != "":
            contribution.is_cds_record_set_date = datetime.datetime.now(
                pytz.timezone("Europe/Zurich")
            )
        else:
            contribution.is_cds_record_set_date = None
        db.session.commit()
        return True

    def set_opencast_processing_finished(self, contribution_id, value):
        """
        Set the Opencast Processing as finished or not for the given contribution

        :param contribution_id:ID of the contribution (12345c1)
        :type contribution_id: str
        :param value: Status of the processing
        :type value: bool
        :return: True on success
        :rtype: bool
        """
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        contribution.opencast_processing_finished = value
        if value:
            contribution.opencast_processing_finished_date = datetime.datetime.now(
                pytz.timezone("Europe/Zurich")
            )
        else:
            contribution.opencast_processing_finished_date = None
        db.session.commit()
        return True

    def set_opencast_processing_status(self, contribution_id, value, last_report=None):
        """
        Set the Opencast Processing as failed or not for the given contribution

        :param last_report:
        :type last_report: str
        :param contribution_id:ID of the contribution (12345c1)
        :type contribution_id: str
        :param value: Status of the processing
        :type value: str
        :return: True on success
        :rtype: bool
        """
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        contribution.opencast_processing_status = value
        if value != "":
            contribution.opencast_processing_status_last_report = last_report
            contribution.opencast_processing_date = datetime.datetime.now(
                pytz.timezone("Europe/Zurich")
            )
        else:
            contribution.opencast_processing_date = None
        db.session.commit()
        return True

    def set_opencast_ready_for_cutting(self, contribution_id, value: bool):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        contribution.opencast_ready_for_cutting = value
        if value:
            contribution.opencast_ready_for_cutting_date = datetime.datetime.now(
                pytz.timezone("Europe/Zurich")
            )
        else:
            contribution.opencast_ready_for_cutting_date = None
        db.session.commit()
        return True

    def set_opencast_ready_for_cutting_status(self, contribution_id, value: str):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        contribution.opencast_ready_for_cutting_status = value
        if value != "":
            contribution.opencast_ready_for_cutting_date = datetime.datetime.now(
                pytz.timezone("Europe/Zurich")
            )
        else:
            contribution.opencast_ready_for_cutting_date = None
        db.session.commit()
        return True

    def set_opencast_acls_finished(self, contribution_id, value):
        """
        Set the value of opencast_acls_finished and its date for the given contribution

        :param contribution_id:ID of the contribution (12345c1)
        :type contribution_id: str
        :param value: Status of the processing
        :type value: bool
        :return: True on success
        :rtype: bool
        """
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        contribution.opencast_acls_finished = value
        if value:
            contribution.opencast_acls_finished_date = datetime.datetime.now(
                pytz.timezone("Europe/Zurich")
            )
        else:
            contribution.opencast_acls_finished_date = None
        db.session.commit()
        return True

    def set_eagreement_status(self, contribution_id, value):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        contribution.e_agreement_status = value
        contribution.e_agreement_status_date = datetime.datetime.now(
            pytz.timezone("Europe/Zurich")
        )
        db.session.commit()
        return True

    def update_last_updated(self, internal_id, action_taken, user=None):
        contribution = IndicoEventContribution.query.get(internal_id)
        self.logger.debug(
            f"{contribution.contribution_id}: Updating last updated to '{action_taken}'"
        )
        if user:
            contribution.last_updated_person = "Anonymous"
            contribution.last_updated_username = user
        else:
            contribution.last_updated_person = (
                current_user.first_name + " " + current_user.last_name
            )
            contribution.last_updated_username = current_user.username

        contribution.last_updated = datetime.datetime.now(
            pytz.timezone("Europe/Zurich")
        )
        contribution.last_updated_action = action_taken
        db.session.commit()
        return True

    def update_send_to_opencast_checkboxes(
        self, internal_id, edit_on_opencast, add_transcription
    ):
        """

        :param internal_id:
        :type internal_id: int
        :param edit_on_opencast: true|false
        :type edit_on_opencast: str
        :param add_transcription: true|false
        :type add_transcription: str
        :return:
        :rtype:
        """
        contribution = IndicoEventContribution.query.get(internal_id)
        contribution.edit_on_opencast = False if edit_on_opencast == "false" else True
        contribution.add_transcription = False if add_transcription == "false" else True
        db.session.commit()
        return True

    def set_opencast_uid(self, internal_id, opencast_uid):
        contribution = IndicoEventContribution.query.get(internal_id)
        contribution.opencast_uid = opencast_uid
        db.session.commit()
        return True

    def set_opencast_uid_by_contribution_id(self, contribution_id, opencast_uid):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one()
        return self.set_opencast_uid(contribution.id, opencast_uid)

    def set_sent_to_opencast_task_id(self, internal_id, value):
        contribution = IndicoEventContribution.query.get(internal_id)
        self.logger.debug(
            f"{contribution.contribution_id}: Task ID will be set to {value}"
        )
        contribution.sent_to_opencast_task_id = value
        db.session.commit()
        return True

    def set_recording_path(self, internal_id, value):
        contribution = IndicoEventContribution.query.get(internal_id)
        if not value.startswith("https") and not value.startswith("http"):
            if not value.startswith(r"\\"):
                contribution.recording_path = r"\\" + value

        contribution.recording_path = value
        contribution.recording_path_date = datetime.datetime.now(
            pytz.timezone("Europe/Zurich")
        )
        db.session.commit()
        return True

    def create(self, contribution_dict):
        self.logger.debug(
            f"Creating a new Contribution {contribution_dict['contribution_id']}"
        )
        contribution_id = contribution_dict["contribution_id"]

        room_name = contribution_dict["room"]
        room_object = RoomDAO.get_or_create_room(room_name)

        self.logger.debug(
            f"{contribution_dict['contribution_id']}: Room will be set to {room_name}"
        )

        existing_contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one_or_none()
        if existing_contribution:
            raise IndicoEventContributionDAOException(
                f"The contribution {contribution_id} already exists"
            )

        existing_contribution = IndicoEventContribution()
        existing_contribution.room_id = room_object.id
        existing_contribution = self.assign_contribution_attrs(
            contribution_id, contribution_dict, existing_contribution
        )

        self.logger.debug(f"Created {contribution_id}. (Not persisted yet)")

        return existing_contribution

    def update(self, contribution_id, contribution_dict):
        self.logger.debug("Updating an existing Contribution")
        existing_contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one_or_none()
        if not existing_contribution:
            raise IndicoEventContributionDAOException(
                f"The contribution {contribution_id} doesn't exists"
            )

        room_name = contribution_dict["room"]
        room_object = RoomDAO.get_or_create_room(room_name)
        existing_contribution.room_id = room_object.id
        existing_contribution = self.assign_contribution_attrs(
            contribution_id, contribution_dict, existing_contribution
        )

        return existing_contribution

    def assign_contribution_attrs(
        self, contribution_id, contribution_dict, contribution
    ):
        title = contribution_dict["title"]
        start_date = contribution_dict["start_date"]
        start_time = contribution_dict["start_time"]
        end_date = contribution_dict["end_date"]
        end_time = contribution_dict["end_time"]
        contribution_type = contribution_dict["_type"]
        description = contribution_dict["description"]
        url = contribution_dict["url"]
        db_id = contribution_dict["db_id"]
        db_id_full = contribution_dict["db_id_full"]
        session = contribution_dict["session"]
        allowed = contribution_dict["allowed"]
        speakers = contribution_dict["speakers"]
        event_type = contribution_dict["type"]

        contribution.title = title
        contribution.start_date = start_date
        contribution.start_time = start_time
        contribution.end_date = end_date
        contribution.end_time = end_time
        contribution.contribution_type = contribution_type
        contribution.description = description
        contribution.url = url
        contribution.db_id = db_id
        contribution.db_id_full = db_id_full
        contribution.session = session
        contribution.allowed = allowed
        contribution.contribution_id = contribution_id
        contribution.speakers = speakers
        contribution.event_type = event_type

        return contribution

    def delete_by_contribution_id(self, contribution_id):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one_or_none()
        if contribution:
            db.session.delete(contribution)
            db.session.commit()
            return True
        else:
            raise IndicoEventContributionDAOException(
                f"Unable to delete contribution {contribution_id}. It doesn't exist"
            )

    def set_contribution_acls_status(
        self, contribution_id, requested_value, status, last_report
    ):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one_or_none()
        if contribution:
            contribution.opencast_acls_requested = requested_value
            contribution.opencast_acls_requested_status = status
            contribution.opencast_acls_requested_last_report = last_report
            new_date = datetime.datetime.now(pytz.timezone("Europe/Zurich"))
            if status == "":
                contribution.opencast_acls_requested_date = None
            else:
                contribution.opencast_acls_requested_date = new_date
            db.session.commit()
        else:
            raise IndicoEventContributionDAOException(
                f"Unable to set acls contribution {contribution_id}. It doesn't exist"
            )

    def set_cds_record_status(
        self, contribution_id, requested_value, status, last_report
    ):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one_or_none()
        if contribution:
            contribution.is_cds_record_requested = requested_value
            contribution.is_cds_record_requested_status = status
            contribution.is_cds_record_requested_last_report = last_report
            new_date = datetime.datetime.now(pytz.timezone("Europe/Zurich"))
            if status == "":
                contribution.is_cds_record_requested_date = None
            else:
                contribution.is_cds_record_requested_date = new_date
            db.session.commit()
        else:
            raise IndicoEventContributionDAOException(
                "Unable to set cds record status for "
                f"contribution {contribution_id}. It doesn't exist"
            )

    def set_cds_subformats_request_status(
        self, contribution_id, requested_value, status, last_report
    ):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one_or_none()
        if contribution:
            contribution.is_cds_subformats_requested = requested_value
            contribution.is_cds_subformats_requested_status = status
            contribution.is_cds_subformats_requested_last_report = last_report
            new_date = datetime.datetime.now(pytz.timezone("Europe/Zurich"))
            if status == "":
                contribution.is_cds_subformats_requested_date = None
            else:
                contribution.is_cds_subformats_requested_date = new_date
            db.session.commit()
        else:
            raise IndicoEventContributionDAOException(
                "Unable to set cds subformats request status"
                f"for contribution {contribution_id}. It doesn't exist"
            )

    def set_transcription_status(
        self,
        contribution_id,
        new_status: TranscriptionStates,
        media_id: Optional[str] = None,
    ):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one_or_none()

        if contribution:
            contribution.transcription_requested_status = new_status
            new_date = datetime.datetime.now(pytz.timezone("Europe/Zurich"))
            contribution.transcription_requested_date = new_date
            if media_id:
                contribution.transcription_media_id = media_id
            db.session.commit()
            return True

        raise IndicoEventContributionDAOException(
            f"Unable to set transcription status for contribution {contribution_id}. It doesn't exist"
        )

    def set_transcription_error_message(self, contribution_id, error_message: str):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one_or_none()

        if contribution:
            contribution.transcription_requested_error_message = error_message
            db.session.commit()
            return True

        raise IndicoEventContributionDAOException(
            f"Unable to set transcription requested error message for contribution {contribution_id}. It doesn't exist"
        )

    def set_transcription_update_status(
        self, contribution_id, new_status: OpencastTranscriptionStates
    ):
        contribution = IndicoEventContribution.query.filter_by(
            contribution_id=contribution_id
        ).one_or_none()

        if contribution:
            contribution.transcription_update_status = new_status
            new_date = datetime.datetime.now(pytz.timezone("Europe/Zurich"))
            contribution.transcription_update_date = new_date
            db.session.commit()
            return True

        raise IndicoEventContributionDAOException(
            f"Unable to set transcription update status for contribution {contribution_id}. It doesn't exist"
        )

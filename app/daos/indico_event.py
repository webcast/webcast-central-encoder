import logging

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.daos.room import RoomDAO
from app.extensions import db
from app.models.events import IndicoEvent, IndicoEventContribution


class IndicoEventDAOException(Exception):
    pass


class IndicoEventDAO:
    def __init__(self, logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("webapp.daos_indico_event")

    def set_recording_path(self, internal_id, value):
        try:
            indico_event = IndicoEvent.query.get(internal_id)
            indico_event.recording_path = value
            db.session.commit()
            return True
        except Exception as e:
            self.logger.error(
                f"Error: Unable to set recording path for IndicoEvent {internal_id}. (error:{e})"
            )
            raise e

    def create(self, event_dict):
        self.logger.debug("Creating a new IndicoEvent")

        indico_id = event_dict["indico_id"]
        room_name = event_dict["room"]
        contributions = event_dict["contributions"]

        existing_event = IndicoEvent.query.filter_by(indico_id=indico_id).one_or_none()
        if existing_event:
            raise IndicoEventDAOException(f"The event {indico_id} already exists")

        room_object = RoomDAO.get_or_create_room(room_name)
        event = IndicoEvent(indico_id=indico_id)
        event = self.assign_event_attrs(event, event_dict, room_object)

        db.session.add(event)
        db.session.commit()

        self.logger.debug("Adding contributions to event")
        for contribution in contributions:
            self.logger.debug(f"Adding contribution {contribution['contribution_id']}")
            self.create_and_append_contribution_to_event(event, contribution)
            if len(contribution["contributions"]) > 0:
                for sub_contribution in contribution["contributions"]:
                    self.logger.debug(
                        f"Adding sub_contribution {sub_contribution['contribution_id']}".format()
                    )
                    self.create_and_append_contribution_to_event(
                        event, sub_contribution
                    )
        return True

    def update(self, indico_id, event_dict):
        self.logger.debug(f"{indico_id}: Updating a existing IndicoEvent")

        room_name = event_dict["room"]
        contributions = event_dict["contributions"]

        existing_event = IndicoEvent.query.filter_by(indico_id=indico_id).one_or_none()
        if not existing_event:
            raise IndicoEventDAOException(
                f"{indico_id}: The event {indico_id} cannot be updated. It DOES NOT exist"
            )

        room_object = RoomDAO.get_or_create_room(room_name)
        existing_event = self.assign_event_attrs(
            existing_event, event_dict, room_object
        )
        db.session.commit()

        current_contributions_ids = [
            contribution.contribution_id
            for contribution in existing_event.contributions
        ]
        found_contributions_ids = []
        self.logger.debug(f"{indico_id}: Updating event contributions")
        indico_contribution_dao = IndicoEventContributionDAO(self.logger)
        for contribution_dict in contributions:
            current_id = contribution_dict["contribution_id"]
            self.logger.debug(f"{indico_id}: Updating contribution {current_id}")
            found_contributions_ids.append(current_id)
            contribution_object = IndicoEventContribution.query.filter_by(
                contribution_id=current_id
            ).one_or_none()
            if contribution_object:
                self.logger.debug(
                    f"{indico_id}: Contribution {current_id} already exists"
                )
                indico_contribution_dao.update(current_id, contribution_dict)
                db.session.commit()
            else:
                self.logger.debug(
                    f"{indico_id}: Contribution {current_id} DOES NOT exist"
                )
                self.create_and_append_contribution_to_event(
                    existing_event, contribution_dict
                )
            if len(contribution_dict["contributions"]) > 0:
                for sub_contribution_dict in contribution_dict["contributions"]:
                    sub_contribution_current_id = sub_contribution_dict[
                        "contribution_id"
                    ]
                    self.logger.debug(
                        f"Updating sub_contribution {sub_contribution_current_id}"
                    )
                    found_contributions_ids.append(sub_contribution_current_id)
                    sub_contribution = IndicoEventContribution.query.filter_by(
                        contribution_id=sub_contribution_current_id
                    ).one_or_none()
                    if sub_contribution:
                        self.logger.debug(
                            f"SubContribution {current_id} already exists"
                        )
                        indico_contribution_dao.update(
                            sub_contribution_current_id, sub_contribution_dict
                        )
                        db.session.commit()
                    else:
                        self.logger.debug(
                            f"SubContribution {current_id} already exists"
                        )
                        self.create_and_append_contribution_to_event(
                            existing_event, sub_contribution_dict
                        )

        self.logger.debug("Remove contributions not in the new list")
        for contribution_id in current_contributions_ids:
            if contribution_id not in found_contributions_ids:
                indico_contribution_dao.delete_by_contribution_id(contribution_id)

        return True

    def create_and_append_contribution_to_event(
        self, existing_event, sub_contribution_dict
    ):
        self.logger.debug(
            f"{existing_event.indico_id}: Creating a new IndicoEventContribution"
        )
        indico_contribution_dao = IndicoEventContributionDAO(self.logger)
        contribution_object = indico_contribution_dao.create(sub_contribution_dict)

        self.logger.debug(
            f"{existing_event.indico_id}: Contribution {contribution_object.contribution_id} created. Adding to event"
        )

        existing_event.contributions.append(contribution_object)
        db.session.add(contribution_object)
        db.session.commit()

        self.logger.debug(
            f"{existing_event.indico_id}: Contribution {contribution_object.contribution_id} added to event"
        )

    def assign_event_attrs(self, event, event_dict, room_object):
        title = event_dict["title"]
        first_contrib = event_dict["first_contrib"]
        audience = event_dict["audience"]
        date = event_dict["date"]
        time = event_dict["time"]
        start_date_time = event_dict["start_date_time"]
        end_date_time = event_dict["end_date_time"]
        recording = event_dict["recording"]
        webcast = event_dict["webcast"]
        static = event_dict["static"]
        custom_app_name = event_dict["custom_app_name"]
        description = event_dict["description"]
        allowed = event_dict["allowed"]
        creator = event_dict["creator"]
        creator_id = event_dict["creator_id"]
        creator_email = event_dict["creator_email"]
        category = event_dict["category"]
        category_id = event_dict["category_id"]
        event_type = event_dict["type"]

        # Create the event
        event.title = title
        event.first_contrib = first_contrib
        event.room_id = room_object.id
        event.audience = audience
        event.date = date
        event.time = time
        event.start_date_time = start_date_time
        event.end_date_time = end_date_time
        event.end_date = end_date_time.strftime("%Y-%m-%d")
        event.description = description
        event.recording = recording
        event.webcast = webcast
        event.static = static
        event.allowed = allowed
        event.custom_app_name = custom_app_name
        event.creator = creator
        event.creator_id = creator_id
        event.creator_email = creator_email
        event.category = category
        event.category_id = category_id
        event.event_type = event_type
        return event

    def set_webcast_running(self, internal_id, value):
        try:
            event = IndicoEvent.query.get(internal_id)
            event.is_webcast_running = value
            db.session.commit()
            return True
        except Exception as e:
            self.logger.error(
                f"Error: Unable to set is_webcast_running for IndicoEvent {internal_id}. (error:{e})"
            )
            raise e

    def set_recording_running(self, internal_id, value):
        try:
            event = IndicoEvent.query.get(internal_id)
            event.is_recording_running = value
            db.session.commit()
            return True
        except Exception as e:
            self.logger.error(
                f"Error: Unable to set is_recording_running for IndicoEvent {internal_id}. (error:{e})"
            )
            raise e

    def set_is_recorded(self, internal_id, value):
        try:
            event = IndicoEvent.query.get(internal_id)
            event.is_recorded = value
            db.session.commit()
            return True
        except Exception as e:
            self.logger.error(
                f"Error: Unable to set is_recorded for IndicoEvent {internal_id}. (error:{e})"
            )
            raise e

    def set_pause_for_splitting(self, internal_id, value):
        try:
            event = IndicoEvent.query.get(internal_id)
            event.paused_for_splitting = value
            db.session.commit()
            return True
        except Exception as e:
            self.logger.error(
                f"Error: Unable to set paused_for_splitting for IndicoEvent {internal_id}. (error:{e})"
            )
            raise e

    def set_stop_time(self, internal_id, value):
        try:
            event = IndicoEvent.query.get(internal_id)
            event.stop_time = value
            db.session.commit()
            return True
        except Exception as e:
            self.logger.error(
                f"Error: Unable to set stop_time for IndicoEvent {internal_id}. (error:{e})"
            )
            raise e

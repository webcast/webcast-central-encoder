import logging

from flask import flash, redirect, render_template, request, session, url_for
from flask_login import current_user

from app.forms.dashboard_forms import FetchEventsForm, FetchSingleEventForm
from app.services.indico.indico_service_2 import IndicoServiceV2
from app.views.blueprints import dashboard2_blueprint
from app.views.decorators import admin_required

error_msg = None

logger = logging.getLogger("webapp.dashboard2")


def _fetch_events_from_indico():
    logger.info("User %s Forced to fetching events from Indico" % current_user.username)
    try:
        indico_service = IndicoServiceV2(logger=logger)
        result = indico_service.get_events()

        if result == "OK":
            return None
        else:
            return result
    except Exception as ex:
        logger.exception("Exception: %s" % ex)
        return ex


@dashboard2_blueprint.route("/", methods=("GET",))
@admin_required
def home():
    if session.get("next_url"):
        next_url = session.get("next_url")
        session.pop("next_url", None)
        return redirect(next_url)

    user = {}
    logger.info("Loading Dashboard")

    if request.args.get("forced"):
        error_msg = _fetch_events_from_indico()
        logger.debug(error_msg)
        # all the needed context data are set by the ajax form selector mixin

    user["id"] = current_user.id

    fetch_events_form = FetchEventsForm()
    fetch_single_event_form = FetchSingleEventForm()

    return render_template(
        "dashboard2/home.html",
        user=user,
        fetch_events_form=fetch_events_form,
        fetch_single_event_form=fetch_single_event_form,
    )


@dashboard2_blueprint.route("/fetch-events/", methods=("POST",))
@admin_required
def fetch_events():
    logger.info("Fetch events")
    form = FetchEventsForm()
    if form.validate_on_submit():
        logger.info(
            "User %s Forced to fetching events from Indico" % current_user.username
        )
        try:
            indico_service = IndicoServiceV2(logger=logger)
            result, message = indico_service.get_events(static=False)
            if result == "OK":
                flash(message, "success")

        except Exception as ex:
            logger.exception("Exception: %s" % ex)
            flash("Failed to fetch events from Indico %s" % ex, "error")
    return redirect(url_for("dashboard2.blueprints.home"))


@dashboard2_blueprint.route("/fetch-single-event/", methods=("POST",))
@admin_required
def fetch_single_event():
    form = FetchSingleEventForm()
    if form.validate_on_submit():
        indico_id = form.indico_id.data
        event_type = form.event_type.data
        audience = form.audience.data
        logger.info(
            f"User {current_user.username} fetched single event from Indico {indico_id}"
        )

        has_webcast = False
        if event_type == "webcast_recording":
            has_webcast = True

        try:
            indico_service = IndicoServiceV2(logger=logger)
            result, message = indico_service.get_event(
                indico_id, audience=audience, webcast=has_webcast
            )
            logger.debug(result)
            logger.debug(message)
            if result == "OK":
                flash(message, "success")
            else:
                flash(message, "error")

        except Exception as ex:
            logger.exception("Exception: %s" % ex)
            flash("Failed to fetch event from Indico %s" % ex, "error")
    return redirect(url_for("dashboard2.blueprints.home"))

from flask import Blueprint


def _blueprint_factory(partial_module_string, url_prefix):
    """
    Generates blueprint objects for view modules.

    :param partial_module_string:  String representing a view module without the absolute path
    (e.g. 'home.index' for pypi_portal.views.home.index).
    :param url_prefix: URL prefix passed to the blueprint.
    :return: Blueprint instance for a view module.
    """
    name = partial_module_string
    import_name = "app.views.{}".format(partial_module_string)
    blueprint = Blueprint(name, import_name, url_prefix=url_prefix)
    return blueprint


loginView_blueprint = _blueprint_factory("authentication.login_view", "")

admin_custom_actions = _blueprint_factory(
    "custom_admin.admin_actions.actions_blueprint", "/_admin_actions/"
)

admin_forms_handlers = _blueprint_factory(
    "custom_admin.forms.blueprints", "/_admin_forms/"
)

dashboard2_blueprint = _blueprint_factory("dashboard2.blueprints", "/")

all_blueprints = (
    loginView_blueprint,
    admin_custom_actions,
    admin_forms_handlers,
    dashboard2_blueprint,
)

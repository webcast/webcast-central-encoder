from flask import render_template, request, session
from flask_login import login_required, logout_user

from app.views.blueprints import loginView_blueprint


@loginView_blueprint.route("/logout")
@login_required
def logout():
    """
    View to log out a user on the site
    :return: Redirects to the users.index after logout
    """
    logout_user()
    return render_template("dashboard2/login.html")


@loginView_blueprint.route("/login")
def login():
    """
    Display the login page
    :return:
    """
    session["next_url"] = request.args.get("next")
    return render_template("dashboard2/login.html")

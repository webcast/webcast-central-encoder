from flask import redirect, request, url_for
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user


class AdminProtectedView(ModelView):
    """
    Handles the authentication of the Flask Admin views
    """

    def is_accessible(self):
        """
        If the user is admin, access will be granted and will return True
        :return: (True|False)
        """
        try:
            return current_user.is_admin
        except AttributeError:
            return False

    def inaccessible_callback(self, name, **kwargs):
        """
        If the user doesn't have access, he will be redirected to the loginView.login page
        :param name:
        :param kwargs:
        :return: A Flask redirect
        """
        # redirect to login page if user doesn't have access
        return redirect(url_for("authentication.login_view.login", next=request.url))

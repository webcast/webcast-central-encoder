import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.extensions import db
from app.forms.opencast_workflow_forms import ContributionIdForm
from app.models.events import IndicoEventContribution
from app.services.mailer.eagreement_email_service import EagreemenentEmailService
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.email")


def send_agreement_reminder(contribution):
    response = EagreemenentEmailService(contribution, logger=logger).send_reminder()
    indico_contribution_dao = IndicoEventContributionDAO()
    indico_contribution_dao.update_last_updated(contribution.id, "Send email reminder")

    if response:
        contribution.e_agreement_status = "SENT"
        db.session.commit()
        result = True
    else:
        result = False

    display_messages(contribution, result)


def display_messages(contribution, result):
    response_data = {}
    if result:
        logger.info(
            "{}: E-Agreement reminder sent successfully.".format(
                contribution.contribution_id
            )
        )

        response_data["result"] = "success"
        response_data[
            "msg"
        ] = "E-Agreement reminder sent successfully for contribution {}".format(
            contribution.contribution_id
        )
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data["msg"] = "Unable to send reminder for e-agreement."
        flash(response_data["msg"], "error")


@admin_custom_actions.route("/_send_email_reminder/", methods=("POST",))
@login_required
def send_agreement_reminder_view():
    form = ContributionIdForm()
    if request.method == "POST":
        contribution_internal_id = form.contribution_id.data
        if form.validate_on_submit():
            contribution_internal_id = form.contribution_id.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)
            logger.info(
                "{}: Sending email reminder...".format(contribution.contribution_id)
            )
            send_agreement_reminder(contribution)

            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view", id=contribution_internal_id
                )
            )

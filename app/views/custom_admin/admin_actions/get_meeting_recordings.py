import json
import logging

from flask import request
from flask.views import View

from app.extensions import csrf
from app.forms.get_zoom_recordings_form import GetZoomRecordingsForm
from app.services.zoom.zoom_api import ZoomApiClient

logger = logging.getLogger("webapp.get_meeting_recordings")


class GetMeetingRecordings(View):
    prefix = ""
    methods = ["POST"]
    decorators = [csrf.exempt]

    def dispatch_request(self):
        logger.debug("Dispatching request...")
        form_class = GetZoomRecordingsForm(request.form)
        if request.method == "POST":
            self.form_valid(form_class)

            if form_class.validate() is True:
                response_data = self.form_valid(form_class)
                return json.dumps({"success": True, "response_data": response_data})
            else:
                return json.dumps(
                    {"success": False, "response_data": self.form_invalid(form_class)}
                )

    def form_valid(self, form):
        try:
            meeting_id = form.meeting_id.data
            zoom_client = ZoomApiClient.get_instance()
            result = zoom_client.recording.get(meeting_id=meeting_id)
            json_result = result.json()
            logger.debug(json_result)
            return {"result": "success", "msg": json_result}

        except Exception as ex:
            return {"result": "error", "msg": str(ex)}

    def form_invalid(self, form):
        response_data = {"result": "error", "msg": "Invalid form"}
        return response_data

import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

import app.models.events
from app.extensions import celery
from app.forms.opencast_workflow_forms import SendToOpencastForm
from app.models.events import IndicoEventContribution
from app.views.blueprints import admin_custom_actions
from app.views.custom_admin.admin_actions.utils import reset_previous_statuses

logger = logging.getLogger("webapp.opencast")


def display_messages(
    contribution: app.models.events.IndicoEventContribution, result: bool
):
    response_data = {}
    if result:
        logger.info(f"{contribution.contribution_id}: Finished requesting ingest.")
        response_data["result"] = "success"
        response_data[
            "msg"
        ] = f"Ingest cancelled for contribution {contribution.contribution_id}"
    else:
        response_data["result"] = "error"
        response_data[
            "msg"
        ] = f"Unable to cancel ingest for contribution {contribution.contribution_id}"
    flash(response_data["msg"], response_data["result"])
    return response_data


def cancel_send_to_opencast_action(
    contribution: app.models.events.IndicoEventContribution,
):
    result = False
    if contribution.is_send_to_opencast_running:
        # Check if there is a task ID and abort the associated task
        if (
            contribution.sent_to_opencast_task_id != ""
            and contribution.sent_to_opencast_task_id is not None
        ):
            celery.control.revoke(contribution.sent_to_opencast_task_id, terminate=True)
            # Reset the current statuses
        reset_previous_statuses(contribution, pending=False)
        result = True
    display_messages(contribution, result)


@admin_custom_actions.route("/_cancel_send_opencast/", methods=("POST",))
@login_required
def cancel_send_opencast_view():
    form = SendToOpencastForm()
    if request.method == "POST":
        if form.validate_on_submit():
            contribution_internal_id = form.contribution_id.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)
            #
            logger.info(f"{contribution.contribution_id}: Cancel sending to Opencast")
            cancel_send_to_opencast_action(contribution)

            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view", id=contribution_internal_id
                )
            )

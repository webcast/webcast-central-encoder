from app.views.blueprints import admin_custom_actions
from app.views.custom_admin.admin_actions.force_as_accepted import ForceAsAccepted
from app.views.custom_admin.admin_actions.force_as_processed import ForceAsProcessed
from app.views.custom_admin.admin_actions.get_meeting_recordings import (
    GetMeetingRecordings,
)
from app.views.custom_admin.admin_actions.request_acls_update_opencast import (
    RequestOpencastAclsUpdate,
)
from app.views.custom_admin.admin_actions.request_cds_record import RequestCdsRecord
from app.views.custom_admin.admin_actions.request_cds_subformats import (
    RequestCdsSubformats,
)

from .add_cds_link_to_indico import add_cds_link_to_indico_view
from .cancel_send_opencast import cancel_send_opencast_view
from .create_event_folders import create_event_folders_view
from .create_event_splits import create_contribution_splits_view
from .create_snow_ticket import create_snow_ticket_view
from .fetch_eagreement import fetch_speaker_release_view
from .publish_video_player_link import publish_video_player_link_view
from .request_transcription import (
    request_transcription_update_rundeck_view,
    request_transcription_view,
)
from .send_email_reminder import send_agreement_reminder_view
from .send_to_opencast import send_to_opencast_view
from .set_do_not_transcribe import set_do_not_transcribe_view
from .set_postprocessing_type import set_postprocessing_type_view
from .set_recording_path import set_recording_path_view

admin_custom_actions.add_url_rule(
    "_force_as_accepted/", view_func=ForceAsAccepted.as_view("force_eagreement")
)

admin_custom_actions.add_url_rule(
    "_create_cds_record/", view_func=RequestCdsRecord.as_view("create_cds_record")
)
admin_custom_actions.add_url_rule(
    "_create_cds_subformats/",
    view_func=RequestCdsSubformats.as_view("create_cds_subformats"),
)
admin_custom_actions.add_url_rule(
    "_request_acls_update_opencast/",
    view_func=RequestOpencastAclsUpdate.as_view("request_acls_update_opencast"),
)
admin_custom_actions.add_url_rule(
    "_force_as_processed/", view_func=ForceAsProcessed.as_view("force_as_processed")
)
admin_custom_actions.add_url_rule(
    "_get_meeting_recordings/",
    view_func=GetMeetingRecordings.as_view("get_meeting_recordings"),
)

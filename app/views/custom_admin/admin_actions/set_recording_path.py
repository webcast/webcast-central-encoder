import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import ContributionIdForm
from app.models.events import IndicoEventContribution
from app.services.recording_path_service import RecordingPathService
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.recording_path")


@admin_custom_actions.route("/_set_recording_path/", methods=("POST",))
@login_required
def set_recording_path_view():
    form = ContributionIdForm()
    if request.method == "POST":
        contribution_internal_id = form.contribution_id.data
        if form.validate_on_submit():
            contribution = IndicoEventContribution.query.get(contribution_internal_id)
            logger.info(f"{contribution.contribution_id}: Setting up recording path...")
            path_set = set_recording_path_action(contribution)
            display_set_path_messages(contribution, path_set)

        return redirect(
            url_for("indicoeventcontribution.edit_view", id=contribution_internal_id)
        )


def set_recording_path_action(contribution):
    path_set = RecordingPathService().set_contribution_recording_path(contribution)
    indico_contribution_dao = IndicoEventContributionDAO()
    indico_contribution_dao.update_last_updated(contribution.id, "Set recording path")

    return path_set


def display_set_path_messages(contribution, path_set):
    response_data = {}
    if path_set:
        logger.info(
            f"{contribution.contribution_id}: Finished setting up recording path."
        )

        response_data["result"] = "success"
        response_data[
            "msg"
        ] = f"{contribution.contribution_id}: Recording path set for contribution"
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data[
            "msg"
        ] = """Unable to set the recording path.
        Event has no recording_path (Stop button on CES was not clicked)"""
        flash(response_data["msg"], "error")
    return response_data

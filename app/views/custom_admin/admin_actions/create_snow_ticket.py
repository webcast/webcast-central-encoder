import logging

from flask import current_app, flash, redirect, request, url_for
from flask_login import login_required

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import ContributionIdForm
from app.models.events import IndicoEventContribution
from app.services.service_now.service_now_service import ServiceNowService
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.snow")


def create_snow_ticket_action(contribution):
    # We create the SNOW ticket if possible
    instance_hostname = current_app.config["INSTANCE_HOSTNAME"]
    contribution_id = contribution.id
    text = f"""
        The contribution https://{instance_hostname}/admin/indicoeventcontribution/edit/?id={contribution_id}
        is ready for postprocessing.
        Please start postprocessing it with the related ServiceNow ticket."""

    swow_result = ServiceNowService(contribution).create_ticket()
    if swow_result and swow_result.get("number", None):
        ServiceNowService(contribution, logger=logger).add_work_note(text)
    response_data = display_messages(contribution, swow_result)
    return response_data


def display_messages(contribution, result):
    """
    Display the messages that will appear on the response and on the website as flash message (Flask)

    :param contribution: The contribution that is handled
    :type contribution: app.models.events.IndicoEventContribution
    :param result: Dictionary with the Service NOW API response
    :type result: dict
    :return: Dictionary with the result, msg and result
    :rtype: dict
    """
    response_data = {}
    if result and result.get("number", None):
        logger.info("Finished creating SNOW ticket for contribution")
        logger.info(
            "Finished creating SNOW ticket for contribution {}".format(
                contribution.contribution_id
            )
        )

        response_data["result"] = "success"
        response_data[
            "msg"
        ] = f"{contribution.contribution_id}: Created SNOW ticket for contribution".format()
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data[
            "msg"
        ] = "Unable to create a SNOW ticket. Invalid response from the SNOW API"
        flash(response_data["msg"], "error")


@admin_custom_actions.route("/_create_snow_ticket/", methods=("POST",))
@login_required
def create_snow_ticket_view():
    form = ContributionIdForm()
    if request.method == "POST":
        contribution_internal_id = form.contribution_id.data
        if form.validate_on_submit():
            contribution_internal_id = form.contribution_id.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)
            indico_contribution_dao = IndicoEventContributionDAO()
            indico_contribution_dao.update_last_updated(
                contribution_internal_id, "Create SNOW ticket"
            )

            if contribution.recording_path == "":
                raise Exception(
                    "Recording path not set for this contribution. Please, set it first."
                )

            logger.info(
                "{}: Creating SNOW ticket for contribution.".format(
                    contribution.contribution_id
                )
            )

            create_snow_ticket_action(contribution)
            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view", id=contribution_internal_id
                )
            )

import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import SetPostprocessingTypeForm
from app.models.events import IndicoEventContribution, PostprocessingType
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.postprocessing_type")


@admin_custom_actions.route("/_set_postprocessing_type/", methods=("POST",))
@login_required
def set_postprocessing_type_view():
    form = SetPostprocessingTypeForm()
    if request.method == "POST":
        contribution_internal_id = form.contribution_id.data
        if form.validate_on_submit():
            contribution_internal_id = form.contribution_id.data
            postprocessing_type = form.postprocessing_type.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)
            logger.info(
                "{}: Setting postprocessing type...".format(
                    contribution.contribution_id
                )
            )
            set_postprocessing_type_action(contribution, postprocessing_type)

        return redirect(
            url_for("indicoeventcontribution.edit_view", id=contribution_internal_id)
        )


def set_postprocessing_type_action(contribution, new_type):
    success = True
    try:
        if new_type not in [
            PostprocessingType.PLAIN_VIDEO,
            PostprocessingType.WEB_LECTURE,
        ]:
            raise Exception("Unknown Postprocessing type: {}".format(new_type))
        indico_contribution_dao = IndicoEventContributionDAO()
        indico_contribution_dao.set_postprocessing_type(contribution.id, new_type)
        indico_contribution_dao.update_last_updated(
            contribution.id, "Set recording path"
        )
    except Exception as e:
        success = False
        logger.error(e)
    response_data = display_messages(contribution, success)

    return response_data


def display_messages(contribution, was_success):
    response_data = {}
    if was_success:
        logger.info(
            "{}: Finished setting up postprocessing_type.".format(
                contribution.contribution_id
            )
        )

        response_data["result"] = "success"
        response_data[
            "msg"
        ] = f"Postprocessing type set for contribution {contribution.contribution_id}"
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data["msg"] = "Unable to set postprocessing type."
        flash(response_data["msg"], "error")
    return response_data

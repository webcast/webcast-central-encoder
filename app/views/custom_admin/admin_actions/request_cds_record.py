import logging

from flask import flash, redirect, request, url_for
from flask.views import View

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import ContributionIdForm
from app.models.events import IndicoEventContribution
from app.services.cds.cds_record_service import CdsRecordService

logger = logging.getLogger("webapp.cds")


def display_messages(contribution, result):
    response_data = {}
    if result:
        logger.info(f"{contribution.contribution_id}: Finished requesting CDS record.")

        response_data["result"] = "success"
        response_data[
            "msg"
        ] = f"{contribution.contribution_id}: Requested CDS record for contribution"
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data[
            "msg"
        ] = "Unable to request CDS record. Invalid response from the CDS API"
        flash(response_data["msg"], "error")
    return response_data


class RequestCdsRecord(View):
    prefix = ""

    methods = ["POST"]

    def dispatch_request(self):
        form_class = ContributionIdForm(request.form)
        logger.info("dispathing request...")
        if request.method == "POST":
            self.form_valid(form_class)
            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view",
                    id=form_class.contribution_id.data,
                )
            )

    def form_valid(self, form):
        try:
            contribution_internal_id = form.contribution_id.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)

            logger.info("{}: Creating CDS record.".format(contribution.contribution_id))
            indico_contribution_dao = IndicoEventContributionDAO()
            indico_contribution_dao.update_last_updated(
                contribution.id, "Request CDS record"
            )
            # We create the SNOW ticket if possible
            result = CdsRecordService(
                contribution, logger=logger
            ).request_create_cds_record()
            response_data = display_messages(contribution, result)

            return response_data

        except Exception as ex:
            logger.exception("Exception on request_cds_record: %s" % ex)
            response_data = {
                "result": "error",
                "msg": "Failed to request the CDS record because of the error %s" % ex,
            }
            flash(response_data["msg"], "error")
            return response_data

    def form_invalid(self, form):
        response_data = {}
        response_data["result"] = "error"
        response_data["msg"] = "Invalid form"
        return response_data

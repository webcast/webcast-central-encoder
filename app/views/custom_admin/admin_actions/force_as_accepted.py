import logging

from flask import flash, redirect, request, url_for
from flask.views import View

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import ContributionIdForm
from app.models.events import IndicoEventContribution
from app.services.indico.indico_eagreement_service import IndicoEagreementStatus
from app.services.service_now.service_now_service import ServiceNowService

logger = logging.getLogger("webapp.force_as_accepted")


def force_as_accepted(contribution):
    indico_contribution_dao = IndicoEventContributionDAO()
    indico_contribution_dao.set_eagreement_status(
        contribution.contribution_id, IndicoEagreementStatus.FORCED
    )
    response_data = display_messages(contribution)

    return response_data


def display_messages(contribution):
    response_data = {}
    logger.info(
        "{}: Finished forcing as accepted contribution.".format(
            contribution.contribution_id
        )
    )

    response_data["result"] = "success"
    response_data["msg"] = "Contribution {} forced as accepted".format(
        contribution.contribution_id
    )

    flash(response_data["msg"], "success")

    return response_data


def display_error(contribution):
    response_data = {}
    logger.info(
        f"{contribution.contribution_id}: Unable to force as accepted contribution."
    )

    response_data["result"] = "error"
    response_data["msg"] = (
        f"Contribution {contribution.contribution_id} doesn't have a Snow Ticket. "
        "Please, create it first and try again"
    )

    flash(response_data["msg"], "error")

    return response_data


class ForceAsAccepted(View):
    prefix = ""

    methods = ["POST"]

    def dispatch_request(self):
        form_class = ContributionIdForm(request.form)
        if request.method == "POST":
            self.form_valid(form_class)
            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view",
                    id=form_class.contribution_id.data,
                )
            )

    def form_valid(self, form):
        response_data = {}
        try:
            contribution_internal_id = form.contribution_id.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)

            if contribution.snow_ticket is None:
                response_data = display_error(contribution)
                return response_data

            logger.info(
                "{}: Forcing as accepted contribution.".format(
                    contribution.contribution_id
                )
            )
            indico_contribution_dao = IndicoEventContributionDAO()
            indico_contribution_dao.update_last_updated(
                contribution.id, "Force as accepted"
            )
            ServiceNowService(contribution, logger=logger).add_work_note(
                "Forced as accepted"
            )
            response_data = force_as_accepted(contribution)

            return response_data

        except Exception as ex:
            logger.exception("Exception on force_as_accepted: %s" % ex)
            response_data["result"] = "error"
            response_data["msg"] = (
                "Failed to set the recording path because of the error %s" % ex
            )
            flash(response_data["msg"], "error")
            return response_data

    def form_invalid(self, form):
        response_data = {"result": "error", "msg": "Invalid form"}
        return response_data

import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import PublishCDSLinkToIndicoForm
from app.models.events import IndicoEventContribution
from app.services.indico.indico_service_2 import IndicoServiceV2
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.cds_indico")


def add_cds_link(contribution, force=False):
    """
    Add a CDS link to the corresponding contribution and return the messages corresponding the response
    :param force: Whether or not to force the link creation
    :type force: bool
    :param contribution: Contribution to be managed
    :type contribution: app.models.events.IndicoEventContribution
    :return: Dictionary with messages
    :rtype: dict
    """
    indico_contribution_dao = IndicoEventContributionDAO()
    indico_contribution_dao.update_last_updated(contribution.id, "Add CDS link")
    result = IndicoServiceV2(logger=logger).insert_cds_link(contribution, force=force)
    response_data = display_messages(contribution, result)

    return response_data


def display_messages(contribution, result):
    """
    Display a message based on the result
    :param contribution: Contribution to manage
    :type contribution: app.models.Events.IndicoEventContribution
    :param result: Response dict
    :type result: dict
    :return: Dict with messages
    :rtype: dict
    """
    response_data = {}
    if result:
        logger.info(
            "{}: Finished adding CDS link to Indico for contribution.".format(
                contribution.contribution_id
            )
        )

        response_data["result"] = "success"
        response_data[
            "msg"
        ] = "Finished adding CDS link to Indico (Contribution: {})".format(
            contribution.contribution_id
        )
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data["msg"] = "Unable to add CDS link to Indico contribution"
        flash(response_data["msg"], "error")
    return response_data


@admin_custom_actions.route("/_add_cds_link_to_indico/", methods=("POST",))
@login_required
def add_cds_link_to_indico_view():
    form = PublishCDSLinkToIndicoForm()
    if request.method == "POST":
        contribution_internal_id = form.contribution_id.data
        if form.validate_on_submit():
            contribution_internal_id = form.contribution_id.data
            logger.debug(
                "Contribution ID: {} force: {}".format(
                    form.contribution_id.data, form.force.data
                )
            )
            force = form.force.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)

            logger.info(
                "{}: Adding CDS link to contribution...".format(
                    contribution.contribution_id
                )
            )

            add_cds_link(contribution, force=force)
            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view", id=contribution_internal_id
                )
            )

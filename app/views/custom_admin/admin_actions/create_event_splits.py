import logging

from flask import jsonify, request

from app.extensions import csrf, db
from app.models.events import IndicoEventContribution
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.create_splits")


@csrf.exempt
@admin_custom_actions.route(
    "_create_contribution_splits/<int:contribution_id>", methods=("POST",)
)
def create_contribution_splits_view(contribution_id: str):
    """
    Set the splits for a given contribution using the data in the request form.

    This view is called using AJAX from the Indico Event edit view in the admin dashboard.

    :param contribution_id: The contribution ID that will be affected
    :type contribution_id: str
    :return: "OK"
    :rtype: str
    """
    if request.method == "POST":
        logger.debug("Creating splits for contribution {}".format(contribution_id))
        # Get form data
        contribution_split = request.form
        contribution_id = contribution_split["contribution_id"]
        camera_start_time = contribution_split["camera_start_time"]
        camera_end_time = contribution_split["camera_end_time"]
        slides_start_time = contribution_split["slides_start_time"]
        slides_end_time = contribution_split["slides_end_time"]
        logger.debug(
            f"Contribution {contribution_id} camera: {camera_start_time}/{camera_end_time}"
        )
        logger.debug(
            f"Contribution {contribution_id} slides: {slides_start_time}/{slides_end_time}"
        )

        # Set and save the data
        contribution = IndicoEventContribution.query.get(contribution_id)
        contribution.camera_start_time = camera_start_time
        contribution.camera_end_time = camera_end_time
        contribution.slides_start_time = slides_start_time
        contribution.slides_end_time = slides_end_time
        db.session.commit()

        return jsonify(result="OK")

import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

from app.daos.metric import MetricsDAO
from app.forms.opencast_workflow_forms import ContributionIdForm
from app.models.events import IndicoEventContribution
from app.models.metrics import AvailabeMetricsNames
from app.services.indico.indico_api_client_v2 import CreateLinkResult
from app.services.video_player_link import VideoPlayerLinkService
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.video_player_link")


@admin_custom_actions.route("/_publish_video_player_link/", methods=("POST",))
@login_required
def publish_video_player_link_view():
    form = ContributionIdForm()
    if request.method == "POST":
        contribution_internal_id = form.contribution_id.data
        if form.validate_on_submit():
            contribution = IndicoEventContribution.query.get(contribution_internal_id)
            logger.info(
                "{}: Publishing video player link...".format(
                    contribution.contribution_id
                )
            )
            service = VideoPlayerLinkService(
                contribution.contribution_id,
                contribution.db_id_full,
                contribution.start_date.year,
                logger=logger,
            )
            result = service.publish_video_player_link_on_indico()
            if result == CreateLinkResult.CREATED or result == CreateLinkResult.UPDATED:
                MetricsDAO.create(
                    AvailabeMetricsNames.PUBLISHED_VIDEO_PLAYER_LINK_INDICO,
                    AvailabeMetricsNames.PUBLISHED_VIDEO_PLAYER_LINK_INDICO,
                )
            display_messages(contribution.contribution_id, result)

        return redirect(
            url_for("indicoeventcontribution.edit_view", id=contribution_internal_id)
        )


def display_messages(contribution_id, result):
    message_type = "success"
    if result == CreateLinkResult.CREATED:
        message = "Created link to video player on Indico"
        logger.info("{}: {}".format(contribution_id, message))
    elif result == CreateLinkResult.UPDATED:
        message = "Updated link to video player on Indico"
        logger.info("{}: {}".format(contribution_id, message))
    else:
        message = "Unable to create or update the video player link on Indico"
        message_type = "error"
        logger.warning("{}: {}".format(contribution_id, message))
        logger.error("Unable to create or update the video player link on Indico")

    flash(message, message_type)

import errno
import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

from app.forms.folders_form import CreateFoldersForm
from app.models.events import IndicoEvent
from app.services.recording_path_service import RecordingPathService
from app.services.smb_service import SMBService
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.create_event_folders")


@admin_custom_actions.route("_create_event_folders/<int:event_id>", methods=("POST",))
@login_required
def create_event_folders_view(event_id):
    form = CreateFoldersForm()
    if request.method == "POST":
        if form.validate_on_submit():
            recording_path = form.recording_path.data
            logger.debug(
                "Creating folders for event {} on {}".format(event_id, recording_path)
            )
            event = IndicoEvent.query.get(event_id)
            message = "Folders for event {} created successfully".format(
                event.indico_id
            )
            message_type = "success"
            recording_path_ok = True

            if not recording_path.startswith(r"\\"):
                message_type = "error"
                message = rf"{event.indico_id}: Recording path should start with \\"
                recording_path_ok = False

            if recording_path.endswith("\\"):
                message_type = "error"
                message = f"{event.indico_id}: Recording path should not end with \\"
                recording_path_ok = False

            if event and recording_path_ok:
                logger.debug(
                    "Recording path for event {} is {}".format(
                        event.indico_id, recording_path
                    )
                )
                event_recording_path = recording_path + "\\" + event.indico_id
                try:
                    try:
                        smb_service = SMBService()
                        recording_path_service = RecordingPathService()
                        create_folder_ignoring_existing(
                            event_recording_path, smb_service
                        )
                        recording_path_service.set_event_recording_path(
                            event, path=event_recording_path
                        )
                        if len(event.contributions) == 0:
                            contribution_recording_path = (
                                event_recording_path + "\\" + event.indico_id
                            )
                            create_folder_ignoring_existing(
                                contribution_recording_path, smb_service
                            )
                        else:
                            for contribution in event.contributions:
                                contribution_recording_path = (
                                    event_recording_path
                                    + "\\"
                                    + contribution.contribution_id
                                )
                                create_folder_ignoring_existing(
                                    contribution_recording_path, smb_service
                                )

                    except OSError as e:
                        if e.errno == errno.EEXIST:
                            logger.debug(
                                "The directory already exists. No need to create it"
                            )
                        else:
                            raise
                except Exception as e:
                    message = (
                        "Unable to create folder for event {} on path {} ({})".format(
                            event.indico_id, event_recording_path, e
                        )
                    )
                    message_type = "error"
                    logger.warning(message)

                flash(message, message_type)

                return redirect(url_for("indicoevent.edit_view", id=event_id))


def create_folder_ignoring_existing(contribution_recording_path, smb_service):
    logger.debug("Creating folder: {}".format(contribution_recording_path))
    try:
        smb_service.create_directory(contribution_recording_path, is_full_path=True)
    except OSError as e:
        if e.errno == errno.EEXIST:
            logger.debug(
                "The directory {} already exists. No need to create it".format(
                    contribution_recording_path
                )
            )
        else:
            raise

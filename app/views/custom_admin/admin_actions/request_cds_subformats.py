import logging

from flask import flash, redirect, request, url_for
from flask.views import View

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import ContributionIdForm
from app.models.events import IndicoEventContribution
from app.services.cds.cds_record_service import CdsRecordService

logger = logging.getLogger("webapp.cds")


def display_messages(contribution_id, result, message):
    response_data = {}
    if result:
        logger.info("{}: Finished requesting CDS subformats.".format(contribution_id))
        flash(message, "success")
    else:
        flash(message, "error")
    return response_data


class RequestCdsSubformats(View):
    prefix = ""
    methods = ["POST"]

    def dispatch_request(self):
        form_class = ContributionIdForm(request.form)
        logger.info("dispathing request...")
        if request.method == "POST":
            self.form_valid(form_class)
            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view",
                    id=form_class.contribution_id.data,
                )
            )

    def form_valid(self, form):
        try:
            contribution_internal_id = form.contribution_id.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)

            logger.info(
                "{}: Requesting CDS subformats.".format(contribution.contribution_id)
            )
            indico_contribution_dao = IndicoEventContributionDAO()
            indico_contribution_dao.update_last_updated(
                contribution.id, "Request CDS subformats"
            )

            valid = CdsRecordService(
                contribution, logger=logger
            ).is_waiting_time_valid_for_subformats()

            if valid:
                result, message = CdsRecordService(
                    contribution, logger=logger
                ).request_cds_subformats_ready_datajson()
            else:
                result = False
                message = "Wait some minutes before requesting the subformats (this will happen in the background)"
            response_data = display_messages(
                contribution.contribution_id, result, message
            )

            return response_data

        except Exception as ex:
            logger.exception("Exception on request_cds_subformats: %s" % ex)
            response_data = {
                "result": "error",
                "msg": "Failed to request the CDS subformats because of the error %s"
                % ex,
            }
            flash(response_data["msg"], "error")
            return response_data

    def form_invalid(self, form):
        response_data = {}
        response_data["result"] = "error"
        response_data["msg"] = "Invalid form"
        return response_data

import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import ContributionIdForm
from app.models.events import IndicoEventContribution
from app.services.indico.indico_eagreement_service import IndicoEagreemenentService
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.views_fetch_eagreement")


def display_messages(contribution, result):
    response_data = {}
    if result:
        logger.info(
            f"{contribution.contribution_id}: E-Agreement fetched successfully for contribution."
        )

        response_data["result"] = "success"
        response_data[
            "msg"
        ] = "E-Agreement fetched successfully for contribution {}".format(
            contribution.contribution_id
        )
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data["msg"] = "Unable fetch the e-agreement."
        flash(response_data["msg"], "error")
    return response_data


@admin_custom_actions.route("/_fetch_speaker_release/", methods=("POST",))
@login_required
def fetch_speaker_release_view():
    form = ContributionIdForm()
    if request.method == "POST":
        if form.validate_on_submit():
            contribution_internal_id = form.contribution_id.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)
            indico_contribution_dao = IndicoEventContributionDAO()
            indico_contribution_dao.update_last_updated(
                contribution_internal_id, "Fetch eagreement"
            )

            logger.info(
                f"{contribution.contribution_id}: Fetching agreement for contribution."
            )

            result = IndicoEagreemenentService(
                contribution, logger=logger
            ).fetch_eagreement_and_update()

            display_messages(contribution, result)
            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view", id=contribution_internal_id
                )
            )

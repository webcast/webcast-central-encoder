import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import SendToOpencastForm
from app.models.events import IndicoEventContribution
from app.services.opencast.opencast_service import OpencastService
from app.tasks.opencast_ingest import ingest_to_opencast_step_by_step_task
from app.utils.times import are_split_times_valid
from app.views.blueprints import admin_custom_actions
from app.views.custom_admin.admin_actions.utils import reset_previous_statuses

logger = logging.getLogger("webapp.opencast")


def send_to_opencast(contribution, edit_on_opencast, add_transcription):
    """

    :param add_transcription:
    :type add_transcription:
    :param edit_on_opencast:
    :type edit_on_opencast: bool
    :param contribution:
    :type contribution: app.models.events.IndicoEventContribution
    :return:
    :rtype:
    """
    result = {}
    edit_on_opencast = str(edit_on_opencast).lower()
    add_transcription = str(add_transcription).lower()

    logger.info(
        f"""{contribution.contribution_id}: Ingesting to Opencast.
        with edit_on_opencast {edit_on_opencast}"""
    )

    if not contribution.can_send_to_opencast:
        result["result"] = "error"
        result[
            "msg"
        ] = "Contribution cannot be sent to Opencast. Is it already running or pending?"
        flash(result["msg"], result["result"])
        return result

    if contribution.recording_path == "":
        result["result"] = "error"
        result["msg"] = "Recording path is not set. Please set it first."
        flash(result["msg"], result["result"])
        return result

    if not contribution.snow_ticket:
        result["result"] = "error"
        result["msg"] = "SNOW ticket not created. Please create it first."
        flash(result["msg"], result["result"])
        return result

    if not contribution.postprocessing_type:
        result["result"] = "error"
        result["msg"] = "Postprocessing type not set. Please set it first."
        flash(result["msg"], result["result"])
        return result

    if edit_on_opencast == "true":
        valid_split_times = are_split_times_valid(contribution)
        if not valid_split_times:
            result["result"] = "error"
            result["msg"] = "Split times don't have the same duration."
            flash(result["msg"], result["result"])
            return result

    oc_service = OpencastService(contribution, logger=logger)
    workflow_status = oc_service.request_workflow_status()
    if workflow_status:
        if "RUNNING" in workflow_status or "PROCESSING" in workflow_status:
            result["result"] = "error"
            result["msg"] = (
                f"There is currently a workflow running {contribution.opencast_uid}. "
                "Stop it first before sending another task."
            )
            flash(result["msg"], result["result"])
            return result

    reset_previous_statuses(contribution)
    indico_contribution_dao = IndicoEventContributionDAO()
    indico_contribution_dao.update_last_updated(contribution.id, "Send to Opencast")
    indico_contribution_dao.update_send_to_opencast_checkboxes(
        contribution.id, edit_on_opencast, add_transcription
    )

    task = ingest_to_opencast_step_by_step_task.apply_async(
        queue="media", args=[contribution.id, edit_on_opencast, add_transcription]
    )
    # Update the task id for the contribution to allow stopping after
    logger.debug(f"{contribution.contribution_id}: Task ID is: {task.id}")
    indico_contribution_dao.set_sent_to_opencast_task_id(contribution.id, task.id)
    display_ingest_messages(contribution)


def display_ingest_messages(contribution):
    response_data = {}
    logger.info("{contribution.contribution_id}: Finished requesting ingest.")

    response_data["result"] = "success"
    response_data[
        "msg"
    ] = f"{contribution.contribution_id}: Ingest to opencast requested for contribution"
    flash(response_data["msg"], "success")

    return response_data


@admin_custom_actions.route("/_send_to_opencast/", methods=("POST",))
@login_required
def send_to_opencast_view():
    form = SendToOpencastForm()
    if request.method == "POST":
        contribution_internal_id = form.contribution_id.data
        if form.validate_on_submit():
            contribution_internal_id = form.contribution_id.data
            edit_on_opencast = form.edit_on_opencast.data
            add_transcription = form.add_transcription.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)

            logger.info(
                f"""{contribution.contribution_id}: Sending to Opencast.
                edit_on_opencast: {edit_on_opencast} add_transcription: {add_transcription}"""
            )

            send_to_opencast(contribution, edit_on_opencast, add_transcription)

            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view", id=contribution_internal_id
                )
            )

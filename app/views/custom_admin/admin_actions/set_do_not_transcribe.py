import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.transcription_workflow import SetDoNotTranscribeForm
from app.models.events import IndicoEventContribution
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.do_not_transcribe")


@admin_custom_actions.route("/_set_do_not_transcribe/", methods=("POST",))
@login_required
def set_do_not_transcribe_view():
    form = SetDoNotTranscribeForm()
    if request.method == "POST":
        contribution_internal_id = form.contribution_id.data
        if form.validate_on_submit():
            contribution_internal_id = form.contribution_id.data
            do_not_transcribe = form.do_not_transcribe.data
            logger.debug(f"Setting do not transcribe to {do_not_transcribe}")
            contribution = IndicoEventContribution.query.get(contribution_internal_id)
            logger.info(f"{contribution.contribution_id}: Setting do not transcribe...")
            set_do_not_transcribe_action(contribution, do_not_transcribe)

        return redirect(
            url_for("indicoeventcontribution.edit_view", id=contribution_internal_id)
        )


def set_do_not_transcribe_action(contribution, new_value):
    success = True
    if new_value not in [True, False]:
        raise Exception(f"Unknown Postprocessing type: {new_value}")
    indico_contribution_dao = IndicoEventContributionDAO()
    indico_contribution_dao.set_do_not_transcribe(contribution.id, new_value)
    indico_contribution_dao.update_last_updated(
        contribution.id, f"Set do not transcribe to {new_value}"
    )

    response_data = display_messages(contribution, success)

    return response_data


def display_messages(contribution, was_success):
    response_data = {}
    if was_success:
        logger.info(
            f"{contribution.contribution_id}: Finished setting up do_not_transcribe."
        )

        response_data["result"] = "success"
        response_data[
            "msg"
        ] = f"do_not_transcribe set for contribution {contribution.contribution_id}"
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data["msg"] = "Unable to set do_not_transcribe."
        flash(response_data["msg"], "error")
    return response_data

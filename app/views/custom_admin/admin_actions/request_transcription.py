import logging

from flask import flash, redirect, request, url_for
from flask_login import login_required

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import ContributionIdForm
from app.models.contribution_workflow.transcription_workflow import (
    OpencastTranscriptionStates,
    TranscriptionStates,
)
from app.models.events import IndicoEventContribution
from app.services.transcription.transcription_service import TranscriptionService
from app.tasks.transcriptions import request_transcription
from app.views.blueprints import admin_custom_actions

logger = logging.getLogger("webapp.views_request_transcription")


def display_messages(contribution_id: str, result: bool):
    response_data = {}
    if result:
        logger.info(f"{contribution_id}: Transcription update requested to TTAAS")

        response_data["result"] = "success"
        response_data["msg"] = "Transcription update requested to TTAAS"
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data["msg"] = "Unable to request transcription to TTAAS."
        flash(response_data["msg"], "error")
    return response_data


def display_messages_opencast(contribution_id: str, result: bool):
    response_data = {}
    if result:
        logger.info(f"{contribution_id}: Transcription update requested to Rundeck")

        response_data["result"] = "success"
        response_data["msg"] = "Transcription update requested to Rundeck"
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data["msg"] = "Unable to request transcription update."
        flash(response_data["msg"], "error")
    return response_data


@admin_custom_actions.route("/_request_transcription/", methods=("POST",))
@login_required
def request_transcription_view():
    form = ContributionIdForm()
    if request.method == "POST":
        if form.validate_on_submit():
            contribution_internal_id = form.contribution_id.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)

            if contribution.do_not_transcribe:
                flash(
                    "Unable to request transcription. Transcription is disabled for this contribution.",
                    "error",
                )
                return redirect(
                    url_for(
                        "indicoeventcontribution.edit_view", id=contribution_internal_id
                    )
                )
            indico_contribution_dao = IndicoEventContributionDAO()
            indico_contribution_dao.update_last_updated(
                contribution_internal_id, "Request transcription"
            )

            logger.info(
                f"{contribution.contribution_id}: Requesting transcription for contribution."
            )

            indico_contribution_dao.set_transcription_status(
                contribution.contribution_id,
                TranscriptionStates.REQUESTING,
                media_id="NOT SET",
            )
            indico_contribution_dao.set_transcription_error_message(
                contribution.contribution_id, error_message=""
            )

            task_id = request_transcription.apply_async(
                queue="transcription", args=[contribution.id]
            )

            display_messages(contribution.contribution_id, task_id)
            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view", id=contribution_internal_id
                )
            )


@admin_custom_actions.route(
    "/_request_transcription_update_rundeck/", methods=("POST",)
)
@login_required
def request_transcription_update_rundeck_view():
    form = ContributionIdForm()
    if request.method == "POST":
        if form.validate_on_submit():
            contribution_internal_id = form.contribution_id.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)
            indico_contribution_dao = IndicoEventContributionDAO()
            indico_contribution_dao.update_last_updated(
                contribution_internal_id, "Request transcription update rundeck"
            )
            indico_contribution_dao.set_transcription_update_status(
                contribution.contribution_id,
                OpencastTranscriptionStates.REQUESTING,
            )

            logger.info(
                f"{contribution.contribution_id}: Requesting transcription update to Rundeck."
            )

            service = TranscriptionService(contribution, logger=logger)
            result = service.request_transcription_update()

            display_messages_opencast(contribution.contribution_id, result)
            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view", id=contribution_internal_id
                )
            )

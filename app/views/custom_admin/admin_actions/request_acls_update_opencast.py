import logging

from flask import flash, redirect, request, url_for
from flask.views import View

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.forms.opencast_workflow_forms import ContributionIdForm
from app.models.events import IndicoEventContribution
from app.services.opencast.opencast_service import OpencastService

logger = logging.getLogger("webapp.opencast_acls")


def request_acls_update(contribution):
    service = OpencastService(contribution, logger=logger)
    indico_contribution_dao = IndicoEventContributionDAO()
    indico_contribution_dao.update_last_updated(contribution.id, "Request ACLs update")
    request_sent = service.request_acl_update_workflow()
    response_data = display_messages(contribution, request_sent)

    return response_data


def display_messages(contribution, ingest_finished):
    response_data = {}
    if ingest_finished:
        logger.debug(
            "{}: Finished request to update ACLs.".format(contribution.contribution_id)
        )

        response_data["result"] = "success"
        response_data[
            "msg"
        ] = f"{contribution.contribution_id}: Requested ACLs update for contribution"
        flash(response_data["msg"], "success")
    else:
        response_data["result"] = "error"
        response_data["msg"] = "Unable to request the ACLs update to Opencast."
        flash(response_data["msg"], "error")
    return response_data


class RequestOpencastAclsUpdate(View):
    prefix = ""
    methods = ["POST"]

    def dispatch_request(self):
        form_class = ContributionIdForm(request.form)
        if request.method == "POST":
            self.form_valid(form_class)
            return redirect(
                url_for(
                    "indicoeventcontribution.edit_view",
                    id=form_class.contribution_id.data,
                )
            )

    def form_valid(self, form):
        response_data = {}
        try:
            contribution_internal_id = form.contribution_id.data
            contribution = IndicoEventContribution.query.get(contribution_internal_id)

            logger.debug(
                "{}: Requesting Opencast ACLs update...".format(
                    contribution.contribution_id
                )
            )

            response_data = request_acls_update(contribution)

            return response_data

        except Exception as ex:
            logger.exception("Exception on request_acls_update_opencast: %s" % ex)
            response_data["result"] = "error"
            response_data["msg"] = (
                "Failed to request Opencast ACLs update due to error %s" % ex
            )
            flash(response_data["msg"], "error")
            return response_data

    def form_invalid(self, form):
        response_data = {}
        response_data["result"] = "error"
        response_data["msg"] = "Invalid form"
        return response_data

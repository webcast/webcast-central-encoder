import logging

import app.models.events
from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.models.contribution_workflow.opencast_workflow import (
    MediaPackageStatus,
    SentToOpencastStatus,
)

logger = logging.getLogger("webapp")


def reset_previous_statuses(
    contribution: app.models.events.IndicoEventContribution, pending=True
):
    """

    :param pending:
    :type pending:
    :param contribution:
    :type contribution:
    :return:
    :rtype:
    """
    logger.info(
        f"{contribution.contribution_id}: Resetting: send to opencast and onward statuses"
    )

    media_package_status = ""
    sent_status = ""
    if pending:
        media_package_status = MediaPackageStatus.PENDING
        sent_status = SentToOpencastStatus.PENDING
    indico_contribution_dao = IndicoEventContributionDAO()
    # Reset send to Opencast
    indico_contribution_dao.set_opencast_uid(contribution.id, None)
    indico_contribution_dao.set_sent_to_opencast_status(
        contribution.id, None, last_report=""
    )
    indico_contribution_dao.set_opencast_processing_finished(
        contribution.contribution_id, False
    )
    indico_contribution_dao.set_opencast_processing_status(
        contribution.contribution_id, ""
    )
    indico_contribution_dao.set_sent_to_opencast_task_id(contribution.id, "")

    # Reset ready for cutting
    indico_contribution_dao.set_opencast_ready_for_cutting(
        contribution.contribution_id, False
    )
    indico_contribution_dao.set_opencast_ready_for_cutting_status(
        contribution.contribution_id, ""
    )

    # Reset ACLS
    indico_contribution_dao.set_opencast_acls_finished(
        contribution.contribution_id, False
    )
    indico_contribution_dao.set_contribution_acls_status(
        contribution.contribution_id, False, "", ""
    )

    # Reset CDS record
    indico_contribution_dao.set_cds_record_status(
        contribution.contribution_id, False, "", ""
    )
    indico_contribution_dao.set_cds_record(contribution.contribution_id, "")
    indico_contribution_dao.set_cds_subformats_request_status(
        contribution.contribution_id, False, "", ""
    )
    indico_contribution_dao.set_media_package_generated_status(
        contribution.id, media_package_status, last_report="Pending..."
    )
    indico_contribution_dao.set_sent_to_opencast_status(
        contribution.id, sent_status, last_report="Pending..."
    )

from wtforms import SelectField

from app.daos.metric import MetricsDAO
from app.models.metrics import AvailabeMetricsNames
from app.views.custom_admin.admin_protected_view import AdminProtectedView


class EncoderView(AdminProtectedView):
    """
    Admin view for the Room Encoders
    """

    form_excluded_columns = ["room_encoder", "type", "disk_space", "api_url"]
    column_exclude_list = ["api_url", "disk_space"]

    form_overrides = dict(encoder_1=SelectField, encoder_2=SelectField)
    form_args = dict(
        encoder_1=dict(choices=[("REC", "Recording"), ("STR", "Streaming")]),
        encoder_2=dict(choices=[("REC", "Recording"), ("STR", "Streaming")]),
    )

    def on_model_delete(self, model):
        super(EncoderView, self).on_model_delete(model)
        MetricsDAO.create(
            AvailabeMetricsNames.REMOVED_ENCODER, AvailabeMetricsNames.REMOVED_ROOM
        )

    def on_model_change(self, form, model, is_created):
        super(EncoderView, self).on_model_change(form, model, is_created)
        if is_created:
            MetricsDAO.create(
                AvailabeMetricsNames.ADDED_ENCODER, AvailabeMetricsNames.ADDED_ROOM
            )

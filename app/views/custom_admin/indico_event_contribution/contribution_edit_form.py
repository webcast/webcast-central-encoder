import logging

from flask import request
from flask_admin import expose
from flask_admin.form import rules
from wtforms import SelectField

from app.forms.opencast_workflow_forms import (
    ContributionIdForm,
    PublishCDSLinkToIndicoForm,
    SendToOpencastForm,
    SetPostprocessingTypeForm,
)
from app.forms.transcription_workflow import SetDoNotTranscribeForm
from app.models.contribution_workflow.languages import AvailableLanguages
from app.models.events import IndicoEventContribution, PostprocessingType
from app.services.indico.indico_eagreement_service import IndicoEagreementStatus
from app.services.service_now.service_now_service import ServiceNowService
from app.services.video_player_link import VideoPlayerLinkService
from app.views.custom_admin.model_with_extra_js import ModelViewWithExtraJs

logger = logging.getLogger("webapp.admin.contribution")


class ContributionEditForm(ModelViewWithExtraJs):
    """
    Admin view for the IndicoEventContribution
    """

    # Template to be used on the edit form
    edit_template = "custom_admin/edit_contribution_template.html"

    # Fields to be displayed on the edit form
    form_rules = [
        # Define field set with header text and four fields
        rules.FieldSet(("run_workflows",), ""),
        rules.HTML(
            """<p class="text-muted">
                  If <strong>run workflows</strong> is enabled, the automatic tasks will
                  be run for this contribution.
                </p>
                <p>Automatic steps will run only on events not older than 2 months.</p>"""
        ),
        rules.FieldSet(
            (
                "camera_start_time",
                "camera_end_time",
            ),
            "Camera split times",
        ),
        rules.HTML(
            """<p class="text-muted">
                          If Split End Time is empty or if it's "00:00:00" the video won't be split.
                    </p>"""
        ),
        rules.FieldSet(
            (
                "slides_start_time",
                "slides_end_time",
            ),
            "Slides split times",
        ),
        rules.HTML(
            """<p class="text-muted">
                          If Split End Time is empty or if it's "00:00:00" the video won't be split.
                    </p>"""
        ),
        rules.FieldSet(("language",), "Language"),
        rules.FieldSet(("zoom_meeting_id",), "Fetch meeting from Zoom"),
        rules.HTML(
            (
                '<button type="button" class="btn btn-primary mb-3" onClick="fetch_recordings()">'
                "Get meeting recordings</button>"
            )
        ),
        rules.HTML(
            """
                <div id="recordings-table-div"></div>"""
        ),
        rules.FieldSet(
            (
                "title",
                "contribution_id",
                "event_type",
                "room",
                "speakers",
                "start_date",
                "start_time",
                "end_date",
                "end_time",
                "description",
            ),
            "Information",
        ),
        rules.HTML("<h3>Workflow</h3>"),
        rules.HTML("<h6>1 - Recording path</h6>"),
        rules.FieldSet(("recording_path",)),
        rules.HTML("<ul>"),
        rules.HTML(
            (
                '<li class="text-muted">'
                "<strong>SMB path</strong>: To the mp4 folder."
                r"(EG: \\cernmedia33.cern.ch\RECORDINGS\TEST-ROOM\949975\949975c1sc1)."
                "</li>"
            )
        ),
        rules.HTML(
            (
                '<li class="text-muted">'
                "<strong>URL to video file</strong>: Supported formats: mp4."
                "(EG: https://webcast-lite.web.cern.ch/static/test/file.mp4)."
                "</li>"
            )
        ),
        rules.HTML(
            (
                '<li class="text-muted">'
                "<strong>CERN Box URL</strong>: Supported formats: mp4. "
                '(EG: "https://cernbox.cern.ch/index.php/s/yOKYcc2YLqEVg83/download").'
                "</li>"
            )
        ),
        rules.HTML(
            (
                '<li class="text-muted">'
                "<strong>Zoom</strong>: Input the meeting ID to retrieve the meeting's recordings"
                "and select the desired one. It will be populated to the field. Supported formats: mp4."
                "</li>"
            )
        ),
        rules.HTML("</ul>"),
        rules.HTML('<fieldset class="form-group">'),
        rules.HTML("<h6>2 - Postprocessing type</h6>"),
        rules.FieldSet(("postprocessing_type",)),
        rules.HTML("<h6>3 - SNOW ticket</h6>"),
        rules.FieldSet(
            (
                "is_snow_ticket_generated",
                "snow_ticket",
            )
        ),
        rules.HTML(
            """<p class="text-muted">
                    A Service Now ticket will be generated and linked to this contribution.
                     If the value of the field is not empty, the SNOW ticket won't be able to be created.
                    </p>"""
        ),
        rules.HTML("</fieldset>"),
        # rules.HTML("<fieldset class=\"form-group\">"),
        # rules.HTML("<h6>3 - Generate media package</h6>"),
        # rules.FieldSet(('media_package_generated_status', 'media_package_generated_last_report', 'has_camera_file',
        #                 'has_slides_file',), ''),
        # rules.HTML("""<p class="text-muted">
        #                 Note: camera.mp4 and/or slides.mp4 files are required to be present on the recording path.
        #             </p>"""),
        # rules.HTML("</fieldset>"),
        rules.HTML('<fieldset class="form-group">'),
        rules.HTML("<h6>4 - Send to Opencast</h6>"),
        rules.FieldSet(("opencast_uid",), ""),
        rules.HTML(
            """<p class="text-muted">
                    Once the media package is generated, it can be sent to Opencast. The UID will link to the
                    related workflow on the Opencast admin.
                  </p>"""
        ),
        rules.HTML("</fieldset>"),
        rules.HTML('<fieldset class="form-group">'),
        rules.HTML("<h6>5 - Indico Agreement</h6>"),
        rules.FieldSet(("e_agreement_status",), ""),
        rules.HTML(
            """<p class="text-muted">
                        Speaker release needs to be signed by the speakers before publishing to CDS and Indico.
                      </p>"""
        ),
        rules.HTML("</fieldset>"),
        rules.HTML('<fieldset class="form-group">'),
        rules.HTML("<h6>6 - Opencast ACLs</h6>"),
        rules.FieldSet(("opencast_acls_requested", "opencast_acls_finished"), ""),
        rules.HTML("</fieldset>"),
        rules.HTML('<fieldset class="form-group">'),
        rules.HTML("<h6>7 - Transcription</h6>"),
        rules.FieldSet(("transcription_requested_status",), ""),
        rules.HTML("</fieldset>"),
        rules.HTML('<fieldset class="form-group">'),
        rules.HTML("<h6>8 - CDS record</h6>"),
        rules.FieldSet(("cds_record",), ""),
        rules.HTML("</fieldset>"),
    ]

    # Arguments passed to the fields in the edit form
    form_args = {
        "recording_path": {
            "render_kw": {
                "placeholder": r"\\cernmedia33.cern.ch\RECORDINGS\Test\TEST-ROOM\949975\<CONTRIB_ID>"
            }
        },
        "split_camera_start_time": {
            "render_kw": {
                "pattern": r"^([0-9]?\d|2[0-9])(?::([0-9]?\d))?(?::([0-9]?\d))?$",
                "title": "Input format must be HH:mm:ss",
            }
        },
        "split_camera_end_time": {
            "render_kw": {
                "pattern": r"^([0-9]?\d|2[0-9])(?::([0-9]?\d))?(?::([0-9]?\d))?$",
                "title": "Input format must be HH:mm:ss",
            }
        },
        "split_slides_start_time": {
            "render_kw": {
                "pattern": r"^([0-9]?\d|2[0-9])(?::([0-9]?\d))?(?::([0-9]?\d))?$",
                "title": "Input format must be HH:mm:ss",
            }
        },
        "split_slides_end_time": {
            "render_kw": {
                "pattern": r"^([0-9]?\d|2[0-9])(?::([0-9]?\d))?(?::([0-9]?\d))?$",
                "title": "Input format must be HH:mm:ss",
            }
        },
        "postprocessing_type": {
            "choices": [
                (PostprocessingType.WEB_LECTURE, PostprocessingType.WEB_LECTURE),
                (PostprocessingType.PLAIN_VIDEO, PostprocessingType.PLAIN_VIDEO),
            ]
        },
        "e_agreement_status": {
            "choices": [
                (
                    IndicoEagreementStatus.NO_INFORMATION,
                    IndicoEagreementStatus.NO_INFORMATION,
                ),
                (IndicoEagreementStatus.PENDING, IndicoEagreementStatus.PENDING),
                (IndicoEagreementStatus.ACCEPTED, IndicoEagreementStatus.ACCEPTED),
                (IndicoEagreementStatus.DECLINED, IndicoEagreementStatus.DECLINED),
                (
                    IndicoEagreementStatus.EMAIL_NOT_SENT,
                    IndicoEagreementStatus.EMAIL_NOT_SENT,
                ),
            ]
        },
        "language": {
            "choices": [
                (AvailableLanguages.ENGLISH, AvailableLanguages.ENGLISH),
                (AvailableLanguages.FRENCH, AvailableLanguages.FRENCH),
                (AvailableLanguages.SPANISH, AvailableLanguages.SPANISH),
                (AvailableLanguages.GERMAN, AvailableLanguages.GERMAN),
            ]
        },
        "event_type": {
            "choices": [
                ("simple_event", "Lecture (simple_event)"),
                ("conference", "Conference"),
                ("meeting", "Meeting"),
            ]
        },
    }

    form_widget_args = {
        "description": {"rows": 10},
        "sent_to_opencast_status": {"readonly": True},
        "sent_to_opencast_last_report": {"readonly": True},
    }

    form_overrides = dict(
        e_agreement_status=SelectField,
        language=SelectField,
        event_type=SelectField,
        postprocessing_type=SelectField,
    )

    def update_model(self, form, model: IndicoEventContribution):
        cds_record = form.cds_record.data
        old_cds_record = model.cds_record

        super().update_model(form, model)

        if old_cds_record != cds_record:
            logger.debug(f"New CDS record is: {cds_record}. Updating SNOW ticket")
            ServiceNowService(model, logger=logger).add_work_note(
                f"CDS Record updated manually. New record is: {cds_record}"
            )

    @expose("/edit/", methods=("GET", "POST"))
    def edit_view(self):
        self._template_args["contribution_id_form"] = ContributionIdForm()
        self._template_args[
            "set_postprocessing_type_form"
        ] = SetPostprocessingTypeForm()
        self._template_args["send_to_opencast_form"] = SendToOpencastForm()
        self._template_args["set_do_not_transcribe_form"] = SetDoNotTranscribeForm()
        self._template_args[
            "publish_cds_link_indico_form"
        ] = PublishCDSLinkToIndicoForm()
        contribution = IndicoEventContribution.query.get(request.args.get("id"))

        video_player_link = VideoPlayerLinkService(
            contribution.contribution_id,
            contribution.db_id_full,
            contribution.start_date.year,
        ).generate_video_player_link()
        self._template_args["video_player_link"] = video_player_link

        return super(ContributionEditForm, self).edit_view()

import logging

from flask import render_template
from markupsafe import Markup

logger = logging.getLogger("webapp.contribution_list")


def snow_ticket_formatter(view, context, model, name):
    """
    Formatter to display all the workflow information of the contribution
    """
    return Markup(
        render_template(
            "custom_admin/macros/table_status_column_content.html", contribution=model
        )
    )


def contribution_id_formatter(view, context, model, name):
    """
    Formatter to display the contribution ID and it's title
    """
    value = getattr(model, name, "Not Set")

    markup_string = "<strong title='{}'>{}</strong>".format(model.title, value)
    return Markup(markup_string)


def has_camera_formatter(view, context, model, name):
    """
    Formatter to display camera and slides icons on the id field
    """
    has_camera_icon = ""
    has_slides_icon = ""
    if model.has_camera_file:
        has_camera_icon = "<i title='Camera file found' class='fas fa-video' style='color:#4caf50'></i> "

    if model.has_slides_file:
        has_slides_icon = "<i title='Slides file found' class='fas fa-film' style='color:#4caf50'></i>"

    markup_string = "{}{}".format(has_camera_icon, has_slides_icon)
    return Markup(markup_string)


def indico_event_formatter(view, context, model, name):
    """
    Formatter to display the date and time of a contribution
    """
    value = getattr(model, name, "")
    html = "<a href='/admin/indicoevent/edit/?id={}' title='Edit event'>{}</a>".format(
        value.id, value.indico_id
    )

    return Markup(html)

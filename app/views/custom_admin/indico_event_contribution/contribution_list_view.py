from app.views.custom_admin.indico_event_contribution.column_formatters.contribution_list import (
    contribution_id_formatter,
    has_camera_formatter,
    indico_event_formatter,
    snow_ticket_formatter,
)
from app.views.custom_admin.model_with_extra_js import ModelViewWithExtraJs


class ContributionListView(ModelViewWithExtraJs):
    # Template to be used on the list view
    list_template = "admin/list.html"
    # Searchable fields on the list view
    column_searchable_list = [
        "title",
        "contribution_id",
        "indico_event.indico_id",
        "cds_record",
        "transcription_media_id",
    ]
    column_filters = ["indico_event.indico_id"]
    column_default_sort = ("last_updated", True)
    column_sortable_list = (
        "contribution_id",
        "media_package_generated_status",
        "start_date_time",
        "sent_to_opencast_status",
        "e_agreement_status",
        "last_updated",
        "cds_record",
    )

    column_labels = {
        "has_camera": "Cam/Slides",
        "room.indico_name": "Room",
        "is_snow_ticket_generated": "Status",
        "start_date": "Date",
        "event": "Indico",
    }

    # List of columns displayed on the list view
    column_list = [
        "has_camera",
        "contribution_id",
        "title",
        "room.indico_name",
        "start_date_time",
        "is_snow_ticket_generated",  # Status
        "indico_event",
        "cds_record",
        "last_updated",
    ]

    # Columns formatted on the list view
    column_formatters = {
        "has_camera": has_camera_formatter,
        "contribution_id": contribution_id_formatter,
        "is_snow_ticket_generated": snow_ticket_formatter,
        "indico_event": indico_event_formatter,
    }

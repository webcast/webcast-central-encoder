import logging

from flask_admin.form import rules
from wtforms import SelectField

from app.daos.api_tokens import ApiTokenDAO
from app.views.custom_admin.model_with_extra_js import ModelViewWithExtraJs

logger = logging.getLogger("webapp.admin")


class ApiTokenView(ModelViewWithExtraJs):
    """
    Admin view for the API Tokens
    """

    form_overrides = dict(resource=SelectField)
    form_args = dict(
        resource=dict(
            choices=[
                ("cds", "CDS"),
                ("ces", "CES"),
                ("opencast", "Opencast"),
                ("indico", "Indico"),
                ("encoders", "Encoders"),
            ]
        )
    )
    form_create_rules = [
        # Define field set with header text and four fields
        rules.FieldSet(
            (
                "name",
                "resource",
            ),
            "",
        ),
    ]

    def create_model(self, form):
        """
        Override the create_model

        :param form:
        :type form:
        :return:
        :rtype:
        """
        logger.info("Creating the API TOKEN")
        form_dict = form.data
        model = ApiTokenDAO.create(form_dict)
        return model

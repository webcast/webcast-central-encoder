import logging

from app.views.custom_admin.indico_event_contribution.contribution_edit_form import (
    ContributionEditForm,
)
from app.views.custom_admin.indico_event_contribution.contribution_list_view import (
    ContributionListView,
)

logger = logging.getLogger("webapp.admin")


class IndicoEventContributionView(ContributionEditForm, ContributionListView):
    """
    The IndicoEventContributionView is composed of 2 views
    """

    pass

from app.views.custom_admin.model_with_extra_js import ModelViewWithExtraJs


class IndicoEventListView(ModelViewWithExtraJs):
    # Template to use on the list view
    list_template = "admin/list.html"

    # List of columns that won't be displayed on the list view
    column_exclude_list = [
        "description",
        "allowed",
        "time",
        "first_contrib",
        "audience",
        "recording",
        "webcast",
        "static",
        "paused_for_splitting",
        "custom_app_name",
        "stop_time",
        "creator",
        "category",
        "category_id",
        "reminder_sent",
        "creator_id",
        "event_type",
    ]
    # Columns searchable
    column_searchable_list = ["title", "indico_id", "room.indico_name", "creator_email"]
    column_default_sort = ("date", True)

    # Columns filters
    column_filters = ["indico_id", "room.indico_name", "is_recorded"]
    # Custom labels for the columns
    column_labels = dict(
        webcast="Supports Webcast",
        recording="Supports Recording",
        running="Is it running",
    )

import logging
import re

from flask import request
from flask_admin import expose
from flask_admin.form import rules
from wtforms import SelectField

from app.forms.folders_form import CreateFoldersForm
from app.models.events import IndicoEvent
from app.views.custom_admin.model_with_extra_js import ModelViewWithExtraJs

logger = logging.getLogger("webapp.admin")


def natural_sort(list, key=lambda s: s):
    """
    Sort the list into natural alphanumeric order.
    """

    def get_alphanum_key_func(key):
        # convert = lambda text: int(text) if text.isdigit() else text
        def convert(text):
            return int(text) if text.isdigit() else text

        return lambda s: [convert(c) for c in re.split("([0-9]+)", key(s))]

    sort_key = get_alphanum_key_func(key)
    list.sort(key=sort_key)


class IndicoEventEditForm(ModelViewWithExtraJs):
    # Template to use on the edit view
    edit_template = "custom_admin/edit_event_template.html"

    # Fields that will be displayed on the edit form
    form_rules = [
        # Define field set with header text and four fields
        rules.FieldSet(
            (
                "title",
                "indico_id",
                "room",
                "description",
                "allowed",
                "date",
                "time",
                "end_date",
                "creator",
                "category",
                "category_id",
            ),
            "Indico",
        ),
        rules.FieldSet(
            (
                "is_webcast_running",
                "is_recording_running",
                "recording_path",
                "is_recorded",
                "contributions",
            ),
            "Workflow",
        ),
        rules.FieldSet(("recording", "webcast", "custom_app_name"), "Encoder"),
        rules.FieldSet(("static", "audience", "reminder_sent", "stop_time"), "Other"),
    ]

    # Arguments passed to the fields in the edit form
    form_args = {
        "event_type": {
            "choices": [
                ("simple_event", "Lecture (simple_event)"),
                ("conference", "Conference"),
                ("meeting", "Meeting"),
            ]
        }
    }

    form_overrides = dict(event_type=SelectField)

    @expose("/edit/", methods=("GET", "POST"))
    def edit_view(self):
        event = IndicoEvent.query.get(request.args.get("id"))
        model_contributions = event.contributions
        # natural_sort(model_contributions, key=lambda x: x.contribution_id)
        create_folders_form = CreateFoldersForm()

        self._template_args["contributions"] = model_contributions
        self._template_args["create_folders_form"] = create_folders_form
        return super(IndicoEventEditForm, self).edit_view()

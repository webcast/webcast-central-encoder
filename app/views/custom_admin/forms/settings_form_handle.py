from flask import flash, redirect, url_for

from app.daos.settings import SettingsDAO
from app.forms.settings_form import SettingsForm
from app.views.blueprints import admin_forms_handlers


@admin_forms_handlers.route("/settings", methods=["POST"])
def settings_form_handle():
    form = SettingsForm()

    if form.validate_on_submit():
        try:
            SettingsDAO.update(form.data)
            flash("Settings saved")
        except Exception as e:
            flash(f"Error updating settings: {str(e)}", "error")

    # If form validation fails, flash errors
    for field, errors in form.errors.items():
        for error in errors:
            flash(f"{field}: {error}", "error")

    return redirect(url_for("admin.index"))

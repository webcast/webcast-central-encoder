import logging

from flask import flash, redirect, url_for

from app.forms.clean_space import FreeDiskForm
from app.tasks.media_packages import remove_older_content
from app.views.blueprints import admin_forms_handlers

logger = logging.getLogger("webapp.admin_free_disk_form")


def calculate_time_delta(since):
    time_deltas = {
        "1m": 60 * 7 * 86400,
        "2w": 2 * 7 * 86400,
        "1w": 7 * 86400,
        "2d": 2 * 86400,
        "now": 0,
    }
    return time_deltas.get(since, 0)


def flash_form_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(f"{field}: {error}", "error")


@admin_forms_handlers.route("/free-disk", methods=["POST"])
def free_disk_form_handle():
    form = FreeDiskForm()

    if form.validate_on_submit():
        try:
            since = form.since.data
            flash("Old files marked for removal: {} ".format(since))
            time_delta = calculate_time_delta(since)
            result = remove_older_content.delay(older_than=time_delta)
            logger.debug(result)
        except Exception as e:
            flash(f"Error loading metrics: {str(e)}", "error")
    else:
        flash_form_errors(form)

    return redirect(url_for("admin.index"))

import logging

from app.views.custom_admin.indico_event.indico_event_edit_form import (
    IndicoEventEditForm,
)
from app.views.custom_admin.indico_event.indico_event_list_view import (
    IndicoEventListView,
)

logger = logging.getLogger("webapp.admin")


class IndicoEventView(IndicoEventEditForm, IndicoEventListView):
    """
    Admin view for the Events
    """

    pass

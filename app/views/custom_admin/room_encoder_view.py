from app.views.custom_admin.admin_protected_view import AdminProtectedView


class RoomEncoderView(AdminProtectedView):
    """
    Admin view for the Room Encoders
    """

    column_list = [
        "encoders",
        "encoder_running",
        "name",
        "recording_path",
        "room",
        "test_running",
    ]
    column_searchable_list = ["encoders.hostname", "room.indico_name"]
    column_labels = {"encoders.hostname": "hostname"}

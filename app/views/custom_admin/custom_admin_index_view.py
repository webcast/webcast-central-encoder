import logging
from datetime import date

from flask import current_app, flash, redirect, request, url_for
from flask_admin import AdminIndexView, expose
from flask_login import current_user

from app.daos.metric import MetricsDAO
from app.daos.settings import SettingsDAO
from app.forms.clean_space import FreeDiskForm
from app.forms.metrics_form import MetricsForm
from app.forms.settings_form import SettingsForm
from app.models.metrics import AvailabeMetricsNames
from app.utils.disk_space import DiskSpaceUtils
from app.views.decorators import requires_login

logger = logging.getLogger("webapp.admin.index")
DEFAULT_NOT_SET = "Not set"


class MyAdminIndexView(AdminIndexView):
    @expose(
        "/",
        methods=(
            "GET",
            "POST",
        ),
    )
    @requires_login
    def index(self):
        if not current_user.is_admin:
            return redirect(url_for("authentication.login_view.logout"))
        arg1 = "Hello"
        opencast_endpoint = current_app.config.get("OPENCAST_ENDPOINT", DEFAULT_NOT_SET)
        instance_hostname = current_app.config.get("INSTANCE_HOSTNAME", DEFAULT_NOT_SET)
        cds_endpoint = current_app.config.get("CDS_URL", DEFAULT_NOT_SET)
        indico_endpoint = current_app.config.get("INDICO_HOSTNAME", DEFAULT_NOT_SET)
        cernmedia_endpoint = current_app.config.get("SERVER", DEFAULT_NOT_SET)
        db_endpoint = current_app.config.get("DB_SERVICE_NAME", DEFAULT_NOT_SET)

        total, used, free = DiskSpaceUtils.get_disk_space_in_downloads_in_GB()
        selected_year = date.today().year

        metrics_form = MetricsForm()

        if request.method == "POST" and metrics_form.validate_on_submit():
            selected_year = metrics_form.selected_year.data
            flash("Displaying stats for year {} ".format(selected_year))

        metrics = self.get_metrics(year=selected_year)

        settings = SettingsDAO.get_all()
        settings_form = SettingsForm(formdata=None, obj=settings)
        form = FreeDiskForm()

        logger.debug(f"Settings: {settings}")
        logger.debug(f"Settings form: {settings_form.data}")

        return self.render(
            "admin/index.html",
            arg1=arg1,
            opencast_endpoint=opencast_endpoint,
            instance_hostname=instance_hostname,
            cds_endpoint=cds_endpoint,
            indico_endpoint=indico_endpoint,
            cernmedia_endpoint=cernmedia_endpoint,
            db_endpoint=db_endpoint,
            total_space=total,
            used_space=used,
            free_space=free,
            form=form,
            settings_form=settings_form,
            metrics=metrics,
            metrics_form=metrics_form,
        )

    def get_metrics(self, year="current_year"):
        if year == "previous_year":
            year = date.today().year - 1
        else:
            year = date.today().year

        from_date = date(year, 1, 1).strftime("%Y-%m-%d")
        to_date = date(year, 12, 31).strftime("%Y-%m-%d")

        rooms_added = MetricsDAO.get_by_name_period(
            AvailabeMetricsNames.ADDED_ROOM, from_date, to_date
        )
        rooms_removed = MetricsDAO.get_by_name_period(
            AvailabeMetricsNames.REMOVED_ROOM, from_date, to_date
        )
        total_rooms_count = len(rooms_added) - len(rooms_removed)
        encoders_added = MetricsDAO.get_by_name_period(
            AvailabeMetricsNames.ADDED_ENCODER, from_date, to_date
        )
        encoders_removed = MetricsDAO.get_by_name_period(
            AvailabeMetricsNames.REMOVED_ENCODER, from_date, to_date
        )
        total_encoders_count = len(encoders_added) - len(encoders_removed)
        using_smb_count = len(
            MetricsDAO.get_by_name_period(
                AvailabeMetricsNames.USED_SMB_MEDIA_PACKAGE, from_date, to_date
            )
        )
        using_zoom_count = len(
            MetricsDAO.get_by_name_period(
                AvailabeMetricsNames.USED_ZOOM_MEDIA_PACKAGE, from_date, to_date
            )
        )
        using_url_count = len(
            MetricsDAO.get_by_name_period(
                AvailabeMetricsNames.USED_URL_MEDIA_PACKAGE, from_date, to_date
            )
        )
        sent_to_opencast_count = len(
            MetricsDAO.get_by_name_period(
                AvailabeMetricsNames.SENT_TO_OPENCAST, from_date, to_date
            )
        )
        cut_on_opencast_count = len(
            MetricsDAO.get_by_name_period(
                AvailabeMetricsNames.CUTTING_IN_OPENCAST, from_date, to_date
            )
        )
        published_cds_link_indico_count = len(
            MetricsDAO.get_by_name_period(
                AvailabeMetricsNames.PUBLISHED_CDS_LINK_INDICO, from_date, to_date
            )
        )
        published_video_player_link_indico_count = len(
            MetricsDAO.get_by_name_period(
                AvailabeMetricsNames.PUBLISHED_VIDEO_PLAYER_LINK_INDICO,
                from_date,
                to_date,
            )
        )

        return {
            "cut_on_opencast_count": cut_on_opencast_count,
            "published_cds_link_indico_count": published_cds_link_indico_count,
            "published_video_player_link_indico_count": published_video_player_link_indico_count,
            "sent_to_opencast_count": sent_to_opencast_count,
            "total_encoders_count": total_encoders_count,
            "total_rooms_count": total_rooms_count,
            "using_smb_count": using_smb_count,
            "using_url_count": using_url_count,
            "using_zoom_count": using_zoom_count,
        }

from flask import url_for

from app.views.custom_admin.admin_protected_view import AdminProtectedView


class ModelViewWithExtraJs(AdminProtectedView):
    """
    Includes an extra JS on Flask Admin Views
    """

    def render(self, template, **kwargs):
        """
        using extra js in render method allow use
        url_for that itself requires an app context
        """
        self.extra_js = [
            url_for("static", filename="js/ajax_actions/with_tooltip.js"),
            url_for("static", filename="js/jquery.cookie.js"),
        ]

        return super(ModelViewWithExtraJs, self).render(template, **kwargs)

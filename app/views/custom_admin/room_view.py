from app.daos.metric import MetricsDAO
from app.models.metrics import AvailabeMetricsNames
from app.views.custom_admin.admin_protected_view import AdminProtectedView


class RoomView(AdminProtectedView):
    """
    Admin view for the Rooms
    """

    form_excluded_columns = ["indico_events", "contributions"]

    def on_model_delete(self, model):
        super(RoomView, self).on_model_delete(model)
        MetricsDAO.create(
            AvailabeMetricsNames.REMOVED_ROOM, AvailabeMetricsNames.REMOVED_ROOM
        )

    def on_model_change(self, form, model, is_created):
        super(RoomView, self).on_model_change(form, model, is_created)
        if is_created:
            MetricsDAO.create(
                AvailabeMetricsNames.ADDED_ROOM, AvailabeMetricsNames.ADDED_ROOM
            )

import logging

from app.extensions import celery
from app.services.indico.indico_service_2 import IndicoServiceV2

logger = logging.getLogger("job.indico_events")


@celery.task
def fetch_next_events():
    """
    Fetch next events from Indico.
    """
    logger.info("Task run: fetch_next_events. INIT.")
    try:
        indico = IndicoServiceV2()
        indico.get_events(static=False)
        logger.info("Task run: fetch_next_events. END.")
    except Exception as ex:
        logger.error("Failed to fetch next events. Error: %s" % ex)

import datetime
import logging
import time

import pytz
from sqlalchemy import or_

from app.daos.indico_event_contribution import IndicoEventContributionDAO
from app.extensions import celery
from app.models.contribution_workflow.transcription_workflow import TranscriptionStates
from app.models.events import IndicoEventContribution
from app.services.service_now.service_now_service import ServiceNowService
from app.services.transcription.transcription_service import TranscriptionService

logger = logging.getLogger("job.request_transcription")


@celery.task
def request_transcription(contribution_internal_id: int):
    """
    :return: "OK" when finished
    """
    contribution = IndicoEventContribution.query.get(contribution_internal_id)
    logger.info(f"{contribution.contribution_id}: Start request_transcription task")

    transcription_service = TranscriptionService(contribution, logger=logger)
    indico_contribution_dao = IndicoEventContributionDAO(logger=logger)
    try:
        result = transcription_service.request_transcription_for_contribution()
    except FileNotFoundError as error:
        logger.error(f"Error requesting transcription. ({error})")
        indico_contribution_dao.set_transcription_status(
            contribution.contribution_id, TranscriptionStates.ERROR
        )
        ServiceNowService(contribution, logger=logger).add_work_note(
            "Error requesting transcription"
        )
        return None

    if result:
        logger.info(f"{contribution.contribution_id}: Transcription requested.")
        indico_contribution_dao.set_transcription_status(
            contribution.contribution_id, TranscriptionStates.WAITING
        )
        ServiceNowService(contribution, logger=logger).add_work_note(
            "Transcription requested"
        )

        return "OK"

    logger.warning(f"{contribution.contribution_id}: Error requesting transcription.")
    error_message = "Error requesting transcription."
    indico_contribution_dao.set_transcription_status(
        contribution.contribution_id, TranscriptionStates.ERROR
    )
    ServiceNowService(contribution, logger=logger).add_work_note(error_message)

    return "ERROR"


@celery.task
def request_ttaas_transcriptions():
    logger.info(f"Start request_ttaas_transcriptions task : {time.ctime()}")

    since = datetime.datetime.now(pytz.timezone("Europe/Zurich")) - datetime.timedelta(
        days=60
    )

    contributions = (
        IndicoEventContribution.query.filter(
            IndicoEventContribution.start_date >= since
        )
        .filter(IndicoEventContribution.opencast_processing_finished)
        .filter(IndicoEventContribution.opencast_acls_finished)
        .filter(IndicoEventContribution.run_workflows)
        .filter(
            IndicoEventContribution.transcription_requested_status
            == TranscriptionStates.NOT_REQUESTED
        )
        .filter(
            or_(
                IndicoEventContribution.do_not_transcribe == False,  # noqa: E712
                IndicoEventContribution.do_not_transcribe == None,  # noqa: E711
            )
        )
        .all()
    )

    logger.debug(f"Requesting transcription for {len(contributions)} contributions")
    for contribution in contributions:
        logger.debug(
            f"{contribution.contribution_id}: Request Transcription contribution"
        )
        request_transcription.apply_async(queue="transcription", args=[contribution.id])

    return "OK"

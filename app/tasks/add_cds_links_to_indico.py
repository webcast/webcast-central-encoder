import datetime
import logging
import time

import pytz
from flask import current_app

from app.extensions import celery
from app.models.events import IndicoEventContribution
from app.services.indico.indico_eagreement_service import IndicoEagreementStatus
from app.services.indico.indico_service_2 import IndicoServiceV2

logger = logging.getLogger("job.add_cds_links_indico")


@celery.task
def add_cds_links_to_indico():
    """


    :return: "OK" when finished
    """
    logger.info("Start add_cds_links_to_indico task : %s" % time.ctime())

    since = datetime.datetime.now(pytz.timezone("Europe/Zurich")) - datetime.timedelta(
        days=60
    )
    contributions = (
        IndicoEventContribution.query.filter(
            IndicoEventContribution.e_agreement_status.in_(
                [IndicoEagreementStatus.ACCEPTED]
            )
        )
        .filter(IndicoEventContribution.start_date >= since)
        .filter_by(opencast_processing_finished=True)
        .filter_by(is_cds_record_set=True)
        .filter_by(run_workflows=True)
        .filter_by(is_cds_link_published=False)
        .all()
    )

    for contribution in contributions:
        logger.debug(
            "{}: Setting CDS link on Indico for contribution".format(
                contribution.contribution_id
            )
        )
        if current_app.config["IS_DEV"]:
            logger.warning(
                """{}: Skipping CDS link on Indico for contribution (IS_DEV is set to True.
                Set it to False if you want the links to appear on Indico)""".format(
                    contribution.contribution_id
                )
            )
            break
        try:
            # We create the SNOW ticket if possible
            result = IndicoServiceV2().insert_cds_link(contribution)
            if result:
                logger.debug(
                    "{}: CDS link set on Indico successfully for contribution".format(
                        contribution.contribution_id
                    )
                )
            else:
                logger.warning(
                    "{}: Unable to set CDS link on Indico for contribution".format(
                        contribution.contribution_id
                    )
                )
        except Exception as e:
            logger.error(
                "{}: Unable to set CDS link on Indico for contribution (Error: {})".format(
                    contribution.contribution_id, e
                )
            )

    return "OK"

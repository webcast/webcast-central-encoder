import logging
import time

from flask import current_app

from app.extensions import celery
from app.services.move_files.move_files_from_folder_service import (
    MoveFilesFromFolderService,
)

logger = logging.getLogger("job.move_epiphan_files")


@celery.task
def move_epiphan_recordings():
    start_time = time.ctime()
    logger.info(f"Task run: move_epiphan_recordings {start_time}")

    move_files_service = MoveFilesFromFolderService(logger=logger)
    move_files_service.move_files_to_subfolders(
        current_app.config["EPIPHAN_NANO_RECORDING_PATH"]
    )
    logger.info(f"Task finished: move_epiphan_recordings {start_time}")

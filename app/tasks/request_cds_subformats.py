import datetime
import logging
import time

import pytz

from app.extensions import celery
from app.models.events import IndicoEventContribution
from app.services.cds.cds_record_service import CdsRecordService
from app.services.indico.indico_eagreement_service import IndicoEagreementStatus

logger = logging.getLogger("job.cds_subformats")


def get_contributions():
    # Only handle new events
    events_newer_than_days = 60
    events_since = datetime.datetime.now(
        pytz.timezone("Europe/Zurich")
    ) - datetime.timedelta(days=events_newer_than_days)
    # Wait several minutes before requesting subformats after the cds record was created
    minutes_after_cds_record_set = 10
    cds_records_since = datetime.datetime.now(
        pytz.timezone("Europe/Zurich")
    ) - datetime.timedelta(minutes=minutes_after_cds_record_set)
    contributions = (
        IndicoEventContribution.query.filter(
            IndicoEventContribution.e_agreement_status.in_(
                [IndicoEagreementStatus.ACCEPTED, IndicoEagreementStatus.FORCED]
            )
        )
        .filter(IndicoEventContribution.start_date >= events_since)
        .filter(IndicoEventContribution.is_cds_record_set_date < cds_records_since)
        .filter(IndicoEventContribution.is_cds_subformats_requested_status != "OK")
        .filter_by(
            opencast_processing_finished=True,
            opencast_acls_finished=True,
            is_cds_record_set=True,
            is_cds_record_requested_status="OK",
            run_workflows=True,
        )
        .all()
    )
    return contributions


@celery.task
def request_cds_subformats():
    """
    - Iterate all contributions with `ACCEPTED` eagreements and not older than.
    2 monhts and processing_finished=True and NO CDS record.
    - For each one, trigger the CDS record request

    :return: "OK" when finished
    """
    logger.info("Start request_cds_subformats task : %s" % time.ctime())

    contributions = get_contributions()

    logger.debug(
        "Requesting CDS subformats for {} contributions".format(len(contributions))
    )
    for contribution in contributions:
        logger.debug(
            "{}: Request CDS subformats for contribution".format(
                contribution.contribution_id
            )
        )

        # We create the SNOW ticket if possible
        try:
            result = CdsRecordService(
                contribution, logger=logger
            ).request_cds_subformats_ready_datajson()
            if result:
                logger.debug(
                    "{}: CDS subformats successfully requested for contribution".format(
                        contribution.contribution_id
                    )
                )
            else:
                logger.warning(
                    "{}: Unable to request CDS subformats for contribution".format(
                        contribution.contribution_id
                    )
                )
        except Exception as e:
            logger.warning(
                "{}: Unable to request CDS subformats for contribution ({})".format(
                    contribution.contribution_id, e
                )
            )

    return "OK"

import logging

from app.extensions import celery
from app.models.events import IndicoEventContribution
from app.services.opencast.opencast_media_package_service import (
    OpencastMediaPackageService,
)
from app.services.opencast.opencast_service import OpencastService

logger = logging.getLogger("job.opencast")


@celery.task()
def ingest_to_opencast_step_by_step_task(
    contribution_internal_id, edit_on_opencast: str, add_transcription: str
):
    """

    :param contribution_internal_id:
    :type contribution_internal_id:
    :param edit_on_opencast:
    :type edit_on_opencast:
    :param add_transcription:
    :type add_transcription:
    :return:
    :rtype:
    """
    logger.info("Task run: ingest_to_opencast_step_by_step_task")
    contribution = IndicoEventContribution.query.get(contribution_internal_id)
    media_package_service = OpencastMediaPackageService(contribution, logger=logger)
    result = media_package_service.create_media_package_files()

    if result == "ERROR":
        return result

    opencast_service = OpencastService(contribution, logger=logger)
    result = opencast_service.ingest_opencast_media_package(
        edit_on_opencast, add_transcription
    )
    return result

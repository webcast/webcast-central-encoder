import datetime
import logging
import time

import pytz
from sqlalchemy import or_

from app.extensions import celery
from app.models.events import IndicoEventContribution
from app.services.indico.indico_eagreement_service import IndicoEagreementStatus
from app.services.opencast.opencast_service import OpencastService

logger = logging.getLogger("job.opencast_acls")


@celery.task
def request_opencast_acls():
    """
    - Iterate all contributions whith `ACCEPTED` eagreements and not older than 2
    monhts and processing_finished=True and NO opencast_acls_requested.
    - For each one, request the ACLs refresh

    :return: "OK" when finished
    """
    logger.info("Start request_acls task : %s" % time.ctime())

    since = datetime.datetime.now(pytz.timezone("Europe/Zurich")) - datetime.timedelta(
        days=60
    )

    contributions = (
        IndicoEventContribution.query.filter(
            IndicoEventContribution.e_agreement_status.in_(
                [IndicoEagreementStatus.ACCEPTED, IndicoEagreementStatus.FORCED]
            )
        )
        .filter(IndicoEventContribution.start_date >= since)
        .filter(
            or_(
                IndicoEventContribution.opencast_acls_requested_status != "ERROR",
                IndicoEventContribution.opencast_acls_requested_status.is_(None),
            )
        )
        .filter(IndicoEventContribution.opencast_processing_finished == True)  # noqa
        .filter(IndicoEventContribution.opencast_acls_requested == False)  # noqa
        .filter(IndicoEventContribution.run_workflows == True)  # noqa
        .all()
    )

    logger.debug("Requesting ACLs for {} contributions".format(len(contributions)))
    errored_contributions = []
    for contribution in contributions:
        logger.debug(
            "{}: Request Opencast new ACLs for contribution".format(
                contribution.contribution_id
            )
        )

        # We create the SNOW ticket if possible
        result = OpencastService(
            contribution, logger=logger
        ).request_acl_update_workflow()
        if not result:
            errored_contributions.append(contribution.contribution_id)

    if len(errored_contributions) > 0:
        logger.warning(
            "Unable to set ACLs for the following contributions: {}".format(
                errored_contributions
            )
        )

    return "OK"

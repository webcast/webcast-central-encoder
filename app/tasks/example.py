import logging
import time

from app.extensions import celery

# logger = get_task_logger(__name__)

logger = logging.getLogger("job.example")


@celery.task
def dummy_task():
    """
    Example task that only returns "OK"
    :return: "OK" when finished
    """
    logger.info("Start dummy task : %s" % time.ctime())
    print("Start dummy task : %s" % time.ctime())
    return "OK"


@celery.task
def check():
    logger.info("Checking every 10 seconds")
    print("Print Checking every 10 seconds")

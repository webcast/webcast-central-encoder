import logging
import os
import shutil
import time

from flask import current_app, render_template

from app.extensions import celery
from app.services.mailer.mail_service import MailService

logger = logging.getLogger("job.media_packages")


def populate_disk_space_email(total_space, free_space):
    prefix = ""
    if current_app.config.get("IS_DEV", False):
        prefix = "-TEST"
    subject = (
        "[CES2"
        + prefix
        + "] Disk is getting full: "
        + current_app.config["INSTANCE_HOSTNAME"]
    )

    email_template = render_template(
        "emails/disk_space.html",
        current_instance=current_app.config["INSTANCE_HOSTNAME"],
        total_space=total_space,
        free_space=free_space,
    )

    return subject, email_template


@celery.task
def remove_older_content(path="/downloads/opencast", older_than=(7 * 86400)):
    """

    :param path:
    :type path:
    :param older_than:
    :type older_than:
    :return:
    :rtype:
    """
    now = time.time()
    removed_files = []
    logger.info("Task run: remove_old_media_packages.")
    logger.info(os.listdir(path))
    for filename in os.listdir(path):
        file_path = os.path.join(path, filename)
        filestamp = os.stat(file_path).st_mtime
        filecompare = now - older_than
        if filestamp < filecompare:
            print("{} will be removed".format(file_path))
            removed_files.append(file_path)
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
            # os.remove(file_path)
    logger.info(
        "Task run: remove_old_media_packages. Removed files: {}".format(removed_files)
    )
    return "OK"


@celery.task
def check_disk_space(path="/downloads"):
    logger.info("Task run: check_disk_space.")
    logger.info(os.listdir(path))
    total, used, free = shutil.disk_usage(current_app.config["DOWNLOADS_FOLDER_PATH"])

    total_space = total // (2**30)
    used_space = used // (2**30)
    free_space = used // (2**30)

    if total > 0 and used_space / total_space >= 0.9:
        subject, message = populate_disk_space_email(total_space, free_space)
        service = MailService(None, logger=logger)
        service.send_html_mail(
            current_app.config["MAIL_FROM"],
            current_app.config["MAIL_TO"],
            subject,
            message,
        )

    logger.info("Task run: check_disk_space ")
    return "OK"

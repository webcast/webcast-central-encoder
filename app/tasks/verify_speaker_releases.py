import datetime
import logging
import time

import pytz

from app.extensions import celery
from app.models.events import IndicoEvent
from app.services.indico.indico_api import IndicoAPI
from app.services.indico.indico_eagreement_service import (
    IndicoEagreemenentService,
    IndicoEagreementStatus,
)

logger = logging.getLogger("job.speaker_releases")


def must_verify_speakers_release(contribution):
    if (
        contribution.opencast_processing_finished is True
        and contribution.run_workflows is True
        and contribution.is_cds_record_set in [None, False]
        and contribution.e_agreement_status
        in [
            IndicoEagreementStatus.PENDING,
            IndicoEagreementStatus.EMAIL_NOT_SENT,
            IndicoEagreementStatus.SENT,
            IndicoEagreementStatus.NO_INFORMATION,
            IndicoEagreementStatus.UNKNOWN,
        ]
    ):
        return True
    return False


@celery.task
def verify_speaker_releases():
    """
    Iterate all contributions with PENDING eagreements and not older than… 2 months?
    For each one, fetch the eagreement status.

    :return: "OK" when finished
    """
    logger.info("Start verify_speaker_releases task : %s" % time.ctime())

    since = datetime.datetime.now(pytz.timezone("Europe/Zurich")) - datetime.timedelta(
        days=30
    )

    indico_events = IndicoEvent.query.filter(IndicoEvent.date >= since).all()

    for event in indico_events:
        response_json = IndicoAPI().fetch_agreement(event.indico_id)
        for contribution in event.contributions:
            try:
                if must_verify_speakers_release(contribution):
                    logger.info(
                        "{}: Fetching agreement for contribution.".format(
                            contribution.contribution_id
                        )
                    )
                    result = IndicoEagreemenentService(
                        contribution, logger=logger
                    ).fetch_eagreement_and_update(api_response=response_json)
                    if result:
                        logger.info(
                            "{}: E-Agreement fetched successfully for contribution.".format(
                                contribution.contribution_id
                            )
                        )
                    else:
                        logger.warning(
                            "{}: Unable fetch the e-agreement".format(
                                contribution.contribution_id
                            )
                        )
            except Exception as e:
                logger.warning(
                    "{}: Unable fetch the e-agreement. Exception: {}".format(
                        contribution.contribution_id, str(e)
                    )
                )
    return "OK"

import datetime
import logging
import time

import pytz
from sqlalchemy import or_

from app.extensions import celery
from app.models.events import IndicoEventContribution
from app.services.cds.cds_record_service import CdsRecordService
from app.services.indico.indico_eagreement_service import IndicoEagreementStatus

logger = logging.getLogger("job.cds_records")


@celery.task
def request_cds_records():
    """
    - Iterate all contributions with `ACCEPTED` eagreements and not older than.
    2 monhts and processing_finished=True and NO CDS record.
    - For each one, trigger the CDS record request

    :return: "OK" when finished
    """
    logger.info("Start request_cds_records task : %s" % time.ctime())

    since = datetime.datetime.now(pytz.timezone("Europe/Zurich")) - datetime.timedelta(
        days=60
    )
    contributions = (
        IndicoEventContribution.query.filter(
            IndicoEventContribution.e_agreement_status.in_(
                [IndicoEagreementStatus.ACCEPTED, IndicoEagreementStatus.FORCED]
            )
        )
        .filter(IndicoEventContribution.start_date >= since)
        .filter(
            or_(
                IndicoEventContribution.is_cds_record_set == None,  # noqa
                IndicoEventContribution.is_cds_record_set == False,  # noqa
            )
        )
        .filter(
            or_(
                IndicoEventContribution.is_cds_record_requested == None,  # noqa
                IndicoEventContribution.is_cds_record_requested == False,  # noqa
            )
        )
        .filter(
            or_(
                IndicoEventContribution.is_cds_record_requested_status != "ERROR",
                IndicoEventContribution.is_cds_record_requested_status.is_(None),
            )
        )
        .filter_by(
            opencast_processing_finished=True,
            opencast_acls_finished=True,
            run_workflows=True,
        )
        .all()
    )

    logger.debug(
        "Requesting CDS records for {} contributions".format(len(contributions))
    )
    for contribution in contributions:
        logger.debug(
            "{}: Request CDS record creation for contribution".format(
                contribution.contribution_id
            )
        )

        # We create the SNOW ticket if possible
        try:
            result = CdsRecordService(
                contribution, logger=logger
            ).request_create_cds_record()
            if result:
                logger.debug(
                    "{}: CDS record successfully requested for contribution".format(
                        contribution.contribution_id
                    )
                )
            else:
                logger.warning(
                    "{}: Unable to request CDS record for contribution".format(
                        contribution.contribution_id
                    )
                )
        except Exception as e:
            logger.warning(
                "{}: Unable to request CDS record for contribution ({})".format(
                    contribution.contribution_id, e
                )
            )

    return "OK"

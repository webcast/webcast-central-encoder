from app.api.public.v1.load_api import load_api_namespaces as load_api_public_namespaces


def load_api_public_blueprints():
    """
    Load all the blueprints, adding the development ones if needed

    :return: A tuple of Blueprint objects
    :rtype: list[flask.Blueprint]
    """
    all_blueprints = (load_api_public_namespaces(),)

    return all_blueprints

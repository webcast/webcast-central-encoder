function validateField(fieldId) {

}

function submitSplitForm(formId) {
    console.log(`Submitting current form ${formId}`)
    // Camera
    let cameraStartTime = document.getElementById("camera_start_time_" + formId);
    let cameraStartTimeMessage = document.getElementById("camera_start_time_" + formId + "_message");
    cameraStartTimeMessage.innerText = ""

    let cameraEndTime = document.getElementById("camera_end_time_" + formId);
    let cameraEndTimeMessage = document.getElementById("camera_end_time_" + formId + "_message");
    cameraEndTimeMessage.innerText = ""

    //Slides
    let slidesStartTime = document.getElementById("slides_start_time_" + formId);
    let slidesStartTimeMessage = document.getElementById("slides_start_time_" + formId + "_message");
    slidesStartTimeMessage.innerText = ""

    let slidesEndTime = document.getElementById("slides_end_time_" + formId);
    let slidesEndTimeMessage = document.getElementById("slides_end_time_" + formId + "_message");
    slidesEndTimeMessage.innerText = ""

    let submit_button = document.getElementById("submit_" + formId);
    submit_button.firstChild.data = "Saving...";

    let regex = /^(([0-9]?\d|2[0-9])(?::([0-9]?\d))?(?::([0-9]?\d)))?$/;

    let cameraStartValid = regex.test(cameraStartTime.value);
    let cameraEndValid = regex.test(cameraEndTime.value);

    let slidesStartValid = regex.test(slidesStartTime.value);
    let slidesEndValid = regex.test(slidesEndTime.value);

    if (cameraStartValid && cameraEndValid && slidesStartValid && slidesEndValid) {
        $.ajax({
            'url': '/_admin_actions/_create_contribution_splits/' + formId,
            'data': {
                contribution_id: formId,
                camera_start_time: cameraStartTime.value,
                camera_end_time: cameraEndTime.value,
                slides_start_time: slidesStartTime.value,
                slides_end_time: slidesEndTime.value,
            },
            'dataType': 'json',
            'method': 'POST'
        }).then(function (data) {
            console.log("Success", data);
            cameraStartTimeMessage.innerText = "Saved";
            cameraStartTimeMessage.style.color = "green";
            cameraEndTimeMessage.innerText = "Saved";
            cameraEndTimeMessage.style.color = "green";

            slidesStartTimeMessage.innerText = "Saved";
            slidesStartTimeMessage.style.color = "green";
            slidesEndTimeMessage.innerText = "Saved";
            slidesEndTimeMessage.style.color = "green";

            submit_button.firstChild.data = "Save";
            return data;
        }).catch(function (error) {
            console.error(error);
            submit_button.firstChild.data = "Save";
        });
    } else {
        console.warn("Fields are not valid");
        if (!cameraStartValid) {
            cameraStartTimeMessage.innerText = "This field is not valid"
            cameraStartTimeMessage.style.color = "red";
        }
        if (!cameraEndValid) {
            cameraEndTimeMessage.innerText = "This field is not valid"
            cameraEndTimeMessage.style.color = "red";
        }
        if (!slidesStartValid) {
            slidesStartTimeMessage.innerText = "This field is not valid"
            slidesStartTimeMessage.style.color = "red";
        }
        if (!slidesEndValid) {
            slidesEndTimeMessage.innerText = "This field is not valid"
            slidesEndTimeMessage.style.color = "red";
        }
        submit_button.firstChild.data = "Save";
    }

};
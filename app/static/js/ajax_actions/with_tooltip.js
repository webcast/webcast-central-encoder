$(document).ready(function () {
    $('.with-tooltip').tooltip();
});

function set_recording_path(url) {
    $("#recording_path").val(url);
}

$(document).on('paste', '#zoom_meeting_id', function(e) {
  e.preventDefault();
  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
  withoutSpaces = withoutSpaces.replace(/\s+/g, '');
  $(this).val(withoutSpaces);
});

function fetch_recordings() {
    let csrf_token = $.cookie('csrftoken');
    let meetingId = $("#zoom_meeting_id").val();
    console.log(`Fetching recordings for meeting ID ${meetingId}...`);

    $.ajax({
        'url': '/_admin_actions/_get_meeting_recordings/',
        'data': {
            meeting_id: meetingId
        },
        'dataType': 'json',
        'method': 'POST',
        'headers': {
            'X-CSRFToken': csrf_token
        }
    }).then(function (data) {// console.log("Success response from get meeting recordings")
        // $(document).trigger('eventFlashMessage', [data.result, data.msg]);
        // reload next events
        console.log("Success");

        let tableDiv = $("#recordings-table-div");
        var content = `<table class='table table-responsive-md mb-3 mt-3'>
                        <thead>
                        <tr>
                        <th>Recording URL</th>
                        <th>Size</th>
                        <th>Duration</th>
                        <th>Status</th>
                        <th>Action</th>
                        </tr>`

        data.response_data.msg.recording_files.forEach(function (element) {
            if (element.file_extension.toLowerCase() === "mp4") {
                content += `<tr>
                    <td>${element.download_url}</td>
                    <td>${element.file_size/1000000} Mb (${element.file_extension})</td>
                    <td>${Math.abs(new Date(element.recording_start) - new Date(element.recording_end)) / 1000} s</td>
                    <td>${element.status}</td>
                    <td><button type="button" class="btn btn-primary" onClick="set_recording_path('${element.download_url}')">Use this one</button></td>
                    </tr>`;
            }
        })

        content += "</table>"
        content += `<div class="alert alert-warning mb-3">Don't forget to save after setting up the recording path.</div>`
        tableDiv.append(content)

    }).catch(function (error) {
        console.error("Error fetching the Zoom meeting's recordings")
        console.log(error)
    });
}

function login_user(){
  $.ajax({
      'url':'/login/login_user/',
      'dataType': 'json',
      'method': 'POST'
  }).success(function(data) {
      // show alert
      $(document).trigger('eventFlashMessage', [data['response_data'].result, data['response_data'].msg]);
      // reload next events
      show_next_events();
    })
}
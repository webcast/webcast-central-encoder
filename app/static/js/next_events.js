$(document).ready(function() {
  // activate tooltips for buttons
  $(".is_tooltip").tooltip();

});

function toggle_encoding_details(container_id) {
  $("#" + container_id).toggle();
}


/* TEST PROGRAM ***********************************************************************************/

function start_test() {
  // if btn is disabled, disable click
  if (!$('#btn_toggle_test').attr('disabled')) {
    var encoder = $('#sel_encoder_test').val();
    // disable all buttons
    $('.btn_start_event').attr('disabled', 'disabled');
    $('#span_toggle_test').html("Starting...");
    var csrf_token = $.cookie('csrftoken');
    $.ajax({
      'url': '_get_next_events/start_test/',
      'data': {
        'encoder_id': encoder
      },
      'dataType': 'json',
      'method': 'POST',
      'async': 'true',
      'headers': {
        'X-CSRFToken': csrf_token
      }
    }).success(function(data) {
      // show alert

      $(document).trigger('eventFlashMessage', [data['response_data'].result, data['response_data'].msg]);

      // reload next events
      show_next_events();
    }).fail(function(error) {
      console.log("ERROR")
    });

  }
}


function stop_test() {
  // if btn is disabled, disable click
  if (!$('#btn_toggle_test').attr('disabled')) {
    var encoder = $('#sel_encoder_test').val();

    // disable all buttons
    $('.btn_start_event').attr('disabled', 'disabled');
    $('#span_toggle_test').html("Stopping...");
    var csrf_token = $.cookie('csrftoken');
    $.ajax({
      'url': '_get_next_events/_stop_test/',
      'data': {
        'encoder_id': encoder
      },
      'dataType': 'json',
      'method': 'POST',
      'headers': {
        'X-CSRFToken': csrf_token
      }
    }).success(function(data) {
      // show alert
      $(document).trigger('eventFlashMessage', [data['response_data'].result, data['response_data'].msg]);
      // reload next events
      show_next_events();
    }).fail(function(error) {
      console.log("ERROR")
    });
  }
}


/* OTHER ***********************************************************************************/

function nextevents_toggle_container(enabled, btn_enable, btn_disable, hidden_input, container_to_toggle) {
  if (enabled) {
    $('#' + btn_enable).addClass('btn-primary disabled');
    $('#' + btn_disable).removeClass('btn-primary disabled');

    $('#' + hidden_input).val("True");

    if (!$('#' + container_to_toggle).is(":visible"))
      $('#' + container_to_toggle).fadeIn('fast');
  } else {
    $('#' + btn_disable).addClass('btn-primary disabled');
    $('#' + btn_enable).removeClass('btn-primary disabled');

    $('#' + hidden_input).val("False");

    if ($('#' + container_to_toggle).is(":visible"))
      $('#' + container_to_toggle).fadeOut('fast');
  }
}


/* GET EVENTS FROM INDICO BUTTONS ***********************************************************************************/

function force_reload_next_events() {
  $.get('_get_next_events/', {
    'forced': true
  },
      function(data) {
    $('#next_events').html(data);
  });
}


$("#btn_ne_refetch_all_events").click(function() {
  force_reload_next_events();
});


$("#btn_ne_get_event").click(function() {
  // clean up previous values
  $('#span_gie_indico_id').html("");
  $('#form_gie_indico_id').val("");

  var indico_id = $('#indico_id_ne_get_event').val();
  if (indico_id.length != 0) {
    // show modal
    $('#span_gie_indico_id').html(indico_id);
    $('#form_gie_indico_id').val(indico_id);
    $('#modal_get_indico_event').modal('show');

  }

});

$("#btn_gie_recording").click(function() {
  $('#btn_gie_recording').addClass('btn-primary disabled');
  $('#btn_gie_webcast').removeClass('btn-primary disabled');

  $('#form_gie_webcast').val("False");
});

$("#btn_gie_webcast").click(function() {
  $('#btn_gie_webcast').addClass('btn-primary disabled');
  $('#btn_gie_recording').removeClass('btn-primary disabled');

  $('#form_gie_webcast').val("True");
});

$("#btn_gie_submit").click(function() {
  var csrf_token = $.cookie('csrftoken');
  if (!$('#btn_gie_submit').attr('disabled')) {
    // hide the modal
    $('#modal_get_indico_event').modal('hide');
    $('#btn_gie_submit').attr('disabled', 'disabled');
    var data = $("#form_get_indico_event").serialize()
    // send form
    $.ajax({
      'url': '_get_next_events/_get_indico_event/',
      'data': data,
      'dataType': 'json',
      'method': 'POST',
      'headers': {
        'X-CSRFToken': csrf_token
      }
    }).success(function(data) {
      // show alert

      $(document).trigger('eventFlashMessage', [data['response_data'].result, data['response_data'].msg]);
      // reload next events

      show_next_events();
    });
  }
});



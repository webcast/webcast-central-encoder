function set_form_cookie_listener(id, form) {
    $('.'+ form ).change(function () {

        // new cookie is set when the option is changed

        $.cookie(id, $('.' + form + ' option:selected').val(), {expires: 90, path: '/'});
        location.reload();
    });
}


function get_form_cookie(id, form) {
    if($.cookie(id) != null)
    {
        // set the option to selected that corresponds to what the cookie is set to
        $('.' + form + ' option[value="' + $.cookie(id) + '"]').attr('selected', 'selected');

    }
}

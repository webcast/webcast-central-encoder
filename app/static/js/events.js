function set_running_processes_value(event, roomencoder, fmle_processes) {
  if (roomencoder.toString() === $('.form-control-' + event + ' option:selected').val()) {
    var running = $('#running_fmle_' + event);
    running.append("Running FMLE: " + fmle_processes);
  }

}

function start_event(indico_id, start_recording_only, monitor) {
  // if btn is disabled, disable click
  if (!$('#btn_toggle_event_' + indico_id).attr('disabled')) {

    var csrf_token = $.cookie('csrftoken');
    var encoder = ($('#sel_encoder_event_' + indico_id).val());
    // disable all buttons
    $('.btn_start_event').attr('disabled', 'disabled');
    $('#span_toggle_event_' + indico_id).html("Starting...");
    $.ajax({
      'url': '/_get_next_events/_start_event/',
      'data': {
        'indico_id': indico_id,
        'encoder_id': encoder,
        'start_recording_only': start_recording_only
      },
      'dataType': 'json',
      'method': 'POST',
      'headers': {
        'X-CSRFToken': csrf_token
      }
    }).success(function(data) {
      // show alert
      $(document).trigger('eventFlashMessage', [data.result, data.msg]);
      // reload next events or monitor
      if (monitor === 'True') {
        show_monitor();
      } else {
        show_next_events();
      }

    });

  }
}

function stop_event(indico_id,monitor) {
  $('#span_toggle_event_' + indico_id).html("Stopping...");
  var csrf_token = $.cookie('csrftoken');
  var encoder = ($('#sel_encoder_event_' + indico_id).val());
  $.ajax({
    'url': '/_get_next_events/_stop_event/',
    'data': {
      'indico_id': indico_id,
      'encoder_id': encoder
    },
    'dataType': 'json',
    'method': 'POST',
    'headers': {
      'X-CSRFToken': csrf_token
    }
  }).success(function(data) {
    // show alert
    $(document).trigger('eventFlashMessage', [data.result, data.msg]);
    // reload next events or monitor
    if (monitor === 'True') {
          show_monitor();
    }
    else {
      show_next_events();
      //location.reload(); // edw isws na eprepe na kanei load to get_next events
    }
  });
}

// Set button in Set STOP time modal window
$("#btn_set_submit").click(function() {
  var csrf_token = $.cookie('csrftoken');
  if (!$('#btn_set_submit').attr('disabled')) {
    // hide the modal
    $('#modal_set_stop_time').modal('hide');
    $('#btn_set_submit').attr('disabled', 'disabled');
    $('#form_set_stop_time_control').val("1");
    // send form
    $.ajax({
      'url': '/_get_next_events/_set_stop_time/',
      'data': $("#form_set_stop_time").serialize(),
      'method': 'POST',
      'headers': {
        'X-CSRFToken': csrf_token
      }
    }).success(function(data) {
      // show alert
      $(document).trigger('eventFlashMessage', [data.result, data.msg]);
      // reload next events
      show_next_events();
    });
  }
});

// Reset button in modal window
$("#btn_reset").click(function() {
  var csrf_token = $.cookie('csrftoken');
  if (!$('#btn_set_submit').attr('enabled')) {
    // hide the modal
    $('#modal_set_stop_time').modal('hide');
    $('#btn_set_submit').attr('enabled', 'enabled');
    $('#form_set_stop_time_control').val("0");
    // send form
    $.ajax({
      'url': '/_get_next_events/_set_stop_time/',
      'data': $("#form_set_stop_time").serialize(),
      'dataType': 'json',
      'method': 'POST',
      'headers': {
        'X-CSRFToken': csrf_token
      }
    }).success(function(data) {
      // show alert
      $(document).trigger('eventFlashMessage', [data.result, data.msg]);
      // reload next events
      show_next_events();
    });
  }
});


function set_stop_event(indico_id, stop_time) {
  // clean up previous values
  $('#span_indico_id').html("");
  $('#form_set_stop_time_indico_id').val("");

  if (stop_time !== 'None') {
    $('#span_stop_time').val(stop_time);

  } else {
    $('#btn_reset').hide()
    $('#span_stop_time').val("");
  }

  if (indico_id.length !== '') {
    // show modal
    $('#span_indico_id').html(indico_id);
    $('#form_set_stop_time_indico_id').val(indico_id);
    $('#modal_set_stop_time').modal('show');
  }
}

function delete_static_event(indico_id) {
  var csrf_token = $.cookie('csrftoken');
  $.ajax({
    'url': '/_get_next_events/_delete_static_event/',
    'data': {
      'indico_id': indico_id
    },
    'dataType': 'json',
    'method': 'POST',
    'headers': {
      'X-CSRFToken': csrf_token
    }
  }).success(function(data) {
    // show alert

    $(document).trigger('eventFlashMessage', [data.result, data.msg]);
    // reload next events
    show_next_events();
  });
}


function reset_fmle(indico_id) {

  var csrf_token = $.cookie('csrftoken');
  if (confirm('All FMLE running processes/programs will be killed! Are you sure?')) {
    $.ajax({
      'url': '/_get_next_events/_reset_fmle/',
      'data': {
        'indico_id': indico_id
      },
      'dataType': 'json',
      'method': 'POST',
      'headers': {
        'X-CSRFToken': csrf_token
      }
    }).success(function(data) {
      // show alert
      $(document).trigger('eventFlashMessage', [data.result, data.msg]);
      // reload next events
      show_next_events();
    });
  }
}


function show_encoder_details(indico_id) {
    $("#encoder_details_"+indico_id).slideToggle();
}

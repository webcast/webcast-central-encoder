$(document).ready(function(){

    // modal loader
    $(document).ajaxStart(function() {
        $("body").addClass("loading");
    }).ajaxStop(function() {
        $("body").removeClass("loading");
    });

    // event listener for alert box
    $(document).bind('eventFlashMessage', function(event, type, text){
        // hide before if not hidden
        $("#alertbox-container").hide();
        // remove type class before
        $("#alertbox").removeClass('alert-danger alert-success alert-warning');
        switch(type){
            case "success":
                $("#alertbox").addClass('alert-success');
                $("#alertbox-title").html('SUCCESS');
                $("#alertbox-text").html(text);
                // info message shows for few secs
                $("#alertbox-container").fadeIn().delay(3000).fadeOut();
                break;
            case "error":
                $("#alertbox").addClass('alert-danger');
                $("#alertbox-title").html('ERROR');
                $("#alertbox-text").html(text);
                // error message does not disappear
                $("#alertbox-container").fadeIn();
                break;
            case "warning":
                $("#alertbox").addClass('alert-warning');
                $("#alertbox-title").html('WARNING');
                $("#alertbox-text").html(text);
                // info message shows for few secs
                $("#alertbox-container").fadeIn().delay(3000).fadeOut();
                break;
        }
    });

});
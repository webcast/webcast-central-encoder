# import datetime

from app.extensions import db
from app.models.base import ModelBase


class Settings(ModelBase):
    """
    Settings model object
    """

    __tablename__ = "settings"

    enable_ravem_notifications = db.Column(db.Boolean(False), default=False)
    enable_webcast_website_notifications = db.Column(db.Boolean(False), default=False)

    # ip2cc_report_email = db.Column(db.Text)
    # h323_prefixes = db.Column(db.Text)
    # # VRCs are expected to be split by ","
    # zoom_connector_ips = db.Column(db.Text, default=zoom_default_vrcs)
    # last_updated = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.utcnow)

import datetime

from app.extensions import db
from app.models.base import ModelBase


class AvailabeMetricsNames:
    PUBLISHED_CDS_LINK_INDICO = "Published CDS record link on Indico"
    PUBLISHED_VIDEO_PLAYER_LINK_INDICO = "Published video player link on Indico"
    SENT_TO_OPENCAST = "Sent to Opencast"
    USED_ZOOM_MEDIA_PACKAGE = "Used Zoom video package"
    USED_SMB_MEDIA_PACKAGE = "Used SMB video package"
    USED_URL_MEDIA_PACKAGE = "Used URL video package"
    STARTED_ENCODER = "Started encoder"
    STOPPED_ENCODER = "Stopped encoder"
    MONITORED_WEBCAST = "Monitored webcast"
    CREATED_SNOW_TICKET = "Created SNOW ticket"
    CUTTING_IN_OPENCAST = "Contribution set to be cut on Opencast"
    ADDED_ENCODER = "Added a new encoder"
    REMOVED_ENCODER = "Removed an encoder"

    ADDED_ROOM = "Added a new room"
    REMOVED_ROOM = "Removed a room"


class Metric(ModelBase):
    __tablename__ = "metric"

    name = db.Column(db.String(255))
    description = db.Column(db.String(255))

    created_date = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    last_updated = db.Column(db.DateTime)

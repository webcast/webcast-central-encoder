import datetime

from flask import current_app

from app.extensions import db
from app.models.base import ModelBase
from app.models.contribution_workflow.cds_workflow import CdsWorkflowAttributes
from app.models.contribution_workflow.indico_workflow import IndicoWorkflowAttributes
from app.models.contribution_workflow.opencast_workflow import (
    MediaPackageStatus,
    OpencastWorkflowAttributes,
    SentToOpencastStatus,
)
from app.models.contribution_workflow.snow_workflow import SnowWorkflowAttributes
from app.models.contribution_workflow.transcription_workflow import (
    TranscriptionStates,
    TranscriptionWorkflowAttributes,
)


class EventType:
    """
    Define valid values for Event Type
    """

    WEBLECTURE = "simple_event"
    CONFERENCE = "conference"
    MEETING = "meeting"


class PostprocessingType:
    """
    Define valid values for Postprocessing Type
    """

    WEB_LECTURE = "web_lecture"
    PLAIN_VIDEO = "plain_video"


class IndicoEvent(ModelBase):
    """
    Model for Indico Events
    """

    __tablename__ = "indico_event"

    indico_id = db.Column(db.String(512), unique=True)
    title = db.Column(db.String(512))
    first_contrib = db.Column(db.String(512))
    audience = db.Column(db.String(512))
    date = db.Column(db.Date)
    time = db.Column(db.Time)
    end_date = db.Column(db.Date)
    start_date_time = db.Column(db.DateTime)
    end_date_time = db.Column(db.DateTime)
    recording = db.Column(db.Boolean(True), default=True)
    webcast = db.Column(db.Boolean(False), default=False)
    # Workflow
    is_webcast_running = db.Column(db.Boolean(False), default=False)
    is_recording_running = db.Column(db.Boolean(False), default=False)
    is_recorded = db.Column(db.Boolean(False))
    # End workflow
    static = db.Column(db.Boolean(False))
    paused_for_splitting = db.Column(db.Boolean(False), default=False)
    custom_app_name = db.Column(db.String(100), nullable=True)
    reminder_sent = db.Column(db.Boolean(False), default=False)
    # needs ('Stop at', auto_now_add=False, blank=True)
    stop_time = db.Column(db.Time, nullable=True)
    recording_path = db.Column(db.String(255))

    description = db.Column(db.Text())
    allowed = db.Column(db.Text())

    creator = db.Column(db.String(255))
    creator_id = db.Column(db.String(255))
    creator_email = db.Column(db.String(255))

    category = db.Column(db.String(255))
    category_id = db.Column(db.Integer)
    event_type = db.Column(db.String(255), default="Unknown")

    room_id = db.Column(db.Integer, db.ForeignKey("room.id"))
    contributions = db.relationship(
        "IndicoEventContribution",
        backref="indico_event",
        lazy=True,
        cascade="all,delete",
    )

    # room = db.relationship('Room', uselist=False, back_populates='indico_event')

    def __repr__(self):
        return f"{self.title} - {self.indico_id}"

    @property
    def running(self):
        return self.is_webcast_running or self.is_recording_running

    @property
    def indico_url(self):
        return f"{current_app.config['INDICO_HOSTNAME']}/event/{self.indico_id}"


class IndicoEventContribution(
    ModelBase,
    CdsWorkflowAttributes,
    IndicoWorkflowAttributes,
    OpencastWorkflowAttributes,
    SnowWorkflowAttributes,
    TranscriptionWorkflowAttributes,
):
    """
    Model for Indico Events Contributions
    """

    __tablename__ = "indico_event_contribution"

    # The local ID
    indico_internal_id = db.Column(
        db.Integer, db.ForeignKey("indico_event.id"), nullable=False
    )

    split_start_time = db.Column(db.String(255))
    split_end_time = db.Column(db.String(255))

    # Separated splits for camera and slides
    camera_start_time = db.Column(db.String(255))
    camera_end_time = db.Column(db.String(255))
    slides_start_time = db.Column(db.String(255))
    slides_end_time = db.Column(db.String(255))

    zoom_meeting_id = db.Column(db.String(255))
    recording_path = db.Column(db.String(255))
    recording_path_date = db.Column(db.DateTime)

    has_camera_file = db.Column(db.Boolean(False))
    has_slides_file = db.Column(db.Boolean(False))

    created_date = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    last_updated = db.Column(db.DateTime)
    last_updated_person = db.Column(db.String(255))
    last_updated_username = db.Column(db.String(255))
    last_updated_action = db.Column(db.String(255))
    postprocessing_type = db.Column(db.String(255), default="")
    postprocessing_type_date = db.Column(db.DateTime)

    room_id = db.Column(db.Integer, db.ForeignKey("room.id"))

    # When this is set to False, the automatic workflows won't be run
    run_workflows = db.Column(db.Boolean, default=True)

    @property
    def is_recording_path_set(self):
        return self.recording_path and self.recording_path != ""

    @property
    def is_postprocessing_type_set(self):
        return self.postprocessing_type and self.postprocessing_type != ""

    @property
    def can_generate_snow_ticket(self):
        return (
            (not self.snow_ticket or self.snow_ticket == "")
            and self.is_recording_path_set
            and self.is_postprocessing_type_set
        )

    @property
    def can_generate_media_package(self):
        return (
            self.is_recording_path_set
            and self.is_postprocessing_type_set
            and self.snow_ticket
            and self.indico_event.is_recorded
        )

    @property
    def is_media_package_running(self):
        return (
            self.media_package_generated_status == MediaPackageStatus.RUNNING
            or self.media_package_generated_status == MediaPackageStatus.REQUESTED
            or self.media_package_generated_status == MediaPackageStatus.PENDING
        )

    @property
    def is_ingest_running(self):
        return (
            self.sent_to_opencast_status == SentToOpencastStatus.RUNNING
            or self.sent_to_opencast_status == SentToOpencastStatus.PENDING
        )

    @property
    def is_processing_running(self):
        return (
            self.opencast_processing_status == "WAITING"
            or self.opencast_processing_status == "RUNNING"
        )

    @property
    def can_send_to_opencast(self):
        return (
            self.is_recording_path_set
            and self.is_postprocessing_type_set
            and self.snow_ticket
            and not self.is_media_package_running
            and not self.is_ingest_running
            and not self.is_processing_running
        )

    @property
    def is_send_to_opencast_running(self):
        return (
            self.is_media_package_running
            or self.is_ingest_running
            or self.opencast_processing_status == "WAITING"
        )

    @property
    def can_fetch_agreement(self):
        return (
            self.e_agreement_status != "ACCEPTED"
            or self.e_agreement_status != "DECLINED"
        )

    @property
    def can_regenerate_acls(self):
        return self.opencast_processing_status == "OK"

    @property
    def can_request_transcription(self):
        return self.opencast_processing_status == "OK"

    @property
    def can_update_opencast_transcription(self):
        return (
            self.transcription_media_id
            and self.transcription_media_id != ""
            and self.transcription_requested_status == TranscriptionStates.COMPLETED
        )

    @property
    def can_request_cds_record(self):
        return (
            self.opencast_acls_requested_status == "OK"
            and self.opencast_processing_status == "OK"
            and (
                self.e_agreement_status == "ACCEPTED"
                or self.e_agreement_status == "FORCED"
            )
        )

    @property
    def is_cds_record_set_prop(self):
        return self.cds_record and self.cds_record != ""

    @property
    def can_request_cds_subformats(self):
        return self.is_cds_record_set_prop

    @property
    def can_publish_cds_link_to_indico(self):
        return (
            self.is_cds_record_set_prop
            and self.is_cds_subformats_requested_status == "OK"
        )

    @property
    def is_restricted(self):
        return self.indico_event.allowed != "{'users': [], 'groups': []}"

    @property
    def contribution_url(self):
        return current_app.config["CDS_URL_RECORD"] % self.cds_record

    def has_camera_split_values(self):
        """Check if the camera end time is set

        Returns:
            boolean: If the split values for the camera are set
        """
        if (
            self.camera_end_time is not None
            and self.camera_end_time != ""
            and self.camera_end_time != "00:00:00"
            and self.camera_start_time is not None
        ):
            return True
        return False

    def has_slides_split_values(self):
        """Check if the camera end time is set

        Returns:
            boolean: If the split values for the camera are set
        """
        if (
            self.slides_end_time is not None
            and self.slides_end_time != ""
            and self.slides_end_time != "00:00:00"
            and self.slides_start_time is not None
        ):
            return True
        return False

    def __repr__(self):
        return f"{self.title} ({self.contribution_id})"

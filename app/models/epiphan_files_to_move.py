import datetime

from app.extensions import db
from app.models.base import ModelBase


class EpiphanFilesToMoveStatus:
    PENDING = "Pending"
    OK = "OK"
    NOT_FOUND = "Not found"


class EpiphanFilesToMove(ModelBase):
    __tablename__ = "epiphan_files_to_move"

    indico_id = db.Column(db.String(255))
    date_created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    status = db.Column(db.String(255), default=EpiphanFilesToMoveStatus.PENDING)
    run_times = db.Column(db.Integer, default=0)

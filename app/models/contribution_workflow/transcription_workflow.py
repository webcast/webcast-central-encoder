import enum

from sqlalchemy_utils.types.choice import ChoiceType

from app.extensions import db


class TranscriptionStates(enum.Enum):
    NOT_REQUESTED = "NOT_REQUESTED"
    REQUESTING = "REQUESTING"
    WAITING = "WAITING"
    TRANSCRIPTION_FINISHED = "TRANSCRIPTION_FINISHED"
    TRANSLATION_FINISHED = "TRANSLATION_FINISHED"
    COMPLETED_CALLBACK_FAILED = "COMPLETED_CALLBACK_FAILED"
    COMPLETED_EMAIL_FAILED = "COMPLETED_EMAIL_FAILED"
    ERROR = "ERROR"
    COMPLETED = "COMPLETED"
    TRY_LATER = "TRY_LATER"


class OpencastTranscriptionStates(enum.Enum):
    NOT_REQUESTED = "Not requested"
    REQUESTING = "Requesting"
    WAITING = "Waiting"
    COMPLETED = "Completed"
    ERROR = "Error"


class TranscriptionWorkflowAttributes(db.Model):
    __abstract__ = True

    transcription_requested_status = db.Column(
        ChoiceType(TranscriptionStates, impl=db.String(255)),
        default=TranscriptionStates.NOT_REQUESTED.value,
    )
    transcription_requested_error_message = db.Column(db.Text)
    transcription_requested_date = db.Column(db.DateTime)

    transcription_media_id = db.Column(db.String(255))

    do_not_transcribe = db.Column(db.Boolean, default=False)
    do_not_transcribe_date = db.Column(db.DateTime)

    # Opencast steps
    transcription_update_status = db.Column(
        ChoiceType(OpencastTranscriptionStates, impl=db.String(255)),
        default=OpencastTranscriptionStates.NOT_REQUESTED.value,
    )
    transcription_update_error_message = db.Column(db.Text)
    transcription_update_date = db.Column(db.DateTime)

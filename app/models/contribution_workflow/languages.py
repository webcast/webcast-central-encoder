class AvailableLanguages:
    ENGLISH = "eng"
    FRENCH = "fra"
    SPANISH = "spa"
    GERMAN = "deu"

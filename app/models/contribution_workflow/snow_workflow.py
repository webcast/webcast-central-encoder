from app.extensions import db


class SnowWorkflowAttributes(db.Model):
    __abstract__ = True

    snow_ticket = db.Column(db.String(255))
    is_snow_ticket_generated = db.Column(db.Boolean(False))
    is_snow_ticket_generated_date = db.Column(db.DateTime)

import datetime
import logging

import pytz
from flask import current_app
from sqlalchemy.ext.hybrid import hybrid_property

from app.extensions import db
from app.models.contribution_workflow.languages import AvailableLanguages

logger = logging.getLogger("webapp")


class IndicoWorkflowAttributes(db.Model):
    __abstract__ = True

    contribution_id = db.Column(db.String(512))
    title = db.Column(db.String(512))
    contribution_type = db.Column(db.String(512))
    description = db.Column(db.Text())
    url = db.Column(db.String(512))
    session = db.Column(db.String(512))
    db_id = db.Column(db.String(512))
    db_id_full = db.Column(db.String(512))
    event_type = db.Column(db.String(512), default="Unknown")

    language = db.Column(db.String(512), default=AvailableLanguages.ENGLISH)

    e_agreement_status = db.Column(db.String(512), default="UNKNOWN")
    e_agreement_status_date = db.Column(db.DateTime)

    allowed = db.Column(db.Text())
    speakers = db.Column(db.Text())

    start_date = db.Column(db.Date)
    start_time = db.Column(db.Time)

    end_date = db.Column(db.Date)
    end_time = db.Column(db.Time)

    @hybrid_property
    def start_date_time(self):
        if not self.start_time:  # start_time can be null
            return None

        datetime_str = f"{self.start_date} {self.start_time}"

        # Create a datetime object from the combined string
        combined_dt = datetime.datetime.strptime(datetime_str, "%Y-%m-%d %H:%M:%S")

        # Set the application timezone
        zurich_tz = pytz.timezone(current_app.config["CURRENT_TIMEZONE"])

        # Localize the datetime object with application's timezone
        localized_dt = zurich_tz.localize(combined_dt)

        return localized_dt

    @start_date_time.expression  # type: ignore
    def start_date_time(cls):
        return cls.start_date

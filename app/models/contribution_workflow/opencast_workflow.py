from app.extensions import db


class MediaPackageStatus:
    PENDING = "PENDING"
    REQUESTED = "REQUESTED"
    RUNNING = "RUNNING"
    ERROR = "ERROR"
    OK = "OK"


class SentToOpencastStatus:
    PENDING = "PENDING"
    REQUESTED = "REQUESTED"
    RUNNING = "RUNNING"
    ERROR = "ERROR"
    OK = "OK"


class OpencastProcessingStatus:
    WAITING = "WAITING"
    RUNNING = "RUNNING"
    ERROR = "ERROR"
    OK = "OK"


class OpencastWorkflowAttributes(db.Model):
    __abstract__ = True

    # Opencast Attributes
    media_package_generated_status = db.Column(db.String(255))
    media_package_generated_date = db.Column(db.DateTime)
    media_package_generated_last_report = db.Column(db.String(255))

    sent_to_opencast_status = db.Column(db.String(255))
    sent_to_opencast_task_id = db.Column(db.String(255))
    sent_to_opencast_date = db.Column(db.DateTime)
    sent_to_opencast_last_report = db.Column(db.String(255))
    edit_on_opencast = db.Column(db.Boolean, default=False)
    add_transcription = db.Column(db.Boolean, default=False)

    opencast_uid = db.Column(db.String(255))
    opencast_processing_finished = db.Column(db.Boolean, default=False)
    opencast_processing_finished_date = db.Column(db.DateTime)
    opencast_processing_status = db.Column(db.String(255))
    opencast_processing_date = db.Column(db.DateTime)
    opencast_processing_status_last_report = db.Column(db.String(255))

    opencast_ready_for_cutting = db.Column(db.Boolean, default=False)
    opencast_ready_for_cutting_status = db.Column(db.String(255))
    opencast_ready_for_cutting_date = db.Column(db.DateTime)

    opencast_acls_requested = db.Column(db.Boolean, default=False)
    opencast_acls_requested_status = db.Column(db.String(255))
    opencast_acls_requested_last_report = db.Column(db.String(255))
    opencast_acls_requested_date = db.Column(db.DateTime)

    opencast_acls_finished = db.Column(db.Boolean, default=False)
    opencast_acls_finished_date = db.Column(db.DateTime)

    last_report = db.Column(db.String(255))

    # End Opencast Attributes

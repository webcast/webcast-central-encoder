from app.extensions import db


class CdsWorkflowAttributes(db.Model):
    __abstract__ = True

    is_cds_link_published = db.Column(db.Boolean(False))
    is_cds_link_published_date = db.Column(db.DateTime)

    is_cds_record_requested = db.Column(db.Boolean(False))
    is_cds_record_requested_status = db.Column(db.String(255))
    is_cds_record_requested_last_report = db.Column(db.String(255))
    is_cds_record_requested_date = db.Column(db.DateTime)

    cds_record = db.Column(db.String(255))
    is_cds_record_set = db.Column(db.Boolean(False), default=False)
    is_cds_record_set_date = db.Column(db.DateTime)

    is_cds_subformats_requested = db.Column(db.Boolean(False), default=False)
    is_cds_subformats_requested_status = db.Column(db.String(255))
    is_cds_subformats_requested_last_report = db.Column(db.String(255))
    is_cds_subformats_requested_date = db.Column(db.DateTime)

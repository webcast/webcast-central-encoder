from sqlalchemy.ext.declarative import declarative_base

from app.extensions import db

Base = declarative_base()


class Room(db.Model):
    """
    Model for Room
    """

    __tablename__ = "room"

    id = db.Column(db.Integer, primary_key=True)
    indico_name = db.Column(db.String(100), unique=True)
    room_name = db.Column(db.String(100))
    supported = db.Column(db.Boolean(), default=False)

    indico_events = db.relationship("IndicoEvent", backref="room", lazy=True)
    contributions = db.relationship(
        "IndicoEventContribution", backref="room", lazy=True
    )

    def __repr__(self):
        return "{indico}".format(indico=self.indico_name)

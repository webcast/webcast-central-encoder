import os
from importlib import import_module

import sentry_sdk
from flask import Flask
from flask_admin import Admin
from flask_admin.menu import MenuLink
from kombu import Exchange, Queue
from sentry_sdk.integrations.flask import FlaskIntegration
from werkzeug.contrib.fixers import ProxyFix

from app.api_loader import load_api_public_blueprints
from app.cli import initialize_cli
from app.daos.settings import SettingsDAO
from app.extensions import cache, celery, csrf, db, jwt, migrate
from app.models.api_token import ApiToken
from app.models.encoders import Room
from app.models.events import IndicoEvent, IndicoEventContribution
from app.utils.auth.cern_openid import load_cern_openid
from app.utils.blacklist_helpers import check_if_token_revoked
from app.utils.celery_probes import LivenessProbe
from app.views.blueprints import all_blueprints
from app.views.custom_admin.api_token_create_view import ApiTokenView
from app.views.custom_admin.custom_admin_index_view import MyAdminIndexView
from app.views.custom_admin.indico_event_contribution_view import (
    IndicoEventContributionView,
)
from app.views.custom_admin.indico_event_view import IndicoEventView
from app.views.custom_admin.room_view import RoomView

app_version = "1.1.0"


def create_app(config_filename):
    """
    Factory to create the application using a file

    :param config_filename: The name of the file that will be used for configuration
    :return: The created application
    """
    app = Flask(__name__)
    app.config.from_object(config_filename)

    if app.config.get("USE_PROXY", False):
        app.wsgi_app = ProxyFix(app.wsgi_app)

    # Load all the availabe models
    with app.app_context():
        db_models_imports = (
            "app.models.users",
            "app.models.encoders",
            "app.models.events",
            "app.models.api_token",
            "app.models.settings",
            "app.models.epiphan_files_to_move",
            "app.models.metrics",
        )

        for module in db_models_imports:
            import_module(module)

    # Initialize the database and the migrations
    setup_database(app)
    # Initializa Views

    init_flask_admin(app)
    initialize_views(app)
    # Enable the CERN Oauth Authentication
    print("Initializing Openid... ", end="")
    app.openid = load_cern_openid(app)
    print("ok")
    # Initialize Cache
    init_redis_cache(app)
    # JWT
    init_jwt_auth(app)
    # Loading API
    print("Loading public API... ", end="")
    load_public_api_v1(app)
    print("ok")
    init_celery(app)
    init_sentry(app)
    # Init CSRF
    csrf.init_app(app)
    print("Initialize cli...", end="")
    # initialize cli, where we add additional commands to the CLI
    initialize_cli(app)
    print("ok")

    @app.before_first_request
    def init_settings():
        print("Creating the settings object if needed... ", end="")
        SettingsDAO.create()

    # Global variables for templates

    @app.context_processor
    def inject_opencast_endpoint():
        opencast_endpoint = app.config["OPENCAST_ENDPOINT"]
        is_dev = app.config["IS_DEV"]
        default_recording_path = app.config["DEFAULT_RECORDING_PATH"]
        cds_url = app.config["CDS_URL"]
        return dict(
            opencast_endpoint=opencast_endpoint,
            is_dev=is_dev,
            default_recording_path=default_recording_path,
            cds_url=cds_url,
            app_version=app_version,
        )

    return app


def init_flask_admin(app):
    print("Initializing Admin... ", end="")
    admin = Admin(
        app,
        name="Recordings Manager Admin",
        index_view=MyAdminIndexView(),
        template_mode="bootstrap4",
    )
    admin.add_link(MenuLink(name="Dashboard", url="/"))
    admin.add_link(
        MenuLink(name="Monitoring", url="https://es-collaborativeapps7.cern.ch/kibana/")
    )
    admin.add_link(
        MenuLink(
            name="Docs", url="https://codimd.web.cern.ch/PkojfsN2SPedpN7NMrPgnQ?view"
        )
    )
    admin.add_view(
        IndicoEventView(
            IndicoEvent, db.session, name="Indico Events", category="Indico"
        )
    )
    admin.add_view(
        IndicoEventContributionView(
            IndicoEventContribution,
            db.session,
            name="Indico Events Contributions",
            category="Indico",
        )
    )
    admin.add_view(RoomView(Room, db.session, name="Rooms"))
    admin.add_view(ApiTokenView(ApiToken, db.session))

    print("ok")


def init_redis_cache(app):
    cache_config = {"CACHE_TYPE": "null"}
    if app.config.get("CACHE_ENABLE", False):
        print("Initializing Redis Caching... ", end="")
        cache_config = {
            "CACHE_TYPE": "redis",
            "CACHE_REDIS_HOST": app.config.get("CACHE_REDIS_HOST", None),
            "CACHE_REDIS_PASSWORD": app.config.get("CACHE_REDIS_PASSWORD", None),
            "CACHE_REDIS_PORT": app.config.get("CACHE_REDIS_PORT", None),
        }
    else:
        print("Caching disabled... ", end="")
    cache.init_app(app, config=cache_config)
    print("ok")


def initialize_views(application):
    """
    Initializes all the application blueprints

    :param application: Application where the blueprints will be registered
    :return:
    """
    print("initiliaze views")

    for bp in all_blueprints:
        import_module(bp.import_name)
        application.register_blueprint(bp)


def setup_database(app):
    """
    Initialize the database and the migrations
    """

    if app.config.get("DB_ENGINE", None) == "postgresql":
        print("Initializing Postgres Database... ", end="")
        app.config[
            "SQLALCHEMY_DATABASE_URI"
        ] = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
            app.config["DB_USER"],
            app.config["DB_PASS"],
            app.config["DB_SERVICE_NAME"],
            app.config["DB_PORT"],
            app.config["DB_NAME"],
        )

        app.config["SQLALCHEMY_RECORD_QUERIES"] = False
        app.config["SQLALCHEMY_POOL_SIZE"] = int(
            app.config.get("SQLALCHEMY_POOL_SIZE", 5)
        )
        app.config["SQLALCHEMY_POOL_TIMEOUT"] = int(
            app.config.get("SQLALCHEMY_POOL_TIMEOUT", 10)
        )
        app.config["SQLALCHEMY_POOL_RECYCLE"] = int(
            app.config.get("SQLALCHEMY_POOL_RECYCLE", 120)
        )
        app.config["SQLALCHEMY_MAX_OVERFLOW"] = int(
            app.config.get("SQLALCHEMY_MAX_OVERFLOW", 3)
        )

    elif app.config.get("DB_ENGINE", None) == "mysql":
        print("Initializing Mysql Database... ", end="")
        app.config[
            "SQLALCHEMY_DATABASE_URI"
        ] = "mysql+pymysql://{0}:{1}@{2}:{3}/{4}?charset=utf8mb4".format(
            app.config["DB_USER"],
            app.config["DB_PASS"],
            app.config["DB_SERVICE_NAME"],
            app.config["DB_PORT"],
            app.config["DB_NAME"],
        )

    else:
        print("Initializing Sqlite Database...", end="")
        _basedir = os.path.abspath(os.path.dirname(__file__))
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
            _basedir, "webapp.db"
        )

    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)
    db.app = app
    print("ok")

    print("Initializing Database Migrations... ", end="")
    migrate.init_app(app, db)
    print("ok")


def load_public_api_v1(application):
    all_blueprints = load_api_public_blueprints()

    for bp in all_blueprints:
        application.register_blueprint(
            bp, url_prefix="{prefix}/{version}".format(prefix="/api", version="v1")
        )


def init_jwt_auth(app):
    """
    Initializes the JWT Authentication and sets the global configuration for the application JWT
    :param app: Application that will be set up
    :return: -
    """
    print("Initializing JWT Authentication... ", end="")
    app.config["JWT_ACCESS_TOKEN_EXPIRES"] = False
    app.config["JWT_BLACKLIST_ENABLED"] = True
    app.config["JWT_BLACKLIST_TOKEN_CHECKS"] = ["access", "refresh"]

    jwt.init_app(app)

    jwt.token_in_blocklist_loader(check_if_token_revoked)
    print("ok")


def init_celery(app=None, config=None):
    """
    Initialize the Celery tasks support. This is used to avoid timeouts when connecting/disconnecting rooms and
    setting up the ip2ccs statuses of the rooms
    :param app: The current Flask application. If it's not set, it will be created using the config parameter
    :param config: Configuration object for the Flask application
    :return: The Celery instance
    """
    print("Initializing Celery... ", end="")
    tasks_modules = [
        "app.tasks.example",
        "app.tasks.media_packages",
        "app.tasks.verify_speaker_releases",
        "app.tasks.request_opencast_acls",
        "app.tasks.request_cds_records",
        "app.tasks.add_cds_links_to_indico",
        "app.tasks.opencast_ingest",
        "app.tasks.indico_events",
        "app.tasks.request_cds_subformats",
        "app.tasks.move_epiphan_files",
        "app.tasks.transcriptions",
    ]

    app = app or create_app(config)
    celery.app = app
    celery.conf.broker_url = app.config["CELERY_BROKER_URL"]
    celery.conf.result_backend = app.config["CELERY_RESULTS_BACKEND"]
    celery.conf.beat_schedule = app.config["CELERY_BEAT_SCHEDULE"]
    celery.conf.include = tasks_modules
    celery.conf.task_serializer = "json"
    celery.conf.result_serializer = "json"
    # 3600 = 1 hour
    # 24 = 1 day
    # 90 = 3 months
    celery.conf.result_expires = 2592000  # 1 month to delete old results from database
    celery.conf.result_extended = True

    default_exchange = Exchange("default", type="direct")
    media_exchange = Exchange("media", type="direct")
    transcription_exchange = Exchange("transcription", type="direct")

    celery.conf.task_queues = (
        Queue("default", default_exchange, routing_key="default"),
        Queue("media", media_exchange, routing_key="media"),
        Queue("transcription", transcription_exchange, routing_key="transcription"),
    )
    celery.conf.task_default_queue = "default"
    celery.conf.task_default_exchange = "default"
    celery.conf.task_default_routing_key = "default"

    celery.conf.task_routes = {
        "app.tasks.example.*": {"queue": "media", "exchange": "media"},
        "app.tasks.opencast_ingest.*": {"queue": "media", "exchange": "media"},
        "app.tasks.transcriptions.*": {
            "queue": "transcription",
            "exchange": "transcription",
        },
    }

    celery.conf.update(app.config)

    print("Adding Celery Liveness Probe... ", end="")
    celery.steps["worker"].add(LivenessProbe)
    print("ok")

    class ContextTask(celery.Task):
        """Make celery tasks work with Flask app context"""

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    print("ok")
    return celery


def init_sentry(application: Flask) -> None:
    if application.config["ENABLE_SENTRY"]:
        print("Initializing Sentry... ", end="")
        # pylint: disable=abstract-class-instantiated
        sentry_sdk.init(
            dsn=application.config["SENTRY_DSN"],
            integrations=[FlaskIntegration()],
            environment=application.config["SENTRY_ENVIRONMENT"],
        )
        print("OK")

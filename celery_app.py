import urllib3

from app.app_factory import init_celery
from app.utils.logger import setup_logs
from secret import config

urllib3.disable_warnings()

# Initialize the Celery worker to run background tasks
celery = init_celery(config=config)

print("Initializing logs...", end="")
setup_logs(
    celery.app,
    "job",
    to_remote=celery.app.config.get("LOG_REMOTE_ENABLED", False),
    to_file=celery.app.config.get("LOG_FILE_ENABLED", False),
    to_mail=celery.app.config.get("LOG_MAIL_ENABLED", False),
)
print("ok")

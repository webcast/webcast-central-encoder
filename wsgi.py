from __future__ import absolute_import

import urllib3
from flask import render_template

from app.app_factory import create_app
from app.templatetags import filters
from app.utils.logger import setup_logs
from secret import config

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

"""
This file is the one loaded on development.
Check the README file to learn how to generate the certificates.
"""

# Flask application
application = create_app(config)


print("Initializing logs...", end="")
setup_logs(
    application,
    "webapp",
    to_remote=application.config.get("LOG_REMOTE_ENABLED", False),
    to_file=application.config.get("LOG_FILE_ENABLED", False),
    to_mail=application.config.get("LOG_MAIL_ENABLED", False),
)
print("ok")

# jinjna filters initialization
application.jinja_env.filters["show_time"] = filters.show_time
application.jinja_env.filters["show_space"] = filters.show_space
application.jinja_env.filters["show_date"] = filters.show_date


@application.route("/")
def layout():
    return render_template("dashboard/index.html")

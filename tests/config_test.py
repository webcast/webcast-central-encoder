import os

from celery.schedules import crontab

CURRENT_TIMEZONE = "Europe/Zurich"
USE_PROXY = True

IS_DEV = True

SERVER = "cernmedia-fake.cern.ch"
SERVER_USER = ""
SERVER_PASS = ""

LOG_LEVEL = "DEV"
SECRET_KEY = (
    "\x7f\x8e\xdd\x08#)A\xf4w\xba\xb5\xb1\xd4\x0cr\xde8\xcd\xdb\x9a~\x93\xf1\x99"
)
###########################################################
# OAUTH
###########################################################

# This line is only required during development if not using SSL
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
# Oauth config
CERN_OPENID_CLIENT_ID = ""
CERN_OPENID_CLIENT_SECRET = ""

ADMIN_GROUP = "admins"

###########################################################
# MAIL
###########################################################

MAIL_HOSTNAME = "cernmx.cern.ch"
MAIL_FROM = "webcast-team@cern.ch"
MAIL_TO = ["rene.fernandez@cern.ch"]
REMINDER_BEFORE_EVENT = 15

###########################################################
# MONARCH
###########################################################

MONARCH_USER = "admin"
MONARCH_PASS = "admin"

# MONARCH_USER = "admin"
# MONARCH_PASS = "webcast0"

###########################################################
# INDICO
###########################################################

INDICO_HOSTNAME = "https://indico.cern.ch"
INDICO_API_KEY = "43f1b7f0-2c47-475e-8ded-e9974f8b5fd9"
INDICO_API_SECRET = "bab82199-f0fe-4403-9463-6e0cfb687a72"
INDICO_EVENT_URL = "https://indico.cern.ch/event/%s"

###########################################################
# WOWZA
###########################################################
WOWZA_WATCH_FROM = "wowza.cern.ch"
TEST_APP = "test"

###########################################################
# MICALA
###########################################################
# micala webservices url
MICALA_WEBSITE_URL = "https://micalaprod.cern.ch"
MICALA_WEBSITE_API_PATH = "/api/index.py"
MICALA_WEBSITE_ACCESS_KEY = "e42ea04a-ed8f-4885-b487-2ebbaa8027a3"
MICALA_WEBSITE_SECRET_KEY = "4f6a1640-1d46-4d32-9646-0f6ba8b205c6"

##########################################################
# RAVEM
###########################################################

RAVEM_PLUGIN_ENABLED = True
RAVEM_URL = "https://ravem.web.cern.ch/api/services/setstatus"
RAVEM_REALM = "Ravem protected"
RAVEM_USERNAME = "ravem"
RAVEM_PSW = "r4v3m!"

###########################################################
# WEBCAST WEBSITE
###########################################################
WEBCAST_WEBSITE_URL = "https://webcast.web.cern.ch"
WEBCAST_WEBSITE_API_PATH = "/api/v1/update-stream/"
WEBCAST_WEBSITE_ACCESS_KEY = "6cadebefedab40a194798c7c37b0828c"
WEBCAST_WEBSITE_SECRET_KEY = "04124228-c30f-44f0-9256-7ffbd7f84d02"

DB_NAME = "webapp"
DB_USER = "user"
DB_PASS = "password"
DB_PORT = 3306
DB_SERVICE_NAME = "db"
DB_ENGINE = "mysql"

#############################################################
# Celery beat schedule for tasks
CELERY_BEAT_SCHEDULE = {
    # 'check_disk_spaces': {
    #     'task': 'app.tasks.check_disk_spaces',
    #     # Every minute
    #     'schedule': crontab(hour=6, minute=0),
    # },
    # 'check_encoders': {
    #     'task': 'app.tasks.check_encoders',
    #     'schedule': crontab(hour=6, minute=0),
    # },
    # 'remove_past_events':{
    #     'task': 'app.tasks.remove_past_events',
    #     'schedule': crontab(hour=2,minute=0),
    # },
    # 'stop_event': {
    #     'task': 'app.tasks.stop_event',
    #     'schedule': crontab(minute="*"),
    # },
    # 'fetch_next_events':{
    #     'task': 'app.tasks.fetch_next_events',
    #     'schedule': crontab(hour="*/3"),
    # },
    # 'send_reminders':{
    #     'task': 'app.tasks.send_reminders',
    #     'schedule': crontab(minute="*/2")
    # },
    # 'heart_beat_check': {
    #     'task': 'app.encoders.cron.heart_beat_check',
    #     'schedule': crontab(minute="*/10")
    # }
    "dummy_task": {
        "task": "app.tasks.example.dummy_task",
        "schedule": crontab(minute="*/5"),
    }
}

CELERY_TASK_SERIALIZER = "json"

###############
# SERVICE NOW
###############
# url for the Service Now Ticket
SNOW_TICKET_URL = (
    "https://cern.service-now.com/u_request_fulfillment.do?sysparm_query=number="
)

SN_USER = "webcast"

SN_API_URL = "https://cern.service-now.com/api/now/v1/table/"

MAIL_SNOW_FROM = "micala.software@cern.ch"
MAIL_SNOW_TO = ["recording-support@cern.ch"]
WEBCAST_SERVICE_MAIL = "service-avc-operation@cern.ch"


# CELERY_BROKER_URL = 'redis://:{}redis:6379/0'.format(CACHE_REDIS_PASSWORD)
CELERY_BROKER_URL = "redis://redis:6379/0"
# CELERY_RESULT_BACKEND = 'redis://:{}redis:6379/0'.format(CACHE_REDIS_PASSWORD)
CELERY_RESULT_BACKEND = "redis://redis:6379/0"

################
# Opencast
################
OPENCAST_ENDPOINT = "https://ocweb01-test2.cern.ch"
OPENCAST_USER = "opencast_system_account"
OPENCAST_PASSWORD = "zisxegrP$c63"
DOWNLOADS_FOLDER_PATH = "/opt/app-root/src/downloads"

##################
# CDS
##################
CDS_CALLBACK_BASE_URL = "https://ces-rene.web.cern.ch"
CDS_URL = "https://cds-test.cern.ch"  # do not add the / at the end
CDS_URL_RECORD = CDS_URL + "/record/%s"
CDS_API_KEY = "720d6d3c-62f1-42c0-b402-8a3813e97300"
CDS_API_SECRET = "720f93ed-7d7a-4216-962f-14d8c1c786b0"

CDS_MARC21_ORIGIN = "/opt/app-root/src/app/templates/cds/cds_record_origin.xml"
CDS_MARC21_XML_DIR = os.path.join(DOWNLOADS_FOLDER_PATH, "cds_marc21")
